package com.app.btmobile

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.app.btmobile.DI.component.AppComponents
import com.app.btmobile.DI.component.DaggerAppComponents
import com.app.btmobile.DI.modules.AppModule
import com.app.btmobile.Utils.AppConstants
import com.app.btmobile.Utils.AppConstants.IS_LIVE_MODE
import com.app.btmobile.Utils.SharedPrefsHelper
import javax.inject.Inject


class MyApplication : MultiDexApplication(), LifecycleObserver {
    lateinit var component: AppComponents

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    override fun onCreate() {
        super.onCreate()
        if (IS_LIVE_MODE) {
            AppConstants.BASE_URL = AppConstants.BASE_LIVE_URL
        } else {
            AppConstants.BASE_URL = AppConstants.BASE_STAG_URL
        }
        component = DaggerAppComponents.builder().appModule(AppModule(this)).build()
        getAppComponent(this).doInjection(this)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {
        fun getAppComponent(context: Context): AppComponents {
            return (context.applicationContext as MyApplication).component
        }
    }

//    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
//    fun onAppBackgrounded() {
//        Debugger.wtf("MyApplication", "onAppBackgrounded")
//        PusherIO.getInstance().disconnectPusher()
//    }
//
//    @OnLifecycleEvent(Lifecycle.Event.ON_START)
//    fun onAppForegrounded() {
//        Debugger.wtf("MyApplication", "onAppForegrounded")
//        if (PusherIO.getInstance().isPusherNotConnected()) {
//            if (sharedPrefsHelper.getUser()?.id != null) {
//                PusherIO.getInstance()
//                    .connectToPusher(
//                        sharedPrefsHelper.getUser()?.id.toString(),
//                        applicationContext
//                    )
//                Debugger.wtf("MyApplication", "Connected")
//            }
//        }
//    }


}