package com.app.btmobile.DI.component

import com.app.btmobile.BASE.BaseActivity
import com.app.btmobile.DI.modules.AppModule
import com.app.btmobile.DI.modules.UtilsModule
import com.app.btmobile.DI.modules.ViewModelsModule
import com.app.btmobile.MyApplication
import com.app.btmobile.UI.Activity.*
import com.app.btmobile.UI.Activity.Delivery.*
import com.app.btmobile.UI.Activity.Fitting.FittingActivity
import com.app.btmobile.UI.Activity.Fitting.TechStationCustomersActivity
import com.app.btmobile.UI.Activity.Fitting.TechStationRentersActivity
import com.app.btmobile.UI.Activity.Nfc.AddTorqueTestingActivity
import com.app.btmobile.UI.Activity.Nfc.NfcActivity
import com.app.btmobile.UI.Activity.Packing.PackingActivity
import com.app.btmobile.UI.Activity.Packing.PackingCustomerScreen
import com.app.btmobile.UI.Activity.Profile.ProfileActivity
import com.app.btmobile.UI.Activity.Return.ReturnCustomerActivity
import com.app.btmobile.UI.Activity.Return.ReturnRentersActivity
import com.app.btmobile.UI.Activity.TransferGear.TranferGearActivity
import com.app.btmobile.UI.Activity.Waiver.WaiverCustomerActivity
import com.app.btmobile.UI.Activity.Waiver.WaiverRentersActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [UtilsModule::class, ViewModelsModule::class, AppModule::class])
interface AppComponents {
    fun doInjection(activity: MyApplication)
    fun doInjection(activity: BaseActivity)
    fun doInjection(activity: SplashScreen)
    fun doInjection(activity: LoginActivity)
    fun doInjection(activity: MainActivity)
    fun doInjection(activity: NfcActivity)
    fun doInjection(activity: DeliveryCustomerActivity)
    fun doInjection(activity: DeliveryRentersActivity)
    fun doInjection(activity: NotesActivity)
    fun doInjection(activity: ConfirmationActivity)
    fun doInjection(activity: InvoiceDetailsActivity)
    fun doInjection(activity: PackingCustomerScreen)
    fun doInjection(activity: PackingActivity)
    fun doInjection(activity: TechStationCustomersActivity)
    fun doInjection(activity: TechStationRentersActivity)
    fun doInjection(activity: FittingActivity)
    fun doInjection(activity: WaiverCustomerActivity)
    fun doInjection(activity: WaiverRentersActivity)
    fun doInjection(activity: ReturnCustomerActivity)
    fun doInjection(activity: ReturnRentersActivity)
    fun doInjection(activity: ProfileActivity)
    fun doInjection(activity: TranferGearActivity)
    fun doInjection(activity: AddTorqueTestingActivity)

}