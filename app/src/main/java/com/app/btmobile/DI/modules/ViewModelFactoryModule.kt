package com.app.btmobile.DI.modules

import androidx.lifecycle.ViewModelProvider
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory):
            ViewModelProvider.Factory
}
