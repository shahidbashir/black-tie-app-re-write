package com.app.btmobile.DI.modules

import androidx.lifecycle.ViewModel
import com.ogoul.kalamtime.di.keys.ViewModelKey
import com.ogoul.kalamtime.viewmodel.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindSignInViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NfcViewModel::class)
    abstract fun bindNfcScanViewModel(viewModel: NfcViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DeliveryViewModel::class)
    abstract fun bindDeliveryViewModel(viewModel: DeliveryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotesViewModel::class)
    abstract fun bindNotesViewModel(viewModel: NotesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConfirmationViewModel::class)
    abstract fun bindConfirmationViewModel(viewModel: ConfirmationViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(PackingViewModel::class)
    abstract fun bindPackingViewModel(viewModel: PackingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WaiverViewModel::class)
    abstract fun bindWaiverViewModel(viewModel: WaiverViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReturnViewModel::class)
    abstract fun bindReturnViewModel(viewModel: ReturnViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(viewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransferGearViewModel::class)
    abstract fun bindTransferGearViewModel(viewModel: TransferGearViewModel): ViewModel


}