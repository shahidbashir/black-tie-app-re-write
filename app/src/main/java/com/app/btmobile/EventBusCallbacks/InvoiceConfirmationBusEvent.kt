package com.app.btmobile.EventBusCallbacks

import com.google.gson.annotations.SerializedName

data class InvoiceConfirmationBusEvent(
    val message: String
)
