package com.app.btmobile.Models

import com.google.gson.annotations.SerializedName

data class BaseModel(
    val success: Boolean,
    val message: String
)
