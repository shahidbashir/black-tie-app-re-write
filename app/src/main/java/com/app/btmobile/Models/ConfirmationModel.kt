package com.app.btmobile.Models

data class ConfirmationModel(
    val `data`: ConfirmationData,
    val success: Boolean
)

data class ConfirmationData(
//    val confirmation_status: Boolean,
//    val hide_confirmation_total: Boolean,


    val cards: List<Card>,
    val hide_amount: Boolean,
    val invoices: List<Invoice>,
    val tip_percentages: List<TipPercentage>,
    val customer_phone: String?,
    val tip_type: String?,
)

data class Card(
    val card: String,
    val id: Int,
    var is_default: Int
)

data class Invoice(
   // val confirmation_status: Boolean,

    val customer_confirmation: Boolean,
    val tech_confirmation: Boolean,
    val tour_op: Boolean,
    val confirmation_tip: String,
    val confirmation_have_tip: Boolean,
    val invoice_status: String,
    val invoice_id: Int,
    val invoice_total: String,
    val invoice_total_tax: String,
    val renters: List<ConfirmationRenter>,
    val sub_total: String
)

data class TipPercentage(
    val display_text: String,
    val tip: String,
    var tipPercentage: String
)

data class ConfirmationRenter(
    val addons: List<Any>,
    val days: Int,
    val `package`: String,
    val renter: String,
    val renter_id: Int,
    val total: String
)