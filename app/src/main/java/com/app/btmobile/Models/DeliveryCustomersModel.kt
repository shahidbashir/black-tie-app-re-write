package com.app.btmobile.Models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DeliveryCustomersMainModel(
    @SerializedName("data")
    var `data`: List<DeliveryCustomersModel>?,
    var meta_data: MetaData?
)

data class MetaData(
    var per_page: String,
    var current_page: String,
    var total: String,
    var total_pages: String,
)

data class DeliveryCustomersModel(
//    var userId: Int,
//    var delivery_id: String? = null,
//    var delivered_date: String? = null,
//    var email: String? = null,
//    var items_returned: String? = null,
//    var status: String? = null,
//    var color: String? = null,
//    var status_indicator_class: String? = null,


    var viewType: Int,
    var customer_name: String? = null,
    var delivery_date: String? = null,
    var number_of_renters: String? = null,
    var return_date: String? = null,
    var shipment_id: Int? = null,
    var packing_status: String? = null,
    var time: String? = null,
    var address: String? = null,
    var lodging: String? = null,
    var map_url: String? = null,
    var is_group_fitting: Boolean = false,
    var reservation_id: Int = 0,
    var van: String? = null,
    var schedule_indicator_class: String? = null,
    var room_number: String? = null,
) : Serializable
