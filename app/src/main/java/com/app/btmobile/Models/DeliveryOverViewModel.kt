package com.app.btmobile.Models

data class DeliveryOverViewModel(
    var viewType: Int,
    var model: DeliveryCustomersModel?,
    var arrayList: ArrayList<DeliveryCustomersModel>?
)