package com.app.btmobile.Models

data class DeliveryRenterMainModel(
//    val itemid: Int,
//    val userId: Int,
//    val shipment_id: Int,
//    val shipment_notes_count: Int,

    val confirmation_status: Boolean,
    val customer_phone: String,
    val delivery_id: Int,
    val is_switch_out: Boolean,
    val renters: List<DeliveryRenterListModel>,
    val reservation_notes_count: Int,
    val switch_out_notes: String?,
)

data class DeliveryRenterListModel(
//    val age: String,
//    val gender: String,
//    val height: String,
//    val shoe_size: String,
//    val itemid: Int,
//    val shipment_id: Int,
//    val userId: Int,
//    val weight: String,






    val addons: List<String>,
    val clothing: String,
    val fitted_color: String,
    val has_history: Boolean,
    val id: Int,
    val is_waiver_signed: Boolean,
    val name: String,
    val `package`: String,
    val packing_color: String,
    val switch_out_reason: String,
)