package com.app.btmobile.Models

class FitSaveAddonsModel(
    var selected_addons: ArrayList<AddonListModel> = ArrayList()
)

class AddonListModel(var id: Int, var selected: Int)
