package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName

data class FittingCustomerModel(
    @SerializedName("data")
    var `data`: List<FittingCustomerData>?,
    @SerializedName("techStation_id")
    var techStationId: Int?
)

data class FittingCustomerData(
    @SerializedName("boot_fitted")
    var bootFitted: String?,
    @SerializedName("is_paid")
    var isPaid: Boolean?,
    @SerializedName("is_signed")
    var isSigned: Boolean?,
    @SerializedName("itemid")
    var itemid: Int?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("number_of_people")
    var numberOfPeople: Int?,
    @SerializedName("open_link")
    var openLink: String?,
    @SerializedName("reservation_id")
    var reservationId: Int?,
    @SerializedName("ski_fitted")
    var skiFitted: String?,
    @SerializedName("techStationData_id")
    var techStationDataId: Int?,
    @SerializedName("total_due")
    var totalDue: Double?,
    @SerializedName("userId")
    var userId: Int?,
    @SerializedName("van_shipment_id")
    var vanShipmentId: Int?,
    @SerializedName("waiver")
    var waiver: String?,
    @SerializedName("waiver_label")
    var waiverLabel: String?
)