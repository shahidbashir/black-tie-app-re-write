package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName

data class FittingRenterModel(
    @SerializedName("data")
    var `data`: List<FittingRenterData>?
)

data class FittingRenterData(
    @SerializedName("boot_fitted")
    var bootFitted: Boolean?,
    @SerializedName("equipment_fitted")
    var equipmentFitted: String?,
    @SerializedName("is_packed_boot")
    var isPackedBoot: Boolean?,
    @SerializedName("is_packed_ski")
    var isPackedSki: Boolean?,
    @SerializedName("is_signed")
    var isSigned: Boolean?,
    @SerializedName("itemid")
    var itemid: Int?,
    @SerializedName("open_link")
    var openLink: String?,
    @SerializedName("package")
    var packageX: String?,
    @SerializedName("renter_id")
    var renterId: Int?,
    @SerializedName("renter_name")
    var renterName: String?,
    @SerializedName("reservation_id")
    var reservationId: Int?,
    @SerializedName("shoe_size")
    var shoeSize: String?,
    @SerializedName("ski_fitted")
    var skiFitted: Boolean?,
    @SerializedName("userId")
    var userId: Int?,
    @SerializedName("van_shipment_id")
    var vanShipmentId: String?
)