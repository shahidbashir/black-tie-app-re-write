package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName

data class GetAbilityShoeSizeHeightListModel(
//    @SerializedName("gender")
//    var gender: List<String>,
//    @SerializedName("shoe_type")
//    var shoeType: List<String>,

    @SerializedName("ability")
    var ability: List<String>,
    @SerializedName("height")
    var height: List<String>,
    @SerializedName("men_shoe_size")
    var menShoeSize: List<String>,

    @SerializedName("women_shoe_size")
    var womenShoeSize: List<String>,
    @SerializedName("youth_shoe_size")
    var youthShoeSize: List<String>
)