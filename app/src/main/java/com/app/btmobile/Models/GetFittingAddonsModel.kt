package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName

data class GetFittingAddonsModel(
    @SerializedName("data")
    var `data`: GetFittingAddonsData
)

data class GetFittingAddonsData(
//    @SerializedName("ability")
//    var ability: String,
//    @SerializedName("add_boot")
//    var addBoot: String,
//    @SerializedName("age")
//    var age: String,
//    @SerializedName("boots")
//    var boots: List<Any>,
//    @SerializedName("din_setting")
//    var dinSetting: String,
//    @SerializedName("dob")
//    var dob: String,
//    @SerializedName("gender")
//    var gender: String,
//    @SerializedName("height")
//    var height: String,
//    @SerializedName("name")
//    var name: String,
//    @SerializedName("shoe_size")
//    var shoeSize: String,
//    @SerializedName("shoe_type")
//    var shoeType: String,
//    @SerializedName("skis")
//    var skis: List<Any>,
//    @SerializedName("weight")
//    var weight: String,

    @SerializedName("addons")
    var addons: List<FittingAddon>,
    @SerializedName("addons_with_barcode")
    var addonsWithBarcodeList: ArrayList<FittingAddon>,
    @SerializedName("clothing")
    var fittedClothingList: ArrayList<FittedClothingModel>,


)

data class FittedClothingModel(
  //  var is_packable: Boolean,

    var id: Int,
    var name: String,
    var status: String,
    var already_rented: Boolean,
    var price: String,
    var selected: Boolean,
    var purchased_clothing: Boolean = false
)

data class FittingAddon(
//    @SerializedName("boot_upgrade")
//    var bootUpgrade: Int,
//    @SerializedName("created_at")
//    var createdAt: String,
//    @SerializedName("created_by")
//    var createdBy: Double,
//    @SerializedName("daily_price")
//    var dailyPrice: String,
//    @SerializedName("description")
//    var description: String,
//    @SerializedName("hide_ops")
//    var hideOps: Int,
//    @SerializedName("image")
//    var image: String,
//    @SerializedName("is_active")
//    var isActive: Int,
//    @SerializedName("is_packable")
//    var isPackable: Int,
//    @SerializedName("max_price")
//    var maxPrice: String,
//    @SerializedName("per_skier")
//    var perSkier: Int,
//    @SerializedName("pricing")
//    var pricing: String,
//    @SerializedName("tax_free")
//    var taxFree: Int,
//    @SerializedName("type")
//    var type: String,
//    @SerializedName("resort_id")
//    var resortId: Int,
//    @SerializedName("updated_at")
//    var updatedAt: String,

    @SerializedName("id")
    var id: Int,
    @SerializedName("name")
    var name: String,
    @SerializedName("price")
    var price: String,
    @SerializedName("selected")
    var selected: Boolean,


    var isAddonWithBarcode: Boolean = false,
    var purchased_addon: Boolean = false,
    @SerializedName("complete_size")
    var completeSize: String = "",
    var is_manual: Int? = 1

)