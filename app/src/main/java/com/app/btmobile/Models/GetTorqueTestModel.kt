package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName

data class GetTorqueTestModel(
    @SerializedName("data")
    var `data`: ViewTorqueTestData,
    @SerializedName("success")
    var success: Boolean
)
data class ViewTorqueTestData(
//    @SerializedName("added_by")
//    var addedBy: String,
//    @SerializedName("barcode")
//    var barcode: String,
//    @SerializedName("name")
//    var name: String,
//    @SerializedName("ski_id")
//    var skiId: Int,
//    @SerializedName("work_done")
//    var workDone: String,

    @SerializedName("condition")
    var condition: String,
    @SerializedName("notes")
    var notes: String,
    @SerializedName("skier_code")
    var skierCode: String,
    @SerializedName("sole_length")
    var soleLength: String,
    @SerializedName("din_setting")
    var dinSetting: String,
    @SerializedName("ski_1_afd")
    var ski1Afd: String,
    @SerializedName("ski_1_forward_lean")
    var ski1ForwardLean: String,
    @SerializedName("ski_1_forward_lean_reading")
    var ski1ForwardLeanReading: String,
    @SerializedName("ski_1_twist_left")
    var ski1TwistLeft: String,
    @SerializedName("ski_1_twist_left_reading")
    var ski1TwistLeftReading: String,
    @SerializedName("ski_1_twist_right")
    var ski1TwistRight: String,
    @SerializedName("ski_1_twist_right_reading")
    var ski1TwistRightReading: String,
    @SerializedName("ski_2_afd")
    var ski2Afd: String,
    @SerializedName("ski_2_forward_lean")
    var ski2ForwardLean: String,
    @SerializedName("ski_2_forward_lean_reading")
    var ski2ForwardLeanReading: String,
    @SerializedName("ski_2_twist_left")
    var ski2TwistLeft: String,
    @SerializedName("ski_2_twist_left_reading")
    var ski2TwistLeftReading: String,
    @SerializedName("ski_2_twist_right")
    var ski2TwistRight: String,
    @SerializedName("ski_2_twist_right_reading")
    var ski2TwistRightReading: String,

    @SerializedName("tested_date")
    var testedDate: String,
    @SerializedName("torque_status")
    var torqueStatus: String,

)


