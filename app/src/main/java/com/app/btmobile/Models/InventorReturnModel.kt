package com.app.btmobile.Models

data class InventorReturnModel(
    val message: String,
    val success: Boolean,
    var barcode: String
)
