package com.app.btmobile.Models

data class LoginModel(
    val `data`: Data
)

data class Data(
    val access_token: String,
    val expires_in: Int,
    val message: String,
    val token_type: String,
    val user: User
)

data class User(
    val email: String,
    val first_name: String,
    val id: Int,
    val last_name: String
)