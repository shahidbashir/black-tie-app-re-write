package com.app.btmobile.Models

data class MassReturnBaseModel(
    val message: String,
    val success: Boolean,
    val data: MassReturnModel,
    var barcode: String
)