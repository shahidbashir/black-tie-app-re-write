package com.app.btmobile.Models;

import java.util.ArrayList;
import java.util.List;

public class MassReturnBigModel {
    String reservationName;
    List<MassReturnModel> arrayList = new ArrayList<>();

    public String getReservationName() {
        return reservationName;
    }

    public void setReservationName(String reservationName) {
        this.reservationName = reservationName;
    }

    public List<MassReturnModel> getArrayList() {
        return arrayList;
    }

    public void setArrayList(List<MassReturnModel> arrayList) {
        this.arrayList = arrayList;
    }
}
