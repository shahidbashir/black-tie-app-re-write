package com.app.btmobile.Models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MassReturnModel {

    public int id;
    @SerializedName("van_shipment_id")
    @Expose
    private String shipment_id;
    @SerializedName("customer_name")
    @Expose
    private String customer_name;
    @SerializedName("reservation_name")
    @Expose
    private String reservation_name;
    @SerializedName("inventory_manufacture")
    @Expose
    private String manufacture;
    @SerializedName("reservation_id")
    @Expose
    private Integer reservation_id;
    @SerializedName("inventory_id")
    @Expose
    private Integer inventory_id;
    @SerializedName("renter_name")
    @Expose
    private String name;
    @SerializedName("inventory_barcode")
    @Expose
    private String barcode;
    @SerializedName("inventory_model")
    @Expose
    private String model;
    @SerializedName("inventory_size")
    @Expose
    private String size;
    @SerializedName("inventory_type")
    @Expose
    private String type;
    @SerializedName("inventory_status")
    @Expose
    private String status;

    public int userId;
    public String date;
    public String return_date;

    public String getShipment_id() {
        return shipment_id;
    }

    public void setShipment_id(String shipment_id) {
        this.shipment_id = shipment_id;
    }

    public MassReturnModel(Integer id, String barcode, String model, String size, String type, String status) {
        this.id = id;
        this.barcode = barcode;
        this.model = model;
        this.size = size;
        this.type = type;
        this.status = status;
    }

    public String getReservation_name() {
        return reservation_name;
    }

    public void setReservation_name(String reservation_name) {
        this.reservation_name = reservation_name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getReservation_id() {
        return reservation_id;
    }

    public void setReservation_id(Integer reservation_id) {
        this.reservation_id = reservation_id;
    }

    public Integer getInventory_id() {
        return inventory_id;
    }

    public void setInventory_id(Integer inventory_id) {
        this.inventory_id = inventory_id;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getReturn_date() {
        return return_date;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public void setReturn_date(String return_date) {
        this.return_date = return_date;
    }
}