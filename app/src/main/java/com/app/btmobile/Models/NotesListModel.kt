package com.app.btmobile.Models

data class NotesListModel(
    val `data`: List<NoteData>,
    val success: Boolean
)

data class NoteData(
//    val created_date: String,
//    val itemid: Int,
//    val userId: Int,
//    val reservation_id: Int,
//    val updated_at: String,
//    val name_initials: String,
//    val edit_note: Boolean,

    val created_at: String,
    val id: Int,
    val notes: String,
)