package com.app.btmobile.Models

data class OwnBootModel(
    val renter_id: Int,
    val bring_boot: String
)
