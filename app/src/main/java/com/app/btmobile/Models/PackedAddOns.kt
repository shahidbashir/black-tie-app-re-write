package com.app.btmobile.Models

import com.google.gson.annotations.SerializedName


data class AddonsModel(
    @SerializedName("addons")
    var addons: ArrayList<Addon>,
    @SerializedName("clothings")
    var clothing: ArrayList<ClothingModel>,
    @SerializedName("addons_with_barcode")
    var addonsWithBarcodeList: ArrayList<Addon>,
    @SerializedName("renter_id")
    var renterId: Int
)

data class Addon(
    @SerializedName("id")
    var id: Int,
    @SerializedName("is_packable")
    var isPackable: Boolean,
    @SerializedName("itemid")
    var itemid: Int,
    @SerializedName("name")
    var name: String,
    @SerializedName("selected")
    var selected: Int,
    @SerializedName("status")
    var status: String,
    @SerializedName("userId")
    var userId: Int,
    @SerializedName("vanShipmentId")
    var vanShipmentId: Int,
    var isAddonWithBarcode: Boolean = false,
    var purchased_addon: Boolean = false,
    @SerializedName("complete_size")
    var completeSize: String = ""
)