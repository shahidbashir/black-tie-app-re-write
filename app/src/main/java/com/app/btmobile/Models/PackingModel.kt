package com.app.btmobile.Models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PackingModel(
    @SerializedName("data")
    var packingData: List<PackingData>
)

data class PackingData(
    @SerializedName("addon_list")
    var addonList: List<PackedInventoryModel>,
    @SerializedName("ski_list")
    var skiList: List<PackedInventoryModel>,
    @SerializedName("boot_list")
    var bootList: List<PackedInventoryModel>,

    @SerializedName("fitted_addon_list")
    var fittedAddonList: List<PackedInventoryModel>,
    @SerializedName("fitted_boot_list")
    var fittedBootList: List<PackedInventoryModel>,
    @SerializedName("fitted_ski_list")
    var fittedSkiList: List<PackedInventoryModel>,

    @SerializedName("clothing_list")
    var clothingList: List<ClothingModel>,
    @SerializedName("existing_boots")
    var existingBoots: List<ExistingBootSkis>,
    @SerializedName("existing_skis")
    var existingSkis: List<ExistingBootSkis>,

    @SerializedName("ability")
    var ability: String,

    @SerializedName("age")
    var age: String,
    @SerializedName("assigned_addon_list")
    var assignedAddonList: List<AssignedAddOns>,
    @SerializedName("assigned_upgrade_addon_list")
    var assignedUpgradeAddonList: List<AssignedAddOns>,


    @SerializedName("clothing")
    var clothing: String,

    @SerializedName("delivery_id")
    var deliveryId: Int,
    @SerializedName("din_setting")
    var dinSetting: String,

    @SerializedName("dob")
    var dob: String,

    @SerializedName("gender")
    var gender: String,
    @SerializedName("have_history")
    var haveHistory: Boolean,
    @SerializedName("height")
    var height: String,
    @SerializedName("is_boot_fitted")
    var isBootFitted: Boolean,
    @SerializedName("is_boot_packed")
    var isBootPacked: Boolean,
    @SerializedName("is_ski_fitted")
    var isSkiFitted: Boolean,
    @SerializedName("is_ski_packed")
    var isSkiPacked: Boolean,
    @SerializedName("is_pack_addons_available")
    var isPackAddonsAvailable: Boolean,
    @SerializedName("is_fit_addons_available")
    var isFitAddonsAvailable: Boolean,
    @SerializedName("is_switch_out")
    var isSwitchOut: Boolean,
    @SerializedName("manual_din")
    var manualDin: String,
    @SerializedName("own_boot")
    var ownBoot: Boolean,

    @SerializedName("package")
    var packageX: String,
    @SerializedName("pole_status")
    var poleStatus: String,
    @SerializedName("poles")
    var poles: String,
    @SerializedName("recommended_din")
    var recommendedDin: String,

    @SerializedName("reservation_id")
    var reservationId: Int,
    @SerializedName("recommended_pole")
    var recommendedPole: Int?,

    @SerializedName("shoe_size")
    var shoeSize: String,
    @SerializedName("shoe_type")
    var shoeType: String,
    @SerializedName("skier_code")
    var skierCode: String,
    @SerializedName("type")
    var type: String,
    @SerializedName("weight")
    var weight: String,

//    @SerializedName("van_shipment_id")
//    var vanShipmentId: String,
//    @SerializedName("din_required")
//    var dinRequired: Boolean,
//    @SerializedName("returned_boot_list")
//    var returnedBootList: List<Any>,
//    @SerializedName("returned_ski_list")
//    var returnedSkiList: List<Any>,
//    @SerializedName("shipment_status")
//    var shipmentStatus: String,
//    @SerializedName("renter_id")
//    var renterId: Int,
//    @SerializedName("renter_name")
//    var renterName: String,
//    @SerializedName("pack_inventory_url")
//    var packInventoryUrl: String,
//    @SerializedName("din_setting_ski")
//    var dinSettingSki: String,


)


data class PackedInventoryModel(
//    @SerializedName("is_packable")
//    var isPackable: Boolean = false,
//    @SerializedName("model")
//    var model: String = "",

    @SerializedName("barcode")
    var barcode: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("manufacture")
    var manufacture: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("size_length")
    var sizeLength: String? = "",
    @SerializedName("sole_length")
    var soleLength: String? = "",
    @SerializedName("status")
    var status: String = "",
    @SerializedName("undo_packing_url")
    var undoPackingUrl: String = "",
    @SerializedName("undo_fitting_switchout_url")
    var undoFittingSwitchoutUrl: String = "",
    @SerializedName("helmet_size")
    val helmetSize: String = ""


)

data class ClothingModel(
    var id: Int,
    var name: String,
    var status: String,
    var is_packable: Boolean,
    var selected: Int = 0,
    var purchased_clothing: Boolean = false

)

data class ExistingBootSkis(
//    @SerializedName("din_setting")
//    @Expose
//    val dinSetting: Any,
//    @SerializedName("tech_initials")
//    @Expose
//    val techInitials: String,
//    @SerializedName("created_at")
//    @Expose
//    val createdAt: String,

    @SerializedName("model")
    var model: String,
    @SerializedName("length")
    @Expose
    val length: String,
    @SerializedName("size")
    @Expose
    var size: String,
    @SerializedName("barcode")
    @Expose
    val barcode: String,


)

data class AssignedAddOns(
//    @SerializedName("id")
//    @Expose
//    var id: Int,
//    @SerializedName("premium_boot")
//    @Expose
//    val premiumBoot: Boolean,
//    @SerializedName("helmet_size")
//    @Expose
//    val helmetSize: String,
//    @SerializedName("undo_packing_url")
//    @Expose
//    val undoPackingUrl: String,
//    @SerializedName("model")
//    @Expose
//    val model: Any,
//    @SerializedName("size_length")
//    @Expose
//    val sizeLength: Any,
//    @SerializedName("manufacture")
//    @Expose
//    val manufacture: Any,
//    @SerializedName("is_packable")
//    @Expose
//    val isPackable: Boolean,

    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("status")
    @Expose
    val status: String,



)