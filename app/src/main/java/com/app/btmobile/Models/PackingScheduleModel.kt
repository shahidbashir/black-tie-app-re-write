package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PackingScheduleModel(
    @SerializedName("schedule")
    var scheduleDeliveries: List<ScheduleDelivery>?,
    @SerializedName("un_schedule")
    var unscheduleDeliveries: List<ScheduleDelivery>?,
    var meta_data: PackingMetaData
)


data class PackingMetaData(
    var per_page: String,
    var current_page: String,
    var total: String,
    var total_pages: String,
)

data class ScheduleDelivery(
//    @SerializedName("color")
//    var color: String?,
//    @SerializedName("delivered_date")
//    var deliveredDate: String?,
//    @SerializedName("items_returned")
//    var itemsReturned: Boolean?,
//    @SerializedName("status")
//    var status: String?,
//    @SerializedName("status_indicator_class")
//    var statusIndicatorClass: String?,
//    @SerializedName("waiver")
//    var waiver: String?,



    var isSchedule: Boolean = false,
    var isFirst: Boolean = false,
    @SerializedName("address")
    var address: String?,
    var lodging: String?,
    @SerializedName("email")
    var email: String?,



    @SerializedName("customer_name")
    var customerName: String?,


    @SerializedName("delivery_date")
    var deliveryDate: String?,

    @SerializedName("delivery_id")
    var deliveryId: Int?,



    @SerializedName("room_number")
    var room_number: String? = null,

    @SerializedName("is_group_fitting")
    var isGroupFitting: Boolean?,


    @SerializedName("map_url")
    var mapUrl: String?,

    @SerializedName("number_of_renters")
    var numberOfRenters: Int?,

    @SerializedName("packing_status")
    var packingStatus: String?,

    @SerializedName("reservation_id")
    var reservationId: Int?,

    @SerializedName("return_date")
    var returnDate: String?,

    @SerializedName("schedule_indicator_class")
    var scheduleIndicatorClass: String?,

    @SerializedName("shipment_id")
    var shipmentId: Int?,



    @SerializedName("time")
    var time: String?,

    @SerializedName("van")
    var van: String?,


) : Serializable