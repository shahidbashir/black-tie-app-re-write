package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName

data class ProfileModel(
    @SerializedName("banned")
    val banned: Int,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("full_name")
    val fullName: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("last_login")
    val lastLogin: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("name_initials")
    val nameInitials: String,
    @SerializedName("option_in")
    val optionIn: String,
    @SerializedName("permissions")
    val permissions: List<Any>,
    @SerializedName("ph_no")
    val phNo: String,
    @SerializedName("resort_id")
    val resortId: Double,
    @SerializedName("status_label")
    val statusLabel: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("user_name")
    val userName: String
)