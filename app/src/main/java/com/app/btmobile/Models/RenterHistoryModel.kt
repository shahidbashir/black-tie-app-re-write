package com.app.btmobile.Models

data class RenterHistoryModel(
    val `data`: RenterHistoryData,
    val success: Boolean
)

data class RenterHistoryData(
    val boots_history: String,
    val renter_id: Int,
    val skis_history: String,
    var renterName: String
)