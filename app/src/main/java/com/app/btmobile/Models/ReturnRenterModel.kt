package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName

class ReturnRenterModel : ArrayList<ReturnRenterModelItem>()

data class ReturnRenterModelItem(
//    @SerializedName("all_renter_returned")
//    var allRenterReturned: String,
//    @SerializedName("return_time")
//    var returnTime: String,
//    @SerializedName("shipment_id")
//    var shipmentId: Int,

    @SerializedName("addons")
    var addons: List<ReturnAddon>,
    @SerializedName("addons_with_barcode")
    var addonsWithBarcodeList: ArrayList<ReturnAddon>,

    @SerializedName("clothing")
    var clothing: ReturnClothing,
    @SerializedName("delivery_time")
    var deliveryTime: String,
    @SerializedName("inventories")
    var inventories: List<ReturnInventory>,
    @SerializedName("renter_id")
    var renterId: Int,
    @SerializedName("renter_name")
    var renterName: String,


    @SerializedName("customer_phone")
    var customerPhone: String,
    @SerializedName("delivery_date")
    var deliveryDate: String,
    @SerializedName("number_of_renters")
    var numberOfRenters: Int,
    @SerializedName("reservation_id")
    var reservation_id: Int?,
    @SerializedName("return_date")
    var returnDate: String,
    @SerializedName("lodging")
    var lodging: String,
    @SerializedName("property_name")
    var property_name: String,
    @SerializedName("room_no")
    var room_no: String?,

)

data class ReturnInventory(
    @SerializedName("barcode")
    var barcode: String,
    @SerializedName("manufacture")
    var manufacture: String,
    @SerializedName("model")
    var model: String,
    @SerializedName("renter_returned")
    var renterReturned: Int,
    @SerializedName("returned_at")
    var returnedAt: String,
    @SerializedName("size_length")
    var sizeLength: String,
    @SerializedName("type")
    var type: String
)

data class ReturnClothing(
//    @SerializedName("returned_at")
//    var returnedAt: String,
//    @SerializedName("type")
//    var type: String,
//    @SerializedName("id")
//    var id: Int,

    @SerializedName("is_returned")
    var isReturned: Int,
    @SerializedName("name")
    var name: String,

)

data class ReturnAddon(
//    @SerializedName("addon_renter_id")
//    var addonRenterId: Int,
//    @SerializedName("detail")
//    var detail: String,
    @SerializedName("id")
    var id: Int,
//    @SerializedName("returned_at")
//    var returnedAt: String,
//    @SerializedName("type")
//    var type: String,

    @SerializedName("is_returned")
    var isReturned: Int,
    @SerializedName("name")
    var name: String,
    @SerializedName("addon_barcode")
    var addonBarcode: String?,
    @SerializedName("addon_size")
    var addonSize: String?,
    var isAddonWithBarcode: Boolean = false,
    var is_manual: Int?
)