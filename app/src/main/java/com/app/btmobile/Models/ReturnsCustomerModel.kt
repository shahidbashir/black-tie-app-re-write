package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

//data class DeliveryCustomersMainModel(
//    @SerializedName("data")
//    var `data`: List<DeliveryCustomersModel>?,
//    var meta_data: MetaData
//)

data class MetaDataReturn(
    var per_page: String,
    var current_page: String,
    var total_pages: String,
)

data class ReturnsCustomerModel(
    @SerializedName("data")
    var `data`: ArrayList<ReturnsCustomerModelItem>?,
    var meta_data: MetaDataReturn?
)

data class ReturnsCustomerModelItem(
//    @SerializedName("customer_phone")
//    var customerPhone: String,
//    @SerializedName("delivered_date")
//    var deliveredDate: String,
//    @SerializedName("delivery_date")
//    var deliveryDate: String,
//    @SerializedName("delivery_id")
//    var deliveryId: String,
//    @SerializedName("email")
//    var email: String,
//    @SerializedName("is_signed")
//    var isSigned: String,
//    @SerializedName("itemid")
//    var itemid: Int,
//    @SerializedName("items_returned")
//    var itemsReturned: String,
//    @SerializedName("reservation_id")
//    var reservation_id: Int?,
//    @SerializedName("return_date")
//    var returnDate: String,
//    @SerializedName("delivery_time")
//    var deliveryTime: String,
//    @SerializedName("userId")
//    var userId: Int,


    @SerializedName("all_renter_returned")
    var allRenterReturned: Boolean,
    @SerializedName("customer_name")
    var customerName: String,
    @SerializedName("is_switchout")
    var isSwitchout: Boolean,

    @SerializedName("map_url")
    var mapUrl: String?,
    @SerializedName("number_of_renters")
    var numberOfRenters: Int,

    @SerializedName("return_notes")
    var returnNotes: List<String>,
    @SerializedName("return_time")
    var returnTime: String,
    @SerializedName("lodging")
    var lodging: String,

    @SerializedName("room_no")
    var roomNo: String,
    @SerializedName("status")
    var status: String,
    @SerializedName("van")
    var van: String,
    @SerializedName("van_shipment_id")
    var vanShipmentId: Int,
    @SerializedName("waiver")
    var waiver: String?,
    var property_name: String

) : Serializable