package com.app.btmobile.Models

import com.google.gson.annotations.SerializedName

data class SaveAddonWithBarcodeModel(
    val message: String,
    @SerializedName("success")
    val status: Boolean,
    var addon_status: String,
    var helmet_size: String,
    var addon_id: Int,
    var is_manual: Boolean = false
)
