package com.app.btmobile.Models

data class SaveFitBootAndSkisByBarcodeModel(
    val message: String,
    val success: Boolean,
    var parameters: HashMap<String, String>,
    var isForBoot: Boolean
)
