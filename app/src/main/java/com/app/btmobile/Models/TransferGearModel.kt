package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName

data class TransferGearModel(
    @SerializedName("data")
    val `data`: TransferGearData,
    @SerializedName("message")
    val message: String,
    @SerializedName("success")
    val success: Boolean
)

data class TransferGearData(
    @SerializedName("inventory_locations")
    val inventoryLocations: List<TransferGearInventoryLocation>
)

data class TransferGearInventoryLocation(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String


) {
    override fun toString(): String {
        return "$name"
    }
}
