package com.app.btmobile.Models

data class UnScheduleInventoryModel(
    val message: String,
    val success: Boolean,
    val van_shipment_id:Int
)
