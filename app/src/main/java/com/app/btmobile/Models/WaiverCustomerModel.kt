package com.app.btmobile.Models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class WaiverCustomerModel : ArrayList<WaiverCustomerModelItem>()

data class WaiverCustomerModelItem(
    @SerializedName("accommodation")
    var accommodation: String,
    @SerializedName("created_at")
    var createdAt: String,
    @SerializedName("created_by")
    var createdBy: String,
    @SerializedName("customer_id")
    var customerId: String,
    @SerializedName("customer_name")
    var customerName: String,
    @SerializedName("days")
    var days: Int,
    @SerializedName("delivery_date")
    var deliveryDate: String,
    @SerializedName("delivery_date_time")
    var deliveryDateTime: String,
    @SerializedName("delivery_location_tax")
    var deliveryLocationTax: Double,
    @SerializedName("discount_amount")
    var discountAmount: Double,
    @SerializedName("first_day_skiing")
    var firstDaySkiing: String,
    @SerializedName("id")
    var id: Int,
    @SerializedName("is_completed")
    var isCompleted: Int,
    @SerializedName("itemid")
    var itemid: Int,
    @SerializedName("last_day_skiing")
    var lastDaySkiing: String,
    @SerializedName("ph_no")
    var phNo: String,
    @SerializedName("rental_length")
    var rentalLength: Int,
    @SerializedName("renters_count")
    var rentersCount: Int,
    @SerializedName("resort_id")
    var resortId: String,
    @SerializedName("sales_tax")
    var salesTax: Double,
    @SerializedName("status")
    var status: String,
    @SerializedName("sub_total")
    var subTotal: Double,
    @SerializedName("tip_amount")
    var tipAmount: Double,
    @SerializedName("total_amount")
    var totalAmount: Double,
    @SerializedName("total_deposit")
    var totalDeposit: Double,
    @SerializedName("total_due")
    var totalDue: Double,
    @SerializedName("tourism_tax")
    var tourismTax: Double,
    @SerializedName("type")
    var type: String,
    @SerializedName("updated_at")
    var updatedAt: String,
    @SerializedName("userId")
    var userId: Int,
    @SerializedName("waiver_status")
    var waiverStatus: String
) : Serializable