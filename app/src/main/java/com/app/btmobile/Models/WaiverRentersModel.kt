package com.app.btmobile.Models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class WaiverRentersModel(
    @SerializedName("renters")
    var renters: List<WaiverRenter>,
    @SerializedName("waiver_text")
    var waiverText: String,
    @SerializedName("parent_guardian_text")
    val parentGuardianText: String? = null,
    @SerializedName("ph_no")
    var phoneNumber: String?,
)

data class WaiverRenter(
    @SerializedName("addons_price")
    var addonsPrice: String,
    @SerializedName("amount")
    var amount: String,
    @SerializedName("bring_boot")
    var bringBoot: Int,
    @SerializedName("check_data")
    var checkData: Int,
    @SerializedName("created_at")
    var createdAt: String,
    @SerializedName("discount")
    var discount: String,
    @SerializedName("dob_year")
    var dobYear: String,
    @SerializedName("experience")
    var experience: String,
    @SerializedName("first_name")
    var firstName: String,
    @SerializedName("ft")
    var ft: String,
    @SerializedName("in")
    var inX: String,
    @SerializedName("is_year_only")
    var isYearOnly: Int,
    @SerializedName("itemid")
    var itemid: Int,
    @SerializedName("last_name")
    var lastName: String,
    @SerializedName("lb")
    var lb: String,
    @SerializedName("number_of_days")
    var numberOfDays: Int,
    @SerializedName("package_id")
    var packageId: Int,
    @SerializedName("package_name")
    var packageName: String,
    @SerializedName("package_price")
    var packagePrice: String,
    @SerializedName("price")
    var price: String,
    @SerializedName("recommended_din")
    var recommendedDin: String,
    @SerializedName("sales_tax")
    var salesTax: String,
    @SerializedName("shoe_country")
    var shoeCountry: String,
    @SerializedName("shoe_size")
    var shoeSize: String,
    @SerializedName("shoe_type")
    var shoeType: String,
    @SerializedName("total_price")
    var totalPrice: String,
    @SerializedName("type")
    var type: String,
    @SerializedName("unit_system")
    var unitSystem: String,
    @SerializedName("updated_at")
    var updatedAt: String,
    @SerializedName("userId")
    var userId: Int,
    @SerializedName("waiver_status")
    var waiverStatus: Boolean,

    @SerializedName("ability")
    var ability: String,
    @SerializedName("age")
    var age: String,
    @SerializedName("din_setting")
    var dinSetting: String,
    @SerializedName("dob")
    var dob: String,
    @SerializedName("reservation_id")
    var reservationId: Int,
    @SerializedName("full_name")
    var fullName: String,
    @SerializedName("gender")
    var gender: String,
    @SerializedName("height")
    var height: String,
    @SerializedName("id")
    var id: Int,
    @SerializedName("signed_status")
    var signedStatus: String,
    @SerializedName("signed_status_switchout")
    var signedStatusSwitchout: String,
    @SerializedName("sole_length")
    var soleLength: String,
    @SerializedName("weight")
    var weight: String,

    var waiver_image_path: String? = null,
    var waiver_din_setting: String? = null

) : Serializable