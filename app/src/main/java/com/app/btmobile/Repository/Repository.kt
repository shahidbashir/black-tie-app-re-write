package com.app.btmobile

import com.app.btmobile.Models.*
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Url
import java.util.ArrayList

class Repository(private val apiCallInterface: ApiCallInterface) {
    fun hitLogin(parameters: HashMap<String, String>): Observable<LoginModel> {
        return apiCallInterface.hitLoginApi(parameters)
    }

    fun hitNfc(auth: String, barcode: String, search_type: String): Observable<NfcModel> {
        return apiCallInterface.hitNfcApi(auth, barcode, search_type)
    }

    fun hitNfcApiForToShow(auth: String, barcode: String): Observable<BaseModel> {
        return apiCallInterface.hitNfcApiForToShow(auth, barcode)
    }

    fun hitInventoryCountStATUSuPDATE(
        auth: String,
        parameters: HashMap<String, Any>,
        search_type: String
    ): Observable<SaveFitBootAndSkisByBarcodeModel> {
        return apiCallInterface.hitInventoryCountStATUSuPDATE(auth, parameters, search_type)
    }

    fun hitViewTorqueTestingApi(auth: String, barcode: String): Observable<GetTorqueTestModel> {
        return apiCallInterface.viewTorqueTestingApi(auth, barcode)
    }

    fun hitAdTorqueTestingApi(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.addTorqueTestingApi(auth, parameters)
    }

    fun hitDeliveryCustomersApi(
        auth: String,
        options: Map<String, String>
    ): Observable<DeliveryCustomersMainModel> {
        return apiCallInterface.hitDeliveryCustomersApi(auth, options)
    }

    suspend fun hitDeliveryCustomersApiCoro(
        auth: String,
        options: Map<String, String>
    ): DeliveryCustomersMainModel {
        return apiCallInterface.hitDeliveryCustomersApiCoro(auth, options)
    }

    fun getVanList(
        auth: String
    ): Observable<ArrayList<VanModel>> {
        return apiCallInterface.getVanList(auth)
    }

    fun hitDeliveryRentersApi(
        auth: String,
        van_shipment_id: Int,
        date: String
    ): Observable<DeliveryRenterMainModel> {
        return apiCallInterface.hitDeliveryRentersApi(auth, van_shipment_id, date)
    }

    fun hitGetRenterHistoryApi(
        auth: String,
        renter_id: Int
    ): Observable<RenterHistoryModel> {
        return apiCallInterface.hitGetRenterHistory(auth, renter_id)
    }

    fun hitSwitchReturnApi(
        auth: String,
        shipmentId: Int
    ): Observable<BaseModel> {
        return apiCallInterface.hitSwitchOutReturnApi(auth, shipmentId)
    }

    fun hitGetNotesListApi(
        auth: String,
        reservation_id: Int
    ): Observable<NotesListModel> {
        return apiCallInterface.hitGetNotesListApi(auth, reservation_id)
    }

    fun hitAddNotes(
        auth: String,
        reservation_id: Int,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.addNotesApi(auth, reservation_id, parameters)
    }

    fun hitGetAllConfirmationInvoices(
        auth: String,
        reservation_id: Int
    ): Observable<ConfirmationModel> {
        return apiCallInterface.getAllConfirmationInvoices(auth, reservation_id)
    }


    fun saveConfirmationData(
        auth: String,
        reservation_id: Int,
        file: RequestBody
    ): Observable<BaseModel> {
        return apiCallInterface.hitSaveConfirmationData(
            auth, reservation_id, file
        )
    }

    fun updateConfirmationData(
        auth: String,
        reservation_id: Int,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.hitUpdateConfirmationData(
            auth, reservation_id, parameters
        )
    }

    fun hitSendToPhoneConfirmation(
        auth: String,
        parameters: HashMap<String, Any>
    ): Observable<BaseModel> {
        return apiCallInterface.hitSendToPhoneConfirmation(auth, parameters)
    }

    fun getUnSchedulePackingList(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<PackingScheduleModel> {
        return apiCallInterface.hitUnScheduleDeliveriesApi(auth, parameters)
    }

    suspend fun getUnSchedulePackingLisCoro(
        auth: String,
        parameters: HashMap<String, String>
    ): PackingScheduleModel {
        return apiCallInterface.hitUnScheduleDeliveriesApiCoro(auth, parameters)
    }

    fun getRenterPackFitData(
        auth: String,
        van_shipment_id: Int,
        renter_id: Int
    ): Observable<PackingModel> {
        return apiCallInterface.hitRenterPackFitDetailsApi(auth, van_shipment_id, renter_id)
    }

    fun unScheduleInventory(
        auth: String,
        delivery_id: Int
    ): Observable<UnScheduleInventoryModel> {
        return apiCallInterface.hitUnScheduleDeliery(
            auth, delivery_id
        )
    }

    fun hitOwnBootApi(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<OwnBootModel> {
        return apiCallInterface.hitOwnBootApi(auth, parameters)
    }

    fun hitBootPackedItem(
        auth: String,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ): Observable<SaveFitBootAndSkisByBarcodeModel> {
        return apiCallInterface.addBootPackApi(auth, van_shipment_id, parameters)
    }

    fun hitSkiPackedItem(
        auth: String,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ): Observable<SaveFitBootAndSkisByBarcodeModel> {
        return apiCallInterface.addSkiPackApi(auth, van_shipment_id, parameters)
    }

    fun hitAddPolesApi(
        auth: String,
        renter_ID: Int,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.addPolesApi(auth, renter_ID, parameters)
    }

    fun hitPackPolesApi(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.hitPackPolesApi(auth, parameters)
    }

    fun hitDeleteInventory(
        auth: String,
        @Url deleteUrl: String
    ): Observable<BaseModel> {
        return apiCallInterface.deleteInventory(auth, deleteUrl)
    }

    fun hitGetAddonsList(
        auth: String,
        renter_ID: Int
    ): Observable<AddonsModel> {
        return apiCallInterface.getAddOnsList(auth, renter_ID)
    }


    fun hitSaveAddOnsList(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.saveAddonsListApi(auth, parameters)
    }

    fun hitSaveAddonsWithBarcodeApi(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<SaveAddonWithBarcodeModel> {
        return apiCallInterface.saveAddonsWithBarcodeApi(auth, parameters)
    }

    fun hitFitSaveAddonsManuallyApi(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<SaveAddonWithBarcodeModel> {
        return apiCallInterface.fitSaveAddonsManual(auth, parameters)
    }

    fun hitGetTechCustomersList(
        auth: String,
        date: String
    ): Observable<TechStationCustomerModel> {
        return apiCallInterface.getTechStationCustomerList(auth, date)
    }


    fun hitGetTechRentersList(
        auth: String,
        van_shipment_id: Int
    ): Observable<TechStationRenterModel> {
        return apiCallInterface.getTechStationRentersList(auth, van_shipment_id)
    }


    fun hitSaveFitBootByBarcode(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ): Observable<SaveFitBootAndSkisByBarcodeModel> {
        return apiCallInterface.saveBootInFittingByBarcode(
            auth,
            renterId,
            van_shipment_id,
            parameters
        )
    }

    fun hitSaveFitSkisByBarcode(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ): Observable<SaveFitBootAndSkisByBarcodeModel> {
        return apiCallInterface.saveSkisInFittingCByBarcode(
            auth,
            renterId,
            van_shipment_id,
            parameters
        )
    }

    fun hitDeleteBootSkisFittingInventory(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.deleteFittingBootsSkisInventory(
            auth,
            parameters
        )
    }

    fun hitSaveFitBootManually(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.saveFitBootManually(
            auth,
            renterId,
            van_shipment_id,
            parameters
        )
    }

    fun hitSaveFitSkiManually(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.saveFitSkisManually(
            auth,
            renterId,
            van_shipment_id,
            parameters
        )
    }

    fun hitUpdateRenterData(
        auth: String,
        renterId: Int,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.updateRenterData(
            auth,
            renterId,
            parameters
        )
    }

    fun hitGetAbilityHeightShowSizeData(
        auth: String
    ): Observable<GetAbilityShoeSizeHeightListModel> {
        return apiCallInterface.getAbilityHeightShowSizeData(
            auth
        )
    }

    fun hitGetFittingAddonsData(
        auth: String,
        renterId: Int,
        van_shipment_id: Int
    ): Observable<GetFittingAddonsModel> {
        return apiCallInterface.getFittingAddonsList(
            auth,
            renterId,
            van_shipment_id
        )
    }

    fun hitSaveFitAddons(
        auth: String,
        options: Map<String, String>,
        fitSaveAddonsModel: FitSaveAddonsModel
    ): Observable<BaseModel> {
        return apiCallInterface.saveFitAddOns(
            auth,
            options,
            fitSaveAddonsModel
        )
    }

    fun hitUpdateAddonStatus(
        auth: String,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.hitUpdateAddonStatus(
            auth,
            parameters
        )
    }


    fun hitUpdateDinSetting(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.updateDinSetting(
            auth,
            renterId,
            van_shipment_id,
            parameters
        )
    }

    fun hitUpdateDinSettingWithDelivery(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>,
        delivery_id: Int
    ): Observable<BaseModel> {
        return apiCallInterface.updateDinSettingWithDeliveryId(
            auth,
            renterId,
            van_shipment_id,
            parameters,
            delivery_id
        )
    }

    fun hitWaiverCustomersApi(
        auth: String,
        date: String
    ): Observable<WaiverCustomerModel> {
        return apiCallInterface.hitGetWaiverCustomersList(auth, date)
    }

    fun hitWaiverRenterSApi(
        auth: String,
        reservation_id: Int,
        delivery_id: Int
    ): Observable<WaiverRentersModel> {
        return apiCallInterface.hitGetWaiverRentersList(auth, reservation_id, delivery_id)
    }


    fun hitWaiverRenterSApi(
        auth: String,
        reservation_id: Int
    ): Observable<WaiverRentersModel> {
        return apiCallInterface.hitGetWaiverRentersList(auth, reservation_id)
    }

    fun hitWaiverRenterSApi(
        auth: String,
        options: Map<String, Any>
    ): Observable<BaseModel> {
        return apiCallInterface.hitSendToPhone(auth, options)
    }

    fun saveWaiverSignature(
        auth: String,
        file: RequestBody
    ): Observable<BaseModel> {
        return apiCallInterface.hitSaveWaiverSignature(
            auth, file
        )
    }

    fun hitReturnCustomersList(
        auth: String,
        options: Map<String, String>
    ): Observable<ReturnsCustomerModel> {
        return apiCallInterface.hitReturnCustomersList(
            auth, options
        )
    }


    suspend fun hitReturnCustomersListCoro(
        auth: String,
        options: Map<String, String>
    ): ReturnsCustomerModel {
        return apiCallInterface.hitReturnCustomersListCoro(
            auth, options
        )
    }

    fun hitMassReturnSearchList(
        auth: String,
        options: Map<String, String>
    ): Observable<MassReturnBaseModel> {
        return apiCallInterface.hitMassReturnSearch(
            auth, options
        )
    }

    fun hitMassReturnApi(
        auth: String,
        options: Map<String, String>
    ): Observable<MassReturnBaseModel> {
        return apiCallInterface.hitMassReturnApi(
            auth, options
        )
    }

    fun hitReturnRenterApi(
        auth: String,
        van_shipment_id: Int
    ): Observable<ReturnRenterModel> {
        return apiCallInterface.hitGetReturnRentersList(
            auth, van_shipment_id
        )
    }

    fun hitReturnAddonOrClothing(
        auth: String,
        van_ship_id: Int,
        parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.hitReturnAddonOrClothing(
            auth,
            van_ship_id,
            parameters
        )
    }

    fun hitMarkAllReturned(
        auth: String,
        van_ship_id: Int
    ): Observable<BaseModel> {
        return apiCallInterface.hitMarkAllReturned(
            auth,
            van_ship_id
        )
    }

    fun hitReturnUsingBarcode(
        auth: String,
        van_ship_id: Int,
        parameters: HashMap<String, String>
    ): Observable<InventorReturnModel> {
        return apiCallInterface.hitReturnUsingBarcode(
            auth,
            van_ship_id,
            parameters
        )
    }

    fun hitRemoveReturn(
        auth: String, parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.hitRemoveReturn(
            auth,
            parameters
        )
    }

    fun hitGetProfileData(
        auth: String
    ): Observable<ProfileModel> {
        return apiCallInterface.hitGetProfileData(
            auth
        )
    }

    fun hitSaveProfileData(
        auth: String, id: Int, file: RequestBody
    ): Observable<BaseModel> {
        return apiCallInterface.hitSaveProfileData(
            auth,
            id,
            file
        )
    }

    fun hitGetTransferGearList(
        auth: String
    ): Observable<TransferGearModel> {
        return apiCallInterface.hitGetTransferGearList(
            auth
        )
    }

    fun hitSaveTransferGear(
        auth: String, parameters: HashMap<String, String>
    ): Observable<BaseModel> {
        return apiCallInterface.hitSaveTransferGearApi(
            auth,
            parameters
        )
    }

}