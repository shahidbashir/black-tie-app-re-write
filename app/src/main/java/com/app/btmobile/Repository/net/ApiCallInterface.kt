package com.app.btmobile

import com.app.btmobile.Models.*
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.*
import java.util.*

interface ApiCallInterface {
    @POST(Urls.LOGIN_API)
    fun hitLoginApi(@Body parameters: Map<String, String>): Observable<LoginModel>

    @Headers("Accept: application/json")
    @GET(Urls.NFC_SCAN_API)
    fun hitNfcApi(
        @Header("Authorization") authorization: String,
        @Path("barcode") barcode: String,
        @Query("search_type") search_type: String
    ): Observable<NfcModel>

    @Headers("Accept: application/json")
    @GET(Urls.FORCE_TO_SHOW)
    fun hitNfcApiForToShow(
        @Header("Authorization") authorization: String,
        @Query("barcode") barcode: String
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @GET(Urls.INVENTORY_COUNT_STATUS_UPDATE)
    fun hitInventoryCountStATUSuPDATE(
        @Header("Authorization") authorization: String,
        @QueryMap parameters: HashMap<String, Any>,
        @Query("search_type") search_type: String
    ): Observable<SaveFitBootAndSkisByBarcodeModel>


    @Headers("Accept: application/json")
    @GET(Urls.VIEW_TORQUE_TESTING)
    fun viewTorqueTestingApi(
        @Header("Authorization") authorization: String,
        @Query("barcode") barcode: String
    ): Observable<GetTorqueTestModel>

    @Headers("Accept: application/json")
    @POST(Urls.ADD_TORQUE_TESTING)
    fun addTorqueTestingApi(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>


    //DeliveriesCopy
    @Headers("Accept: application/json")
    @GET(Urls.DELIVERY_CUSTOMER_API)
    fun hitDeliveryCustomersApi(
        @Header("Authorization") authorization: String,
        @QueryMap options: Map<String, String>
    ): Observable<DeliveryCustomersMainModel>

    //Coro
    @Headers("Accept: application/json")
    @GET(Urls.DELIVERY_CUSTOMER_API)
    suspend fun hitDeliveryCustomersApiCoro(
        @Header("Authorization") authorization: String,
        @QueryMap options: Map<String, String>
    ): DeliveryCustomersMainModel

    @Headers("Accept: application/json")
    @GET(Urls.GET_VAN_LIST)
    fun getVanList(
        @Header("Authorization") authorization: String
    ): Observable<ArrayList<VanModel>>

    //Get Renters List New
    @Headers("Accept: application/json")
    @GET(Urls.DELIVERY_RENTERS_API)
    fun hitDeliveryRentersApi(
        @Header("Authorization") authorization: String,
        @Path("van_shipment_id") van_shipment_id: Int,
        @Path("date") date: String
    ): Observable<DeliveryRenterMainModel>

    @Headers("Accept: application/json")
    @GET(Urls.RENTER_HISTORY_API)
    fun hitGetRenterHistory(
        @Header("Authorization") authorization: String,
        @Query("renter_id") renter_id: Int
    ): Observable<RenterHistoryModel>

    @Headers("Accept: application/json")
    @GET(Urls.SWITCH_OUT_RETURN_API)
    fun hitSwitchOutReturnApi(
        @Header("Authorization") authorization: String,
        @Path("van_ship_id") van_ship_id: Int
    ): Observable<BaseModel>


    @Headers("Accept: application/json")
    @GET(Urls.NOTES_LIST_API)
    fun hitGetNotesListApi(
        @Header("Authorization") authorization: String,
        @Path("reservation_id") reservation_id: Int
    ): Observable<NotesListModel>

    @Headers("Accept: application/json")
    @POST(Urls.ADD_NOTES_API)
    fun addNotesApi(
        @Header("Authorization") authorization: String,
        @Path("reservation_id") reservation_id: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @GET(Urls.CONFIRMATION_INVOICES)
    fun getAllConfirmationInvoices(
        @Header("Authorization") authorization: String,
        @Path("reservation_id") reservation_id: Int
    ): Observable<ConfirmationModel>

    @Headers("Accept: application/json")
    @POST(Urls.SAVE_CONFIRMATION_DATA)
    fun hitSaveConfirmationData(
        @Header("Authorization") authorization: String,
        @Path("reservation_id") reservationID: Int,
        @Body file: RequestBody
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.UPDATE_CONFIRMATION_DATA)
    fun hitUpdateConfirmationData(
        @Header("Authorization") authorization: String,
        @Path("reservation_id") reservationID: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.SEND_TO_PHONE_CONFIRMATION)
    fun hitSendToPhoneConfirmation(
        @Header("Authorization") authorization: String,
        @QueryMap parameters: HashMap<String, Any>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @GET(Urls.UNSCHEDULE_DELIVEIRES)
    fun hitUnScheduleDeliveriesApi(
        @Header("Authorization") authorization: String,
        @QueryMap parameters: HashMap<String, String>
    ): Observable<PackingScheduleModel>

    @Headers("Accept: application/json")
    @GET(Urls.UNSCHEDULE_DELIVEIRES)
    suspend fun hitUnScheduleDeliveriesApiCoro(
        @Header("Authorization") authorization: String,
        @QueryMap parameters: HashMap<String, String>
    ): PackingScheduleModel


    @Headers("Accept: application/json")
    @GET(Urls.RENTER_PACK_FIT_DETAILS)
    fun hitRenterPackFitDetailsApi(
        @Header("Authorization") authorization: String,
        @Path("van_shipment_id") van_shipment_id: Int,
        @Path("renter_id") renter_id: Int
    ): Observable<PackingModel>

    @Headers("Accept: application/json")
    @POST(Urls.UNSCHEDULE_DELIVERY)
    fun hitUnScheduleDeliery(
        @Header("Authorization") authorization: String,
        @Path("delivery_id") delivery_id: Int
    ): Observable<UnScheduleInventoryModel>

    @Headers("Accept: application/json")
    @POST(Urls.OWN_BOOT)
    fun hitOwnBootApi(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<OwnBootModel>

    @Headers("Accept: application/json")
    @POST(Urls.ADD_SKI_PACK)
    fun addSkiPackApi(
        @Header("Authorization") authorization: String,
        @Path("van_shipment_id") van_shipment_id: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<SaveFitBootAndSkisByBarcodeModel>

    @Headers("Accept: application/json")
    @POST(Urls.ADD_BOOT_PACK)
    fun addBootPackApi(
        @Header("Authorization") authorization: String,
        @Path("van_shipment_id") van_shipment_id: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<SaveFitBootAndSkisByBarcodeModel>

    @Headers("Accept: application/json")
    @POST(Urls.ADD_POLES)
    fun addPolesApi(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.PACK_POLES)
    fun hitPackPolesApi(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST
    fun deleteInventory(
        @Header("Authorization") authorization: String,
        @Url deleteUrl: String
    ): Observable<BaseModel>


    @Headers("Accept: application/json")
    @GET(Urls.GET_ADD_ONS_LIST)
    fun getAddOnsList(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int
    ): Observable<AddonsModel>

    @Headers("Accept: application/json")
    @POST(Urls.SAVE_ADD_ONS_LIST)
    fun saveAddonsListApi(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>


    @Headers("Accept: application/json")
    @POST(Urls.SAVE_ADDON_WITH_BARCODE)
    fun saveAddonsWithBarcodeApi(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<SaveAddonWithBarcodeModel>

    @Headers("Accept: application/json")
    @POST(Urls.FIT_SAVE_ADDON_MANUAL)
    fun fitSaveAddonsManual(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<SaveAddonWithBarcodeModel>


    @Headers("Accept: application/json")
    @GET(Urls.GET_TECH_CUSTOMERS_LIST)
    fun getTechStationCustomerList(
        @Header("Authorization") authorization: String,
        @Query("date") date: String
    ): Observable<TechStationCustomerModel>

    @Headers("Accept: application/json")
    @GET(Urls.GET_TECH_RENTERS_LIST)
    fun getTechStationRentersList(
        @Header("Authorization") authorization: String,
        @Path("van_shipment_id") van_shipment_id: Int
    ): Observable<TechStationRenterModel>

    @Headers("Accept: application/json")
    @POST(Urls.FIT_BOOT_BY_BARCODE)
    fun saveBootInFittingByBarcode(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int,
        @Path("van_shipment") van_shipment: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<SaveFitBootAndSkisByBarcodeModel>

    @Headers("Accept: application/json")
    @POST(Urls.FIT_SKI_BY_BARCODE)
    fun saveSkisInFittingCByBarcode(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int,
        @Path("van_shipment") van_shipment: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<SaveFitBootAndSkisByBarcodeModel>

    @Headers("Accept: application/json")
    @POST(Urls.DELETE_FITTING_BOOT_SKIS_BY_BARCODE)
    fun deleteFittingBootsSkisInventory(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.FIT_BOOT_MANUALY)
    fun saveFitBootManually(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int,
        @Path("van_shipment") van_shipment: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.FIT_SKI_MANUALY)
    fun saveFitSkisManually(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int,
        @Path("van_shipment") van_shipment: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>


    @Headers("Accept: application/json")
    @POST(Urls.UPDATE_RENTER_DATA)
    fun updateRenterData(
        @Header("Authorization") authorization: String,
        @Path("id") renter_id: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @GET(Urls.GET_ABILITY_HEIGHT_SHOW_SIZE_Data)
    fun getAbilityHeightShowSizeData(
        @Header("Authorization") authorization: String
    ): Observable<GetAbilityShoeSizeHeightListModel>

    @Headers("Accept: application/json")
    @GET(Urls.GET_FITTING_ADDONS_LIST)
    fun getFittingAddonsList(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int,
        @Path("van_shipment") van_shipment: Int
    ): Observable<GetFittingAddonsModel>


    @Headers("Accept: application/json")
    @POST(Urls.SAVE_FITTING_ADDONS)
    fun saveFitAddOns(
        @Header("Authorization") authorization: String,
        @QueryMap options: Map<String, String>,
        @Body fitSaveAddonsModel: FitSaveAddonsModel
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.UPDATE_ADDON_STATUS)
    fun hitUpdateAddonStatus(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.UPDATE_DIN_SETTINGS)
    fun updateDinSetting(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int,
        @Path("van_shipment") van_shipment: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.UPDATE_DIN_SETTINGS)
    fun updateDinSettingWithDeliveryId(
        @Header("Authorization") authorization: String,
        @Path("renter_id") renter_id: Int,
        @Path("van_shipment") van_shipment: Int,
        @Body parameters: HashMap<String, String>,
        @Query("delivery_id") delivery_id: Int
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @GET(Urls.WAIVER_CUSTOMERS_LIST)
    fun hitGetWaiverCustomersList(
        @Header("Authorization") authorization: String,
        @Query("date") date: String
    ): Observable<WaiverCustomerModel>

    @Headers("Accept: application/json")
    @GET(Urls.WAIVER_RENTERS_LIST)
    fun hitGetWaiverRentersList(
        @Header("Authorization") authorization: String,
        @Path("reservation_id") reservation_id: Int,
        @Query("delivery_id") delivery_id: Int
    ): Observable<WaiverRentersModel>


    @Headers("Accept: application/json")
    @GET(Urls.WAIVER_RENTERS_LIST)
    fun hitGetWaiverRentersList(
        @Header("Authorization") authorization: String,
        @Path("reservation_id") reservation_id: Int
    ): Observable<WaiverRentersModel>

    @Headers("Accept: application/json")
    @POST(Urls.SEND_TO_PHONE)
    @JvmSuppressWildcards
    fun hitSendToPhone(
        @Header("Authorization") authorization: String,
        @QueryMap options: Map<String, Any>
    ): Observable<BaseModel>


    @Headers("Accept: application/json")
    @POST(Urls.SAVE_WAIVER_SIGNATURE)
    fun hitSaveWaiverSignature(
        @Header("Authorization") authorization: String,
        @Body file: RequestBody
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @GET(Urls.RETURN_CUSTOMER)
    fun hitReturnCustomersList(
        @Header("Authorization") authorization: String,
        @QueryMap options: Map<String, String>
    ): Observable<ReturnsCustomerModel>

    @Headers("Accept: application/json")
    @GET(Urls.RETURN_CUSTOMER)
    suspend fun hitReturnCustomersListCoro(
        @Header("Authorization") authorization: String,
        @QueryMap options: Map<String, String>
    ): ReturnsCustomerModel

    @Headers("Accept: application/json")
    @POST(Urls.MASS_RETURN_SEARCH)
    fun hitMassReturnSearch(
        @Header("Authorization") authorization: String,
        @QueryMap options: Map<String, String>
    ): Observable<MassReturnBaseModel>


    @Headers("Accept: application/json")
    @POST(Urls.MASS_RETURN)
    fun hitMassReturnApi(
        @Header("Authorization") authorization: String,
        @QueryMap options: Map<String, String>
    ): Observable<MassReturnBaseModel>

    @Headers("Accept: application/json")
    @GET(Urls.RETURN_RENTERS_NEW)
    fun hitGetReturnRentersList(
        @Header("Authorization") authorization: String,
        @Path("van_shipment_id") van_shipment_id: Int
    ): Observable<ReturnRenterModel>

    @Headers("Accept: application/json")
    @GET(Urls.MARK_ALL_AS_RETURNED)
    fun hitMarkAllReturned(
        @Header("Authorization") authorization: String,
        @Path("van_shipment_id") van_shipment_id: Int
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.RETURN_USING_BARCODE)
    fun hitReturnUsingBarcode(
        @Header("Authorization") authorization: String,
        @Path("van_ship_id") van_ship_id: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<InventorReturnModel>

    @Headers("Accept: application/json")
    @POST(Urls.RETURN_REMOVE)
    fun hitRemoveReturn(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>


    @Headers("Accept: application/json")
    @POST(Urls.RETURN_ADDON_OR_CLOTHING)
    fun hitReturnAddonOrClothing(
        @Header("Authorization") authorization: String,
        @Path("van_ship_id") van_ship_id: Int,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

    @Headers("Accept: application/json")
    @POST(Urls.GET_PROFILE_DATA)
    fun hitGetProfileData(
        @Header("Authorization") authorization: String
    ): Observable<ProfileModel>

    @Headers("Accept: application/json")
    @POST(Urls.SAVE_PROFILE_DATA)
    fun hitSaveProfileData(
        @Header("Authorization") authorization: String,
        @Path("id") id: Int,
        @Body file: RequestBody
    ): Observable<BaseModel>


    @Headers("Accept: application/json")
    @GET(Urls.GET_TRANSFER_GEAR_LIST)
    fun hitGetTransferGearList(
        @Header("Authorization") authorization: String
    ): Observable<TransferGearModel>

    @Headers("Accept: application/json")
    @POST(Urls.SAVE_TRANSFER_GEAR)
    fun hitSaveTransferGearApi(
        @Header("Authorization") authorization: String,
        @Body parameters: HashMap<String, String>
    ): Observable<BaseModel>

}
