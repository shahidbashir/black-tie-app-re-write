package com.app.btmobile

import com.app.btmobile.Utils.AppConstants
import com.app.btmobile.Utils.AppConstants.BASE_URL

class Urls {
    companion object {
        //Login
        const val LOGIN_API = "api/auth/login"
        const val INVENTORY_COUNT_STATUS_UPDATE = "api/v1/inventory-count/status-update"

        //Nfc
        const val NFC_SCAN_API = "api/v1/search-inventory/{barcode}"
        const val FORCE_TO_SHOW = "api/v1/force-back"
        const val ADD_TORQUE_TESTING = "/api/v1/store-torque"
        const val VIEW_TORQUE_TESTING = "/api/v1/view-torque"

        //Deliveries
        const val DELIVERY_CUSTOMER_API = "api/v1/deliveries"
        const val GET_VAN_LIST = "api/v1/vans"
        const val DELIVERY_RENTERS_API = "api/v1/van-shipment/reservation/{van_shipment_id}/{date}"
        const val RENTER_HISTORY_API = "api/v1/renters/history"
        const val SWITCH_OUT_RETURN_API = "api/v1/returns/{van_ship_id}/switch-out-return"

        //Notes
        const val NOTES_LIST_API = "api/v1/reservations/notes/{reservation_id}"
        const val ADD_NOTES_API = "api/v1/reservations/notes/{reservation_id}"

        //Confirmation
        const val CONFIRMATION_INVOICES = "api/v1/confirmation-invoices/{reservation_id}"
        const val SAVE_CONFIRMATION_DATA = "api/v1/save-confirmation-invoice/{reservation_id}"
        const val UPDATE_CONFIRMATION_DATA = "api/v1/update-confirmation-invoice/{reservation_id}"
        const val SEND_TO_PHONE_CONFIRMATION = "api/v1/confirmation/send-to-phone"

        //Packing
        const val UNSCHEDULE_DELIVEIRES = "api/v1/unschedule-deliveries"
        const val RENTER_PACK_FIT_DETAILS = "/api/v1/pack-station/{van_shipment_id}/{renter_id}"
        const val UNSCHEDULE_DELIVERY = "/api/v1/temporary-schedule/{delivery_id}"
        const val OWN_BOOT = "/api/v1/update/own-boot"

        // const val ADD_PACKED_ITEMS = "api/v1/van-shipment/add/inventory/{van_shipment_id}"
        const val ADD_BOOT_PACK = "api/v1/van-shipment/pack-inventory-boot/{van_shipment_id}"
        const val ADD_SKI_PACK = "api/v1/van-shipment/pack-inventory-ski/{van_shipment_id}"
        const val GET_ADD_ONS_LIST = "api/v1/list/addons/{renter_id}"
        const val SAVE_ADD_ONS_LIST = "api/v1/pack/addons"
        const val SAVE_ADDON_WITH_BARCODE = "api/v1/addon-with-barcode"
        const val FIT_SAVE_ADDON_MANUAL = "api/v1/addon-with-manual-fit"
        var SAVE_ADD_ONS_LIST_LINK = AppConstants.BASE_URL + "api/v1/pack/addons"
        var SAVE_ADD_ONS_LIST_LINK_VOLLEY = AppConstants.BASE_URL + FIT_SAVE_ADDON_MANUAL

        const val ADD_POLES = "api/v1/add-poles/{renter_id}"
        const val PACK_POLES = "api/v1/pack/poles"

        //Tech Station
        const val GET_TECH_CUSTOMERS_LIST = "api/v1/list/tech-station"
        const val GET_TECH_RENTERS_LIST = "api/v1/tech-station/{van_shipment_id}"

        const val FIT_BOOT_BY_BARCODE = "api/v1/fit-boot/{renter_id}/{van_shipment}"
        const val FIT_SKI_BY_BARCODE = "api/v1/fit-ski/{renter_id}/{van_shipment}"
        const val DELETE_FITTING_BOOT_SKIS_BY_BARCODE = "/api/v1/tech-station/undo-fitting"

        const val FIT_BOOT_MANUALY = "api/v1/fit-new-boot/{renter_id}/{van_shipment}"
        const val FIT_SKI_MANUALY = "api/v1/fit-new-ski/{renter_id}/{van_shipment}"

        const val UPDATE_RENTER_DATA = "api/v1/renters/update/{id}"
        const val GET_ABILITY_HEIGHT_SHOW_SIZE_Data = "api/v1/renters/data"

        const val GET_FITTING_ADDONS_LIST = "api/v1/add-equipment/{renter_id}/{van_shipment}"
        const val SAVE_FITTING_ADDONS = "api/v1/list/save-addons"
        const val UPDATE_ADDON_STATUS = "/api/v1/addons/status"
        const val UPDATE_DIN_SETTINGS = "api/v1/update-din-setting/{renter_id}/{van_shipment}"

        //Waiver
        const val WAIVER_CUSTOMERS_LIST = "api/v1/waivers"
        const val WAIVER_RENTERS_LIST = "/api/v1/waivers/{reservation_id}"
        const val SEND_TO_PHONE = "api/v1/waivers/send-to-phone"
        const val SAVE_WAIVER_SIGNATURE = "api/v1/waivers/store"

        //Returns
        const val RETURN_CUSTOMER = "api/v1/returns"
        const val MASS_RETURN = "/api/v1/mass-return"
        const val MASS_RETURN_SEARCH = "/api/v1/mass-return-search"
        const val RETURN_RENTERS = "/api/v1/returns/{van_shipment_id}/view"
        const val RETURN_RENTERS_NEW = "/api/v1/returns/{van_shipment_id}/view-new"
        const val MARK_ALL_AS_RETURNED = "/api/v1/returns/{van_shipment_id}/mark/return"
        const val RETURN_USING_BARCODE = "/api/v1/returns/{van_ship_id}/barcode"
        const val RETURN_REMOVE = "api/v1/undo-return"
        const val RETURN_ADDON_OR_CLOTHING =
            "/api/v1/returns/{van_ship_id}/return-addon-or-clothing"

        //Profile
        const val GET_PROFILE_DATA = "api/auth/me"
        const val SAVE_PROFILE_DATA = "api/v1/update/profile/{id}"

        //Transfer Gear
        const val GET_TRANSFER_GEAR_LIST = "api/v1/inventory-locations/transfer-gear"
        const val SAVE_TRANSFER_GEAR = "api/v1/inventory-locations/transfer-gear/save"


    }
}