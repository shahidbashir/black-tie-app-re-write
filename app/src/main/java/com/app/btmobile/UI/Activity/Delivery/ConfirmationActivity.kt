package com.app.btmobile.UI.Activity.Delivery

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.UI.Adapter.InvoicesAdapter
import com.app.btmobile.UI.Dialog.CommonDialog
import com.app.btmobile.UI.Dialog.SendToPhoneDialog
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityCofirmationBinding
import com.ogoul.kalamtime.viewmodel.ConfirmationViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.util.*
import javax.inject.Inject

class ConfirmationActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityCofirmationBinding
    lateinit var loaderDialog: LoaderDialog

    lateinit var adapter: InvoicesAdapter
    var allItems: ArrayList<Invoice> =
        ArrayList<Invoice>()
    var reservationId = 0

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: ConfirmationViewModel
    lateinit var confirmationModel: ConfirmationModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCofirmationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setupViewModel()
        setupToolbarAndLoader()
        setUpRecyclerview()
    }

    fun getIntentData() {
        if (intent != null) {
            reservationId = intent.getIntExtra("reservationId", -1)
        }
    }


    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(ConfirmationViewModel::class.java)
        viewModel.getConfirmationData().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
        viewModel.getSaveConfirmationData().observe(this, androidx.lifecycle.Observer {
            consumeSaveDataResponse(it)
        })
        viewModel.getUpdateConfirmationData().observe(this, androidx.lifecycle.Observer {
            consumeUpdateDataResponse(it)
        })
    }

    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Confirmation Page"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })
    }

    fun hitUpdateConfirmationData(position: Int, notes: String, reservationChanged: String) {
        val params = HashMap<String, String>()
        params["reservation_changed"] = reservationChanged
        params["notes"] = notes
        params["invoice_id"] = allItems[position].invoice_id.toString()

        viewModel.hitUpdateConfirmationData(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), reservationId, params
        )
    }

    fun dialaogUpdateConfirmationDataAgain(
        position: Int,
        notes: String,
        reservationChanged: String
    ) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "This confirmation has already been submitted. Are you sure you want to send it again?",
                positiveButtonText = "Yes",
                negativeButtonText = "No",
                isOnlyOkButtonVisible = false
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                hitUpdateConfirmationData(position, notes, reservationChanged)
                commonDialog.dismiss()
            }
        })
    }

    fun setUpRecyclerview() {
        adapter = InvoicesAdapter(this, allItems, { from, position ->
            onItemClickListener(from, position)
        }, { notes, position, reservationChanged ->
            if (allItems[position].tech_confirmation &&
                allItems[position].customer_confirmation
            ) {
                dialaogUpdateConfirmationDataAgain(position, notes, reservationChanged)
            } else {
                hitUpdateConfirmationData(position, notes, reservationChanged)
            }

        })
        binding.rvInvoices.setHasFixedSize(true)
        val mLayoutManager1: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvInvoices.layoutManager = (mLayoutManager1)
        binding.rvInvoices.itemAnimator = (DefaultItemAnimator())
        binding.rvInvoices.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun onItemClickListener(from: String, position: Int) {
        when (from) {
            "next" -> {
                moveToInvoiceDetails(allItems[position].invoice_id, position)
            }
            "sendToPhone" -> {
                showSendToPhoneDialog(allItems[position].invoice_id)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        hitApi()
    }

    fun hitApi() {
        if (reservationId != -1) {
            viewModel.hitGetConfirmationData(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                reservationId
            )
        }
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<ConfirmationModel>
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                confirmationModel = (apiResponse.data as ConfirmationModel)
                if (confirmationModel.success) {
                    allItems.clear()
                    if (confirmationModel.data.invoices.isNotEmpty()) {
                        allItems.addAll(confirmationModel.data.invoices)
                        binding.noNotesYetTv.visibility = View.GONE
                    } else {
                        binding.noNotesYetTv.visibility = View.VISIBLE
                    }
                    adapter.haveCards(confirmationModel.data.cards.isNotEmpty())
                    adapter.hideStatus(confirmationModel.data.hide_amount)

                    // adapter.notifyDataSetChanged()
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@ConfirmationActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ConfirmationActivity)
            }
        }
    }

    private fun consumeSaveDataResponse(apiResponse: ApiResponse<BaseModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var notesData: BaseModel = (apiResponse.data as BaseModel)
                if (!notesData.message.isNullOrEmpty()) {
                    showToast(this, notesData.message, notesData.success)
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@ConfirmationActivity, apiResponse.error.toString()
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ConfirmationActivity)
            }
        }
    }

    private fun consumeUpdateDataResponse(apiResponse: ApiResponse<BaseModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var notesData: BaseModel = (apiResponse.data as BaseModel)
                if (!notesData.message.isNullOrEmpty()) {
                    showToast(this, notesData.message, notesData.success)
                }
                hitApi()
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@ConfirmationActivity, apiResponse.error.toString()
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ConfirmationActivity)
            }
        }
    }


    fun moveToInvoiceDetails(invoiceId: Int, position: Int) {
        val intent = Intent(this@ConfirmationActivity, InvoiceDetailsActivity::class.java)
        intent.putExtra("reservationId", reservationId)
        intent.putExtra("position", position)
        intent.putExtra("invoiceId", invoiceId)
        intent.putExtra("fromDeliveries", false)
        startActivity(intent)
    }

    fun showSendToPhoneDialog(invoiceId: Int) {
        val sendToPhoneDialog: SendToPhoneDialog =
            SendToPhoneDialog.newInstance(confirmationModel.data.customer_phone)
        sendToPhoneDialog.show(supportFragmentManager, "")
        sendToPhoneDialog.setonSendClickListener(object : SendToPhoneDialog.OnSendClick {
            override fun onSend(phoneNum: String) {
                val params = HashMap<String, Any>()
                params["reservation_id"] = reservationId.toString()
                params["invoice_id"] = invoiceId.toString()
                if (phoneNum.isNotEmpty()) {
                    params["send_to_customer"] = "0"
                    params["phone"] = phoneNum
                } else {
                    params["send_to_customer"] = "1"
                }
                viewModel.hitSendToPhoneConfirmation(
                    sharedPrefsHelper.getUser()
                        ?.accessToken.toString(), params
                )
            }
        })
    }


}