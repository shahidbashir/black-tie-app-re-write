package com.app.btmobile.UI.Activity.Delivery

import android.app.Activity
import android.app.DatePickerDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.SearchView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.UI.Adapter.DeliveryCustomerAdapter
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityDeliveryCustomerActivityBinding
import com.google.gson.Gson
import com.ogoul.kalamtime.viewmodel.DeliveryViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class DeliveryCustomerActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityDeliveryCustomerActivityBinding
    lateinit var loaderDialog: LoaderDialog

    lateinit var adapter: DeliveryCustomerAdapter

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: DeliveryViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    var allItems: ArrayList<DeliveryOverViewModel> =
        ArrayList<DeliveryOverViewModel>()

    var vanList: ArrayList<VanModel> =
        ArrayList<VanModel>()
    lateinit var vanAdapter: ArrayAdapter<VanModel>

    var allItemBigArray: ArrayList<DeliveryCustomersModel> = ArrayList<DeliveryCustomersModel>()


    var selectedDate: String = "";
    var isOwnVan: String = "my_van"
    var txtAmPm: String = ""
    var isFirstTime: Boolean = true
    var selectedSortBy = "Delivery Time"
    var selectedStatus = "Pending"


    var isPerviousGrouping: Boolean = false

    var c = Calendar.getInstance()
    var mYear = c[Calendar.YEAR]
    var mMonth = c[Calendar.MONTH]
    var mDay = c[Calendar.DAY_OF_MONTH]

    var currentPage: String = "1"
    var metaData: MetaData? = null

    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeliveryCustomerActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupViews()
        setupToolbarAndLoader()
        setupRecyclerview()
        setUpVanListSpinner()
        setUpStatusSpinner()
        setUpSortBySpinner()
        hitApi()
        viewModel.getVanList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString()
        )

    }


    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(DeliveryViewModel::class.java)
        viewModel.deliveryCustomerResponse().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
        viewModel.getVanListResponse().observe(this, androidx.lifecycle.Observer {
            consumeVanListResponse(it)
        })
    }

    private fun setupRecyclerview() {
        adapter = DeliveryCustomerAdapter(this, allItems)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvOverview.layoutManager = mLayoutManager
        binding.rvOverview.itemAnimator = DefaultItemAnimator()
        binding.rvOverview.adapter = adapter
        binding.rvOverview.itemAnimator = null
        upwardPagination()
    }

    private fun upwardPagination() {
        binding.rvOverview.addOnScrollListener(object :
            PaginationScrollUpListener(binding.rvOverview.layoutManager as LinearLayoutManager) {
            override val isLastPage: Boolean
                get() = metaData?.current_page?.toInt() == metaData?.total_pages?.toInt()
            override val isLoading: Boolean
                get() = isFooterProgressVisible() || isSearchOpen()

            override fun showDownArrow(show: Boolean) {
            }

            override fun loadMoreItems() {
                Log.wtf(
                    "upwardPagination",
                    "${metaData?.current_page?.toInt()} / ${metaData?.total_pages?.toInt()} /  ${metaData?.current_page?.toInt() == metaData?.total_pages?.toInt()}"
                )
                Log.wtf(
                    "upwardPagination",
                    "${isFooterProgressVisible()} / ${isSearchOpen()} / ${loaderDialog.isLoaderVisible()}"
                )

                currentPage = currentPage.toInt().inc().toString()
                binding.pbFooter.visibility = View.VISIBLE
                Debugger.wtf("CheckApiHit", "loadMoreItems")
                hitApi(fromPagination = false)
            }
        })
    }

    fun isFooterProgressVisible(): Boolean {
        return binding.pbFooter.visibility == View.VISIBLE
    }


    private fun setupViews() {
        selectedDate = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.tvDateSelected.text = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.rgAmPm.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbBoth -> {
                    txtAmPm = ""
                }
                R.id.rbAm -> {
                    txtAmPm = "AM"
                }
                R.id.rbPm -> {
                    txtAmPm = "PM"
                }
            }
            Debugger.wtf("CheckApiHit", "rgAmPm")
            hitApi()
        }
        binding.rgFindDelivery.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbMyOwnVan -> {
                    isOwnVan = "my_van"
                    binding.spVanList.visibility = View.GONE
                    Debugger.wtf("CheckApiHit", "rbMyOwnVan")
                    hitApi()
                    return@setOnCheckedChangeListener
                }
                R.id.rbAllDeliveries -> {
                    isOwnVan = "all"
                    binding.spVanList.visibility = View.GONE
                    Debugger.wtf("CheckApiHit", "rbAllDeliveries")
                    hitApi()
                    return@setOnCheckedChangeListener
                }
                R.id.rbSelectVan -> {
                    if (!vanList.isNullOrEmpty()) {
                        isOwnVan = ""
                        binding.spVanList.setSelection(0)
                        binding.spVanList.visibility = View.VISIBLE
                    }
                }
            }
        }
        binding.btnSelectDate.setOnClickListener {
            openDatePicker()
        }
    }

    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()


        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Deliveries"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { onBackPressed() })
    }

    fun setUpVanListSpinner() {
        vanAdapter = ArrayAdapter(this, R.layout.spinner_layout, vanList)
        vanAdapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spVanList.setAdapter(vanAdapter)

        binding.spVanList.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                if (!isFirstTime) {
                    try {
                        if (!vanList.isNullOrEmpty()) {
                            isOwnVan = vanList[position].id.toString()
                        }
                    } catch (e: Exception) {
                        Debugger.wtf("setUpVanListSpinner", "OnItemSelectedListener ${e.message}")
                    }
                    Debugger.wtf("CheckApiHit", "spVanList")
                    hitApi()

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun setUpSortBySpinner() {
        selectedSortBy = "Delivery Time"
        val sortByValue =
            arrayOf("All", "Packed", "Not Packed", "Delivery Time", "Last Name", "Lodging")
        val sortByText =
            arrayOf("All", "Packed", "Not Packed", "Delivery Time", "Last Name", "Lodging")
        val adapter = ArrayAdapter(this, R.layout.spinner_layout, sortByText)
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spinnerSortBy.adapter = adapter

        try {
            binding.spinnerSortBy.setSelection(0)
        } catch (e: Exception) {
            Debugger.wtf("setUpStatusSpinner", "${e.message}")
        }
        binding.spinnerSortBy.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                try {
                    if (!isFirstTime) {
                        selectedSortBy = sortByValue[position]
                        Debugger.wtf("CheckApiHit", "spinnerSortBy")
                        hitApi()
                    }
                } catch (e: Exception) {
                    Debugger.wtf("setUpStatusSpinner", "OnItemSelectedListener ${e.message}")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun setUpStatusSpinner() {
        selectedStatus = "Pending"
        val sortByText =
            arrayOf("Pending", "Completed")
        val adapter = ArrayAdapter(this, R.layout.spinner_layout, sortByText)
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spinnerStatus.adapter = adapter

        try {
            binding.spinnerStatus.setSelection(0)
        } catch (e: Exception) {
            Debugger.wtf("setUpStatusSpinner", "${e.message}")
        }
        binding.spinnerStatus.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                try {
                    if (!isFirstTime) {
                        selectedStatus = sortByText[position]
                        Debugger.wtf("CheckApiHit", "spinnerStatus")
                        hitApi()
                    }
                } catch (e: Exception) {
                    Debugger.wtf("setUpStatusSpinner", "OnItemSelectedListener ${e.message}")
                }
                isFirstTime = false
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }


//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.home_menu, menu)
//        return super.onCreateOptionsMenu(menu)
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> return true
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.delivery_cus_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView
        searchView?.setSearchableInfo(
            searchManager
                .getSearchableInfo(componentName)
        )
        searchView?.maxWidth = Integer.MAX_VALUE
        searchView?.queryHint = "Last Name";

        searchMessage()


        return super.onCreateOptionsMenu(menu)
    }

    private fun searchMessage() {
        io.reactivex.Observable.create<String> { emitter ->
            searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    emitter.onNext(query)
                    return false
                }

                override fun onQueryTextChange(query: String): Boolean {
                    emitter.onNext(query)
                    return false
                }
            })

        }.debounce(200, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Debugger.wtf("searchQuery", it + " sd")
                filterItems(it.toString())
            }, {
                Debugger.wtf("Search error", it.message.toString())
            }, {
                Debugger.wtf("Search Complete", "<---------->")
            })
    }

    fun filterItems(charString: String) {
        if (charString.isEmpty()) {
            allItems.clear()
            showData(allItemBigArray)
        } else {
            allItems.clear()
            var filterList: ArrayList<DeliveryCustomersModel> = ArrayList<DeliveryCustomersModel>()
            allItemBigArray.let {
                for (row in it) {
                    if (row.customer_name.toString().contains(charString, true)) {
                        filterList.add(row)
                    }
                }
            }
            showData(filterList)

        }
    }


    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            if (!searchView?.isIconified!!) {
                searchView?.isIconified = true;
                searchView?.onActionViewCollapsed();
            }
        }
    }

    override fun onBackPressed() {
        if (searchView != null) {
            if (!searchView!!.isIconified) {
                searchView!!.isIconified = true
                searchView?.onActionViewCollapsed();
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    fun hitApi(fromPagination: Boolean = true) {
        if (fromPagination) {
            currentPage = "1"
            allItems.clear()
            allItemBigArray.clear()
        }
        val date: String = parseDateToddMMddyyy(selectedDate) ?: ""
        val data: MutableMap<String, String> =
            HashMap()
        data["date"] = date
        data["from_van"] =
            if (isOwnVan.isNotEmpty()) isOwnVan else (binding.spVanList.selectedItem as VanModel).id.toString()
        if (txtAmPm.isNotEmpty()) {
            data["shift"] = txtAmPm
        }
        data["sortby"] = selectedSortBy
        data["filter_by_status"] = selectedStatus
        data["current_page"] = currentPage
        data["per_page"] = AppConstants.PER_PAGE


        Debugger.wtf("hitApi", "${data.toString()}")
        viewModel.hitDeliveryCustomerApiCoro(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), data
        )

        if (searchView != null) {
            if (searchView?.isIconified == false) {
                searchView?.isIconified = true;
            }
        }
    }

    fun hideAllProgressLoaders() {
        loaderDialog.dismissDialog()
        binding.pbFooter.visibility = View.GONE
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<DeliveryCustomersMainModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                if (searchView != null) {
                    if (!searchView!!.isIconified) {
                        searchView!!.isIconified = true
                        searchView?.onActionViewCollapsed();
                    }
                }
                setViewAndChildrenEnabled(binding.layDelivery, false)
                loaderDialog.showDialog(!isFooterProgressVisible())
                //loaderDialog.showDialog(true)
            }
            Status.SUCCESS -> {
                hideAllProgressLoaders()
                Debugger.wtf(
                    "consumeResponse",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )

                metaData = apiResponse.data?.meta_data
                if (metaData?.total_pages?.toInt() == 0 && apiResponse.data?.data.isNullOrEmpty()) {
                    allItemBigArray.clear()
                    allItems.clear()
                } else {
                    allItemBigArray.addAll(apiResponse.data?.data as ArrayList<DeliveryCustomersModel>)
                    try {
                        allItemBigArray =
                            allItemBigArray.distinct() as ArrayList<DeliveryCustomersModel>
                    } catch (e: Exception) {

                    }
                }

                showData(allItemBigArray)
                setViewAndChildrenEnabled(binding.layDelivery, true)
            }
            Status.ERROR -> {
                hideAllProgressLoaders()
                Debugger.wtf(
                    "consumeResponse",
                    "consumeResponse ERROR: " + apiResponse.error.toString()
                )
                setViewAndChildrenEnabled(binding.layDelivery, true)
                showToast(
                    this@DeliveryCustomerActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                hideAllProgressLoaders()
                logout(this@DeliveryCustomerActivity)
            }
        }
    }

    private fun consumeVanListResponse(
        apiResponse:
        ApiResponse<ArrayList<VanModel>>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeVanListResponse",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )
                vanList.clear()
                if (!apiResponse.data.isNullOrEmpty()) {
                    vanList.addAll(apiResponse.data as ArrayList<VanModel>)
                    vanAdapter.notifyDataSetChanged()
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(
                    "consumeVanListResponse",
                    "consumeResponse ERROR: " + apiResponse.error.toString()
                )
                loaderDialog.dismissDialog()
                showToast(
                    this@DeliveryCustomerActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@DeliveryCustomerActivity)
            }
        }
    }

    private fun showData(allGroupList: ArrayList<DeliveryCustomersModel>) {
        Debugger.wtf("showData", "${allGroupList.size}")

        if (allGroupList.size > 0) {
            allItems.clear()
            binding.noDeliveriesTv.visibility = View.GONE
            var groupingList: ArrayList<DeliveryCustomersModel> =
                ArrayList<DeliveryCustomersModel>()
            for (model in allGroupList) {
                if (model.is_group_fitting) {
                    isPerviousGrouping = true
                    groupingList.add(model)
                } else {
                    if (isPerviousGrouping) {
                        val overViewGroupingModel =
                            DeliveryOverViewModel(1, null, groupingList)
                        allItems.add(overViewGroupingModel)
                        groupingList = ArrayList<DeliveryCustomersModel>()
                    }
                    val overViewModel = DeliveryOverViewModel(2, model, null)
                    allItems.add(overViewModel)
                    isPerviousGrouping = false
                }
            }
            if (isPerviousGrouping) {
                val overViewGroupingModel =
                    DeliveryOverViewModel(1, null, groupingList)
                allItems.add(overViewGroupingModel)
                groupingList = ArrayList<DeliveryCustomersModel>()
                isPerviousGrouping = false
            }


            adapter.notifyDataSetChanged()
        } else if (allItems.isNullOrEmpty()) {
            allItems.clear()
            adapter.notifyDataSetChanged()
            binding.noDeliveriesTv.visibility = View.VISIBLE
        }
    }


    private fun openDatePicker() {
        val datePickerDialog: DatePickerDialog =
            object : DatePickerDialog(
                this@DeliveryCustomerActivity,
                null, mYear, mMonth, mDay
            ) {
                override fun onDateChanged(
                    @NonNull view: DatePicker,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    mYear = year
                    mMonth = monthOfYear
                    mDay = dayOfMonth
                    selectedDate = "${mMonth + 1}/$dayOfMonth/$year"
                    binding.tvDateSelected.setText("${mMonth + 1}/$dayOfMonth/$year")
                    currentPage = "1"
                    Debugger.wtf("CheckApiHit", "openDatePicker")
                    hitApi()
                    dismiss()
                }
            }
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).visibility = View.GONE
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).visibility = View.GONE
    }

    fun callNextActivity(modelCopy: DeliveryCustomersModel?) {
        val i = Intent(this, DeliveryRentersActivity::class.java)
        i.putExtra("from", AppConstants.DeliveryCustomerActivity)
        i.putExtra("modelObject", modelCopy)
        resultLauncher.launch(i)
    }

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                hitApi()
            }
        }

    fun isSearchOpen(): Boolean {
        if (searchView != null) {
            if (!searchView!!.isIconified) {
                return true
            }
        }
        return false
    }
}