package com.app.btmobile.UI.Activity.Delivery

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.UI.Activity.Fitting.FittingActivity
import com.app.btmobile.UI.Activity.Packing.PackingActivity
import com.app.btmobile.UI.Activity.Waiver.WaiverRentersActivity
import com.app.btmobile.UI.Adapter.DeliveryRentersAdapter
import com.app.btmobile.UI.Dialog.CommonDialog
import com.app.btmobile.UI.Dialog.RenterHistoryDialog
import com.app.btmobile.UI.Dialog.SwitchOutReasonDialog
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityDeliveryRentersBinding
import com.google.gson.Gson
import com.ogoul.kalamtime.viewmodel.DeliveryViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DeliveryRentersActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityDeliveryRentersBinding
    lateinit var loaderDialog: LoaderDialog

    lateinit var adapter: DeliveryRentersAdapter
    var allItemsNew: ArrayList<DeliveryRenterListModel> =
        ArrayList()


    var reservationId = 0
    var mapUrl: String? = null
    var shipmentId = 0


    var switchOutNotes: String? = ""
    var switchOutReason = ""

    var isSwitchOut = false
    var deliveryId = -1
    var selectedDate = ""
    var parsed_date = ""
    lateinit var model: DeliveryCustomersModel


    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: DeliveryViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeliveryRentersBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setupViewModel()
        setupToolbarAndLoader()
        setUpRecyclerview()
        binding.btnConfirmation.setOnClickListener {
            moveToConfirmationScreen()
        }
    }

    fun getIntentData() {
        if (intent != null) {
            if (intent.getStringExtra("from")!! == AppConstants.DeliveryCustomerActivity) {
                model = intent.getSerializableExtra("modelObject") as DeliveryCustomersModel
                reservationId = model.reservation_id
                mapUrl = model.map_url
                selectedDate = model.delivery_date ?: ""
                shipmentId = model.shipment_id ?: 0

                var address = if (!model.address.isNullOrEmpty()) {
                    model.address
                } else if (!model.lodging.isNullOrEmpty()) {
                    model.lodging
                } else {
                    ""
                }

                setValues(
                    model.delivery_date.toString(),
                    model.return_date.toString(),
                    address.toString(),
                    model.time.toString(),
                    model.number_of_renters.toString(),
                    model.room_number
                )
            } else if (intent.getStringExtra("from")!! == AppConstants.PackingCustomerScreen) {
                val model: ScheduleDelivery =
                    intent.getSerializableExtra("modelObject") as ScheduleDelivery

                Debugger.wtf("modelObject", "modelObject ${Gson().toJson(model)}")
                reservationId = model.reservationId ?: 0
                mapUrl = model.mapUrl
                selectedDate = model.deliveryDate ?: ""
                shipmentId = model.shipmentId ?: 0

                var address = if (!model.address.isNullOrEmpty()) {
                    model.address
                } else if (!model.lodging.isNullOrEmpty()) {
                    model.lodging
                } else {
                    ""
                }
                setValues(
                    model.deliveryDate.toString(),
                    model.returnDate.toString(),
                    address.toString(),
                    model.time.toString(),
                    model.numberOfRenters.toString(),
                    model.room_number,
                )
            }
        }

        binding.rvResId.text = "Res ID: ${reservationId.toString()}"
    }

    private fun convertDateFormat(dateValue: String): String? {
        return SimpleDateFormat("MM-dd-yyyy")
            .format(SimpleDateFormat("MMM dd, yyyy").parse(dateValue))
    }

    private fun setValues(
        delivery_date: String,
        return_date: String,
        address: String,
        time: String,
        number_of_renters: String,
        roomNo: String?
    ) {
        try {
            parsed_date = convertDateFormat(selectedDate).toString()
            Log.d("Parse date", parsed_date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        binding.run {
            var newFormatDeliveryDate: String? = null
            var newFormatReturnDate: String? = null
            try {
                newFormatDeliveryDate = reFormatDate(delivery_date)
                newFormatReturnDate = reFormatDate(return_date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            rentersRoomNameTv.text = address
            rentersTimeTv.text = time
            rentersDayTv.text = "$newFormatDeliveryDate - ${newFormatReturnDate ?: "Pending"}"
            rentersCountTv.text = "$number_of_renters Renters"

            if (!roomNo.isNullOrEmpty()) {
                tvRoomNum.visibility = View.VISIBLE
                tvRoomNum.text = "Room No: $roomNo"
            } else {
                tvRoomNum.visibility = View.GONE
                tvRoomNum.text = ""
            }

            btnSwNotes.setOnClickListener {
                showSwitchOutReasonDialog(
                    "Switch Out Reason",
                    switchOutNotes ?: ""
                )
            }
        }

        binding.goToNotesTv.setOnClickListener {
            moveToNotesScreen()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.delivery_renter_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
            R.id.menuDirection -> {
                GoToMap(
                    this,
                    mapUrl.toString()
                )

            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(DeliveryViewModel::class.java)
        viewModel.deliveryRenterResponse().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })

        viewModel.getRenterHistoryResponse().observe(this, androidx.lifecycle.Observer {
            consumeRenterHistoryResponse(it)
        })

        viewModel.getSwitchHistoryResponse().observe(this, androidx.lifecycle.Observer {
            consumeSwitchOutReturnResponse(it)
        })
    }

    override fun onResume() {
        super.onResume()
        if (shipmentId != null && parsed_date != null) {
            viewModel.hitDeliveryRenterApi(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                shipmentId, parsed_date
            )
        } else {
            showToast(this, "Something went wrong $selectedDate")
        }
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<DeliveryRenterMainModel>
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                loadData(apiResponse.data as DeliveryRenterMainModel)
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@DeliveryRentersActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@DeliveryRentersActivity)
            }
        }
    }

    private fun consumeRenterHistoryResponse(
        apiResponse:
        ApiResponse<RenterHistoryModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var data: RenterHistoryModel? = apiResponse.data as RenterHistoryModel
                if (data != null) {
                    if (data.success) {
                        val renterHistoryDialog: RenterHistoryDialog =
                            RenterHistoryDialog.newInstance(
                                data.data.boots_history.toString() + "\n \n" +
                                        data.data.skis_history,
                                data.data.renterName.toString() + "'s History"
                            )
                        renterHistoryDialog.show(supportFragmentManager, "") //S
                    }
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@DeliveryRentersActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@DeliveryRentersActivity)
            }
        }
    }

    private fun consumeSwitchOutReturnResponse(
        apiResponse:
        ApiResponse<BaseModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var data: BaseModel? = apiResponse.data as BaseModel
                if (data != null) {
                    showToast(
                        this@DeliveryRentersActivity,
                        "${data.message}",
                        data.success
                    )
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@DeliveryRentersActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@DeliveryRentersActivity)
            }
        }
    }


    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog(isCancelable = false)

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Delivery Renters"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener {
            setResult(RESULT_OK, intent);
            finish()
        })
    }

    fun setUpRecyclerview() {
        adapter = DeliveryRentersAdapter(this, allItemsNew, isSwitchOut) { from, position ->
            onItemClickListener(from, position)
        }
        binding.rvDetailsRenters.setHasFixedSize(true)
        val mLayoutManager1: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvDetailsRenters.layoutManager = (mLayoutManager1)
        binding.rvDetailsRenters.itemAnimator = (DefaultItemAnimator())
        binding.rvDetailsRenters.adapter = adapter
        binding.rvDetailsRenters.isNestedScrollingEnabled = false
    }

    fun onItemClickListener(from: String, position: Int) {
        var model = allItemsNew[position]
        when (from) {
            "Pack" -> {
                moveToPackingScreen(
                    model.id,
                    model.name
                )
            }
            "Fit" -> {
                moveToFittingScreen(model.id, model.name)
            }
            "Waiver" -> {
                showRenterDetailsFromWaiver(model)
            }
            "History" -> {
                viewModel.hitGetRenterHistory(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    model.id,
                    model.name.toString()

                )
            }
            "SwitchOut" -> {
                showSwitchOutReasonDialog(
                    "Switch Out Reason",
                    model.switch_out_reason

                )
            }
            "SwitchOutReturn" -> {
                viewModel.hitSwitchReturn(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    shipmentId
                )
            }
        }

    }

    fun showSwitchOutReasonDialog(title: String, description: String) {
        val switchOutReasonDialog: SwitchOutReasonDialog =
            SwitchOutReasonDialog.newInstance(
                description,
                title
            )
        switchOutReasonDialog.show(supportFragmentManager, "") //S
    }

    @SuppressLint("RestrictedApi")
    fun loadData(model: DeliveryRenterMainModel) {
        binding.run {
            if (model != null) {
                isSwitchOut = model.is_switch_out
                deliveryId = model.delivery_id
                if (model.confirmation_status) {
                    btnConfirmation.setBackgroundColor(
                        ContextCompat.getColor(
                            this@DeliveryRentersActivity,
                            R.color.green
                        )
                    )
                }
                if (!model.customer_phone.isNullOrEmpty() &&
                    !model.customer_phone.isNullOrBlank()
                ) {
                    ivCall.visibility = View.VISIBLE
                    rentersPhoneNumber.text = (model.customer_phone)
                } else {
                    ivCall.visibility = View.INVISIBLE
                }
                switchOutNotes = model.switch_out_notes

                btnSwNotes.visibility = (View.GONE)
                if (!switchOutNotes.isNullOrEmpty()) {
                    btnSwNotes.visibility = (View.VISIBLE)
                }
                if (model.reservation_notes_count > 0) {
                    renterNotesIv.supportBackgroundTintList = (
                            ContextCompat.getColorStateList(
                                this@DeliveryRentersActivity, R.color.red
                            ))
                }
                if (!model.renters.isNullOrEmpty()) {
                    allItemsNew.clear()
                    allItemsNew.addAll(model.renters)
                    Debugger.wtf("renters", "${model.renters}")
                }
                Debugger.wtf("renters", "Ok")
                //  adapter.notifyDataSetChanged()
                adapter.showSwitchOutItems(model.is_switch_out)
            } else {
                showToast(this@DeliveryRentersActivity, "Data not Found $selectedDate")
            }
        }
    }

    @Throws(ParseException::class)
    private fun reFormatDate(dateIn: String): String? {
        var simpleDateFormat =
            SimpleDateFormat("MMM dd, yyyy")
        val date = simpleDateFormat.parse(dateIn)
        simpleDateFormat = SimpleDateFormat("MM/dd/yy")
        Debugger.wtf("my_new_date", simpleDateFormat.format(date))
        return simpleDateFormat.format(date)
    }

    fun moveToNotesScreen() {
        val i = Intent(this, NotesActivity::class.java)
        i.putExtra("reservationId", reservationId)
        startActivity(i)
    }

    fun moveToConfirmationScreen() {
        val i = Intent(this, ConfirmationActivity::class.java)
        i.putExtra("reservationId", reservationId)
        startActivity(i)
    }

    fun moveToPackingScreen(renterId: Int, renterName: String?) {
        val intent = Intent(
            this,
            PackingActivity::class.java
        )
        intent.putExtra("renterId", renterId)
        intent.putExtra("shipmentId", shipmentId)
        intent.putExtra("renterName", renterName)
        startActivity(intent)
    }

    fun moveToFittingScreen(
        renterId: Int, renterName: String?
    ) {
        val i = Intent(this, FittingActivity::class.java)
        i.putExtra("renterId", renterId)
        i.putExtra("shipmentId", shipmentId)
        i.putExtra("renterName", renterName)
        startActivity(i)
    }

    fun showRenterDetailsFromWaiver(model: DeliveryRenterListModel) {
        Debugger.wtf("showRenterDetailsFromWaiver", "color " + model.fitted_color)
        if (model.fitted_color.contains("f05050") ||
            model.fitted_color.contains("ffbd4a")
        ) {
            showAlertForDin("Please confirm Din Setting first.")
        } else {
            val intent = Intent(
                this,
                WaiverRentersActivity::class.java
            )
            intent.putExtra("reservationId", reservationId)
            intent.putExtra("renter_id", model.id)
            intent.putExtra("fromFitting", "Yes")
            intent.putExtra("deliveryId", deliveryId)
            intent.putExtra("switchKeep", isSwitchOut)
            intent.putExtra("isDinVisible", true)
            startActivity(intent)
        }
    }

    fun showAlertForDin(description: String) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                description
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                commonDialog.dismiss()
            }
        })
    }

    override fun onBackPressed() {
        setResult(RESULT_OK, intent);
        super.onBackPressed()

    }
}