package com.app.btmobile.UI.Activity.Delivery

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.*
import android.widget.RadioButton
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.EventBusCallbacks.InvoiceConfirmationBusEvent
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.UI.Adapter.InvoiceRenterListAdapter
import com.app.btmobile.UI.Adapter.TipAdapter
import com.app.btmobile.UI.Dialog.SignatureDialog
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityInvoiceDetailsBinding
import com.ogoul.kalamtime.viewmodel.ConfirmationViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.connection.ConnectionEventListener
import com.pusher.client.connection.ConnectionState
import com.pusher.client.connection.ConnectionStateChange
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.util.*
import javax.inject.Inject


class InvoiceDetailsActivity : AppCompatActivity(), View.OnClickListener {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityInvoiceDetailsBinding
    lateinit var loaderDialog: LoaderDialog


    lateinit var adapterRenters: InvoiceRenterListAdapter
    var allItemsRenters: ArrayList<ConfirmationRenter> =
        ArrayList<ConfirmationRenter>()

    var reservationId = -1
    var invoice_id = -1
    var position = -1
    var selectedTip = ""
    var selectedTipindex = -1
    var isTipPerRenter: Boolean = true
    var signatureBitmap: Bitmap? = null
    var subTotalPrice = ""
    var amountSign = "$"
    var tipPercentageList: ArrayList<TipPercentage> =
        ArrayList<TipPercentage>()
    lateinit var tipAdapter: TipAdapter

    var tip_amount = ""
    lateinit var model: ConfirmationData
    lateinit var pusher: Pusher

    var api_key = ""
    var cluster = ""
    var channal_name = ""
    var event_name = ""


    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: ConfirmationViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInvoiceDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setupViewModel()
        setupToolbarAndLoader()
        setUpRecyclerview()
        connectToPusher()


        binding.tvConfirmSign.setOnClickListener(this)
        binding.btnAddNewCard.setOnClickListener(this)
        binding.tvCustomTip.setOnClickListener(this)
        binding.tvNoTip.setOnClickListener(this)

        binding.etOtherTip.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty() &&
                    s.toString().isNotBlank()
                ) {
                    binding.totalTipTv.text = "${amountSign}${s.toString()}.00"
                }else{
                    binding.totalTipTv.text = "${amountSign}0.00"
                }
            }
        })
    }

    fun getIntentData() {
        if (intent != null) {
            reservationId = intent.getIntExtra("reservationId", -1)
            invoice_id = intent.getIntExtra("invoiceId", -1)
            position = intent.getIntExtra("position", 0)
        }
    }


    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(ConfirmationViewModel::class.java)
        viewModel.getConfirmationData().observe(this, androidx.lifecycle.Observer {
            consumeRentersResponse(it)
        })
        viewModel.getSaveConfirmationData().observe(this, androidx.lifecycle.Observer {
            consumeSaveDataResponse(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun consumeSaveDataResponse(apiResponse: ApiResponse<BaseModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                binding.tvConfirmSuccessMsg.visibility = View.GONE
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var notesData: BaseModel = (apiResponse.data as BaseModel)
                if (!notesData.message.isNullOrEmpty()) {
                    showToast(this, notesData.message, notesData.success)
                    showToast(this, notesData.message, notesData.success)
                }
                if (notesData.success) {
                    binding.tvConfirmSuccessMsg.visibility = View.VISIBLE
                } else {
                    binding.tvConfirmSuccessMsg.visibility = View.GONE
                }
                object : CountDownTimer(500, 1) {
                    override fun onTick(millisUntilFinished: Long) {
                        binding.invoiceScrollView.fullScroll(View.FOCUS_DOWN)
                    }

                    override fun onFinish() {}
                }.start()
                clearValues()
                loaderDialog.dismissDialog()
                hitGetConfirmationDataApi()

            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@InvoiceDetailsActivity, apiResponse.error.toString()
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@InvoiceDetailsActivity)
            }
        }
    }

    fun clearValues() {
        binding.run {
            rgCard.clearCheck()
            rgCard.removeAllViews()

//        rgTips.clearCheck();
            selectedTip = ""
            signatureBitmap = null
            tvTipPercentage.text = ""
            tipLay.visibility = View.GONE
            etOtherTip.visibility = View.GONE
            etOtherTip.setText("")
            tip_amount = ""
            tipAdapter.clearTipSelection(-1)
            handleTipBottomButtons()
        }
    }

    fun handleTipBottomButtons(option: String = "all") {
        if (option == "other") {
            binding.tvCustomTip.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.selectedGray
                )
            )
        } else {
            binding.tvCustomTip.background = ContextCompat.getDrawable(
                this,
                R.drawable.border_rounded
            )
        }

        if (option == "no") {
            binding.tvNoTip.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    R.color.selectedGray
                )
            )
        } else {
            binding.tvNoTip.background = ContextCompat.getDrawable(
                this,
                R.drawable.border_rounded
            )
        }
    }


    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Invoice Details"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })
    }

    fun setUpRecyclerview() {
        adapterRenters = InvoiceRenterListAdapter(
            this,
            allItemsRenters
        )
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvConfirmation.layoutManager = (mLayoutManager)
        binding.rvConfirmation.itemAnimator = (DefaultItemAnimator())
        binding.rvConfirmation.adapter = adapterRenters
        binding.rvConfirmation.isNestedScrollingEnabled = false


        tipAdapter = TipAdapter(this, selectedTipindex)
        binding.rvTip.setHasFixedSize(true)
        val mLayoutManager1: RecyclerView.LayoutManager = GridLayoutManager(this, 4)
        binding.rvTip.layoutManager = mLayoutManager1
        binding.rvTip.itemAnimator = DefaultItemAnimator()
        binding.rvTip.adapter = tipAdapter
    }

    override fun onResume() {
        super.onResume()
        hitGetConfirmationDataApi()
    }

    fun hitGetConfirmationDataApi() {
        // clearValues()
        if (reservationId != -1) {
            viewModel.hitGetConfirmationData(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                reservationId
            )
        }
    }

    private fun consumeRentersResponse(
        apiResponse:
        ApiResponse<ConfirmationModel>
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var notesData: ConfirmationModel = (apiResponse.data as ConfirmationModel)
                if (notesData.success) {
                    allItemsRenters.clear()
                    loadData(notesData.data)
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@InvoiceDetailsActivity,
                    apiResponse.error.toString()
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@InvoiceDetailsActivity)
            }
        }
    }

    fun loadData(model: ConfirmationData) {
        this.model = model

        binding.run {
            var invoiceNewList = model.invoices
            for (i in invoiceNewList.indices) {
                if (invoiceNewList.get(i).invoice_id.toString()
                        .equals(invoice_id.toString(), true)
                ) {
                    if (model.invoices[i].sub_total != null &&
                        model.invoices[i].sub_total != "N/A"
                    ) {
                        subTotalPrice = model.invoices[i].sub_total
                    }
                    var isCadDollar = subTotalPrice.contains("CAD", true)
                    amountSign = if (isCadDollar) "CAD $" else "$"
                    allItemsRenters.addAll(invoiceNewList.get(i).renters)
                    if (model.hide_amount) {
                        totalPriceTv.text = "${amountSign}0.00"
                        totalTaxTv.text = "${amountSign}0.00"
                        tvSubTotal.text = "${amountSign}0.00"
                    } else {
                        totalPriceTv.text = model.invoices[i].invoice_total
                        totalTaxTv.text = model.invoices[i].invoice_total_tax.toString()
                        tvSubTotal.text = model.invoices[i].sub_total
                    }

                    if (model.invoices[i].confirmation_have_tip) {
                        if (!model.invoices[i].confirmation_tip.equals("\$0.00", true)) {
                            totalTipTv.text = model.invoices[i].confirmation_tip
                        } else {
                            totalTipTv.text = "No Tip"
                        }
                    } else {
                        if (!model.invoices[i].confirmation_tip.equals("\$0.00", true)) {
                            totalTipTv.text = model.invoices[i].confirmation_tip
                        }
                    }

                    if (model.invoices[position].customer_confirmation) {
                        tvConfirmSign.setBackgroundColor(
                            ContextCompat.getColor(
                                this@InvoiceDetailsActivity,
                                R.color.green
                            )
                        )
                    }
                    addCards(model.cards)
                    tipPercentageList.clear()
                    isTipPerRenter = model.tip_type.equals("per_renter", true)
                    model.tip_percentages.forEach {
                        getCalculatedTip(it, isTipPerRenter)
                    }

                    if (model.hide_amount) {
                        rvTip.visibility = View.GONE
                    } else {
                        rvTip.visibility = View.VISIBLE
                    }
                    adapterRenters.hideStatus(model.hide_amount)
                    // adapterRenters.notifyDataSetChanged()

                    Debugger.wtf("tip_type", "${model.tip_type} / $isTipPerRenter")
                    tipAdapter.showTipData(tipPercentageList, isTipPerRenter, amountSign)
                }
            }
        }
    }

    fun getCalculatedTip(model: TipPercentage, isTipPerRenter: Boolean) {
        try {
            var totalPrice = subTotalPrice
            if (totalPrice != "") {
                // totalPrice = totalPrice.substring(1, totalPrice.length() - 1);
                val separated = totalPrice.split("\\$").toTypedArray()
                Debugger.wtf("separated", "${separated.toString()}")
                if (separated.isNotEmpty()) {
                    totalPrice = separated[separated.size - 1]
                    totalPrice = totalPrice.replace(",", "")
                        .replace("$", "").replace("CAD", "")
                } else {
                    totalPrice = "0"
                }
                Debugger.wtf("separated", separated.size.toString() + " / " + totalPrice)
                var tipPercenp = java.lang.Double.valueOf(totalPrice) / 100
                if (tipPercenp > 0) {
                    tipPercenp *= model.tip.toDouble()
                }
                val tipSt = String.format("%.2f", tipPercenp)

                if (isTipPerRenter) {
                    if (allItemsRenters.size > 0) {
                        model.tipPercentage = String.format("%.2f",model.tip.toDouble() * allItemsRenters.size)
                    } else {
                        model.tipPercentage = String.format("%.2f",model.tip.toDouble())
                    }
                } else {
                    if (separated.size > 1) {
                        model.tipPercentage =
                            if (separated[0] == "") "$tipSt" else separated[0] + tipSt
                    } else {
                        model.tipPercentage = "$tipSt"
                    }
                }
                tipPercentageList.add(model)
//            tipAdapter.notifyDataSetChanged()
            }
        } catch (e: Exception) {
            Debugger.wtf("getCalculatedTip", "Exception : ${e.message}")
        }
    }


    fun addCards(cards: List<Card>?) {
        binding.run {
            if (cards != null) {
                rgCard.clearCheck()
                rgCard.removeAllViews()
                if (cards.isNotEmpty()) {
                    tvOr.visibility = View.VISIBLE
                    saveCardTv.visibility = View.GONE
                    rgCard.visibility = View.VISIBLE
                } else {
                    tvOr.visibility = View.GONE
                    saveCardTv.visibility = View.GONE
                    rgCard.visibility = View.GONE
                }
                for (cardModel in cards) {
                    val rdbtn = RadioButton(this@InvoiceDetailsActivity)
                    rdbtn.id = cardModel.id
                    rdbtn.text = "Use card on file " + cardModel.card
                    rdbtn.setOnClickListener(this@InvoiceDetailsActivity)
                    rdbtn.isChecked = cardModel.is_default == 1
//                    if (cardModel.is_default==1){
//                        rdbtn.isChecked=true
//                    }
                    rgCard.addView(rdbtn)

                }
            } else {
                tvOr.visibility = View.GONE
                saveCardTv.visibility = View.GONE
                rgCard.visibility = View.GONE
            }
        }
    }

    fun hitConfirmApi() {
        if (validateConfirmData()) {
            if (binding.etOtherTip.getVisibility() == View.VISIBLE) {
                tip_amount = binding.etOtherTip.getText().toString()
            }
            viewModel.hitSaveConfirmationData(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                reservationId,
                signatureData(
                    signatureBitmap!!,
                    binding.rgCard.checkedRadioButtonId.toString() + "",
                    selectedTip, tip_amount + "",
                    invoice_id.toString() + ""
                )
            )
        }
    }

    override fun onClick(v: View?) {
        binding.run {
            if (v === tvConfirmSign) {
                hitConfirmApi()
            } else if (v === btnAddNewCard) {
                startActivity(
                    Intent(this@InvoiceDetailsActivity, WebActivity::class.java)
                        .putExtra("reservationId", reservationId)
                )
            } else if (v === tvCustomTip) {
                totalTipTv.text = ""
                handleTipBottomButtons("other")
                tipAdapter.clearTipSelection(-1)
                tipSelectedListner(-1, "Other")
            } else if (v === tvNoTip) {
                handleTipBottomButtons("no")
                tipAdapter.clearTipSelection(-1)
                tipSelectedListner(-1, "None")
            }

        }

    }

    fun tipSelectedListner(checkedId: Int, fromOther: String = "") {
        binding.run {
            selectedTipindex = checkedId
            tipAdapter.notifyDataSetChanged()
            Debugger.wtf("onCheckedChanged", checkedId.toString() + " / " + selectedTipindex)
            selectedTip = checkedId.toString() + ""
            Debugger.wtf("selectedTip", checkedId.toString() + "")
            tip_amount = ""
            if (!fromOther.isNullOrEmpty()) {
                if (fromOther.equals("Other", true)) {
                    tvTipPercentage.text = ""
                    tipLay.visibility = View.GONE
                    etOtherTip.visibility = View.VISIBLE
                    selectedTip = "other"
                    tip_amount = etOtherTip.text.toString()
                    totalTipTv.text = ""
                } else if (fromOther.equals("None", true)) {
                    tvTipPercentage.text = ""
                    tipLay.visibility = View.GONE
                    etOtherTip.visibility = View.GONE
                    etOtherTip.setText("")
                    tip_amount = ""
                    selectedTip = "none"
                    totalTipTv.text = "No Tip"
                } else {

                }
            } else {
                handleTipBottomButtons()
                etOtherTip.visibility = View.GONE
                val tipPercent: Double = tipPercentageList[checkedId].tip.toDouble()
                selectedTip = tipPercent.toString() + ""
                tip_amount = tipPercentageList[checkedId].tipPercentage
                totalTipTv.text = "${amountSign}$tip_amount"

                Debugger.wtf("selectedTip", selectedTip + " / " + selectedTip)
                // tipAmount(tipPercent)
            }
        }
    }

    fun showSignatureDialog(signatureUrl: String) {
        val signatureDialog: SignatureDialog =
            SignatureDialog.newInstance(
                "title",
                signatureBitmap
            )
        signatureDialog.setOnContinueCancelClick(object : SignatureDialog.OnSignatureSaved {
            override fun onSignDone(signBitmap: Bitmap) {
                signatureBitmap = signBitmap
                binding.tvConfirmSign.setBackgroundColor(
                    ContextCompat.getColor(
                        this@InvoiceDetailsActivity,
                        R.color.green
                    )
                )
                hitConfirmApi()
            }
        })
        signatureDialog.show(supportFragmentManager, "") //S
    }

    fun validateConfirmData(): Boolean {
//        if (rgCard.getCheckedRadioButtonId()==-1){
//            Toast.makeText(mContext,"Please Select A Card",Toast.LENGTH_LONG).show();
//        }else
        binding.run {
            if (etOtherTip.getVisibility() == View.VISIBLE && etOtherTip.getText().toString()
                    .isEmpty()
            ) {
                showToast(
                    this@InvoiceDetailsActivity,
                    "Please Enter Tip Amount"
                )
                return false
            } else if (selectedTip === "") {
                showToast(
                    this@InvoiceDetailsActivity,
                    "Please complete Tip section before signing or choose no tip."
                )
                return false
            } else if (signatureBitmap == null) {
                showToast(
                    this@InvoiceDetailsActivity,
                    "Please Write Your Signature"
                )
                showSignatureDialog("")
                return false
            }
        }
        return true
    }

    private fun signatureData(
        signatureBitmap: Bitmap,
        customer_payment_profile_id: String,
        tip: String, tip_amount: String,
        invoice_id: String
    ): RequestBody {

        val builder: MultipartBody.Builder = MultipartBody.Builder().setType(MultipartBody.FORM)
        val byteArrayOutputStream = ByteArrayOutputStream()
        signatureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
        if (binding.rgCard.checkedRadioButtonId != -1) {
            builder.addFormDataPart("customer_payment_profile_id", customer_payment_profile_id)
        }
        builder
            .addFormDataPart("tip", tip)
            .addFormDataPart("tip_amount", tip_amount)
            .addFormDataPart("invoice_id", invoice_id)
            .addFormDataPart("customer_signature", encoded)
        Debugger.wtf("Data", "->Tip: $tip")
        Debugger.wtf("Data2", "->Tip Amount: $tip_amount")
        return builder.build()
    }

    fun connectToPusher() {

        //Live App Credentials
        api_key = "ad332a821da1fbdf158f"
        cluster = "mt1"
        channal_name = "customer-confirmation-${sharedPrefsHelper.getUser()?.id.toString()}"
        event_name = "confirmation-done"

        //My Testing Project
//        api_key = "6568425b0481e8ecb538"
//        cluster = "ap4"
//        channal_name = "my-channel-${
//            sharedPrefsHelper.getUser()?.id.toString()
//        }"
//        event_name = "my-event"
        Debugger.wtf("channal_name", "$channal_name")

        val options = PusherOptions()
        options.setCluster(cluster);
        pusher = Pusher(api_key, options)
        pusher.connect(object : ConnectionEventListener {

            override fun onConnectionStateChange(change: ConnectionStateChange?) {
                Debugger.wtf(
                    "Pusher",
                    "State changed from ${change?.previousState} to ${change?.currentState}"
                )
            }

            override fun onError(message: String?, code: String?, e: java.lang.Exception?) {
                Debugger.wtf(
                    "Pusher",
                    "There was a problem connecting! code (${code.toString()}), " +
                            "message (${message.toString()}), exception(${e?.message})"
                )
            }
        }, ConnectionState.ALL)

        val channel = pusher.subscribe(channal_name)
        channel.bind(event_name) { event ->
            runOnUiThread {
                hitGetConfirmationDataApi()
            }
            Debugger.wtf("Pusher", "Received event with data: $event")
            //NOTIFICATION
            val data = event.data
            var msg = ""
            try {
                val jsonObject = JSONObject(data)
                msg = jsonObject.getString("msg")

                val mBuilder: NotificationCompat.Builder
                val notificationType = "Tag Notifications"

                val mNotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    setupNotificationChannels(mNotificationManager, notificationType)
                }
                val defaultSoundUri =
                    RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                mBuilder = NotificationCompat.Builder(this, notificationType)
                    .setContentTitle(title)
                    .setSmallIcon(R.drawable.icon)
                    .setContentText(msg)
                    .setColor(Color.parseColor("#337ab7"))
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSound(defaultSoundUri)
                mNotificationManager.notify(
                    System.currentTimeMillis().toInt(),
                    mBuilder.build()
                )

            } catch (e: Exception) {
                Debugger.wtf("Exception", "Exception : ${e.message}")
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun setupNotificationChannels(notification: NotificationManager, type: String) {
        val adminChannelDescription = "BTMobile Notifications"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val audioAttributes = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()
        val adminChannel = NotificationChannel(
            type,
            type,
            NotificationManager.IMPORTANCE_DEFAULT
        )
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        adminChannel.setSound(defaultSoundUri, audioAttributes)
        notification.createNotificationChannel(adminChannel)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this::pusher.isInitialized) {
            pusher.disconnect()
        }
    }


//    @Subscribe(threadMode = ThreadMode.MAIN)
//    fun onMessageEvent(event: InvoiceConfirmationBusEvent) {
//        hitGetConfirmationDataApi()
//    }
//
//    override fun onStart() {
//        super.onStart()
//        EventBus.getDefault().register(this)
//    }
//
//    override fun onStop() {
//        super.onStop()
//        EventBus.getDefault().unregister(this)
//    }

}

