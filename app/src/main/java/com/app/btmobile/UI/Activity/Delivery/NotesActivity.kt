package com.app.btmobile.UI.Activity.Delivery

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.Status
import com.app.btmobile.UI.Adapter.NotesAdapter
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityNotesBinding
import com.ogoul.kalamtime.viewmodel.NotesViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.util.*
import javax.inject.Inject

class NotesActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityNotesBinding
    lateinit var loaderDialog: LoaderDialog

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: NotesViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    lateinit var adapter: NotesAdapter
    var allItems: ArrayList<NoteData> =
        ArrayList<NoteData>()
    var reservationId = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getIntentData()
        setUpViewModel()
        setupToolBarAndLoader();
        setUpRecyclerview()

        binding.btnSendNotes.setOnClickListener {
            if (binding.edNotes.text.isNotBlank() &&
                binding.edNotes.text.isNotBlank()
            ) {
                val map = HashMap<String, String>()
                map["notes"] = binding.edNotes.text.toString()
                viewModel.hitAddNotes(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    reservationId,
                    map
                )
            }
        }
    }

    fun getIntentData() {
        if (intent != null) {
            reservationId = intent.getIntExtra("reservationId", -1)
        }
    }

    fun setUpViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(NotesViewModel::class.java)
        viewModel.getNotesListResponse().observe(this, Observer {
            consumeNotesList(it)
        })

        viewModel.addNotesResponse().observe(this, Observer {
            consumeAddNotes(it)
        })
    }


    fun setupToolBarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Nfc Scan"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })

    }

    fun setUpRecyclerview() {
        adapter = NotesAdapter(this, allItems)
        binding.rvNotes.setHasFixedSize(true)
        val mLayoutManager1: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvNotes.layoutManager = (mLayoutManager1)
        binding.rvNotes.itemAnimator = (DefaultItemAnimator())
        binding.rvNotes.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        hitFetchNotesApi()
    }

    fun hitFetchNotesApi() {
        if (reservationId != -1) {
            viewModel.hitGetNotesList(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                reservationId
            )
        }
    }

    private fun consumeNotesList(apiResponse: ApiResponse<NotesListModel>) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var notesData: NotesListModel = (apiResponse.data as NotesListModel)
                if (notesData.success) {
                    allItems.clear()
                    if (notesData.data.isNotEmpty()) {
                        allItems.addAll(notesData.data)
                        binding.noNotesYetTv.visibility = View.GONE
                    } else {
                        binding.noNotesYetTv.visibility = View.VISIBLE
                    }
                    adapter.notifyDataSetChanged()
                    if (allItems.size > 0) {
                        binding.rvNotes.scrollToPosition(allItems.size - 1)
                    }
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@NotesActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@NotesActivity)
            }
        }
    }

    private fun consumeAddNotes(apiResponse: ApiResponse<BaseModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var notesData: BaseModel = (apiResponse.data as BaseModel)
                if (notesData.success) {
                    showToast(
                        this@NotesActivity,
                        "${notesData.message.toString()}",
                        notesData.success
                    )
                }
                loaderDialog.dismissDialog()
                hitFetchNotesApi()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@NotesActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@NotesActivity)
            }
        }
    }

}