package com.app.btmobile.UI.Activity.Delivery

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.app.btmobile.Models.ReturnInventory
import com.app.btmobile.R
import com.app.btmobile.UI.Dialog.CommonDialog
import com.app.btmobile.Utils.AppConstants
import com.app.btmobile.Utils.Debugger

class WebActivity : AppCompatActivity() {
    var webView: WebView? = null
    var progressBar: ProgressBar? = null
    var reservationId = 0
    var webLint = "payment-profile/"
    var context: Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        context = this
        webView = findViewById<View>(R.id.webview) as WebView
        progressBar = findViewById<View>(R.id.progress) as ProgressBar
        reservationId = intent.getIntExtra("reservationId", -1)
        webLint = AppConstants.BASE_URL + webLint + reservationId
        Debugger.wtf("webLint", webLint)
        webView!!.webChromeClient = MyWebChromeClient()
        webView!!.webViewClient = webClient()
        webView!!.settings.loadWithOverviewMode = true
        webView!!.settings.setSupportZoom(true)
        webView!!.settings.javaScriptEnabled = true
        webView!!.addJavascriptInterface(
            WebAppInterface(), "Android"
        )
        webView!!.loadUrl(webLint)
    }

    inner class MyWebChromeClient : WebChromeClient() {
        override fun onProgressChanged(view: WebView, newProgress: Int) {
            progressBar!!.visibility = View.VISIBLE
            progressBar!!.progress = newProgress
        }
    }

    inner class webClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView,
            url: String
        ): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            progressBar!!.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        if (webView!!.canGoBack()) {
            webView!!.goBack()
            progressBar!!.visibility = View.GONE
        } else {
            super.onBackPressed()
        }
    }

    inner class WebAppInterface() {
        @JavascriptInterface
        fun showToast(msg: String?) {
            if (!msg.toString().equals("close", true)) {
                showAlert(msg.toString())
                // com.app.btmobile.Utils.showToast(context, msg.toString())
            }else {
                finish()
            }
        }

    }

    fun showAlert(msg: String) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "Thank You",
                isOnlyOkButtonVisible = true,
                isDialogCancelable = false
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                finish()
            }

        })
    }
}