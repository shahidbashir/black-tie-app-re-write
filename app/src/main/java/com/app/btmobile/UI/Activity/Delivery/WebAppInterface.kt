package com.app.btmobile.UI.Activity.Delivery

import android.app.Activity
import android.webkit.JavascriptInterface

class WebAppInterface(private val context: Activity) {
    @JavascriptInterface
    fun showToast(msg: String?) {
        if (!msg.toString().equals("close", true)) {
            com.app.btmobile.Utils.showToast(context, msg.toString())
        }
        context.finish()
    }


}