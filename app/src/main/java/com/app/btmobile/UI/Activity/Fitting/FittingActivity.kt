package com.app.btmobile.UI.Activity.Fitting

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.nfc.NfcAdapter
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.app.btmobile.*
import com.app.btmobile.Models.*
import com.app.btmobile.UI.Activity.Waiver.WaiverRentersActivity
import com.app.btmobile.UI.Adapter.PackingSkiBootsAdapter
import com.app.btmobile.UI.Adapter.PreviousBootDataAdapter
import com.app.btmobile.UI.Adapter.PreviousSkiDataAdapter
import com.app.btmobile.UI.Dialog.*
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityFittingBinding
import com.google.gson.Gson
import com.ogoul.kalamtime.viewmodel.DeliveryViewModel
import com.ogoul.kalamtime.viewmodel.PackingViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FittingActivity : AppCompatActivity(), View.OnClickListener {
    var pendingIntent: PendingIntent? = null
    var nfcAdapter: NfcAdapter? = null
    lateinit var nfcReaderUtils: NfcReaderUtils

    private val TAG = this.javaClass.simpleName

    lateinit var binding: ActivityFittingBinding
    lateinit var loaderDialog: LoaderDialog

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var packingViewModel: PackingViewModel
    lateinit var deliveryViewModel: DeliveryViewModel


    var confirmOwnBoot = false

    var renter_id = 0
    var shipment_id: Int = 0
    var renterName: String = ""

    var switchOutNotes = ""
    var switchOutReason = ""

    var isSwitchOut = false
    var isFirstTime = true

    var dinSetting = ""
    var reservation_id = -1
    var dob = ""
    var inventoryType = ""
    var isInventorySki = false
    var isDinGreen = false
    var switchKeep: Boolean = false
    var isDinVisible: Boolean = true
    private var deliveryId = 0


    var clothingListFinal: MutableList<ClothingModel> = ArrayList()
    val spinnerArray: MutableList<String> = ArrayList()

    lateinit var spinnerArrayAdapter: ArrayAdapter<String>


    var allItems1: ArrayList<PackedInventoryModel> = ArrayList()
    var allItems2: ArrayList<PackedInventoryModel> = ArrayList()
    var allItems3: ArrayList<PackedInventoryModel> = ArrayList()
    var allItems4: ArrayList<PackedInventoryModel> = ArrayList()
    var allItems5: ArrayList<PackedInventoryModel> = ArrayList()
    var allItems6: ArrayList<PackedInventoryModel> = ArrayList()

    var existingSkiList: ArrayList<ExistingBootSkis> = ArrayList()
    var existingBootList: ArrayList<ExistingBootSkis> = ArrayList<ExistingBootSkis>()

    lateinit var adapter1: PackingSkiBootsAdapter
    lateinit var adapter2: PackingSkiBootsAdapter
    lateinit var adapter3: PackingSkiBootsAdapter
    lateinit var adapter4: PackingSkiBootsAdapter
    lateinit var adapter5: PackingSkiBootsAdapter
    lateinit var adapter6: PackingSkiBootsAdapter
    lateinit var previousSkiDataAdapter: PreviousSkiDataAdapter
    lateinit var previousBootDataAdapter: PreviousBootDataAdapter

    lateinit var packingData: PackingData
    lateinit var getAbilityShoeSizeHeightListModel: GetAbilityShoeSizeHeightListModel

    var fitAddonWithBarcodeDialog: FitAddonWithBarcodeDialog =
        FitAddonWithBarcodeDialog.newInstance()
    lateinit var fitBootSkiInventoryDialog: FitBootSkiInventoryDialog
    lateinit var showFitAddonsListDialog: ShowFitAddonsListDialog

    fun setUpClickListeners() {

        binding.tvAddBootManually.setOnClickListener(this)
        binding.btnAddOn.setOnClickListener(this)
        binding.addSkiBootBigBtn.setOnClickListener(this)
        binding.addSkiBigBtn.setOnClickListener(this)
        binding.addHelmetBigBtn.setOnClickListener(this)

        binding.addSkiBootSmallBtn.setOnClickListener(this)
        binding.addSkiSmallBtn.setOnClickListener(this)
        binding.addHelmetSmallBtn.setOnClickListener(this)
        binding.viewHistoryTv.setOnClickListener(this)
        binding.btnDin.setOnClickListener(this)
        binding.tvPolePackStatus.setOnClickListener(this)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.fitting_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.goToWaiver -> {
                if (isDinGreen) {
                    moveToWaiverPage()
                } else {
                    if (inventoryType.equals("ski", ignoreCase = true) ||
                        inventoryType.equals("skier", ignoreCase = true)
                    ) {
                        showAlert("Please confirm Din Setting first.")

                    } else {
                        moveToWaiverPage()
                    }
                }
            }
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
            R.id.editRenterDetails -> {
                updateRenterDataDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun moveToWaiverPage() {
        if (reservation_id != -1) {
            val intent = Intent(
                this@FittingActivity,
                WaiverRentersActivity::class.java
            )
            intent.putExtra("reservationId", reservation_id)
            intent.putExtra("renter_id", renter_id)
            intent.putExtra("switchKeep", switchKeep)
            intent.putExtra("isDinVisible", isDinVisible)
//            if (switchKeep) {
//                intent.putExtra("deliveryId", deliveryId);
//            }
            intent.putExtra("deliveryId", deliveryId)
            intent.putExtra("fromFitting", "Yes")
            startActivity(intent)
        }
    }

    fun showAlert(description: String) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                description
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                commonDialog.dismiss()
            }
        })
    }

    fun showForceToFitSkiAndBootDialog(isForBoot: Boolean, parameters: HashMap<String, String>) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "This item is not available. You still want to issue?",
                "Yes", "No",
                isOnlyOkButtonVisible = false
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                parameters["force_back"] = "1"
                hitApiForFitBootAndSkis(isForBoot, parameters)
                commonDialog.dismiss()
            }
        })
    }

    fun fitPoleAlert() {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "Are you sure you want to fit this \n' " + binding.spinnerPolePack.selectedItem.toString() + " ' pole?",
                isOnlyOkButtonVisible = false,
                negativeButtonText = "Cancel",
                positiveButtonText = "Yes"
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                hitAddPoleApi(binding.spinnerPolePack.selectedItem.toString())
            }

        })
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFittingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setUpViewModel()
        initialseSpinner()
        setupToolBarAndLoader()
        setUpRecyclerViews()
        setUpClickListeners()
        loadShoeSizeHeightAbilityList()
        nfcBarcodeInitialise()

        binding.ownBootCB.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            confirmOwnBoot = b
            val map = HashMap<String, String>()
            map["renter_id"] = renter_id.toString()
            map["bring_boot"] = if (b) "1" else "0"
            packingViewModel.hitOwnBootApi(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                map
            )
        })
    }


    fun showFitAddonsSizeManuallyDialog(addonStatus: String, addonId: String) {
        var addAddonWithBarcodeDialog =
            AddAddonSizeManuallyDialog.newInstance(addonStatus, addonId)
        addAddonWithBarcodeDialog.show(supportFragmentManager, "")
        addAddonWithBarcodeDialog.setOnSubmitListener(object :
            AddAddonSizeManuallyDialog.OnScanClick {
            override fun clickOnSaveButton(
                addonStatus: String,
                addonId: String,
                sizeType: String,
                size: String,
            ) {
                saveRentersAddOnsWithVolley(addonStatus, addonId, sizeType, size)
                addAddonWithBarcodeDialog.dismiss()
            }

            override fun onDismiss() {
                addAddonWithBarcodeDialog.dismiss()
            }
        })


    }

    fun saveRentersAddOnsWithVolley(
        addonStatus: String,
        addonId: String,
        sizeType: String,
        size: String,
    ) {
        loaderDialog.showDialog()
        val lgoinRequest: StringRequest =
            object : StringRequest(
                Method.POST,
                Urls.SAVE_ADD_ONS_LIST_LINK_VOLLEY,
                Response.Listener { response ->
                    var mainObj: JSONObject? = null
                    try {
                        Debugger.wtf("saveRentersAddOnsWithVolley", response)
                        mainObj = JSONObject(response)
                        var model: SaveAddonWithBarcodeModel = Gson().fromJson(
                            mainObj.toString(),
                            SaveAddonWithBarcodeModel::class.java
                        )
                        if (model.status) {
                            if (showFitAddonsListDialog != null) {
                                if (showFitAddonsListDialog.isVisible) {
                                    showFitAddonsListDialog.checkAddonItem(
                                        model.addon_id,
                                        model.addon_status,
                                        model.helmet_size,
                                        if (model.is_manual) 1 else 0
                                    )
                                }
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Debugger.wtf(
                            "saveRentersAddOnsWithVolley",
                            "Try Catch" + e.message
                        )
                    } finally {
                        loaderDialog.dismissDialog()
                        getPackingData()
                    }
                },
                Response.ErrorListener { error ->
                    Debugger.wtf(
                        "saveRentersAddOnsWithVolley",
                        "Failer " + error.message
                    )
                    loaderDialog.dismissDialog()
                }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val params: MutableMap<String, String> =
                        java.util.HashMap()
                    params["Authorization"] =
                        "bearer ${sharedPrefsHelper.getUser()?.accessToken.toString()}"
                    return params
                }

                @Throws(AuthFailureError::class)
                override fun getParams(): Map<String, String> {
                    val params =
                        java.util.HashMap<String, String>()
                    params["renter_id"] = renter_id.toString()
                    params["manual_inventory[addon_id]"] = addonId
                    params["status"] = "Fitted"
                    if (!sizeType.contains("Select", true)) {
                        params["manual_inventory[size_type]"] = sizeType
                    }
                    if (!size.contains("Select", true)) {
                        params["manual_inventory[size]"] = size
                    }

                    Debugger.wtf("packAddons4", "params$params")
                    return params
                }
            }
        lgoinRequest.retryPolicy = DefaultRetryPolicy(
            600000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        VolleySingleton.getInstance(this)?.addToRequestQueue(lgoinRequest)
        VolleySingleton.getInstance(this)?.requestQueue?.getCache()?.clear()
    }

    fun initilizaBarCodeScanDialog(
        addOnsStatus: String = "",
        addonId: String = "",
        addonName: String,
        addonSize: String,
        is_manual: Int = 0
    ) {
        fitAddonWithBarcodeDialog =
            FitAddonWithBarcodeDialog.newInstance(
                addOnsStatus, addonId,
                addonName, addonSize, is_manual
            )
        fitAddonWithBarcodeDialog.show(supportFragmentManager, "") //Show t
        fitAddonWithBarcodeDialog.setOnSubmitListener(object :
            FitAddonWithBarcodeDialog.OnScanClick {
            override fun onScanBarcode(barcode: String) {
                if (fitAddonWithBarcodeDialog != null) {
                    if (fitAddonWithBarcodeDialog.isVisible) {
                        Debugger.wtf(
                            "nfcBarcode",
                            "barcode"
                        )
                        hitSaveAddonWithBarcode(
                            barcode,
                            fitAddonWithBarcodeDialog.addonStatus
                        )

                        fitAddonWithBarcodeDialog.dismiss()
                    }
                }
            }

            override fun clickOnAddSizeManuallyBtn(addonId: String) {
                showFitAddonsSizeManuallyDialog(addOnsStatus, addonId)
                fitAddonWithBarcodeDialog.dismiss()
            }

            override fun clickOnUndoAddon(addonId: String) {
                hitUndoAddonWithoutBarcode(addonId)
                fitAddonWithBarcodeDialog.dismiss()
            }

            override fun onDismiss() {
                Debugger.wtf("addScanBarcodeDialog", "onDismiss")
                //  stopNfcScan()
            }
        })
    }

    fun hitSaveAddonWithBarcode(barcode: String, addOnStatus: String) {
        val map = HashMap<String, String>()
        map["renter_id"] = renter_id.toString() + ""
        map["barcode"] = barcode
        map["status"] = addOnStatus
        if (addOnStatus.equals("Returned", true)) {
            map["van_shipment_id"] = shipment_id.toString()
        }

        packingViewModel.hitSaveAddonsWithBarcodeApi(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            map
        )
    }

    fun hitUndoAddonWithoutBarcode(addonId: String) {
        val map = HashMap<String, String>()
        map["renter_id"] = renter_id.toString() + ""
        map["addon_id"] = addonId

        map["status"] = "Packed"
        map["is_manual"] = "1"
        packingViewModel.hitSaveAddonsWithBarcodeApi(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            map
        )
    }

    fun nfcBarcodeInitialise() {
        pendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcReaderUtils = NfcReaderUtils()
        nfcReaderUtils.setOnNfcScanListener(object : NfcReaderUtils.OnNfcScanListener {
            override fun nfcBarcode(nfcCode: String) {
                Debugger.wtf("nfcBarcode", "In $nfcCode")
                if (this@FittingActivity::fitBootSkiInventoryDialog.isInitialized) {
                    Debugger.wtf("nfcBarcode", "1 $nfcCode")
                    if (fitBootSkiInventoryDialog.isVisible) {
                        hitSaveFitBootSkisByBarcode(
                            nfcCode,
                            fitBootSkiInventoryDialog.getKeepSwitchOut(),
                            fitBootSkiInventoryDialog.isBootAdded
                        )
                        fitBootSkiInventoryDialog.dismiss()
                    }
                }

                if (fitAddonWithBarcodeDialog != null) {
                    if (fitAddonWithBarcodeDialog.isVisible) {
                        Debugger.wtf("nfcBarcode", "2 $nfcCode")
                        hitSaveAddonWithBarcode(nfcCode, fitAddonWithBarcodeDialog.addonStatus)
                        fitAddonWithBarcodeDialog.dismiss()
                    }
                }
            }
        })

        if (nfcAdapter != null) {
            nfcAdapter?.let { nfc ->
                nfc.isEnabled?.let { enable ->
                    if (!enable) {
                        openNfcSettings(this)
                    }
                }
            }

        } else {
            showToast(this, "There is no NFC Functionality in this device")
        }
    }

    fun loadShoeSizeHeightAbilityList() {
        packingViewModel.hitGetAbilityHeightShowSizeData(
            sharedPrefsHelper.getUser()?.accessToken.toString()
        )
    }

    fun initialseSpinner() {
        binding.run {

//        final List<String> spinnerArray = new ArrayList<>();
            spinnerArray.add("Select")

            for (i in 30..54 step 2) {
                spinnerArray.add(i.toString())
            }

            spinnerArrayAdapter = ArrayAdapter<String>(
                this@FittingActivity, android.R.layout.simple_spinner_item,
                spinnerArray
            )

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerPolePack.adapter = spinnerArrayAdapter
            spinnerPolePack.addOnAttachStateChangeListener(object :
                View.OnAttachStateChangeListener {
                override fun onViewAttachedToWindow(view: View) {}
                override fun onViewDetachedFromWindow(view: View) {}
            })

            spinnerPolePack.onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    view: View,
                    i: Int,
                    l: Long
                ) {
                    if (spinnerPolePack.selectedItem.toString() != "Select") {
                        if (!isFirstTime) {
                            fitPoleAlert()
                        }
                        isFirstTime = false
                    }
                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {}
            }
        }
    }

    fun getIntentData() {
        if (intent != null) {
            renter_id = intent.getIntExtra("renterId", 0)
            shipment_id = intent.getIntExtra("shipmentId", 0)
            renterName = intent.getStringExtra("renterName")!!
        }
    }


    fun setUpViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        packingViewModel = ViewModelProvider(this, factory).get(PackingViewModel::class.java)
        deliveryViewModel = ViewModelProvider(this, factory).get(DeliveryViewModel::class.java)
        packingViewModel.renterPackFitModel().observe(this, Observer {
            consumePackingData(it)
        })
        packingViewModel.ownBootResponse().observe(this, Observer {
            consumeOwnBootResponse(it)
        })
        deliveryViewModel.getRenterHistoryResponse().observe(this, androidx.lifecycle.Observer {
            consumeRenterHistoryResponse(it)
        })
        packingViewModel.addPolesResponse().observe(this, Observer {
            consumeAddPolesResponse(it)
        })
        packingViewModel.getSaveFitBootAndSkiByBarcode().observe(this, Observer {
            consumeSaveFitBootAndSkisByBarcode(it)
        })
        packingViewModel.getAbilityShoeSizeHeightListResponse().observe(this, Observer {
            consumegetAbilityShoeSizeHeightListResponse(it)
        })
        packingViewModel.getGetFittingAddonsModelResponse().observe(this, Observer {
            consumeGetFittingAddonsModelResponse(it)
        })
        packingViewModel.updateDinSettingResponse().observe(this, Observer {
            consumeUpdateDinSettingResponse(it)
        })

        packingViewModel.getResponseSaveAddonsWithBarcode().observe(this, Observer {
            consumeSaveAddonsWithBarcode(it)
        })

    }

    private fun consumeSaveAddonsWithBarcode(apiResponse: ApiResponse<SaveAddonWithBarcodeModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeSaveAddonsWithBarcode",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )
                var model: SaveAddonWithBarcodeModel =
                    (apiResponse.data as SaveAddonWithBarcodeModel)
                if (model.status) {
                    if (showFitAddonsListDialog != null) {
                        if (showFitAddonsListDialog.isVisible) {
                            showFitAddonsListDialog.checkAddonItem(
                                model.addon_id,
                                model.addon_status,
                                model.helmet_size,
                                if (model.is_manual) 1 else 0
                            )
                        }
                    }
                }
                showToast(this, model.message, model.status)
                loaderDialog.dismissDialog()
                getPackingData()

            }
            Status.ERROR -> {
                Debugger.wtf(
                    TAG, "consumeResponse ERROR: " + apiResponse.error.toString()
                )
                loaderDialog.dismissDialog()

                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@FittingActivity)
            }
        }

    }


    private fun consumeUpdateDinSettingResponse(apiResponse: ApiResponse<BaseModel>) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                try {
                    var data: BaseModel? = apiResponse.data as BaseModel
                    if (data != null) {
                        if (data.success) {
                            binding.btnDin.getBackground().setColorFilter(
                                Color.parseColor("#2dd57b"),
                                PorterDuff.Mode.SRC_ATOP
                            )
                            isDinGreen = true
                            getPackingData()
                        } else {
                            isDinGreen = false
                        }
                        if (data.message != null) {
                            showToast(this, data.message, data.success)
                        }
                    }
                } catch (e: Exception) {
                    Debugger.wtf(TAG, "Erorrr-------------> : ${e.message}")
                } finally {
                    loaderDialog.dismissDialog()
                    getPackingData()
                }


            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@FittingActivity)
            }
        }
    }

    private fun consumeGetFittingAddonsModelResponse(apiResponse: ApiResponse<GetFittingAddonsModel>) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeGetFittingAddonsModelResponse",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )
                var data: GetFittingAddonsModel? = apiResponse.data as GetFittingAddonsModel
                if (data != null) {
                    var onlyPackAddonsBarcode: List<FittingAddon> =
                        data.data.addonsWithBarcodeList.filter {
                            it.isAddonWithBarcode
                        }
                    val allAddons: ArrayList<FittingAddon> = ArrayList()
                    allAddons.addAll(data.data.addons)
                    allAddons.addAll(onlyPackAddonsBarcode)

                    showFitAddOnsListDialog(
                        allAddons,
                        data.data.fittedClothingList
                    )

                    loaderDialog.dismissDialog()
                }
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@FittingActivity)
            }
        }
    }

    private fun consumegetAbilityShoeSizeHeightListResponse(apiResponse: ApiResponse<GetAbilityShoeSizeHeightListModel>) {
        when (apiResponse.status) {
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumegetAbilityShoeSizeHeightListResponse",
                    "consumegetAbilityShoeSizeHeightListResponse SUCCESS : ${apiResponse.data}"
                )
                var data: GetAbilityShoeSizeHeightListModel? =
                    apiResponse.data as GetAbilityShoeSizeHeightListModel
                if (data != null) {
                    getAbilityShoeSizeHeightListModel = data
                }
            }
            Status.ERROR -> {
                Debugger.wtf(
                    "consumegetAbilityShoeSizeHeightListResponse",
                    "consumeResponse ERROR: " + apiResponse.error.toString()
                )
                loaderDialog.dismissDialog()
                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                Debugger.wtf("consumegetAbilityShoeSizeHeightListResponse", "else")
                // loaderDialog.dismissDialog()
                //  logout(this@FittingActivity)
            }
        }

    }

    private fun consumePackingData(apiResponse: ApiResponse<PackingModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var model: PackingModel = (apiResponse.data as PackingModel)
                if (model.packingData.isNotEmpty()) {
                    deliveryId = model.packingData.first().deliveryId
                    packingData = model.packingData.first()
                    loadData(packingData)
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(
                    TAG, "consumeResponse ERROR: " + apiResponse.error.toString()
                )
                loaderDialog.dismissDialog()

                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@FittingActivity)
            }
        }

    }

    private fun consumeRenterHistoryResponse(
        apiResponse:
        ApiResponse<RenterHistoryModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var data: RenterHistoryModel? = apiResponse.data as RenterHistoryModel
                if (data != null) {
                    if (data.success) {
                        val renterHistoryDialog: RenterHistoryDialog =
                            RenterHistoryDialog.newInstance(
                                data.data.boots_history.toString() + "\n \n" +
                                        data.data.skis_history,
                                data.data.renterName.toString() + "'s History"
                            )
                        renterHistoryDialog.show(supportFragmentManager, "") //S
                    }
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@FittingActivity)
            }
        }
    }

    private fun consumeOwnBootResponse(
        apiResponse:
        ApiResponse<OwnBootModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                loaderDialog.dismissDialog()
                getPackingData()

            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@FittingActivity)
            }
        }
    }

    private fun consumeAddPolesResponse(
        apiResponse:
        ApiResponse<BaseModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                try {
                    var data: BaseModel? = apiResponse.data as BaseModel
                    if (data != null) {
                        if (data.message != null) {
                            showToast(this, data.message, data.success)
                        }
                    }
                } catch (e: Exception) {
                    Debugger.wtf(TAG, "Erorrr-------------> : ${e.message}")
                } finally {
                    loaderDialog.dismissDialog()
                    getPackingData()
                }


            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@FittingActivity)
            }
        }
    }


    private fun consumeSaveFitBootAndSkisByBarcode(
        apiResponse:
        ApiResponse<SaveFitBootAndSkisByBarcodeModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                try {
                    var data: SaveFitBootAndSkisByBarcodeModel? =
                        apiResponse.data as SaveFitBootAndSkisByBarcodeModel
                    if (data != null) {
                        if (data.message != null) {
//                            if (data.message.equals("Equipment not available", true)) {
                            if (data.message.equals(
                                    "This equipment is fitted to another renter.",
                                    true
                                )
                            ) {
                                showForceToFitSkiAndBootDialog(data.isForBoot, data.parameters)
                            } else {
                                showToast(this, data.message, data.success)
                                getPackingData()
                            }
                        }
                    }
                } catch (e: Exception) {
                    Debugger.wtf(TAG, "Erorrr-------------> : ${e.message}")
                } finally {
                    loaderDialog.dismissDialog()
                }


            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@FittingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@FittingActivity)
            }
        }
    }

    fun setupToolBarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = renterName
        supportActionBar!!.subtitle = "Renter ID: $renter_id"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })

    }

    fun getPackingData() {
        packingViewModel.hitRenterPackFitData(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            shipment_id,
            renter_id
        )
    }

    fun hitAddPoleApi(poles: String) {
        val map = HashMap<String, String>()
        map["poles"] = poles
        map["renter_id"] = renter_id.toString()
        packingViewModel.hitAddPoles(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            renter_id,
            map
        )
    }

    fun hitGetAddOnsApi() {
        packingViewModel.hitGetAddonsList(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            renter_id
        )
    }

    fun setUpRecyclerViews() {
        binding.run {
            adapter1 =
                PackingSkiBootsAdapter(this@FittingActivity, allItems1, false, false, true)
            val mLayoutManager1: RecyclerView.LayoutManager =
                LinearLayoutManager(this@FittingActivity)
            rvPackedBoot.setLayoutManager(mLayoutManager1)
            rvPackedBoot.setItemAnimator(DefaultItemAnimator())
            rvPackedBoot.setAdapter(adapter1)
            adapter2 =
                PackingSkiBootsAdapter(this@FittingActivity, allItems2, false, false, true)
            val mLayoutManager2: RecyclerView.LayoutManager =
                LinearLayoutManager(this@FittingActivity)
            rvPackedSki.setLayoutManager(mLayoutManager2)
            rvPackedSki.setItemAnimator(DefaultItemAnimator())
            rvPackedSki.setAdapter(adapter2)
            adapter3 =
                PackingSkiBootsAdapter(this@FittingActivity, allItems3, true, false, true)
            val mLayoutManager3: RecyclerView.LayoutManager =
                LinearLayoutManager(this@FittingActivity)
            rvPackedAddOns.setLayoutManager(mLayoutManager3)
            rvPackedAddOns.setItemAnimator(DefaultItemAnimator())
            rvPackedAddOns.setAdapter(adapter3)
            adapter4 =
                PackingSkiBootsAdapter(this@FittingActivity, allItems4, false, false, false)
            val mLayoutManager4: RecyclerView.LayoutManager =
                LinearLayoutManager(this@FittingActivity)
            rvFittingSkiBoot.setLayoutManager(mLayoutManager4)
            rvFittingSkiBoot.setItemAnimator(DefaultItemAnimator())
            rvFittingSkiBoot.setAdapter(adapter4)
            adapter5 =
                PackingSkiBootsAdapter(this@FittingActivity, allItems5, false, false, false)
            val mLayoutManager5: RecyclerView.LayoutManager =
                LinearLayoutManager(this@FittingActivity)
            rvFittingSki.setLayoutManager(mLayoutManager5)
            rvFittingSki.setItemAnimator(DefaultItemAnimator())
            rvFittingSki.setAdapter(adapter5)

            adapter6 =
                PackingSkiBootsAdapter(this@FittingActivity, allItems6, true, false, false)
            val mLayoutManager6: RecyclerView.LayoutManager =
                LinearLayoutManager(this@FittingActivity)
            rvFittingHelmet.setLayoutManager(mLayoutManager6)
            rvFittingHelmet.setItemAnimator(DefaultItemAnimator())
            rvFittingHelmet.setAdapter(adapter6)


            previousSkiDataAdapter =
                PreviousSkiDataAdapter(this@FittingActivity, existingSkiList)
            rvPreviousSkiData.setHasFixedSize(true)
            rvPreviousSkiData.layoutManager = LinearLayoutManager(this@FittingActivity)
            rvPreviousSkiData.adapter = previousSkiDataAdapter

            previousBootDataAdapter =
                PreviousBootDataAdapter(this@FittingActivity, existingBootList)
            rvPreviousBootData.setHasFixedSize(true)
            rvPreviousBootData.layoutManager = LinearLayoutManager(this@FittingActivity)
            rvPreviousBootData.adapter = previousBootDataAdapter

        }
    }

    @SuppressLint("RestrictedApi")
    fun loadMoreData(model: PackingData) {
        binding.run {
            if (model.isSwitchOut) {
                switchKeep = true
                existingSkiList.clear()
                if (model.existingSkis.isNotEmpty()) {
                    tvPreviousSkiData.visibility = View.VISIBLE
                    rvPreviousSkiDataLine.visibility = View.VISIBLE
                    existingSkiList.addAll(model.existingSkis)
                    previousSkiDataAdapter.notifyDataSetChanged()
                }
                existingBootList.clear()
                if (model.existingBoots.size > 0) {
                    tvPreviousBootData.visibility = View.VISIBLE
                    rvPreviousBootDataLine.visibility = View.VISIBLE
                    existingBootList.addAll(model.existingBoots)
                    previousBootDataAdapter.notifyDataSetChanged()
                }
//                            switchKeepLayout.setVisibility(View.VISIBLE);
            } else {
                switchKeep = false
//                            switchKeepLayout.setVisibility(View.GONE);
            }

            if (model.type == "snowboard") {
                layRecommendedPole.visibility = View.GONE
                layPolesFit.visibility = View.GONE
                dinSettingsFit.visibility = View.GONE
                dinPacking.visibility = View.GONE
                isDinVisible = false
            }

            if (!model.poles.isNullOrBlank()) {
                isFirstTime = true
                val position = spinnerArrayAdapter.getPosition(model.poles)
                spinnerPolePack.setSelection(position, true)
            }
            if (model.poleStatus.equals("Fitted", true)) {
                tvPolePackStatus.supportBackgroundTintList = (
                        ContextCompat.getColorStateList(
                            this@FittingActivity, R.color.green
                        ))
            }


            var upgradedAddon = ""
            if (!model.assignedUpgradeAddonList.isNullOrEmpty()) {
                layFitBootText.visibility = View.VISIBLE
                for (i in 0 until model.assignedUpgradeAddonList.size) {
                    upgradedAddon = if (i == model.assignedUpgradeAddonList.size - 1) {
                        upgradedAddon + model.assignedUpgradeAddonList.get(i).name
                    } else {
                        upgradedAddon + model.assignedUpgradeAddonList.get(i).name
                            .toString() + ","
                    }

                }
                Debugger.wtf("Model", "->" + model.assignedUpgradeAddonList.size)
            } else {
                layFitBootText.visibility = View.GONE
            }
            if (upgradedAddon != null) {
                if (upgradedAddon == "Premium Boot Upgrade - With HEATED BOOTS") {
                    tvBootsFit.setText("Heated Boots")
                } else {
                    tvBootsFit.setText(upgradedAddon)
                }
            }
            val stringList: MutableList<String> = ArrayList()
            if (model.assignedAddonList.isNotEmpty()) {
                var name = ""
                stringList.clear()
                for (i in 0 until model.assignedAddonList.size) {
                    name = if (i == model.assignedAddonList.size - 1) {
                        name + model.assignedAddonList.get(i).name
                    } else {
                        name + model.assignedAddonList.get(i).name + ", "
                    }
                }
                tvAssignedAddOnListFit.text = name
                var addOnStatus: String = ""
                if (!model.assignedAddonList.isNullOrEmpty()) {
                    model.assignedAddonList.forEach {
                        if (it.status == "Packed") {
                            addOnStatus = "Packed"
                        }
                        if (it.status == "Fitted") {
                            addOnStatus = "Fitted"
                        }
                    }
                }

                if (addOnStatus == "Packed") {
                    addOnsPackedStatusFit.visibility = View.VISIBLE
                    addOnsFitStatusFit.visibility = View.GONE
                } else if (addOnStatus == "Fitted") {
                    addOnsPackedStatusFit.visibility = View.VISIBLE
                    addOnsFitStatusFit.visibility = View.VISIBLE
                } else {
                    addOnsPackedStatusFit.visibility = View.GONE
                    addOnsFitStatusFit.visibility = View.GONE
                }
            }
            Debugger.wtf(
                "getDin_setting_ski", model.manualDin.toString() + " / " +
                        model.recommendedDin
            )

            if (model.manualDin != "N/A") {
                btnDin.background.setColorFilter(
                    Color.parseColor("#2dd57b"),
                    PorterDuff.Mode.SRC_ATOP
                )
                tvDinSettingsNew.text = model.manualDin
                isDinGreen = true
            } else if (model.recommendedDin != "N/A") {
                tvDinSettingsNew.text = model.recommendedDin
            } else {
                tvDinSettingsNew.text = "N/A"
                isDinGreen = false
            }

            if (!model.fittedBootList.isNullOrEmpty()) {
                if (!model.fittedBootList[0].soleLength.isNullOrBlank()) {
                    tvSoleLengthNew.text = model.fittedBootList.get(0).soleLength
                }

            } else {
                tvSoleLengthNew.text = "N/A"
            }
            if (model.skierCode != null) {
                tvSkierCodeNew.setText(model.skierCode)
            } else {
                tvSkierCodeNew.setText("N/A")
            }
        }
    }

    fun loadData(model: PackingData) {
        binding.run {
            Debugger.wtf("loadData", Gson().toJson(model))
            if (model != null) {
                ownBootCB.isChecked = model.ownBoot
                confirmOwnBoot = model.ownBoot
                inventoryType = model.type
                isDinVisible = true
                isInventorySki = inventoryType.equals("ski", ignoreCase = true) ||
                        inventoryType.equals("skier", ignoreCase = true)
                if (model.type != null) {
                    if (isInventorySki) {
                        if (model.dinSetting != null
                            && !model.dinSetting.equals("N/A")
                        ) {
                            dinPacking.visibility = View.VISIBLE
                            dinPacking.text = "DIN " + model.dinSetting
                        } else {
                            dinPacking.visibility = View.GONE
                        }
                    }
                    if (model.haveHistory) {
                        viewHistoryTv.visibility = View.VISIBLE
                    } else {
                        viewHistoryTv.visibility = View.GONE
                    }

                    if (model.isSkiPacked) {
                        SkiPackStatusLay.visibility = View.VISIBLE
                    } else {
                        SkiPackStatusLay.visibility = View.GONE
                    }
                    if (model.isSkiFitted) {
                        SkiFittedStatusLay.visibility = View.VISIBLE
                    } else {
                        SkiFittedStatusLay.visibility = View.GONE
                    }

                    if (model.isBootPacked) {
                        BootPackStatusLay.visibility = View.VISIBLE
                    } else {
                        BootPackStatusLay.visibility = View.GONE
                    }
                    if (model.isBootFitted) {
                        BootFittedStatusLay.visibility = View.VISIBLE
                    } else {
                        BootFittedStatusLay.visibility = View.GONE
                    }

                    if (model.recommendedPole != null) {
                        if (model.recommendedPole.toString().isNotEmpty()) {
                            layRecommendedPole.visibility = View.VISIBLE
                            tvRecommendedPole.text = model.recommendedPole.toString()
                        } else {
                            layRecommendedPole.visibility = View.GONE
                        }
                    } else {
                        layRecommendedPole.visibility = View.GONE
                    }

                    if (model.packageX != null) {
                        packageTypeTv.setText(model.packageX)
                    }
                    if (model.age != null) {
                        packingAgeTv.setText(model.age.toString())
                    }
                    if (model.gender != null) {
                        packingGenderTv.setText(model.gender.toString())
                    }
                    if (model.height != null) {
                        packingHeightTv.setText(model.height.toString())
                    }
                    if (model.weight != null) {
                        packingWeightTv.setText(model.weight.toString())
                    }
                    if (model.shoeSize != null) {
                        packingShoeSizeTv.setText(model.shoeSize.toString())
                    }
                    if (model.shoeType != null) {
                        packingShoeTypeTv.setText(model.shoeType.toString())
                    }
                    if (model.ability != null) {
                        packingAbilityTv.setText(model.ability.toString())
                    }

                    if (model.dob != null) {
                        dob = model.dob
                    } else {
                        dob = ""
                    }
                    if (model.dinSetting != null
                        && !model.dinSetting.equals("N/A")
                    ) {
                        dinSetting = model.dinSetting
                    } else {
                        //dinSetting = "";
                    }

                    if (model.isFitAddonsAvailable) {
                        addHelmetBigTv.setTextColor(setColor(this@FittingActivity, R.color.red))
                    } else {
                        addHelmetBigTv.setTextColor(setColor(this@FittingActivity, R.color.black))
                    }
                    if (!model.clothingList.isNullOrEmpty()) {
                        for (i in 0 until model.clothingList.size) {
                            if (model.clothingList.get(i).status == "Packed") {
                                ClothingPackStatusLay.visibility = View.VISIBLE
                                ClothingFitStatusLay.visibility = View.GONE
                            } else if (model.clothingList.get(i).status == "Fitted" ||
                                model.clothingList.get(i).status.equals("Returned")
                            ) {
                                ClothingPackStatusLay.visibility = View.VISIBLE
                                ClothingFitStatusLay.visibility = View.VISIBLE
                            } else {
                                ClothingPackStatusLay.visibility = View.GONE
                                ClothingFitStatusLay.visibility = View.GONE
                            }

                        }
                    }
                    tvClothingFit.text = model.clothing
                    reservation_id = model.reservationId
                    rvPackedBoot.setVisibility(View.VISIBLE)
                    addSkiPackBootBigBtn.visibility = View.GONE
                    if (!model.bootList.isNullOrEmpty()) {
                        allItems1.clear()
                        allItems1.addAll(model.bootList)
                        adapter1.notifyDataSetChanged()
                        Debugger.wtf(
                            "CheckEmpty", "bootList " +
                                    "${model.bootList} allItems1 ${allItems1.size}"
                        )
                    } else {
                        if (confirmOwnBoot) {
                            premiumFitSkiBootTv.text = "Own Boot\n"
                            bootPackTv.text = "Own Boot"
                            bootPackTv.setTextColor(
                                setColor(
                                    this@FittingActivity,
                                    R.color.green
                                )
                            )
                            rvPackedBoot.visibility = View.GONE
                            addSkiPackBootBigBtn.visibility = View.VISIBLE
                        } else {
                            allItems1.clear()
                            val model11: PackedInventoryModel =
                                PackedInventoryModel(name = "Not Packed Yet")
                            allItems1.add(model11)
                            adapter1.notifyDataSetChanged()
                        }

                    }
                    if (!model.skiList.isNullOrEmpty()) {
                        allItems2.clear()
                        allItems2.addAll(model.skiList)
                        adapter2.notifyDataSetChanged()
                    } else {
                        allItems2.clear()
                        val model11: PackedInventoryModel =
                            PackedInventoryModel(name = "Not Packed Yet")
                        allItems2.add(model11)
                        adapter2.notifyDataSetChanged()
                    }

                    if (!model.addonList.isNullOrEmpty()) {
                        allItems3.clear()
                        for (mode in model.addonList) {
//                        if (mode.getIsPackable) {
//                            allItems3.add(mode);
//                        }
                            allItems3.add(mode)
                        }
                        adapter3.notifyDataSetChanged()
                    } else {
                        allItems3.clear()
                        val model11: PackedInventoryModel =
                            PackedInventoryModel(name = "Not Packed Yet")
                        allItems3.add(model11)
                        adapter3.notifyDataSetChanged()
                    }
                    //------------------Fitting List -------------
                    if (model.fittedBootList != null) {
                        if (model.fittedBootList.size > 0) {
                            rvFittingSkiBoot.visibility = View.VISIBLE
                            addSkiBootBigBtn.visibility = View.GONE
                            addSkiBootSmallBtn.visibility = View.GONE
                            allItems4.clear()
                            allItems4.addAll(model.fittedBootList)
                            adapter4.notifyDataSetChanged()
                            Debugger.wtf(
                                "CheckEmpty", "fittedBootList " +
                                        "${model.fittedBootList} allItems4 ${allItems4.size}"
                            )

                            if (confirmOwnBoot) {
                                premiumFitSkiBootTv.text = "Own Boot\n"
                                bootPackTv.text = "Own Boot"
                                bootPackTv.setTextColor(resources.getColor(R.color.green))
                                rvPackedBoot.setVisibility(View.GONE)
                                addSkiPackBootBigBtn.visibility = View.VISIBLE
                            } else {
                                if (model.bootList.size === 0) {
                                    allItems1.clear()
                                    val model11: PackedInventoryModel =
                                        PackedInventoryModel(name = "Fit")
                                    allItems1.add(model11)
                                    adapter1.notifyDataSetChanged()
                                }
                            }
                        } else {
                            rvFittingSkiBoot.visibility = View.GONE
                            addSkiBootBigBtn.visibility = View.VISIBLE
                            addSkiBootSmallBtn.visibility = View.GONE
                            if (confirmOwnBoot) {

                                //   premiumSkiBootTv.setText("Own Boot");
                                premiumFitSkiBootTv.text = "Own Boot\n"

//                        if (model.getPackage != null) {
//                            premiumSkiTv.setText(model.getPackage.toString());
//                            premiumFitSkiTv.setText(model.getPackage.toString());
//                        }
                                bootPackTv.text = "Own Boot"
                                bootPackTv.setTextColor(resources.getColor(R.color.green))
                                rvPackedBoot.setVisibility(View.GONE)
                                addSkiPackBootBigBtn.visibility = View.VISIBLE
                            }
                        }
                    }
                    if (model.fittedSkiList != null) {
                        if (model.fittedSkiList.size > 0) {
                            rvFittingSki.visibility = View.VISIBLE
                            addSkiBigBtn.visibility = View.GONE
                            addSkiSmallBtn.visibility = View.GONE
                            allItems5.clear()
                            allItems5.addAll(model.fittedSkiList)
                            adapter5.notifyDataSetChanged()

                            //For Change Pack Skis Status to Fit
                            if (model.skiList.size === 0) {
                                allItems2.clear()
                                val model11: PackedInventoryModel =
                                    PackedInventoryModel(name = "Fit")
                                allItems2.add(model11)
                                adapter2.notifyDataSetChanged()
                            }
                        } else {
                            rvFittingSki.visibility = View.GONE
                            addSkiBigBtn.visibility = View.VISIBLE
                            addSkiSmallBtn.visibility = View.GONE
                        }
                    }
                    if (model.fittedAddonList != null) {
                        if (model.fittedAddonList.size > 0) {
                            rvFittingHelmet.visibility = View.VISIBLE
                            addHelmetBigBtn.visibility = View.GONE
                            addHelmetSmallBtn.visibility = View.VISIBLE
                            allItems6.clear()
                            for (mode in model.fittedAddonList) {
//                        if (mode.getIsPackable) {
//                            allItems6.add(mode);
//                        }
                                allItems6.add(mode)
                            }
                            adapter6.notifyDataSetChanged()
                        } else {
                            rvFittingHelmet.visibility = View.GONE
                            addHelmetBigBtn.visibility = View.VISIBLE
                            addHelmetSmallBtn.visibility = View.GONE
                        }
                    }

                } else {
                    showToast(
                        this@FittingActivity,
                        "No Record Found"
                    )
                }
            }
            loadMoreData(model)
        }
    }

    override fun onClick(view: View) {
        binding.run {
            if (btnDin.id == view.id) {
                showUpdateAddonStatusDialog()
            } else if (addSkiBootBigBtn.id == view.id ||
                addSkiBootSmallBtn.id == view.id
            ) {
                if (!confirmOwnBoot) {
                    if (cbConfirmData.isChecked) {
                        showAddBootSkisBarcodeDialog(true, switchKeep, inventoryType)
                    } else {
                        showToast(
                            this@FittingActivity, "Please Confirm Renter's Data Before Fitting"
                        )
                    }
                } else {
                    if (cbConfirmData.isChecked) {
                        showAddManuallyBootDialog(switchKeep, inventoryType)
                    } else {
                        showToast(
                            this@FittingActivity, "Please Confirm Renter's Data Before Fitting"
                        )
                    }
                }
            } else if (addSkiBigBtn.getId() == view.id ||
                addSkiSmallBtn.getId() == view.id
            ) {
                if (cbConfirmData.isChecked) {
                    showAddBootSkisBarcodeDialog(false, switchKeep, inventoryType)
                } else {
                    showToast(
                        this@FittingActivity, "Please Confirm Renter's Data Before Fitting"
                    )
                }
            } else if (addHelmetBigBtn.getId() == view.id || addHelmetSmallBtn.getId() == view.id ||
                btnAddOn.id == view.id
            ) {
                if (cbConfirmData.isChecked) {
                    packingViewModel.hitGetFittingAddonsData(
                        sharedPrefsHelper.getUser()?.accessToken.toString(),
                        renter_id,
                        shipment_id
                    )
                } else {
                    showToast(
                        this@FittingActivity, "Please Confirm Renter's Data Before Fitting"
                    )
                }
            } else if (viewHistoryTv.id == view.id) {
                deliveryViewModel.hitGetRenterHistory(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    renter_id,
                    renterName
                )
            } else if (tvAddBootManually.id == view.id) {
                showAddManuallyBootDialog(isSwitchOut, inventoryType)
            } else if (tvPolePackStatus.id == view.id) {
                if (binding.spinnerPolePack.selectedItem.toString() != "Select") {
                    fitPoleAlert()
                }
            }
        }
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        nfcReaderUtils.resolveIntent(intent!!)
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
        Debugger.wtf("PackingActivity123", "onResume")

        getPackingData()
    }

    override fun onPause() {
        super.onPause()
        if (nfcAdapter != null) {
            nfcAdapter!!.disableForegroundDispatch(this)
        }
    }

    override fun onStop() {
        super.onStop()
        Debugger.wtf("PackingActivity123", "onStop")
    }

    fun stopNfcScan() {
        if (nfcAdapter != null) {
            nfcAdapter!!.disableForegroundDispatch(this)
        }
    }

    fun showAddBootSkisBarcodeDialog(
        isBootAdded: Boolean,
        isSwitchOut: Boolean,
        inventoryType: String
    ) {

//        if (nfcAdapter != null) {
//            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
//        }
        fitBootSkiInventoryDialog =
            FitBootSkiInventoryDialog.newInstance(isBootAdded, isSwitchOut, inventoryType)
        fitBootSkiInventoryDialog.show(supportFragmentManager, "")
        fitBootSkiInventoryDialog.setOnSubmitListener(object :
            FitBootSkiInventoryDialog.OnSubmitClick {
            override fun onSubmitClick(
                isBootAdded: Boolean,
                barcode: String,
                keepSwitchText: String
            ) {
                hitSaveFitBootSkisByBarcode(barcode, keepSwitchText, isBootAdded)
                Debugger.wtf(
                    "showAddBootSkisBarcodeDialog",
                    "$isBootAdded / $barcode / $keepSwitchText"
                )
            }

            override fun onSwitchClick(isBootAdded: Boolean, keepSwitchText: String) {
                hitKeepOldBootSkis(isBootAdded, keepSwitchText)
                Debugger.wtf("showAddBootSkisBarcodeDialog", "$isBootAdded / $keepSwitchText")
            }

            override fun addManualButtonClick(isBootAdded: Boolean) {
                if (isBootAdded) {
                    showAddManuallyBootDialog(isSwitchOut, inventoryType)
                } else {
                    showAddManuallySkiDialog(isSwitchOut, inventoryType)
                }
                Debugger.wtf("showAddBootSkisBarcodeDialog", "$isBootAdded")
            }

            override fun onDismiss() {
                Debugger.wtf("showAddBootSkisBarcodeDialog", "onDismiss")
                // stopNfcScan()
            }
        })
    }

    fun hitKeepOldBootSkis(isBootAdded: Boolean, keepSwitchText: String) {
        if (isBootAdded) {
            if (!existingBootList.isNullOrEmpty()) {
                hitSaveFitBootSkisByBarcode(
                    existingBootList[0].barcode,
                    keepSwitchText,
                    isBootAdded
                )
            } else {
                showToast(this@FittingActivity, "There is no previous Equipment.")
            }
        } else {
            if (!existingSkiList.isNullOrEmpty()) {
                hitSaveFitBootSkisByBarcode(
                    existingSkiList[0].barcode,
                    keepSwitchText,
                    isBootAdded
                )
            } else {
                showToast(this@FittingActivity, "There is no previous Equipment.")
            }
        }
    }

    fun showDeleteInventoryAlert(model: PackedInventoryModel, isFromPacking: Boolean) {
        var name: String = ""
        if (model.name.isNullOrEmpty()) {
            name = if (isFromPacking) "Packing" else "Fitting"
        } else {
            name = model.name.toString()
        }
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "Are you sure you want to delete this \n' " + name + " ' Inventory?"
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                Debugger.wtf("showDeleteInvertoryAlert", "showDeleteInvertoryAlert")
                if (isFromPacking) {
                    packingViewModel.hitDeleteInventory(
                        sharedPrefsHelper.getUser()?.accessToken.toString(),
                        model.undoPackingUrl
                    )
                } else {
                    if (switchKeep) {
                        packingViewModel.hitDeleteInventory(
                            sharedPrefsHelper.getUser()?.accessToken.toString(),
                            model.undoFittingSwitchoutUrl
                        )
                    } else {
                        val map =
                            HashMap<String, String>()
                        map["renter_id"] = renter_id.toString() + ""
                        map["shipment_id"] = shipment_id.toString()
                        map["barcode"] = model.barcode
                        packingViewModel.hitDeleteFittingBootSkisInventory(
                            sharedPrefsHelper.getUser()?.accessToken.toString(),
                            map
                        )
                    }
                }
            }

        })
    }


    fun hitSaveFitBootSkisByBarcode(
        barcode: String,
        keepSwitchText: String,
        isForBoot: Boolean
    ) {
        val map = HashMap<String, String>()
        map["renter_id"] = renter_id.toString() + ""
        map["shipment_id"] = shipment_id.toString() + ""
        map["barcode"] = barcode
        map["swap"] = keepSwitchText
        map["force_back"] = "0"
        if (isForBoot) {
            map["type"] = "boot"
        } else {
            map["tech_initials"] = "TS"
            map["type"] = "ski"
        }

        hitApiForFitBootAndSkis(isForBoot, map)
    }

    fun hitApiForFitBootAndSkis(
        isForBoot: Boolean,
        map: HashMap<String, String>
    ) {
        if (isForBoot) {
            packingViewModel.hitSaveFitBootByBarcode(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                renter_id,
                shipment_id,
                map
            )
        } else {
            packingViewModel.hitSaveFitSkiByBarcode(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                renter_id,
                shipment_id,
                map
            )
        }
    }

    fun showAddManuallyBootDialog(isSwitchOut: Boolean, inventoryType: String) {
        val addBootManullyDialog: AddBootManuallyDialog =
            AddBootManuallyDialog.newInstance(isSwitchOut, inventoryType)
        addBootManullyDialog.show(supportFragmentManager, "") //S
        addBootManullyDialog.setOnSubmitListener(object : AddBootManuallyDialog.OnSubmitClick {
            override fun onSubmitClick(
                manufacture: String,
                bootSize: String,
                soleLength: String,
                keepSwitchText: String
            ) {
                val map =
                    HashMap<String, String>()
                map["model"] = ""
                map["size"] = bootSize
                map["length"] = soleLength
                map["swap"] = keepSwitchText
                map["manufacture"] = manufacture

                packingViewModel.hitSaveFitBootManually(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    renter_id,
                    shipment_id,
                    map
                )

                Debugger.wtf(
                    "addBootManullyDialog",
                    "$manufacture / $bootSize / $soleLength / $keepSwitchText"
                )
            }

            override fun onSwitchClick(keepSwitchText: String) {
                hitKeepOldBootSkis(true, keepSwitchText)
                Debugger.wtf("addBootManullyDialog", "$keepSwitchText")
            }

        })

    }

    fun showAddManuallySkiDialog(isSwitchOut: Boolean, inventoryType: String) {
        val addSkiManuallyDialog: AddSkiManuallyDialog =
            AddSkiManuallyDialog.newInstance(isSwitchOut, inventoryType)
        addSkiManuallyDialog.show(supportFragmentManager, "") //S
        addSkiManuallyDialog.setOnSubmitListener(object : AddSkiManuallyDialog.OnSubmitClick {
            override fun onSubmitClick(
                model: String, bindingModel: String, keepSwitchText: String
            ) {
                val map = HashMap<String, String>()
                map["model"] = model
                map["bind_model"] = bindingModel
                map["swap"] = keepSwitchText
                packingViewModel.hitSaveFitSkiManually(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    renter_id,
                    shipment_id,
                    map
                )
                Debugger.wtf(
                    "addBootManullyDialog",
                    "$model / $bindingModel"
                )
            }

            override fun onSwitchClick(keepSwitchText: String) {
                hitKeepOldBootSkis(false, keepSwitchText)
                Debugger.wtf("addBootManullyDialog", "$keepSwitchText")
            }
        })
    }

    fun updateRenterDataDialog() {
        val updateRenterDataDialog: UpdateRenterDataDialog =
            UpdateRenterDataDialog.newInstance(getAbilityShoeSizeHeightListModel, packingData)
        updateRenterDataDialog.show(supportFragmentManager, "") //S
        updateRenterDataDialog.setOnSubmitListener(object :
            UpdateRenterDataDialog.OnUpdateRenterDataListener {
            override fun onUpdateClick(
                dob: String,
                gender: String,
                shoeType: String,
                height: String,
                weight: String,
                showSize: String,
                ability: String
            ) {
                val map = HashMap<String, String>()
                map["gender"] = gender
                map["shoe_type"] = shoeType
                if (height.isNotEmpty() && height != "select item") map["height"] = height
                if (showSize.isNotEmpty() && showSize != "0.0") map["shoe_size"] = showSize
                if (ability.isNotEmpty() && ability != "select item") map["ability"] = ability
                if (dob != "") map["dob"] = dob
                if (weight.isNotEmpty()) map["lb"] = weight
                Debugger.wtf("onUpdateClick", "${map.toString()}")
                packingViewModel.hitUpdateRenterData(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    renter_id, map
                )
            }

        })

    }

    fun showFitAddOnsListDialog(
        addOnsList: ArrayList<FittingAddon>,
        fittedClothingList: ArrayList<FittedClothingModel>
    ) {
        showFitAddonsListDialog =
            ShowFitAddonsListDialog.newInstance(
                addOnsList,
                fittedClothingList
            )
        showFitAddonsListDialog.show(supportFragmentManager, "") //S
        showFitAddonsListDialog.setOnSubmitListener(object :
            ShowFitAddonsListDialog.OnAddonsSubmitted {
            override fun clickOnAddonsWithBarcode(
                status: String, addonId: String,
                addonName: String, addonSize: String,
                is_manual: Int
            ) {
                initilizaBarCodeScanDialog(
                    addOnsStatus = status, addonId,
                    addonName, addonSize,
                    is_manual
                )
            }

            override fun onSubmit(
                addOnsList: ArrayList<FittingAddon>,
                clothingList: ArrayList<FittedClothingModel>
            ) {
                Debugger.wtf("onSubmit", "${Gson().toJson(addOnsList)}")
                val addonModel = FitSaveAddonsModel()
                addonModel.selected_addons = ArrayList()

                for (i in 0 until addOnsList.size) {
                    val model: FittingAddon = addOnsList.get(i)
                    addonModel.selected_addons.add(
                        AddonListModel(
                            model.id,
                            if (model.selected) 1 else 0
                        )
                    )
                }
                Debugger.wtf(
                    "selected_addons",
                    "${addonModel.selected_addons.size} / ${addOnsList.size}"
                )

                val params = HashMap<String, String>()
                params["renter_id"] = renter_id.toString()
                if (clothingList.isNotEmpty()) {
                    params["selected_clothing"] = clothingList[0].id.toString()
                }
                packingViewModel.hitSaveFitAddonsList(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    params,
                    addonModel
                )
            }

        })

    }

    fun showUpdateAddonStatusDialog(
        packedInventoryModel: PackedInventoryModel
    ) {
        val showDialog: UpdateAddonStatusDialog =
            UpdateAddonStatusDialog.newInstance(
                packedInventoryModel
            )
        showDialog.show(supportFragmentManager, "")
        showDialog.setOnSubmitListener(object : UpdateAddonStatusDialog.OnAddOnStatusSubmitted {
            override fun onSubmit(addonId: String, status: String, helmetSize: String) {
                val map =
                    HashMap<String, String>()
                map["renter_id"] = renter_id.toString() + ""
                map["addon_id"] = addonId
                map["status"] = status
                if (helmetSize != "") {
                    map["helmet_size"] = helmetSize
                }

                packingViewModel.hitUpdateAddonStatus(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    map
                )
            }
        })
    }

    fun showUpdateAddonStatusDialog() {
        val showDialog: UpdateDinSettingDialog =
            UpdateDinSettingDialog.newInstance(binding.tvDinSettingsNew.text.toString())
        showDialog.show(supportFragmentManager, "")
        showDialog.setOnUpdateDinSettingSubmit(object :
            UpdateDinSettingDialog.OnUpdateDinSettingSaved {
            override fun onDinSettingDone(dinSetting: String) {
                val map = HashMap<String, String>()
                map["din_setting"] = dinSetting
                if (switchKeep) {
                    packingViewModel.hitUpdateDinSettingWithDelivery(
                        sharedPrefsHelper.getUser()?.accessToken.toString(),
                        renter_id,
                        shipment_id,
                        map,
                        deliveryId
                    )
                } else {
                    packingViewModel.hitUpdateDinSetting(
                        sharedPrefsHelper.getUser()?.accessToken.toString(),
                        renter_id,
                        shipment_id,
                        map
                    )
                }

            }

        })
    }
}