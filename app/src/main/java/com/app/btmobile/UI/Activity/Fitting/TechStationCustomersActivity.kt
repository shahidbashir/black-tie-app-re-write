package com.app.btmobile.UI.Activity.Fitting

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.TechCustomerModel
import com.app.btmobile.Models.TechStationCustomerModel
import com.app.btmobile.MyApplication
import com.app.btmobile.Status
import com.app.btmobile.UI.Adapter.TechStationCustomerAdapter
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityTechStationCustomerBinding
import com.ogoul.kalamtime.viewmodel.PackingViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.util.*
import javax.inject.Inject

class TechStationCustomersActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityTechStationCustomerBinding
    lateinit var loaderDialog: LoaderDialog

    lateinit var techCustomerAdapter: TechStationCustomerAdapter

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: PackingViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    var allItems: ArrayList<TechCustomerModel> =
        ArrayList()


    var selectedDate: String = "";

    var c = Calendar.getInstance()
    var mYear = c[Calendar.YEAR]
    var mMonth = c[Calendar.MONTH]
    var mDay = c[Calendar.DAY_OF_MONTH]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTechStationCustomerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupViews()
        setupToolbarAndLoader()
        setupRecyclerview()
    }

    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(PackingViewModel::class.java)
        viewModel.getTechCustomerListResponse().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
    }

    private fun setupRecyclerview() {
        techCustomerAdapter = TechStationCustomerAdapter(this, allItems)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvOverview.layoutManager = mLayoutManager
        binding.rvOverview.itemAnimator = DefaultItemAnimator()
        binding.rvOverview.adapter = techCustomerAdapter
    }

    private fun setupViews() {
        selectedDate = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.tvDateSelected.text = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.btnSelectDate.setOnClickListener {
            openDatePicker()
        }
    }

    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Tech Station"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })
    }


    override fun onResume() {
        super.onResume()
        hitApi()
    }

    fun hitApi() {
        val date: String = parseDateToddMMddyyy(selectedDate) ?: ""
        Debugger.wtf("hitApi", "${date.toString()}")
        viewModel.hitGetAllTechCustomersList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), date
        )
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<TechStationCustomerModel>
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                loaderDialog.dismissDialog()
                val techCustomerList = apiResponse.data
                showData(techCustomerList?.data as ArrayList<TechCustomerModel>)
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@TechStationCustomersActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@TechStationCustomersActivity)
            }
        }
    }

    private fun showData(allGroupList: ArrayList<TechCustomerModel>) {
        Debugger.wtf("showData", "${allGroupList.size}")
        if (allGroupList.size > 0) {
            binding.noDeliveriesTv.visibility = View.GONE
            allItems.clear()
            allItems.addAll(allGroupList)
            techCustomerAdapter.notifyDataSetChanged()
        } else {
            allItems.clear()
            techCustomerAdapter.notifyDataSetChanged()
            binding.noDeliveriesTv.visibility = View.VISIBLE
        }
    }


    private fun openDatePicker() {
        val datePickerDialog: DatePickerDialog =
            object : DatePickerDialog(
                this@TechStationCustomersActivity,
                null, mYear, mMonth, mDay
            ) {
                override fun onDateChanged(
                    @NonNull view: DatePicker,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    mYear = year
                    mMonth = monthOfYear
                    mDay = dayOfMonth
                    selectedDate = "${mMonth + 1}/$dayOfMonth/$year"
                    binding.tvDateSelected.setText("${mMonth + 1}/$dayOfMonth/$year")
                    hitApi()
                    dismiss()
                }
            }
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).visibility = View.GONE
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).visibility = View.GONE
    }

    fun goToViewDetails(vanShipmentId: Int) {
        val i = Intent(this, TechStationRentersActivity::class.java)
        i.putExtra("vanShipmentId", vanShipmentId)
        startActivity(i)
    }
}