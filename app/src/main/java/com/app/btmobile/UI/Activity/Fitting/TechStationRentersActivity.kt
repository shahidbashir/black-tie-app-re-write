package com.app.btmobile.UI.Activity.Fitting

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.TechRenterData
import com.app.btmobile.Models.TechStationRenterModel
import com.app.btmobile.MyApplication
import com.app.btmobile.Status
import com.app.btmobile.UI.Activity.Packing.PackingActivity
import com.app.btmobile.UI.Adapter.TechStationRenterAdapter
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityTechStationRentersBinding
import com.ogoul.kalamtime.viewmodel.PackingViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.util.*
import javax.inject.Inject

class TechStationRentersActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityTechStationRentersBinding
    lateinit var loaderDialog: LoaderDialog

    lateinit var techRenterAdapter: TechStationRenterAdapter

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: PackingViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    var allItems: ArrayList<TechRenterData> =
        ArrayList()

    var vanShipmentId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTechStationRentersBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setupViewModel()
        setupToolbarAndLoader()
        setupRecyclerview()
    }


    fun getIntentData() {
        if (intent != null) {
            vanShipmentId = intent.getIntExtra("vanShipmentId", -1)
        }
    }

    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(PackingViewModel::class.java)
        viewModel.getTechRentersListResponse().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
    }

    private fun setupRecyclerview() {
        techRenterAdapter = TechStationRenterAdapter(this, allItems) { from, position ->
            run {
                onItemClickListener(from, position)
            }
        }
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvTechRenters.layoutManager = mLayoutManager
        binding.rvTechRenters.itemAnimator = DefaultItemAnimator()
        binding.rvTechRenters.adapter = techRenterAdapter
    }

    fun onItemClickListener(from: String, position: Int) {
        var model = allItems[position]
        when (from) {
            "next" -> {
                goToFittingScreen(model)
            }
            "boot", "ski" -> {
                goToPackingScreen(model)
            }
            "waiver" -> {
                goToWaiverScreen(model)
            }
        }


    }


    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Tech Station"
        supportActionBar!!.subtitle = "Renters"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })
    }

    override fun onResume() {
        super.onResume()
        hitApi()
    }

    fun hitApi() {
        viewModel.hitGetAllTechRentersList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), vanShipmentId
        )
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<TechStationRenterModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                loaderDialog.dismissDialog()
                val techCustomerList = apiResponse.data
                showData(techCustomerList?.data as ArrayList<TechRenterData>)
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@TechStationRentersActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@TechStationRentersActivity)
            }
        }
    }


    private fun showData(allGroupList: ArrayList<TechRenterData>) {
        Debugger.wtf("showData", "${allGroupList.size}")
        if (allGroupList.size > 0) {
            binding.noRecordTv.visibility = View.GONE
            allItems.clear()
            allItems.addAll(allGroupList)
            techRenterAdapter.notifyDataSetChanged()
        } else {
            allItems.clear()
            techRenterAdapter.notifyDataSetChanged()
            binding.noRecordTv.visibility = View.VISIBLE
        }
    }

    fun goToFittingScreen(
        techRenterData: TechRenterData
    ) {
        val i = Intent(this, FittingActivity::class.java)
        i.putExtra("renterId", techRenterData.renterId)
        i.putExtra("shipmentId", techRenterData.vanShipmentId.toInt())
        i.putExtra("renterName", techRenterData.renterName)
        startActivity(i)
    }

    fun goToPackingScreen(techRenterData: TechRenterData) {
        val intent = Intent(this, PackingActivity::class.java)
        intent.putExtra("renterId", techRenterData.renterId)
        intent.putExtra("shipmentId", techRenterData.vanShipmentId.toInt())
        intent.putExtra("renterName", techRenterData.renterName)
        startActivity(intent)
    }

    fun goToWaiverScreen(techRenterData: TechRenterData) {
//        val intent = Intent(this, WaiversShowActivity::class.java)
//        intent.putExtra("reservationId", techRenterData.reservationId)
//        intent.putExtra("renter_id", techRenterData.renterId)
//        intent.putExtra("fromFitting", "Yes")
//        startActivity(intent)
    }
}