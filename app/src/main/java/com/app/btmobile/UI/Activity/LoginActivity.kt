package com.app.btmobile.UI.Activity

import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.Nullable
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.btmobile.ApiResponse
import com.app.btmobile.BASE.BaseActivity
import com.app.btmobile.Models.LoginModel
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityLoginBinding
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.gson.Gson
import com.ogoul.kalamtime.viewmodel.LoginViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import javax.inject.Inject

class LoginActivity : BaseActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityLoginBinding
    lateinit var loaderDialog: LoaderDialog

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: LoginViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    private var mAppUpdateManager: AppUpdateManager? = null
    private val RC_APP_UPDATE = 11


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        MyApplication.getAppComponent(this).doInjection(this)
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        if (!AppConstants.IS_LIVE_MODE) {
            binding.layEnvironment.visibility = View.VISIBLE
            binding.tvEnvironment.text = if (AppConstants.IS_LIVE_MODE) "Live" else "Staging"
        }
        try {
            binding.tvVersion.text =
                StringBuilder("Version").append(" ").append(
                    packageManager?.getPackageInfo(
                        (this).packageName,
                        0
                    )?.versionName
                ).toString()
        } catch (e: Exception) {

        }

        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        viewModel.loginResponse().observe(this, Observer {
            consumeResponse(it)
        })


//        binding.etEmail.setText("tech2@rezosystems.com");
//        binding.etPassword.setText("123456");
        binding.btnSignIn.setOnClickListener {
            if (isValidate()) {
                val params = HashMap<String, String>()
                params["email"] = binding.etEmail.text.toString().trim()
                params["password"] = binding.etPassword.text.toString()
                viewModel.hitLogin(params)
            }
        }
    }

    private fun isValidate(): Boolean {
        if (binding.etEmail.text.toString().isEmpty()) {
            showToast(this@LoginActivity, "Email field is required")
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.text.toString())
                .matches()
        ) {
            showToast(this@LoginActivity, "Enter valid email")
            return false;
        } else if (binding.etPassword.text.toString().isEmpty()) {
            showToast(this@LoginActivity, "Password field is required")
            return false;
        } else if (binding.etPassword.text.length < 6) {
            showToast(this@LoginActivity, "Password must be at least 6 characters")
            return false;
        } else {
            return true;
        }
    }

    private fun consumeResponse(apiResponse: ApiResponse<LoginModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                loaderDialog.dismissDialog()
                renderResponse(apiResponse.data as LoginModel)
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
            }
            Status.ERROR -> {
                loaderDialog.dismissDialog()
                showToast(this@LoginActivity, "Invalid Credentials")
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
            }
            else -> {
            }
        }
    }

    private fun renderResponse(response: LoginModel?) {
        Debugger.wtf(TAG, "socketResponse:  ${Gson().toJson(response)}")
        response?.let { res ->
            val user1: LoginModel? = response
            try {
                if (user1?.data != null && user1?.data.message
                        .contentEquals("Logged In successfully.")
                ) {
                    val appUser = AppUser()
                    var data = user1.data.user
                    appUser.id = data.id
                    appUser.password = binding.etPassword.text.toString()
                    appUser.accessToken = user1.data.access_token
                    appUser.email = binding.etEmail.text.toString()
                    appUser.firstName = data.first_name
                    appUser.lastName = data.last_name
                    sharedPrefsHelper.setUser(appUser)
//                    PusherIO.getInstance()
//                        .connectToPusher(
//                            sharedPrefsHelper.getUser()?.id.toString(),
//                            this
//                        )
                    val i = Intent(this@LoginActivity, MainActivity::class.java)
                    startActivity(i)
                    finish()
                } else {
                    showToast(this@LoginActivity, "Invalid Credentials")
                }
            } catch (e: Exception) {
                Debugger.wtf(TAG, "Error ${e.message}")
            } finally {
                loaderDialog.dismissDialog()
            }
        }
    }

    fun checkUpdate() {
        mAppUpdateManager = AppUpdateManagerFactory.create(this)
        mAppUpdateManager?.registerListener(installStateUpdatedListener)
        mAppUpdateManager?.appUpdateInfo?.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE /*AppUpdateType.IMMEDIATE*/)
            ) {
                try {
                    mAppUpdateManager?.startUpdateFlowForResult(
                        appUpdateInfo,
                        AppUpdateType.FLEXIBLE /*AppUpdateType.FLEXIBLE*/,
                        this@LoginActivity,
                        RC_APP_UPDATE
                    )
                } catch (e: SendIntentException) {
                    e.printStackTrace()
                }
            } else {
                Log.wtf("checkUpdate", "checkForAppUpdateAvailability: something else")
            }
        }
    }

    private var installStateUpdatedListener: InstallStateUpdatedListener =
        object : InstallStateUpdatedListener {
            override fun onStateUpdate(state: InstallState) {
                if (state.installStatus() == InstallStatus.DOWNLOADED) {
                    //CHECK THIS if AppUpdateType.FLEXIBLE, otherwise you can skip
                    popupSnackBarForCompleteUpdate()
                } else if (state.installStatus() == InstallStatus.INSTALLED) {
                    if (mAppUpdateManager != null) {
                        mAppUpdateManager?.unregisterListener(this)
                    }
                } else {
                    Log.wtf("InstallStateUpdatedListener", ": state: " + state.installStatus())
                }
            }
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_APP_UPDATE) {
            if (resultCode != RESULT_OK) {
                Log.wtf("onActivityResult", "onActivityResult: app download failed")
            }
        }
    }

    private fun popupSnackBarForCompleteUpdate() {
        val snackBar: Snackbar = Snackbar.make(
            findViewById(R.id.rootView),
            "New app is ready!",
            Snackbar.LENGTH_INDEFINITE
        )
        snackBar.setAction("Install") {
            if (mAppUpdateManager != null) {
                mAppUpdateManager?.completeUpdate()
            }
        }
        snackBar.setActionTextColor(setColor(this, R.color.blue))
        snackBar.show()
    }

    override fun onStart() {
        super.onStart()
        checkUpdate()
    }

    override fun onStop() {
        super.onStop()
        if (mAppUpdateManager != null) {
            mAppUpdateManager?.unregisterListener(installStateUpdatedListener)
        }
    }
}

