package com.app.btmobile.UI.Activity

import android.app.PendingIntent
import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.app.btmobile.*
import com.app.btmobile.Models.Addon
import com.app.btmobile.Models.BaseModel
import com.app.btmobile.Models.ClothingModel
import com.app.btmobile.Models.SaveFitBootAndSkisByBarcodeModel
import com.app.btmobile.UI.Activity.Delivery.DeliveryCustomerActivity
import com.app.btmobile.UI.Activity.Fitting.TechStationCustomersActivity
import com.app.btmobile.UI.Activity.Nfc.NfcActivity
import com.app.btmobile.UI.Activity.Packing.PackingCustomerScreen
import com.app.btmobile.UI.Activity.Profile.ProfileActivity
import com.app.btmobile.UI.Activity.Return.ReturnCustomerActivity
import com.app.btmobile.UI.Activity.TransferGear.TranferGearActivity
import com.app.btmobile.UI.Activity.Waiver.WaiverCustomerActivity
import com.app.btmobile.UI.Dialog.AddScanBarcodeDialog
import com.app.btmobile.UI.Dialog.CommonDialog
import com.app.btmobile.UI.Dialog.InventoryCountDialog
import com.app.btmobile.UI.Dialog.LogoutDialog
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityMainBinding
import com.ogoul.kalamtime.viewmodel.NfcViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    var pendingIntent: PendingIntent? = null
    var nfcAdapter: NfcAdapter? = null
    lateinit var nfcReaderUtils: NfcReaderUtils

    lateinit var loaderDialog: LoaderDialog


    var inventoryCountDialog: InventoryCountDialog = InventoryCountDialog.newInstance()

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: NfcViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view: View = binding.root
        setContentView(view)

        setUpViewModel()
        initialseNfcDialog()

        binding.delivery.setOnClickListener {
            val i = Intent(this@MainActivity, DeliveryCustomerActivity::class.java)
            startActivity(i)
        }

        binding.nfc.setOnClickListener {
            val i = Intent(this@MainActivity, NfcActivity::class.java)
            startActivity(i)
        }

        binding.pack.setOnClickListener {
            val i = Intent(this@MainActivity, PackingCustomerScreen::class.java)
            startActivity(i)
        }

        binding.tech.setOnClickListener {
            val i = Intent(this@MainActivity, TechStationCustomersActivity::class.java)
            startActivity(i)
        }
        binding.waiver.setOnClickListener {
            val i = Intent(this@MainActivity, WaiverCustomerActivity::class.java)
            startActivity(i)
        }
        binding.returno.setOnClickListener {
            val i = Intent(this@MainActivity, ReturnCustomerActivity::class.java)
            startActivity(i)
        }
        binding.profile.setOnClickListener {
            val i = Intent(this@MainActivity, ProfileActivity::class.java)
            startActivity(i)
        }

        binding.inventoryCountLay.setOnClickListener {
            initilizaBarCodeScanDialog()
        }

        binding.transferGearLay.setOnClickListener {
            val i = Intent(this@MainActivity, TranferGearActivity::class.java)
            startActivity(i)
        }
        binding.logout.setOnClickListener {
            val confirmDialog: LogoutDialog = LogoutDialog.newInstance()
            confirmDialog.show(supportFragmentManager, "") //Show the Dialog
            confirmDialog.setOnContinueCancelClick(object : LogoutDialog.OnLogoutClick {
                //listen on Clicked Button
                override fun onContinueClicked() {
                    //  PusherIO.getInstance().disconnectPusher()
                    logout(this@MainActivity)
                }
            })
        }
    }

    fun setUpViewModel() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(NfcViewModel::class.java)
        viewModel.ResponseInventoryCount().observe(this, Observer {
            consumeForceAvailable(it)
        })
    }

    private fun consumeForceAvailable(apiResponse: ApiResponse<SaveFitBootAndSkisByBarcodeModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeForceAvailable",
                    "consumeResponse SUCCESS : ${apiResponse.data}"
                )
                loaderDialog.dismissDialog()
                var data: SaveFitBootAndSkisByBarcodeModel? =
                    apiResponse.data as SaveFitBootAndSkisByBarcodeModel
                if (data != null) {
                    if (data.message != null) {
                        if (data.message.equals(
                                "Inventory is sold. if you want to available send force_back true",
                                true
                            )
                        ) {
                            showForceToPackSkiAndBootDialog(data.parameters)
                        } else {
                            showToast(this, data.message, data.success)
                            if (inventoryCountDialog != null) {
                                if (inventoryCountDialog.isVisible) {
                                    inventoryCountDialog.clearEditText()
                                    inventoryCountDialog.unselectHelmet()
                                }
                            }
                        }
                    }


                }

            }
            Status.ERROR -> {
                Debugger.wtf(
                    "consumeForceAvailable",
                    "consumeResponse ERROR: " + apiResponse.error.toString()
                )
                loaderDialog.dismissDialog()
                showToast(
                    this@MainActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@MainActivity)
            }
        }
    }


    fun initialseNfcDialog() {
        pendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcReaderUtils = NfcReaderUtils()
        nfcReaderUtils.setOnNfcScanListener(object : NfcReaderUtils.OnNfcScanListener {
            override fun nfcBarcode(nfcCode: String) {
                if (inventoryCountDialog != null) {
                    if (inventoryCountDialog.isVisible) {
                        hitInventoryCountApi(nfcCode)
                    }
                }
            }
        })

        if (nfcAdapter != null) {
            nfcAdapter?.let { nfc ->
                nfc.isEnabled?.let { enable ->
                    if (!enable) {
                        openNfcSettings(this)
                    }
                }
            }

        } else {
            // showToast(this, "There is no NFC Functionality in this device")
        }
    }

    fun initilizaBarCodeScanDialog() {
        inventoryCountDialog = InventoryCountDialog.newInstance()
        inventoryCountDialog.show(supportFragmentManager, "") //Show t
        inventoryCountDialog.setOnSubmitListener(object : InventoryCountDialog.OnScanClick {
            override fun onScanBarcode(barcode: String, isBoot: Boolean, isAddon: Boolean) {
                if (inventoryCountDialog != null) {
                    if (inventoryCountDialog.isVisible) {
                        hitInventoryCountApi(barcode)
                    }
                }
            }

            override fun onDismiss() {
                Debugger.wtf("inventoryCountDialog", "onDismiss")
                //  stopNfcScan()
            }
        })
    }

    fun showForceToPackSkiAndBootDialog(
        parameters: HashMap<String, String>,
    ) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "Inventory is sold, if this is a mistake and you want to add it back into live inventory, press 'Yes' ",
                "Yes", "No",
                isOnlyOkButtonVisible = false
            )
        commonDialog.show(supportFragmentManager, "") //S

        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                commonDialog.dismiss()
                hitInventoryCountApi(parameters["barcode"].toString(), true)

            }
        })
    }

    fun hitInventoryCountApi(barcode: String, isForceBack: Boolean = false) {
        val map = HashMap<String, Any>()
        map["barcode"] = barcode
        if (isForceBack) {
            map["force_back"] = true
        }
        var search_type = if (inventoryCountDialog.isHelmetChecked()) "addon" else "inventory"
        viewModel.hitInventoryCountStATUSuPDATE(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            map,
            search_type
        )
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        nfcReaderUtils.resolveIntent(intent!!)
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
    }
}