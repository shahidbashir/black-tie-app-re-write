package com.app.btmobile.UI.Activity.Nfc

import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.nfc.NfcAdapter
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.BaseModel
import com.app.btmobile.Models.NfcModel
import com.app.btmobile.MyApplication
import com.app.btmobile.Status
import com.app.btmobile.UI.Dialog.CommonDialog
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityAddTorqueTestingBinding
import com.app.btmobile.databinding.ActivityNfcBinding
import com.ogoul.kalamtime.viewmodel.NfcViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.util.HashMap
import javax.inject.Inject

class AddTorqueTestingActivity : AppCompatActivity(), RadioGroup.OnCheckedChangeListener {
    private val TAG = this.javaClass.simpleName

    lateinit var binding: ActivityAddTorqueTestingBinding
    lateinit var loaderDialog: LoaderDialog

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: NfcViewModel
    lateinit var barcode: String
    lateinit var skiId: String
    lateinit var manufactureName: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddTorqueTestingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setUpViewModel()
        setupToolBarAndLoader();

        binding.rgTwistLeft.setOnCheckedChangeListener(this)
        binding.rgTwistRight.setOnCheckedChangeListener(this)
        binding.rgForwardLean.setOnCheckedChangeListener(this)
        binding.rgAFD.setOnCheckedChangeListener(this)

        binding.rgTwistLeft2.setOnCheckedChangeListener(this)
        binding.rgTwistRight2.setOnCheckedChangeListener(this)
        binding.rgForwardLean2.setOnCheckedChangeListener(this)
        binding.rgAFD2.setOnCheckedChangeListener(this)

        binding.btnSubmit.setOnClickListener {
            if (checkFields()) {
                binding.run {
                    val map = HashMap<String, String>()
                    map["ski_id"] = skiId

                    map["skier_code"] = etSkierCode.text.toString()
                    map["sole_length"] = etSoleLength.text.toString()
                    map["din_setting"] = etDinSetting.text.toString()

                    //For Ski 1
                    map["ski_1_twist_left_reading"] = etTwistLeft.text.toString()
                    map["ski_1_twist_right_reading"] = etTwistRight.text.toString()
                    map["ski_1_forward_lean_reading"] = etForwardLean.text.toString()

                    map["ski_1_twist_left"] = if (rbPassTwistLeft.isChecked) "1" else "0"
                    map["ski_1_twist_right"] = if (rbPassTwistRight.isChecked) "1" else "0"
                    map["ski_1_forward_lean"] = if (rbPassForwardLean.isChecked) "1" else "0"
                    map["ski_1_afd"] = if (rbPassAFD.isChecked) "1" else "0"

                    //For Ski 2
                    map["ski_2_twist_left_reading"] = etTwistLeft2.text.toString()
                    map["ski_2_twist_right_reading"] = etTwistRight2.text.toString()
                    map["ski_2_forward_lean_reading"] = etForwardLean2.text.toString()

                    map["ski_2_twist_left"] = if (rbPassTwistLeft2.isChecked) "1" else "0"
                    map["ski_2_twist_right"] = if (rbPassTwistRight2.isChecked) "1" else "0"
                    map["ski_2_forward_lean"] = if (rbPassForwardLean2.isChecked) "1" else "0"
                    map["ski_2_afd"] = if (rbPassAFD2.isChecked) "1" else "0"


                    map["condition"] = etCondition.text.toString()
                    if (etNotes.text.toString().trim().isNotEmpty()) {
                        map["notes"] = etNotes.text.toString().trim()
                    }
                    viewModel.hitAdTorqueTestingApi(
                        sharedPrefsHelper.getUser()?.accessToken.toString(),
                        map
                    )
                }
            }

        }
    }

    fun getIntentData() {
        if (intent != null) {
            barcode = intent.getStringExtra("barcode").toString()
            skiId = intent.getStringExtra("skiId").toString()
            manufactureName = intent.getStringExtra("manufactureName").toString()
            binding.tvBarcode.text = barcode
            binding.tvManufacture.text = manufactureName
        }
    }

    fun setUpViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(NfcViewModel::class.java)
        viewModel.AddTorqueTestingResponse().observe(this, Observer {
            consumeResponse(it)
        })
    }

    fun setupToolBarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Add Torque Test"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })

    }

    private fun consumeResponse(apiResponse: ApiResponse<BaseModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                loaderDialog.dismissDialog()
                var data: BaseModel? = apiResponse.data as BaseModel
                clearAllValues()
                if (data != null) {
                    if (data.message != null) {
                        showToast(this, data.message, data.success)
                        showAlert(data.message)
                    }
                }
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@AddTorqueTestingActivity,
                    "${apiResponse.error.toString()}"
                )
                clearAllValues()
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@AddTorqueTestingActivity)
            }
        }
    }

    fun checkFields(): Boolean {
        binding.run {
            if (TextUtils.isEmpty(etSkierCode.text.toString())) {
                showToast(this@AddTorqueTestingActivity, "Enter Skier Code")
                etSkierCode.requestFocus()
                etSkierCode.error = "Enter Skier Code..!!"
                return false
            } else if (TextUtils.isEmpty(etSoleLength.text.toString())) {
                showToast(this@AddTorqueTestingActivity, "Enter Sole Length")
                etSoleLength.requestFocus()
                etSoleLength.error = "Enter Sole Length..!!"
                return false
            } else if (TextUtils.isEmpty(etDinSetting.text.toString())) {
                showToast(this@AddTorqueTestingActivity, "Enter Din Setting")
                etDinSetting.requestFocus()
                etDinSetting.error = "Enter Din Setting..!!"
                return false
            }
//            else if (TextUtils.isEmpty(etTwistLeft.text.toString())) {
//                showToast(this@AddTorqueTestingActivity, "Enter Twist Left Reading")
//                etTwistLeft.requestFocus()
//                etTwistLeft.error = "Enter Twist Left Reading..!!"
//                return false
//            }
            else if (rgTwistLeft.checkedRadioButtonId == -1) {
                rbPFailTwistLeft.error = "Select Option"
                rbPassTwistLeft.error = "Select Option"
                showToast(this@AddTorqueTestingActivity, "Please Check Twist Lefts Options")
                return false
            }
//            else if (TextUtils.isEmpty(etTwistRight.text.toString())) {
//                showToast(this@AddTorqueTestingActivity, "Enter Twist Right Reading")
//                etTwistRight.requestFocus()
//                etTwistRight.error = "Enter Twist Right Reading..!!"
//                return false
//            }
            else if (rgTwistRight.checkedRadioButtonId == -1) {
                rbPassTwistRight.error = "Select Option"
                rbPFailTwistRight.error = "Select Option"
                showToast(this@AddTorqueTestingActivity, "Please Check Twist Rights Options")
                return false
            }
//            else if (TextUtils.isEmpty(etForwardLean.text.toString())) {
//                showToast(this@AddTorqueTestingActivity, "Enter Forward Lean Reading")
//                etForwardLean.requestFocus()
//                etForwardLean.error = "Enter Forward Lean Reading..!!"
//
//                return false
//            }
            else if (rgForwardLean.checkedRadioButtonId == -1) {
                rbPassForwardLean.error = "Select Option"
                rbPFailForwardLean.error = "Select Option"
                showToast(this@AddTorqueTestingActivity, "Please Check Forward Lean Options")
                return false
            } else if (rgAFD.checkedRadioButtonId == -1) {
                rbPassAFD.error = "Select Option"
                rbPFailAFD.error = "Select Option"
                showToast(this@AddTorqueTestingActivity, "Please Check AFD Options")
                return false
            }
//            if (TextUtils.isEmpty(etTwistLeft2.text.toString())) {
//                showToast(this@AddTorqueTestingActivity, "Enter Twist Left Reading")
//                etTwistLeft2.requestFocus()
//                etTwistLeft2.error = "Enter Twist Left Reading..!!"
//                return false
//            }
//            else
                if (rgTwistLeft2.checkedRadioButtonId == -1) {
                rbPFailTwistLeft2.error = "Select Option"
                rbPassTwistLeft2.error = "Select Option"
                showToast(this@AddTorqueTestingActivity, "Please Check Twist Lefts Options")
                return false
            }
//                else if (TextUtils.isEmpty(etTwistRight2.text.toString())) {
//                showToast(this@AddTorqueTestingActivity, "Enter Twist Right Reading")
//                etTwistRight2.requestFocus()
//                etTwistRight2.error = "Enter Twist Right Reading..!!"
//                return false
//            }
                else if (rgTwistRight2.checkedRadioButtonId == -1) {
                rbPassTwistRight2.error = "Select Option"
                rbPFailTwistRight2.error = "Select Option"
                showToast(this@AddTorqueTestingActivity, "Please Check Twist Rights Options")
                return false
            }
//                else if (TextUtils.isEmpty(etForwardLean2.text.toString())) {
//                showToast(this@AddTorqueTestingActivity, "Enter Forward Lean Reading")
//                etForwardLean2.requestFocus()
//                etForwardLean2.error = "Enter Forward Lean Reading..!!"
//                return false
//            }
                else if (rgForwardLean2.checkedRadioButtonId == -1) {
                rbPassForwardLean2.error = "Select Option"
                rbPFailForwardLean2.error = "Select Option"
                showToast(this@AddTorqueTestingActivity, "Please Check Forward Lean Options")
                return false
            } else if (rgAFD2.checkedRadioButtonId == -1) {
                rbPassAFD2.error = "Select Option"
                rbPFailAFD2.error = "Select Option"
                showToast(this@AddTorqueTestingActivity, "Please Check AFD Options")
                return false
            }
            return true
        }
    }

    fun clearAllValues() {
        binding.run {
            etSkierCode.setText("")
            etSoleLength.setText("")
            etDinSetting.setText("")

            etTwistLeft.setText("")
            etTwistRight.setText("")
            etForwardLean.setText("")
            etAFD.setText("")

            etTwistLeft2.setText("")
            etTwistRight2.setText("")
            etForwardLean2.setText("")
            etAFD2.setText("")

            etCondition.setText("")
            etNotes.setText("")

            rbPassTwistLeft.isChecked = false
            rbPassTwistRight.isChecked = false
            rbPassForwardLean.isChecked = false
            rbPassAFD.isChecked = false

            rbPassTwistLeft2.isChecked = false
            rbPassTwistRight2.isChecked = false
            rbPassForwardLean2.isChecked = false
            rbPassAFD2.isChecked = false

            rbPFailTwistLeft.isChecked = false
            rbPFailTwistRight.isChecked = false
            rbPFailForwardLean.isChecked = false
            rbPFailAFD.isChecked = false

            rbPFailTwistLeft2.isChecked = false
            rbPFailTwistRight2.isChecked = false
            rbPFailForwardLean2.isChecked = false
            rbPFailAFD2.isChecked = false
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        binding.run {
            when (group) {
                rgTwistLeft -> {
                    rbPassTwistLeft.error = null
                    rbPFailTwistLeft.error = null
                }
                rgTwistRight -> {
                    rbPassTwistRight.error = null
                    rbPFailTwistRight.error = null
                }
                rgForwardLean -> {
                    rbPassForwardLean.error = null
                    rbPFailForwardLean.error = null
                }
                rgAFD -> {
                    rbPassAFD.error = null
                    rbPFailAFD.error = null
                }
                rgTwistLeft2 -> {
                    rbPassTwistLeft2.error = null
                    rbPFailTwistLeft2.error = null
                }
                rgTwistRight2 -> {
                    rbPassTwistRight2.error = null
                    rbPFailTwistRight2.error = null
                }
                rgForwardLean2 -> {
                    rbPassForwardLean2.error = null
                    rbPFailForwardLean2.error = null
                }
                rgAFD2 -> {
                    rbPassAFD2.error = null
                    rbPFailAFD2.error = null
                }

            }
        }
    }

    fun showAlert(description: String) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                description,
                isOnlyOkButtonVisible = true,
                isDialogCancelable = false
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                finish()
                commonDialog.dismiss()
            }
        })
    }
}