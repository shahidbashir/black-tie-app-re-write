package com.app.btmobile.UI.Activity.Nfc

import android.app.PendingIntent
import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityNfcBinding
import com.google.gson.Gson
import com.ogoul.kalamtime.viewmodel.NfcViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.util.HashMap
import javax.inject.Inject

class NfcActivity : AppCompatActivity() {
    var pendingIntent: PendingIntent? = null
    var nfcAdapter: NfcAdapter? = null
    lateinit var nfcReaderUtils: NfcReaderUtils

    private val TAG = this.javaClass.simpleName

    lateinit var binding: ActivityNfcBinding
    lateinit var loaderDialog: LoaderDialog

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: NfcViewModel
    var skiId: Int = 0
    var manufactureName: String = ""
    var isSkiInventory: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNfcBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViewModel()
        setupToolBarAndLoader();

        pendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcReaderUtils = NfcReaderUtils()
        nfcReaderUtils.setOnNfcScanListener(object : NfcReaderUtils.OnNfcScanListener {
            override fun nfcBarcode(nfcCode: String) {
                binding.etNfc.setText(nfcCode)
                var search_type = if (binding.cbHelmet.isChecked) "addon" else "inventory"
                viewModel.hitNfcScan(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    binding.etNfc.text.toString(),
                    search_type
                )
            }
        })

        if (nfcAdapter != null) {
            nfcAdapter?.let { nfc ->
                nfc.isEnabled?.let { enable ->
                    if (!enable) {
                        openNfcSettings(this)
                    }
                }
            }

        } else {
            showToast(this, "There is no NFC Functionality in this device")
        }


        binding.btnNfScan.setOnClickListener {
            if (binding.etNfc.text.toString().isNotEmpty() &&
                binding.etNfc.text.toString().isNotBlank()
            ) {
                var search_type = if (binding.cbHelmet.isChecked) "addon" else "inventory"
                viewModel.hitNfcScan(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    binding.etNfc.text.toString(),
                    search_type
                )
            } else {
                showToast(this@NfcActivity, "Please enter barcode")
            }
        }

        binding.btnForceAvailable.setOnClickListener {
            if (binding.etNfc.text.toString().isNotEmpty() &&
                binding.etNfc.text.toString().isNotBlank()
            ) {
                viewModel.hitForceAvailable(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    binding.etNfc.text.toString()
                )
            } else {
                showToast(this@NfcActivity, "Please enter barcode")
            }
        }


        binding.btnAddTorque.setOnClickListener {
            startActivity(
                Intent(this, AddTorqueTestingActivity::class.java)
                    .putExtra("barcode", binding.etNfc.text.toString())
                    .putExtra("skiId", skiId.toString())
                    .putExtra("manufactureName", manufactureName)
            )
        }

        binding.cbHelmet.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                if (binding.etNfc.text.toString().isNotEmpty() &&
                    binding.etNfc.text.toString().isNotBlank()
                ) {
                    var search_type = if (binding.cbHelmet.isChecked) "addon" else "inventory"
                    viewModel.hitNfcScan(
                        sharedPrefsHelper.getUser()?.accessToken.toString(),
                        binding.etNfc.text.toString(),
                        search_type
                    )
                }
            }
        }
    }

    fun setUpViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(NfcViewModel::class.java)
        viewModel.NfcScanResponse().observe(this, Observer {
            consumeResponse(it)
        })
        viewModel.ForceAvailableResponse().observe(this, Observer {
            consumeForceAvailable(it)
        })

        viewModel.GetViewTorqueTesting().observe(this, Observer {
            consumeViewTorqueTesting(it)
        })
    }

    fun viewTorqueData(model: ViewTorqueTestData) {
        binding.run {
            tvVTDate.text = model.testedDate
            tvVTStatus.text = model.torqueStatus
            if (model.torqueStatus.equals("pass", true)) {
                tvVTStatus.setTextColor(setColor(this@NfcActivity, R.color.green))
            } else {
                tvVTStatus.setTextColor(setColor(this@NfcActivity, R.color.red))
            }

            tvVTSkierCode.text = model.skierCode.toString()
            tvVTSoleLength.text = model.soleLength.toString()
            tvVTDinSetting.text = model.dinSetting.toString()

            //For Ski 1
            tvVTTwistLeftReading.text = model.ski1TwistLeftReading.toString()
            tvVTTwistRightReading.text = model.ski1TwistRightReading.toString()
            tvVTForwardReading.text = model.ski1ForwardLeanReading.toString()

            checkIfStatusFail(tvVTTwistLeftStatus, model.ski1TwistLeft.toString())
            checkIfStatusFail(tvVTTwistRightStatus, model.ski1TwistRight.toString())
            checkIfStatusFail(tvVTForwardStatus, model.ski1ForwardLean.toString())
            checkIfStatusFail(tvVTAFDStatus, model.ski1Afd.toString())
            //For Ski 2
            tvVTTwistLeftReading2.text = model.ski2TwistLeftReading.toString()
            tvVTTwistRightReading2.text = model.ski2TwistRightReading.toString()
            tvVTForwardReading2.text = model.ski2ForwardLeanReading.toString()

            checkIfStatusFail(tvVTTwistLeftStatus2, model.ski2TwistLeft.toString())
            checkIfStatusFail(tvVTTwistRightStatus2, model.ski2TwistRight.toString())
            checkIfStatusFail(tvVTForwardStatus2, model.ski2ForwardLean.toString())
            checkIfStatusFail(tvVTAFDStatus2, model.ski2Afd.toString())

            tvVTConditions.text = model.condition
            tvVTNotes.text = model.notes
        }

    }

    fun checkIfStatusFail(textView: TextView, isPass: String) {
        if (isPass == "1") {
            textView.text = "Pass"
            textView.setTextColor(setColor(this@NfcActivity, R.color.green))
        } else {
            textView.text = "Fail"
            textView.setTextColor(setColor(this@NfcActivity, R.color.red))
        }

    }

    private fun consumeViewTorqueTesting(apiResponse: ApiResponse<GetTorqueTestModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeViewTorqueTesting",
                    " SUCCESS : ${apiResponse.data}"
                )
                loaderDialog.dismissDialog()
                var model: GetTorqueTestModel? = apiResponse.data as GetTorqueTestModel
                if (model != null) {
                    if (!model.data.torqueStatus.equals("Not Tested", true)) {
                        binding.tvVTLay.visibility = View.VISIBLE
                        viewTorqueData(model.data)
                    }
                } else {
                    binding.tvVTLay.visibility = View.GONE
                }

            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeViewTorqueTesting ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@NfcActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@NfcActivity)
            }
        }
    }

    private fun consumeForceAvailable(apiResponse: ApiResponse<BaseModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                loaderDialog.dismissDialog()
                var data: BaseModel? = apiResponse.data as BaseModel
                if (data != null) {
                    if (data.message != null) {
                        showToast(this, data.message, data.success)
                    }
                    if (data.success) {
                        var search_type = if (binding.cbHelmet.isChecked) "addon" else "inventory"
                        viewModel.hitNfcScan(
                            sharedPrefsHelper.getUser()?.accessToken.toString(),
                            binding.etNfc.text.toString(),
                            search_type
                        )
                    }
                }

            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@NfcActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@NfcActivity)
            }
        }
    }


    fun setupToolBarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Inventory Management"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })

    }

    private fun consumeResponse(apiResponse: ApiResponse<NfcModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                hideAllViews()
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}")
                loaderDialog.dismissDialog()
                binding.cbHelmet.isChecked = false
                var nfcModel = apiResponse.data as NfcModel
                if (nfcModel.status.equals("not-found", true)) {
                    binding.tvStatusName.visibility = View.VISIBLE
                    binding.tvStatusName.text = "Item not found"
                } else {
                    ShowData(nfcModel)
                }
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                hideAllViews()
                loaderDialog.dismissDialog()
                showToast(
                    this@NfcActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@NfcActivity)
            }
        }
    }

    fun hideAllViews() {
        binding.run {
            tvType.text = ""
            tvHelmetType.text = ""
            tvManufacture.text = ""
            tvHelemtManufacture.text = ""
            tvModel.text = ""
            tvHelmetModel.text = ""
            tvSize.text = ""
            tvHelmetSize.text = ""
            tvLength.text = ""
            tvLocation.text = ""
            tvPackage.text = ""
            tvBrokenAt.text = ""
            tvVTSkierCode.text = ""
            tvVTSoleLength.text = ""
            tvVTDinSetting.text = ""
            tvVTTwistLeftReading.text = ""
            tvVTTwistRightReading.text = ""
            tvVTForwardReading.text = ""
            tvVTTwistLeftReading2.text = ""
            tvVTTwistRightReading2.text = ""
            tvVTForwardReading2.text = ""
            tvVTConditions.text = ""
            tvVTNotes.text = ""
            tvReservationId.text = ""
            tvReservationName.text = ""
            tvRenterName.text = ""
            tvReturnDate.text = ""
            tvStatusName.visibility = View.GONE
            btnAddTorque.visibility = View.GONE
            btnForceAvailable.visibility = View.GONE
            layBroken.visibility = View.GONE

            layInventoryDetails.visibility = View.VISIBLE
            cvHelmetDetails.visibility = View.GONE
            tvRenterResDetailsLay.visibility = View.GONE
            tvVTLay.visibility = View.GONE

        }
    }

    fun ShowData(nfcModel: NfcModel) {
        val barcodeResponse = nfcModel.`object`
        if (barcodeResponse != null) {
            binding.run {
                if (barcodeResponse.search_type.equals("Inventory", true)) {
                    cvHelmetDetails.visibility = View.GONE
                    layInventoryDetails.visibility = View.VISIBLE
                    skiId = barcodeResponse.id
                    if (!barcodeResponse.type.isNullOrEmpty()) {


                        tvType.text = barcodeResponse.type
                        isSkiInventory = barcodeResponse.type.equals("ski", ignoreCase = true) ||
                                barcodeResponse.type.equals("skier", ignoreCase = true)

                        if (isSkiInventory) {
                            viewModel.hitViewTorqueTestingApi(
                                sharedPrefsHelper.getUser()?.accessToken.toString(),
                                binding.etNfc.text.toString()
                            )
                            btnAddTorque.visibility = View.VISIBLE
                        }
                    }
                    if (!barcodeResponse.manufacture.isNullOrEmpty()) {
                        manufactureName = barcodeResponse.manufacture
                        tvManufacture.text = manufactureName
                    }
                    if (!barcodeResponse.model.isNullOrEmpty()) {
                        tvModel.text = barcodeResponse.model
                    }
                    if (!barcodeResponse.size.isNullOrEmpty()) {
                        laySize.visibility = View.VISIBLE
                        tvSize.text = barcodeResponse.size
                    } else {
                        laySize.visibility = View.GONE
                    }
                    if (!barcodeResponse.length.isNullOrEmpty()) {
                        layLength.visibility = View.VISIBLE
                        tvLength.text = barcodeResponse.length
                    } else {
                        layLength.visibility = View.GONE
                    }
                    if (!barcodeResponse.inventory_location_name.isNullOrEmpty()) {
                        tvLocation.text = barcodeResponse.inventory_location_name
                    }
                    if (barcodeResponse.packages_resort != null) {
                        if (barcodeResponse.packages_resort.isNotEmpty()) {
                            tvPackage.text = (barcodeResponse.packages_resort[0])
                        }
                    }
                    if (!barcodeResponse.status.isNullOrEmpty()) {
                        tvStatusName.visibility = View.VISIBLE
                        tvStatusName.text = barcodeResponse.status
                        if (barcodeResponse.status.equals("Rented", true)) {
                            tvStatusName.setBackgroundColor(
                                setColor(
                                    this@NfcActivity,
                                    R.color.yellow
                                )
                            )
//                            if (isSkiInventory) {
                                showDetailsCard(barcodeResponse)
//                            }
                        } else {
                            tvStatusName.setBackgroundColor(
                                setColor(
                                    this@NfcActivity,
                                    R.color.blue
                                )
                            )
                        }

                        if (barcodeResponse.status.equals("Broken", true)) {
                            layBroken.visibility = View.VISIBLE
                            tvBrokenAt.text = barcodeResponse.broken_at
                        } else {
                            layBroken.visibility = View.GONE
                            tvBrokenAt.text = ""
                        }
                    }
                    if (!barcodeResponse.status.equals("available", true)) {
                        binding.btnForceAvailable.visibility = View.VISIBLE
                    }
                } else {
                    cvHelmetDetails.visibility = View.VISIBLE
                    layInventoryDetails.visibility = View.GONE

                    tvHelmetType.text = barcodeResponse.type
                    if (!barcodeResponse.manufacture.isNullOrEmpty()) {
                        tvHelemtManufacture.text = barcodeResponse.manufacture
                    }
                    if (!barcodeResponse.model.isNullOrEmpty()) {
                        tvHelmetModel.text = barcodeResponse.model
                    }
                    if (!barcodeResponse.size.isNullOrEmpty()) {
                        layHelmetSize.visibility = View.VISIBLE
                        tvHelmetSize.text = barcodeResponse.size
                    } else {
                        layHelmetSize.visibility = View.GONE
                    }

                    if (!barcodeResponse.status.isNullOrEmpty()) {
                        tvHelmetStatus.visibility = View.VISIBLE
                        tvHelmetStatus.text = barcodeResponse.status
                        if (barcodeResponse.status.equals("Rented", true)
                            || barcodeResponse.status.equals("Fitted", true)
                        ) {
                            tvHelmetStatus.setBackgroundColor(
                                setColor(
                                    this@NfcActivity,
                                    R.color.yellow
                                )
                            )
                            showDetailsCard(barcodeResponse)

                        } else {
                            tvHelmetStatus.setBackgroundColor(
                                setColor(
                                    this@NfcActivity,
                                    R.color.blue
                                )
                            )
                        }

                    }

                }
            }
        }
    }

    fun showDetailsCard(barcodeResponse: BarcodeData) {
        binding.run {
            if (barcodeResponse.detail != null) {
                tvRenterResDetailsLay.visibility = View.VISIBLE
                tvReservationId.text = barcodeResponse.detail.reservation_id
                tvReservationName.text = barcodeResponse.detail.reservation_name
                tvRenterName.text = barcodeResponse.detail.renter_name
                tvReturnDate.text = barcodeResponse.detail.return_date
            }

        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        nfcReaderUtils.resolveIntent(intent!!)
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
        if (binding.etNfc.text.toString().isNotEmpty() &&
            binding.etNfc.text.toString().isNotBlank()
        ) {
            var search_type = if (binding.cbHelmet.isChecked) "addon" else "inventory"
            viewModel.hitNfcScan(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                binding.etNfc.text.toString(),
                search_type
            )
        }
    }

    override fun onPause() {
        super.onPause()
        if (nfcAdapter != null) {
            nfcAdapter!!.disableForegroundDispatch(this)
        }
    }
}