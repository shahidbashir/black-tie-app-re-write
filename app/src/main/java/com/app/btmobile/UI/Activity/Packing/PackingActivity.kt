package com.app.btmobile.UI.Activity.Packing

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.nfc.NfcAdapter
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.app.btmobile.*
import com.app.btmobile.Models.*
import com.app.btmobile.UI.Activity.Fitting.FittingActivity
import com.app.btmobile.UI.Adapter.PackingSkiBootsAdapter
import com.app.btmobile.UI.Dialog.*
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityPackingBinding
import com.google.gson.Gson
import com.ogoul.kalamtime.viewmodel.DeliveryViewModel
import com.ogoul.kalamtime.viewmodel.PackingViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class PackingActivity : AppCompatActivity(), View.OnClickListener {
    var pendingIntent: PendingIntent? = null
    var nfcAdapter: NfcAdapter? = null
    lateinit var nfcReaderUtils: NfcReaderUtils

    private val TAG = this.javaClass.simpleName

    lateinit var binding: ActivityPackingBinding
    lateinit var loaderDialog: LoaderDialog

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var packingViewModel: PackingViewModel
    lateinit var deliveryViewModel: DeliveryViewModel


    var confirmOwnBoot = false

    var renter_id = 0
    var shipment_id: Int = 0
    var renterName: String = ""
    var inventoryType = ""

    var switchOutNotes = ""
    var switchOutReason = ""

    var isSwitchOut = false
    var isFirstTime = true


    var clothingListFinal: MutableList<ClothingModel> = ArrayList()
    val spinnerArray: MutableList<String> = ArrayList()

    lateinit var spinnerArrayAdapter: ArrayAdapter<String>


    var allItems1: ArrayList<PackedInventoryModel> = ArrayList()
    var allItems2: ArrayList<PackedInventoryModel> = ArrayList()
    var allItems3: ArrayList<PackedInventoryModel> = ArrayList()

    lateinit var adapter1: PackingSkiBootsAdapter
    lateinit var adapter2: PackingSkiBootsAdapter
    lateinit var adapter3: PackingSkiBootsAdapter

    var addScanBarcodeDialog: AddScanBarcodeDialog = AddScanBarcodeDialog.newInstance()
    lateinit var dialogShowAddOnsList: ShowPackedAddonsListDialog

    lateinit var packingData: PackingData

    fun setUpClickListeners() {
        binding.addSkiBootBigBtn.setOnClickListener(this)
        binding.addSkiBigBtn.setOnClickListener(this)
        binding.addHelmetBigBtn.setOnClickListener(this)

        binding.addSkiBootSmallBtn.setOnClickListener(this)
        binding.addSkiSmallBtn.setOnClickListener(this)
        binding.addHelmetSmallBtn.setOnClickListener(this)
        binding.viewHistoryTv.setOnClickListener(this)

        binding.tvPolePackStatus.setOnClickListener(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPackingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setUpViewModel()
        initialseSpinner()
        setupToolBarAndLoader()
        setUpRecyclerViews()
        setUpClickListeners()
        initialseNfcDialog()

        binding.ownBootCB.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            confirmOwnBoot = b
            val map = HashMap<String, String>()
            map["renter_id"] = renter_id.toString()
            map["bring_boot"] = if (b) "1" else "0"
            packingViewModel.hitOwnBootApi(
                sharedPrefsHelper.getUser()?.accessToken.toString(),
                map
            )
        })
    }

    fun initialseNfcDialog() {
        pendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcReaderUtils = NfcReaderUtils()
        nfcReaderUtils.setOnNfcScanListener(object : NfcReaderUtils.OnNfcScanListener {
            override fun nfcBarcode(nfcCode: String) {
                if (addScanBarcodeDialog != null) {
                    if (addScanBarcodeDialog.isVisible) {
                        Debugger.wtf("nfcBarcode", nfcCode + "/ " + addScanBarcodeDialog.isAddon)
                        if (addScanBarcodeDialog.isAddon) {
                            hitSaveAddonWithBarcode(nfcCode, addScanBarcodeDialog.addonStatus)
                        } else {
                            hitBarCodeUploadApi(nfcCode, isBoot = addScanBarcodeDialog.isBoot)
                        }
                        addScanBarcodeDialog.dismiss()
                    }
                }
            }
        })

        if (nfcAdapter != null) {
            nfcAdapter?.let { nfc ->
                nfc.isEnabled?.let { enable ->
                    if (!enable) {
                        openNfcSettings(this)
                    }
                }
            }

        } else {
            showToast(this, "There is no NFC Functionality in this device")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.packing_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.goToFit -> {
                moveToFittingScreen()
            }
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun initialseSpinner() {
        binding.run {
            spinnerArray.add("Select")

            for (i in 30..54 step 2) {
                spinnerArray.add(i.toString())
            }

            spinnerArrayAdapter = ArrayAdapter<String>(
                this@PackingActivity, android.R.layout.simple_spinner_item,
                spinnerArray
            )

            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerPolePack.adapter = spinnerArrayAdapter
            spinnerPolePack.addOnAttachStateChangeListener(object :
                View.OnAttachStateChangeListener {
                override fun onViewAttachedToWindow(view: View) {}
                override fun onViewDetachedFromWindow(view: View) {}
            })

            spinnerPolePack.onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(
                    adapterView: AdapterView<*>?,
                    view: View,
                    i: Int,
                    l: Long
                ) {
                    if (spinnerPolePack.selectedItem.toString() != "Select") {
                        if (!isFirstTime) {
                            packPoleAlert()
                        }
                        isFirstTime = false
                    }
                }

                override fun onNothingSelected(adapterView: AdapterView<*>?) {}
            }
        }
    }

    fun getIntentData() {
        if (intent != null) {
            renter_id = intent.getIntExtra("renterId", 0)
            shipment_id = intent.getIntExtra("shipmentId", 0)
            renterName = intent.getStringExtra("renterName")!!
        }
    }

    fun setUpViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        packingViewModel = ViewModelProvider(this, factory).get(PackingViewModel::class.java)
        deliveryViewModel = ViewModelProvider(this, factory).get(DeliveryViewModel::class.java)
        packingViewModel.renterPackFitModel().observe(this, Observer {
            consumePackingData(it)
        })
        packingViewModel.ownBootResponse().observe(this, Observer {
            consumeOwnBootResponse(it)
        })
        deliveryViewModel.getRenterHistoryResponse().observe(this, androidx.lifecycle.Observer {
            consumeRenterHistoryResponse(it)
        })
        packingViewModel.addPackedItemResponse().observe(this, Observer {
            consumeAddPackedItemResponse(it)
        })
        packingViewModel.addPolesResponse().observe(this, Observer {
            consumeAddPolesResponse(it)
        })
        packingViewModel.getAddOnsListResponse().observe(this, Observer {
            consumeGetAddonsResponse(it)
        })
        packingViewModel.getResponseSaveAddonsWithBarcode().observe(this, Observer {
            consumeSaveAddonsWithBarcode(it)
        })
    }

    private fun consumeSaveAddonsWithBarcode(apiResponse: ApiResponse<SaveAddonWithBarcodeModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeSaveAddonsWithBarcode",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )
                var model: SaveAddonWithBarcodeModel =
                    (apiResponse.data as SaveAddonWithBarcodeModel)
                if (model.status) {
                    if (dialogShowAddOnsList != null) {
                        if (dialogShowAddOnsList.isVisible) {
                            dialogShowAddOnsList.checkAddonItem(
                                model.addon_id,
                                model.addon_status,
                                model.helmet_size
                            )
                        }
                    }
                }
                showToast(this, model.message, model.status)
                loaderDialog.dismissDialog()
                getPackingData()
            }
            Status.ERROR -> {
                Debugger.wtf(
                    TAG, "consumeResponse ERROR: " + apiResponse.error.toString()
                )
                loaderDialog.dismissDialog()

                showToast(
                    this@PackingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@PackingActivity)
            }
        }

    }

    private fun consumePackingData(apiResponse: ApiResponse<PackingModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                try {
                    Debugger.wtf("consumePackingData", "Success: ")
                    var model: PackingModel = (apiResponse.data as PackingModel)
                    if (model.packingData.isNotEmpty()) {
                        packingData = model.packingData.first()
                        loadData(packingData)
                    }
                    Debugger.wtf("consumePackingData", "Load All Data: ")
                } catch (e: Exception) {
                    Debugger.wtf("consumePackingData", "Exception: ${e.message}")
                } finally {
                    Debugger.wtf("consumePackingData", "Finally: ")
                    loaderDialog.dismissDialog()
                }
            }
            Status.ERROR -> {
                Debugger.wtf("consumePackingData", "ERROR: ${apiResponse.error.toString()}")
                loaderDialog.dismissDialog()
                showToast(
                    this@PackingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@PackingActivity)
            }
        }

    }

    private fun consumeRenterHistoryResponse(
        apiResponse:
        ApiResponse<RenterHistoryModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var data: RenterHistoryModel? = apiResponse.data as RenterHistoryModel
                if (data != null) {
                    if (data.success) {
                        val renterHistoryDialog: RenterHistoryDialog =
                            RenterHistoryDialog.newInstance(
                                data.data.boots_history.toString() + "\n \n" +
                                        data.data.skis_history,
                                data.data.renterName.toString() + "'s History"
                            )
                        renterHistoryDialog.show(supportFragmentManager, "") //S
                    }
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@PackingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@PackingActivity)
            }
        }
    }

    private fun consumeOwnBootResponse(
        apiResponse:
        ApiResponse<OwnBootModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                loaderDialog.dismissDialog()
                getPackingData()

            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@PackingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@PackingActivity)
            }
        }
    }


    private fun consumeAddPackedItemResponse(
        apiResponse:
        ApiResponse<SaveFitBootAndSkisByBarcodeModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeAddPackedItemResponse",
                    "consumeResponse SUCCESS : ${apiResponse.data}"
                )
                try {
                    var data: SaveFitBootAndSkisByBarcodeModel? =
                        apiResponse.data as SaveFitBootAndSkisByBarcodeModel
                    if (data != null) {
                        if (data.message != null) {
                            if (data.message.equals("Equipment not available.", true)) {
                                showForceToPackSkiAndBootDialog(data.parameters, data.isForBoot)
                            } else {
                                showToast(this, data.message, data.success)
                                getPackingData()
                            }
                        }
                    }
                } catch (e: Exception) {
                    Debugger.wtf(TAG, "Erorrr-------------> : ${e.message}")
                } finally {
                    loaderDialog.dismissDialog()
                }

            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@PackingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@PackingActivity)
            }
        }
    }

    private fun consumeAddPolesResponse(
        apiResponse:
        ApiResponse<BaseModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var data: BaseModel? = apiResponse.data as BaseModel
                if (data != null) {
                    showToast(this, data.message, data.success)
                }

                loaderDialog.dismissDialog()
                getPackingData()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@PackingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@PackingActivity)
            }
        }
    }

    private fun consumeGetAddonsResponse(
        apiResponse:
        ApiResponse<AddonsModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeGetAddonsResponse",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )
                var data: AddonsModel? = apiResponse.data as AddonsModel
                if (data != null) {
                    var onlyPackAddons: List<Addon> = data.addons.filter {
                        it.isPackable && it.purchased_addon
                    }
                    var onlyPackAddonsBarcode: List<Addon> = data.addonsWithBarcodeList.filter {
                        it.isPackable && it.purchased_addon
                    }
                    var onlyPackClothing: List<ClothingModel> = data.clothing.filter {
                        it.purchased_clothing
                    }

                    val allAddons: ArrayList<Addon> = ArrayList()
                    allAddons.addAll(onlyPackAddons)
                    allAddons.addAll(onlyPackAddonsBarcode)
//                    showAddOnsListDialog(
//                        allAddons,
//                        clothingListFinal as ArrayList<ClothingModel>
//                    )
                    showAddOnsListDialog(
                        allAddons,
                        onlyPackClothing as ArrayList<ClothingModel>
                    )
                }

                loaderDialog.dismissDialog()

            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@PackingActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@PackingActivity)
            }
        }
    }

    fun setupToolBarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = renterName
        supportActionBar!!.subtitle = "Renter ID: $renter_id"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })

    }

    fun getPackingData() {
        packingViewModel.hitRenterPackFitData(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            shipment_id,
            renter_id
        )
    }

    fun hitBarCodeUploadApi(barcode: String, force_back: String = "0", isBoot: Boolean) {
        val map = HashMap<String, String>()
        map["renter_id"] = renter_id.toString() + ""
        map["van_shipment_id"] = shipment_id.toString() + ""
        map["barcode"] = barcode
        map["force_back"] = force_back


        packingViewModel.hitAddPackedItems(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            shipment_id,
            map,
            isBoot
        )
    }

    fun hitSaveAddonWithBarcode(barcode: String, addOnStatus: String) {
        val map = HashMap<String, String>()
        map["renter_id"] = renter_id.toString() + ""
        map["barcode"] = barcode
        map["status"] = addOnStatus
        if (addOnStatus.equals("Returned", true)) {
            map["van_shipment_id"] = shipment_id.toString()
        }

        packingViewModel.hitSaveAddonsWithBarcodeApi(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            map
        )
    }

    fun hitPackPoleApi() {
        val map = HashMap<String, String>()
        map["poles"] = binding.spinnerPolePack.selectedItem.toString()
        map["renter_id"] = renter_id.toString()
        packingViewModel.hitPackPolesApi(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            map
        )
    }

    fun hitGetAddOnsApi() {
        packingViewModel.hitGetAddonsList(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            renter_id
        )
    }

    fun setUpRecyclerViews() {
        binding.run {
            adapter1 = PackingSkiBootsAdapter(this@PackingActivity, allItems1, false, true, true)
            val mLayoutManager1: RecyclerView.LayoutManager =
                LinearLayoutManager(this@PackingActivity)
            rvPackingSkiBoot.layoutManager = mLayoutManager1
            rvPackingSkiBoot.itemAnimator = DefaultItemAnimator()
            rvPackingSkiBoot.adapter = adapter1
            adapter2 = PackingSkiBootsAdapter(this@PackingActivity, allItems2, false, true, true)
            val mLayoutManager2: RecyclerView.LayoutManager =
                LinearLayoutManager(this@PackingActivity)
            rvPackingSki.layoutManager = mLayoutManager2
            rvPackingSki.itemAnimator = DefaultItemAnimator()
            rvPackingSki.adapter = adapter2
            adapter3 = PackingSkiBootsAdapter(this@PackingActivity, allItems3, true, true, true)
            val mLayoutManager3: RecyclerView.LayoutManager =
                LinearLayoutManager(this@PackingActivity)
            rvPackingHelmet.layoutManager = mLayoutManager3
            rvPackingHelmet.itemAnimator = DefaultItemAnimator()
            rvPackingHelmet.adapter = adapter3
        }
    }

    fun initilizaBarCodeScanDialog(
        isBoot: Boolean = false, isAddon: Boolean = false,
        addOnsStatus: String = ""
    ) {
        addScanBarcodeDialog = AddScanBarcodeDialog.newInstance(isBoot, isAddon, addOnsStatus)
        addScanBarcodeDialog.show(supportFragmentManager, "") //Show t
        addScanBarcodeDialog.setOnSubmitListener(object : AddScanBarcodeDialog.OnScanClick {
            override fun onScanBarcode(barcode: String, isBoot: Boolean, isAddon: Boolean) {
                if (addScanBarcodeDialog != null) {
                    if (addScanBarcodeDialog.isVisible) {
                        Debugger.wtf("nfcBarcode", barcode + "/ " + addScanBarcodeDialog.isAddon)
                        if (isAddon) {
                            hitSaveAddonWithBarcode(barcode, addScanBarcodeDialog.addonStatus)
                        } else {
                            hitBarCodeUploadApi(barcode, isBoot = isBoot)
                        }
                        addScanBarcodeDialog.dismiss()
                    }
                }
            }

            override fun onDismiss() {
                Debugger.wtf("addScanBarcodeDialog", "onDismiss")
                //  stopNfcScan()
            }
        })
    }

    private fun loadData(model: PackingData) {
        binding.run {
            Debugger.wtf("loadData", Gson().toJson(model))
            ownBootCB.isChecked = model.ownBoot
            confirmOwnBoot = model.ownBoot
            inventoryType = model.type
            if (model.type != null) {
                if (inventoryType.equals("ski", ignoreCase = true) ||
                    inventoryType.equals("skier", ignoreCase = true)
                ) {
                    if (model.dinSetting != null && model.dinSetting != "N/A") {
                        dinPacking.visibility = View.VISIBLE
                        dinPacking.text = "DIN " + model.dinSetting
                    } else {
                        dinPacking.visibility = View.GONE
                    }
                }
                if (model.haveHistory) {
                    viewHistoryTv.visibility = View.VISIBLE
                } else {
                    viewHistoryTv.visibility = View.GONE
                }
                if (model.isSkiPacked) {
                    SkiPackStatusLay.visibility = View.VISIBLE
                } else {
                    SkiPackStatusLay.visibility = View.GONE
                }
                if (model.isBootFitted) {
                    SkiFittedStatusLay.visibility = View.VISIBLE
                } else {
                    SkiFittedStatusLay.visibility = View.GONE
                }

                if (model.isBootPacked) {
                    BootPackStatusLay.visibility = View.VISIBLE
                } else {
                    BootPackStatusLay.visibility = View.GONE
                }
                if (model.isBootFitted) {
                    BootFittedStatusLay.visibility = View.VISIBLE
                } else {
                    BootFittedStatusLay.visibility = View.GONE
                }
                if (model.recommendedPole != null) {
                    if (model.recommendedPole.toString().isNotEmpty()) {
                        layRecommendedPole.visibility = View.VISIBLE
                        tvRecommendedPole.text = model.recommendedPole.toString()
                    } else {
                        layRecommendedPole.visibility = View.GONE
                    }
                } else {
                    layRecommendedPole.visibility = View.GONE
                }

                if (model.packageX != null) {
                    packageTypeTv.text = model.packageX.toString()
                }
                if (model.age != null) {
                    packingAgeTv.text = model.age.toString()
                }
                if (model.gender != null) {
                    packingGenderTv.text = model.gender.toString()
                }
                if (model.height != null) {
                    packingHeightTv.text = model.height.toString()
                }
                if (model.weight != null) {
                    packingWeightTv.text = model.weight.toString()
                }
                if (model.shoeSize != null) {
                    packingShoeSizeTv.text = model.shoeSize.toString()
                }
                if (model.shoeType != null) {
                    packingShoeTypeTv.text = model.shoeType.toString()
                }
                if (model.ability != null) {
                    packingAbilityTv.text = model.ability.toString()
                }
                if (model.isPackAddonsAvailable) {
                    addHelmetBigTv.setTextColor(setColor(this@PackingActivity, R.color.red))
                } else {
                    addHelmetBigTv.setTextColor(setColor(this@PackingActivity, R.color.green))
                }
                if (!model.clothingList.isNullOrEmpty()) {
                    for (i in 0 until model.clothingList.size) {
                        if (model.clothingList.get(i).status == "Packed") {
                            ClothingPackStatusLay.visibility = View.VISIBLE
                            ClothingFitStatusLay.visibility = View.GONE
                        } else if (model.clothingList.get(i).status == "Fitted" ||
                            model.clothingList.get(i).status == "Returned"
                        ) {
                            ClothingPackStatusLay.visibility = View.VISIBLE
                            ClothingFitStatusLay.visibility = View.VISIBLE
                        } else {
                            ClothingPackStatusLay.visibility = View.GONE
                            ClothingPackStatusLay.visibility = View.GONE
                        }

                    }
                }
                if (confirmOwnBoot) {
                    rvPackingSkiBoot.visibility = View.GONE
                    addSkiBootBigBtn.visibility = View.VISIBLE
                    addSkiBootSmallBtn.visibility = View.GONE

                    premiumSkiBootTv.text = "Own Boot \n"
                    bootPackTv.text = "Own Boot"
                    bootPackTv.setTextColor(setColor(this@PackingActivity, R.color.green))
                    if (model.packageX != null) {
                        //premiumSkisTv.text=model.getPackage().toString();
                    }
                } else {

                    bootPackTv.text = "Pack"
                    bootPackTv.setTextColor(setColor(this@PackingActivity, R.color.red))

                    if (!model.bootList.isNullOrEmpty()) {
                        showBootRv(model.bootList)
                    } else if (!model.fittedBootList.isNullOrEmpty()) {
                        showBootRv(model.fittedBootList)
                    } else {
                        rvPackingSkiBoot.visibility = View.GONE
                        addSkiBootBigBtn.visibility = View.VISIBLE
                        addSkiBootSmallBtn.visibility = View.GONE
                    }

                }
                if (!model.skiList.isNullOrEmpty()) {
                    showSkiRv(model.skiList)
                } else if (!model.fittedSkiList.isNullOrEmpty()) {
                    showSkiRv(model.fittedSkiList)
                } else {
                    rvPackingSki.visibility = View.GONE
                    addSkiBigBtn.visibility = View.VISIBLE
                    addSkiSmallBtn.visibility = View.GONE

                }
                if (!model.addonList.isNullOrEmpty()) {
                    showAddOnsRv(model.addonList)
                } else if (!model.fittedAddonList.isNullOrEmpty()) {
                    showAddOnsRv(model.addonList)
                } else {
                    rvPackingHelmet.visibility = View.GONE
                    addHelmetBigBtn.visibility = View.VISIBLE
                    addHelmetSmallBtn.visibility = View.GONE
                }
            } else {
                showToast(this@PackingActivity, "No Record Found")
            }
            newLoadData(model)
        }
    }

    fun showBootRv(bootList: List<PackedInventoryModel>) {
        allItems1.clear()
        allItems1.addAll(bootList)
        adapter1.notifyDataSetChanged()
        binding.run {
            rvPackingSkiBoot.visibility = View.VISIBLE
            addSkiBootBigBtn.visibility = View.GONE
            addSkiBootSmallBtn.visibility = View.VISIBLE
        }
    }

    fun showSkiRv(skisList: List<PackedInventoryModel>) {
        allItems2.clear()
        allItems2.addAll(skisList)
        adapter2.notifyDataSetChanged()
        binding.run {
            rvPackingSki.visibility = View.VISIBLE
            addSkiBigBtn.visibility = View.GONE
            addSkiSmallBtn.visibility = View.VISIBLE
        }

    }

    fun showAddOnsRv(addOnsList: List<PackedInventoryModel>) {
        allItems3.clear()
        allItems3.addAll(addOnsList)
        adapter3.notifyDataSetChanged()
        binding.run {
            rvPackingHelmet.visibility = View.VISIBLE
            addHelmetBigBtn.visibility = View.GONE
            addHelmetSmallBtn.visibility = View.VISIBLE
        }

    }

    @SuppressLint("RestrictedApi")
    fun newLoadData(model: PackingData) {
        binding.run {
            isSwitchOut = model.isSwitchOut
            if (model.type.equals("snowboard", true)) {
                layPoles.visibility = View.GONE
                dinSettings.visibility = View.GONE
                layRecommendedPole.visibility = View.GONE
            }
            if (!model.poles.isNullOrEmpty()) {
                isFirstTime = true
                val position: Int = spinnerArrayAdapter.getPosition(model.poles)
                spinnerPolePack.setSelection(position, true)
            }
            if (!model.poleStatus.isNullOrEmpty()) {

                //if (model.poleStatus.equals("Packed", true)) {
                tvPolePackStatus.supportBackgroundTintList = (
                        ContextCompat.getColorStateList(
                            this@PackingActivity, R.color.green
                        ))
                //}
            }
            var upgradedAddon = ""
            if (!model.assignedUpgradeAddonList.isNullOrEmpty()) {
                layPackBootText.visibility = View.VISIBLE
                for (i in 0 until model.assignedUpgradeAddonList.size) {
                    upgradedAddon = if (i == model.assignedUpgradeAddonList.size - 1) {
                        upgradedAddon + model.assignedUpgradeAddonList.get(i).name
                    } else {
                        upgradedAddon + model.assignedUpgradeAddonList.get(i).name
                            .toString() + ","
                    }

                }
                Debugger.wtf("Model", "->" + model.assignedUpgradeAddonList.size)
            } else {
                layPackBootText.visibility = View.GONE
            }
            if (upgradedAddon != null) {
                if (upgradedAddon == "Premium Boot Upgrade - With HEATED BOOTS") {
                    tvBoots.text = "Heated Boots"
                } else {
                    tvBoots.text = upgradedAddon
                }
            }
            Debugger.wtf("getAssignedAddonList", "" + model.assignedAddonList.size)
            val stringList: MutableList<String> = ArrayList()
            if (model.assignedAddonList != null) {
                if (model.assignedAddonList.size > 0) {
//                        ArrayList<NewPackingModel.Datum.AssignedAddonList> assignedAddonLists = new ArrayList<>();
                    var name = ""
                    stringList.clear()
                    for (i in 0 until model.assignedAddonList.size) {
//                            if (!model.assignedAddonList.get(i).getStatus().equals("Not Packed")) {
                        stringList.add(model.assignedAddonList.get(i).name)
                        //                            }
                    }
                    Debugger.wtf("stringList", "->" + stringList.size)
                    if (stringList.size > 0) {
                        for (i in stringList.indices) {
                            name = if (i == stringList.size - 1) {
                                if (stringList[i].equals("Damage Waiver", ignoreCase = true)) {
                                    name + "D.W."
                                } else if (stringList[i].equals(
                                        "Premium Boot Upgrade - With HEATED BOOTS",
                                        ignoreCase = true
                                    )
                                ) {
                                    name + "Heated Boots"
                                } else {
                                    name + stringList[i]
                                }
                            } else {
                                if (stringList[i].equals("Damage Waiver", ignoreCase = true)) {
                                    name + "D.W." + ", "
                                } else if (stringList[i].equals(
                                        "Premium Boot Upgrade - With HEATED BOOTS",
                                        ignoreCase = true
                                    )
                                ) {
                                    name + "Heated Boots" + ", "
                                } else {
                                    name + stringList[i] + ", "
                                }
                            }
                        }
                    }
                    //                        tvClothing.text=model.getClothing());
                    tvAssignedAddOnList.text = name
                    var addOnStatus: String = ""
                    if (!model.assignedAddonList.isNullOrEmpty()) {
                        model.assignedAddonList.forEach {
                            if (it.status == "Packed") {
                                addOnStatus = "Packed"
                            }
                            if (it.status == "Fitted") {
                                addOnStatus = "Fitted"
                            }
                        }
                    }

                    if (addOnStatus == "Packed") {
                        addOnsPackedStatus.visibility = View.VISIBLE
                        addOnsFitStatus.visibility = View.GONE
                    } else if (addOnStatus == "Fitted") {
                        addOnsPackedStatus.visibility = View.VISIBLE
                        addOnsFitStatus.visibility = View.VISIBLE
                    } else {
                        addOnsPackedStatus.visibility = View.GONE
                        addOnsFitStatus.visibility = View.GONE
                    }
                    //                        Debugger.wtf("abc", "->" + model.assignedAddonList.get(0).getPremiumBoot());
                }
            }
            val clothingList: MutableList<String> = ArrayList()
            var nameClothing = ""
            if (model.clothingList != null) {
                if (model.clothingList.size > 0) {
                    clothingList.clear()
                    clothingListFinal.clear()
                    for (i in 0 until model.clothingList.size) {
                        if (model.clothingList.get(i).is_packable) {
                            clothingListFinal.add(model.clothingList.get(i))
                            clothingList.add(model.clothingList.get(i).name)
                        }
                    }
                }
                if (clothingList.size > 0) {
                    Debugger.wtf("clothingLatest2", "->" + clothingList.size)
                    for (i in clothingList.indices) {
                        if (i == clothingList.size - 1) {
                            nameClothing = nameClothing + clothingList[i]
                            Debugger.wtf("clothingLatest3", "->$nameClothing")
                        } else {
                            nameClothing = nameClothing + clothingList[i] + ", "
                            Debugger.wtf("clothingLatest4", "->$nameClothing")
                        }
                    }
                }
                Debugger.wtf("clothingLatest5", "" + clothingList.size + " -> " + nameClothing)
                tvClothing.text = nameClothing
            }
            if (model.bootList.size > 0) {
                if (!model.bootList.get(0).soleLength.isNullOrEmpty()) {
                    tvSoleLengthPacking.text = model.bootList.get(0).soleLength
                }
            } else if (model.fittedBootList.size > 0) {
                if (!model.fittedBootList.get(0).soleLength.isNullOrEmpty()) {
                    tvSoleLengthPacking.text =
                        model.fittedBootList.get(0).soleLength

                }
            } else {
                tvSoleLengthPacking.text = "N/A"
            }
            if (model.manualDin != "N/A") {
                btnDin.background.setColorFilter(
                    Color.parseColor("#2dd57b"),
                    PorterDuff.Mode.SRC_ATOP
                )
                tvDinSettingsPacking.text = model.manualDin
            } else if (model.recommendedDin != "N/A") {
                tvDinSettingsPacking.text = model.recommendedDin
            } else {
                tvDinSettingsPacking.text = "N/A"
            }

//            if (!model.dinSetting.equals("")) {
//                tvDinSettingsPacking.text = model.dinSetting
//            }
            if (model.skierCode != null) {
                tvSkierCodePacking.text = model.skierCode.toString()
            } else {
                tvSkierCodePacking.text = "N/A"
            }
        }

    }

    override fun onClick(v: View?) {
        when (v) {
            binding.viewHistoryTv -> {
                deliveryViewModel.hitGetRenterHistory(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    renter_id,
                    renterName
                )
            }
            binding.addSkiBootBigBtn, binding.addSkiBootSmallBtn -> {
                if (!confirmOwnBoot) {
                    initilizaBarCodeScanDialog(isBoot = true)
                }

            }
            binding.addSkiBigBtn, binding.addSkiSmallBtn -> {
                initilizaBarCodeScanDialog(isBoot = false)
            }
            binding.addHelmetBigBtn, binding.addHelmetSmallBtn -> {
                hitGetAddOnsApi()
            }
            binding.tvPolePackStatus -> {
                if (binding.spinnerPolePack.selectedItem.toString() != "Select") {
                    packPoleAlert()
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        nfcReaderUtils.resolveIntent(intent!!)
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
        getPackingData()
    }

    override fun onPause() {
        super.onPause()
        // stopNfcScan()
    }

    fun stopNfcScan() {
        if (nfcAdapter != null) {
            nfcAdapter!!.disableForegroundDispatch(this)
        }
    }

    fun showDeleteInvertoryAlert(model: PackedInventoryModel) {
        var name: String = ""
        if (model.name.isNullOrEmpty()) {
            name = "Packing"
        } else {
            name = model.name.toString()
        }
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "Are you sure you want to delete this \n' " + name + " ' Inventory?"
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                Debugger.wtf("showDeleteInvertoryAlert", "showDeleteInvertoryAlert")
                packingViewModel.hitDeleteInventory(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    model.undoPackingUrl
                )
            }

        })
    }

    fun packPoleAlert() {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "Are you sure you want to pack this \n' " + binding.spinnerPolePack.selectedItem.toString() + " ' pole?",
                isOnlyOkButtonVisible = false,
                negativeButtonText = "Cancel",
                positiveButtonText = "Yes"
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                hitPackPoleApi()
            }

        })
    }

    fun showAddOnsListDialog(
        addOnsList: ArrayList<Addon>,
        clothingList: ArrayList<ClothingModel>
    ) {
        dialogShowAddOnsList =
            ShowPackedAddonsListDialog.newInstance(
                addOnsList, clothingList
            )
        dialogShowAddOnsList.show(supportFragmentManager, "") //S
        dialogShowAddOnsList.setOnSubmitListener(object :
            ShowPackedAddonsListDialog.OnAddonsSubmitted {
            override fun onSubmit(
                addOnsList: ArrayList<Addon>,
                clothingList: ArrayList<ClothingModel>
            ) {
                saveRentersAddOnsWithVolley(addOnsList, clothingList)
            }

            override fun clickOnAddonsWithBarcode(status: String) {
                initilizaBarCodeScanDialog(isAddon = true, addOnsStatus = status)
            }
        })
    }

    fun saveRentersAddOnsWithVolley(
        addOnsList: ArrayList<Addon>,
        clothingList: ArrayList<ClothingModel>
    ) {

        loaderDialog.showDialog()
        val lgoinRequest: StringRequest =
            object : StringRequest(
                Method.POST,
                Urls.SAVE_ADD_ONS_LIST_LINK,
                Response.Listener { response ->
                    var mainObj: JSONObject? = null
                    try {
                        Debugger.wtf("saveRentersAddOnsWithVolley", response)
                        mainObj = JSONObject(response)
                        getPackingData()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                        Debugger.wtf(
                            "saveRentersAddOnsWithVolley",
                            "Try Catch" + e.message
                        )
                    } finally {
                        loaderDialog.dismissDialog()
                    }
                },
                Response.ErrorListener { error ->
                    Debugger.wtf(
                        "saveRentersAddOnsWithVolley",
                        "Failer " + error.message
                    )
                    loaderDialog.dismissDialog()
                }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val params: MutableMap<String, String> =
                        HashMap()
                    params["Authorization"] =
                        "bearer ${sharedPrefsHelper.getUser()?.accessToken.toString()}"
                    return params
                }

                @Throws(AuthFailureError::class)
                override fun getParams(): Map<String, String> {
                    val params =
                        HashMap<String, String>()
                    params["renter_id"] = renter_id.toString() + ""
                    Debugger.wtf("packAddons", "renter_id$renter_id")
                    for (i in 0..addOnsList.size - 1) {
                        val `val` = i.toString()
                        val model = addOnsList[i]
                        params["addons[$`val`][id]"] = model.id.toString()
                        params["addons[$`val`][selected]"] = model.selected.toString()
                    }
                    for (i in 0..clothingList.size - 1) {
                        val `val` = i.toString()
                        val model = clothingList[i]
                        params["clothing[$`val`][id]"] = model.id.toString()
                        params["clothing[$`val`][selected]"] = model.selected.toString()
                    }

                    Debugger.wtf("packAddons4", "params$params")
                    return params
                }
            }
        lgoinRequest.retryPolicy = DefaultRetryPolicy(
            600000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        VolleySingleton.getInstance(this)?.addToRequestQueue(lgoinRequest)
        VolleySingleton.getInstance(this)?.requestQueue?.getCache()?.clear()
    }

    fun showUpdateAddonStatusDialog(
        packedInventoryModel: PackedInventoryModel
    ) {
        val showDialog: UpdateAddonStatusDialog =
            UpdateAddonStatusDialog.newInstance(
                packedInventoryModel
            )
        showDialog.show(supportFragmentManager, "")
        showDialog.setOnSubmitListener(object : UpdateAddonStatusDialog.OnAddOnStatusSubmitted {
            override fun onSubmit(addonId: String, status: String, helmetSize: String) {
                val map =
                    HashMap<String, String>()
                map["renter_id"] = renter_id.toString() + ""
                map["addon_id"] = addonId
                map["status"] = status
                if (helmetSize != "") {
                    map["helmet_size"] = helmetSize
                }

                packingViewModel.hitUpdateAddonStatus(
                    sharedPrefsHelper.getUser()?.accessToken.toString(),
                    map
                )
            }
        })
    }

    fun showForceToPackSkiAndBootDialog(
        parameters: HashMap<String, String>,
        isBoot: Boolean
    ) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "This item is not available. You still want to issue?",
                "Yes", "No",
                isOnlyOkButtonVisible = false
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                hitBarCodeUploadApi(parameters["barcode"].toString(), "1", isBoot)
                commonDialog.dismiss()
            }
        })
    }

    fun moveToFittingScreen() {
        val i = Intent(this, FittingActivity::class.java)
        i.putExtra("renterId", renter_id)
        i.putExtra("shipmentId", shipment_id)
        i.putExtra("renterName", renterName)
        startActivity(i)
    }

}