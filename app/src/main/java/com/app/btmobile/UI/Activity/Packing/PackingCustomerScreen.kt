package com.app.btmobile.UI.Activity.Packing

import android.app.Activity
import android.app.DatePickerDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.SearchView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.UI.Activity.Delivery.DeliveryRentersActivity
import com.app.btmobile.UI.Adapter.PackingScheduleAdapter
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityPackingCustomerBinding
import com.ogoul.kalamtime.viewmodel.PackingViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class PackingCustomerScreen : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityPackingCustomerBinding
    lateinit var loaderDialog: LoaderDialog

    lateinit var adapter: PackingScheduleAdapter

    lateinit var selectedUnScheduleDelivery: ScheduleDelivery

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: PackingViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper
    var selectedSort = "all"
    var isFirstTime: Boolean = true


    var allItems: ArrayList<ScheduleDelivery> =
        ArrayList<ScheduleDelivery>()
    var allItemBigArray: ArrayList<ScheduleDelivery> = ArrayList<ScheduleDelivery>()
    var scheduleArray: ArrayList<ScheduleDelivery> = ArrayList<ScheduleDelivery>()
    var unscheduleArray: ArrayList<ScheduleDelivery> = ArrayList<ScheduleDelivery>()
    private var searchView: SearchView? = null


    var selectedDate: String = "";
    var isOwnVan: String = "0"
    var txtAmPm: String = ""

    var currentPage: String = "1"
    var metaData: PackingMetaData? = null

    var c = Calendar.getInstance()
    var mYear = c[Calendar.YEAR]
    var mMonth = c[Calendar.MONTH]
    var mDay = c[Calendar.DAY_OF_MONTH]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPackingCustomerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupViews()
        setupToolbarAndLoader()
        setupRecyclerview()
        setUpSortSpinner()
        hitApi()
    }

    fun setUpSortSpinner() {
        selectedSort = "all"
        val sortByValue =
            arrayOf("all", "Packed", "Not Packed", "Delivery Time", "Last Name", "Lodging")
        val sortByText =
            arrayOf("All", "Packed", "Not Packed", "Delivery Time", "Last Name", "Lodging")
        val adapter = ArrayAdapter(this, R.layout.spinner_layout, sortByText)
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spinnerSortBy.setAdapter(adapter)

        binding.spinnerSortBy.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                if (!isFirstTime) {
                    selectedSort = sortByValue[position]
                    hitApi()
                }
                isFirstTime = false
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })
    }


    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(PackingViewModel::class.java)
        viewModel.unScheduleResponse().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
        viewModel.unScheduleInventoryResponse().observe(this, androidx.lifecycle.Observer {
            consumeUnScheduleResponse(it)
        })
    }


    private fun setupRecyclerview() {
        adapter = PackingScheduleAdapter(this, allItems, { from, index ->
            onItemClickListener(from, index)
        })
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvPackingRenters.layoutManager = mLayoutManager
        binding.rvPackingRenters.itemAnimator = DefaultItemAnimator()
        binding.rvPackingRenters.adapter = adapter
        binding.rvPackingRenters.itemAnimator = null

        upwardPagination()
    }

    private fun upwardPagination() {
        binding.rvPackingRenters.addOnScrollListener(object :
            PaginationScrollUpListener(binding.rvPackingRenters.layoutManager as LinearLayoutManager) {
            override val isLastPage: Boolean
                get() = metaData?.current_page?.toInt() == metaData?.total_pages?.toInt()
            override val isLoading: Boolean
                get() = isFooterProgressVisible() || isSearchOpen()

            override fun showDownArrow(show: Boolean) {

            }

            override fun loadMoreItems() {
                currentPage = currentPage.toInt().inc().toString()
                binding.pbFooter.visibility = View.VISIBLE
                hitApi(fromPagination = false)
            }
        })
    }

    fun onItemClickListener(from: String, position: Int) {
        when (from) {
            "next" -> {
                if (allItems.size > position && allItems[position].isSchedule) {
                    moveToDeliveryRenterScreen(allItems[position])
                } else {
                    selectedUnScheduleDelivery = allItems[position]
                    viewModel.hitUnScheduleInventory(
                        sharedPrefsHelper.getUser()
                            ?.accessToken.toString(), allItems[position].deliveryId ?: 0
                    )

                }

            }
        }

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> return true
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.delivery_cus_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView
        searchView?.setSearchableInfo(
            searchManager
                .getSearchableInfo(componentName)
        )
        searchView?.maxWidth = Integer.MAX_VALUE
        searchView?.queryHint = "Last Name";

        searchMessage()


        return super.onCreateOptionsMenu(menu)
    }

    private fun searchMessage() {
        io.reactivex.Observable.create<String> { emitter ->
            searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    emitter.onNext(query)
                    return false
                }

                override fun onQueryTextChange(query: String): Boolean {
                    emitter.onNext(query)
                    return false
                }
            })

        }.debounce(200, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Debugger.wtf("searchQuery", it)
                filterItems(it.toString())
            }, {
                Debugger.wtf("Search error", it.message.toString())
            }, {
                Debugger.wtf("Search Complete", "<---------->")
            })
    }

    fun filterItems(charString: String) {
        if (charString.isEmpty()) {
            showData(allItemBigArray)
        } else {
            var filterList: ArrayList<ScheduleDelivery> = ArrayList<ScheduleDelivery>()
            var filterScheduleList: ArrayList<ScheduleDelivery> = ArrayList<ScheduleDelivery>()
            var filterUnScheduleList: ArrayList<ScheduleDelivery> = ArrayList<ScheduleDelivery>()
            unscheduleArray.let {
                for (row in it) {
                    if (row.customerName.toString()
                            .contains(charString, true)
                    ) {
                        filterUnScheduleList.add(row)
                    }
                }
                filterUnScheduleList.forEachIndexed { index, item ->
                    var model = item
                    model.isFirst = index == 0
                    filterList.add(model)

                }
            }
            scheduleArray.let {
                for (row in it) {
                    if (row.customerName.toString()
                            .contains(charString, true)
                    ) {
                        filterScheduleList.add(row)
                    }
                }
                filterScheduleList.forEachIndexed { index, item ->
                    var model = item
                    model.isFirst = index == 0
                    filterList.add(model)

                }
            }

            showData(filterList)
        }
    }


    private fun setupViews() {
        selectedDate = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.tvDateSelected.text = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.btnSelectDate.setOnClickListener {
            openDatePicker()
        }
    }

    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()


        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Pack Station"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { onBackPressed() })
    }


    override fun onResume() {
        super.onResume()
        if (searchView != null) {
            if (!searchView?.isIconified!!) {
                searchView?.isIconified = true;
                searchView?.onActionViewCollapsed();
            }
        }
    }

    override fun onBackPressed() {
        if (searchView != null) {
            if (!searchView!!.isIconified) {
                searchView!!.isIconified = true
                searchView?.onActionViewCollapsed();
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    fun hitApi(fromPagination: Boolean = true) {
        if (fromPagination) {
            currentPage = "1"
            allItems.clear()
            allItemBigArray.clear()
            scheduleArray.clear()
            unscheduleArray.clear()
        }
        val date: String = parseDateToddMMddyyy(selectedDate) ?: ""
        val data: HashMap<String, String> =
            HashMap()
        data["date"] = date
        data["all"] = isOwnVan
        data["sortby"] = selectedSort
        data["current_page"] = currentPage
        data["per_page"] = AppConstants.PER_PAGE


        Debugger.wtf("hitApi", "${data.toString()}")
        viewModel.hitDeliveryCustomerApiCoro(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), data
        )
    }

    fun hideAllProgressLoaders() {
        loaderDialog.dismissDialog()
        binding.pbFooter.visibility = View.GONE
    }

    private fun consumeUnScheduleResponse(apiResponse: ApiResponse<UnScheduleInventoryModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var mode: UnScheduleInventoryModel = apiResponse.data as UnScheduleInventoryModel
                showToast(this, mode.message, isSuccess = mode.success)
                selectedUnScheduleDelivery.shipmentId = mode.van_shipment_id
                moveToDeliveryRenterScreen(selectedUnScheduleDelivery)
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@PackingCustomerScreen,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@PackingCustomerScreen)
            }
        }

    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<PackingScheduleModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                if (searchView != null) {
                    if (!searchView!!.isIconified) {
                        searchView!!.isIconified = true
                        searchView?.onActionViewCollapsed();
                    }
                }
                setViewAndChildrenEnabled(binding.layPackCustomer, false)
                loaderDialog.showDialog(!isFooterProgressVisible())
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                var model: PackingScheduleModel? = apiResponse.data as PackingScheduleModel

                metaData = model?.meta_data
                model?.unscheduleDeliveries?.let {
                    if (it.isNotEmpty()) {
                        if (unscheduleArray.isNullOrEmpty()) {
                            it.first().isFirst = true
                        }
                        unscheduleArray.addAll(it)

                    }
                }
                model?.scheduleDeliveries?.let {
                    if (it.isNotEmpty()) {
                        if (scheduleArray.isNullOrEmpty()) {
                            it.first().isFirst = true
                        }
                        it.map { sch ->
                            sch.isSchedule = true
                        }
                        scheduleArray.addAll(it)
                    }
                }

                allItemBigArray.clear()

                if (metaData?.total_pages?.toInt() == 0 && apiResponse.data?.unscheduleDeliveries.isNullOrEmpty() &&
                    apiResponse.data?.scheduleDeliveries.isNullOrEmpty()
                ) {
                    allItems.clear()
                } else {
                    allItemBigArray.addAll(unscheduleArray)
                    allItemBigArray.addAll(scheduleArray)
                    try {
                        allItemBigArray =
                            allItemBigArray.distinct() as ArrayList<ScheduleDelivery>
                    } catch (e: Exception) {

                    }
                }

                showData(allItemBigArray)
                setViewAndChildrenEnabled(binding.layPackCustomer, true)
                hideAllProgressLoaders()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                hideAllProgressLoaders()
                setViewAndChildrenEnabled(binding.layPackCustomer, true)
                showToast(
                    this@PackingCustomerScreen,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                hideAllProgressLoaders()
                logout(this@PackingCustomerScreen)
            }
        }
    }

    private fun showData(list: ArrayList<ScheduleDelivery>) {
        Log.wtf("showData", "${list.size}")
        if (!list.isNullOrEmpty()) {
            binding.noDeliveriesTv.visibility = View.GONE
            allItems.clear()
            allItems.addAll(list)
            adapter.notifyDataSetChanged()
        } else {
            allItems.clear()
            adapter.notifyDataSetChanged()
            binding.noDeliveriesTv.visibility = View.VISIBLE
        }
    }


    private fun openDatePicker() {
        val datePickerDialog: DatePickerDialog =
            object : DatePickerDialog(
                this@PackingCustomerScreen,
                null, mYear, mMonth, mDay
            ) {
                override fun onDateChanged(
                    @NonNull view: DatePicker,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    mYear = year
                    mMonth = monthOfYear
                    mDay = dayOfMonth
                    selectedDate = "${mMonth + 1}/$dayOfMonth/$year"
                    binding.tvDateSelected.setText("${mMonth + 1}/$dayOfMonth/$year")
                    hitApi()
                    dismiss()
                }
            }
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).visibility = View.GONE
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).visibility = View.GONE
    }

    fun moveToDeliveryRenterScreen(modelCopy: ScheduleDelivery) {
        val i = Intent(this, DeliveryRentersActivity::class.java)
        i.putExtra("from", AppConstants.PackingCustomerScreen)
        i.putExtra("modelObject", modelCopy)
        resultLauncher.launch(i)
    }

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                hitApi()
            }
        }

    fun isFooterProgressVisible(): Boolean {
        return binding.pbFooter.visibility == View.VISIBLE
    }

    fun isSearchOpen(): Boolean {
        if (searchView != null) {
            if (!searchView!!.isIconified) {
                return true
            }
        }
        return false
    }
}