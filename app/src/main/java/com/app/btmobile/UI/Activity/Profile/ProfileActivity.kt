package com.app.btmobile.UI.Activity.Profile

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.BaseModel
import com.app.btmobile.Models.ProfileModel
import com.app.btmobile.MyApplication
import com.app.btmobile.Status
import com.app.btmobile.Urls
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityProfileBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.ogoul.kalamtime.viewmodel.ProfileViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.util.*
import javax.inject.Inject

class ProfileActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityProfileBinding
    lateinit var loaderDialog: LoaderDialog

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: ProfileViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    private val MY_CAMERA_REQUEST_CODE = 100
    private val MY_STORAGE_REQUEST_CODE = 200

    private val PICK_IMAGE_CAMERA = 1
    val PICK_IMAGE_GALLERY = 2

    var selectedBitmap: Bitmap? = null
    var profileForLocalDb: Bitmap? = null
    private val imgPath: String? = null


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupToolbarAndLoader()
        binding.cameraProfile.setOnClickListener(View.OnClickListener {
            selectImage()
        })
        binding.saveUserProfileBtn.setOnClickListener(View.OnClickListener {
            binding.run {
                if (checkFields()) {
                    var data = profileData(
                        selectedBitmap,
                        firstNameEd.text.toString().trim { it <= ' ' },
                        lastNameEd.text.toString().trim { it <= ' ' },
                        passwordEd.text.toString().trim { it <= ' ' },
                        confirmPasswordEd.text.toString().trim { it <= ' ' }
                    )
                    viewModel.hitSaveProfileData(
                        sharedPrefsHelper.getUser()?.accessToken.toString(),
                        sharedPrefsHelper.getUser()?.id!!, data!!
                    )

                }
            }
        })

        hitApi()

    }

    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(ProfileViewModel::class.java)
        viewModel.getProfileResponse().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })

        viewModel.getBaseResponse().observe(this, androidx.lifecycle.Observer {
            consumeBaseResponse(it)
        })
    }

    private fun consumeBaseResponse(
        apiResponse:
        ApiResponse<BaseModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                try {
                    var data: BaseModel? = apiResponse.data as BaseModel
                    if (data != null) {
                        if (data.message != null) {
                            showToast(this, data.message, data.success)
                        }
                    }
                } catch (e: Exception) {
                    Debugger.wtf(TAG, "Erorrr-------------> : ${e.message}")
                } finally {
                    loaderDialog.dismissDialog()
                    hitApi()
                }


            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@ProfileActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ProfileActivity)
            }
        }
    }


    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Profile"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })
    }


    fun hitApi() {
        viewModel.hitGetWaiverRentersList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString()
        )
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<ProfileModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                showData(apiResponse.data as ProfileModel)
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@ProfileActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ProfileActivity)
            }
        }
    }

    private fun showData(profileMode: ProfileModel) {
        binding.run {
            firstNameEd.setText(profileMode.firstName)
            lastNameEd.setText(profileMode.lastName)
            emailAddressTv.setText(profileMode.email)
            if (!profileMode.image.isNullOrEmpty()) {
                //  progressShowImg.visibility = View.VISIBLE
                Debugger.wtf("llsls", AppConstants.BASE_URL + profileMode.image)
                Glide.with(this@ProfileActivity)
                    .asBitmap()
                    .load(AppConstants.BASE_URL + profileMode.image)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            // progressShowImg.visibility = View.GONE
                            userImageNav.setImageBitmap(resource)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                        }
                    })
            }

        }
    }

    //ChooseImaeDialog
    @RequiresApi(Build.VERSION_CODES.M)
    fun selectImage() {
        try {
            val options = arrayOf<CharSequence>("Take Photo", "Choose From Gallery", "Cancel")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setTitle("Select Option")
            builder.setItems(options, DialogInterface.OnClickListener { dialog, item ->
                if (options[item] == "Take Photo") {
                    dialog.dismiss()
                    if (checkSelfPermission(Manifest.permission.CAMERA) !== PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(Manifest.permission.CAMERA),
                            MY_CAMERA_REQUEST_CODE
                        )
                    } else {
                        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(intent, PICK_IMAGE_CAMERA)
                    }
                } else if (options[item] == "Choose From Gallery") {
                    dialog.dismiss()
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !== PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            MY_STORAGE_REQUEST_CODE
                        )
                    } else {
                        val pickPhoto =
                            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY)
                    }
                } else if (options[item] == "Cancel") {
                    dialog.dismiss()
                }
            })
            builder.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults)
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, PICK_IMAGE_CAMERA)
                showToast(this, "Camera permission granted")
            } else {
                showToast(this, "Camera permission denied")
            }
        } else if (requestCode == MY_STORAGE_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val pickPhoto =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY)
                showToast(this, "Storage permission granted")
            } else {
                showToast(this, "Storage permission denied")
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_CAMERA) {
            if (resultCode == RESULT_OK) {
                try {
                    val selectedImage = data?.data
                    selectedBitmap = data?.extras!!["data"] as Bitmap?
                    val bytes = ByteArrayOutputStream()
                    selectedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
                    binding.userImageNav.setImageBitmap(selectedBitmap)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    Debugger.wtf("onActivityResult", e.message)
                    showToast(this, "Something went wrong")
                }
            } else {
                showToast(this, "You haven't picked Image")
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            if (resultCode == RESULT_OK) {
                try {
                    val selectedImage = data?.data
                    selectedBitmap = MediaStore.Images.Media.getBitmap(
                        this.contentResolver,
                        selectedImage
                    )
                    val bytes = ByteArrayOutputStream()
                    selectedBitmap?.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
                    binding.userImageNav.setImageBitmap(selectedBitmap)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    Debugger.wtf("onActivityResult", e.message)
                    showToast(this, "Something went wrong")
                }
            } else {
                showToast(this, "You haven't picked Image")
            }
        }
    }

    fun checkFields(): Boolean {
        binding.run {
            if (TextUtils.isEmpty(firstNameEd.text.toString())) {
                showToast(this@ProfileActivity, "Enter Your First Name")
                firstNameEd.requestFocus()
                firstNameEd.error = "Enter Your First Name..!!"
                return false
            } else if (TextUtils.isEmpty(lastNameEd.text.toString())) {
                showToast(this@ProfileActivity, "Enter Your Last Name")
                lastNameEd.requestFocus()
                lastNameEd.error = "Enter Your Last Name..!!"
                return false
            } else if (!TextUtils.isEmpty(passwordEd.text.toString())
                || !TextUtils.isEmpty(confirmPasswordEd.text.toString())
            ) {
                if (TextUtils.isEmpty(passwordEd.text.toString())) {
                    showToast(this@ProfileActivity, "Enter Your Password")
                    passwordEd.requestFocus()
                    passwordEd.error = "Enter Your Password..!!"
                    return false
                } else if (passwordEd.text.toString().length < 6) {
                    showToast(this@ProfileActivity, "Password must be greater then 5 digits")
                    passwordEd.requestFocus()
                    passwordEd.error = "Password Must Be greater than 5 digits"
                    return false
                } else if (TextUtils.isEmpty(confirmPasswordEd.text.toString())) {
                    showToast(this@ProfileActivity, "Enter Your Confirm Password")
                    confirmPasswordEd.requestFocus()
                    confirmPasswordEd.error = "Enter Your Confirm Password..!!"
                    return false
                } else if (confirmPasswordEd.text.toString().length < 6) {
                    showToast(this@ProfileActivity, "Password must be greater then 5 digits")
                    confirmPasswordEd.requestFocus()
                    confirmPasswordEd.error = "Password Must Be greater than 5 digits"
                    return false
                } else if (!passwordEd.text.toString()
                        .equals(confirmPasswordEd.text.toString(), true)
                ) {
                    showToast(this@ProfileActivity, "Password Does Not Match")
                    return false
                }
            }
            return true
        }
    }

    private fun profileData(
        signatureBitmap: Bitmap?,
        firstName: String,
        lastName: String,
        password: String,
        confirmPassword: String
    ): RequestBody? {
        val builder: MultipartBody.Builder = MultipartBody.Builder().setType(MultipartBody.FORM)
        builder.addFormDataPart("first_name", firstName)
            .addFormDataPart("last_name", lastName)
        if (password != ""
            && confirmPassword != ""
        ) {
            builder.addFormDataPart("old_password", sharedPrefsHelper.getUser()?.password!!)
            builder.addFormDataPart("new_password", password)
            builder.addFormDataPart("new_password_confirmation", confirmPassword)
        }
        if (signatureBitmap != null) {
            val bos = ByteArrayOutputStream()
            signatureBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos)
            builder.addFormDataPart(
                "image", "image", RequestBody.create(
                    MultipartBody.FORM,
                    bos.toByteArray()
                )
            )
        }
        return builder.build()
    }

    fun decodeBase64(bmp: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        return stream.toByteArray()
    }


}
