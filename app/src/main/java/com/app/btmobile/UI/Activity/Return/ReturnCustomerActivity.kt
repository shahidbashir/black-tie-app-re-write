package com.app.btmobile.UI.Activity.Return

import android.app.Activity
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.SearchView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.UI.Adapter.ReturnAdapter
import com.app.btmobile.UI.Dialog.CommonDialog
import com.app.btmobile.UI.Dialog.ReturnSearchByBarcodeDialog
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityReturnCustomerBinding
import com.google.gson.Gson
import com.ogoul.kalamtime.viewmodel.ReturnViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList

class ReturnCustomerActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityReturnCustomerBinding
    lateinit var loaderDialog: LoaderDialog

    var pendingIntent: PendingIntent? = null
    var nfcAdapter: NfcAdapter? = null
    lateinit var nfcReaderUtils: NfcReaderUtils


    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: ReturnViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    var allItems: ArrayList<ReturnsCustomerModelItem> = ArrayList()
    var allItemBigArray: ArrayList<ReturnsCustomerModelItem> =
        ArrayList<ReturnsCustomerModelItem>()
    lateinit var adapter: ReturnAdapter

    var selectedDate: String = "";
    var selectedSort = ""
    var selectedStatus = "Pending"
    var isFirstTime: Boolean = true


    var c = Calendar.getInstance()
    var mYear = c[Calendar.YEAR]
    var mMonth = c[Calendar.MONTH]
    var mDay = c[Calendar.DAY_OF_MONTH]
    var isOwnVan: String = "0"

    var currentPage: String = "1"
    var metaData: MetaDataReturn? = null
    private var searchView: SearchView? = null


    var returnSearchByBarcodeDialog: ReturnSearchByBarcodeDialog =
        ReturnSearchByBarcodeDialog.newInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReturnCustomerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupViews()
        setUpStatusSpinner()
        setupToolbarAndLoader()
        setupRecyclerview()
        handleNfc()
        hitApi()

    }


    fun handleNfc() {
        pendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcReaderUtils = NfcReaderUtils()
        nfcReaderUtils.setOnNfcScanListener(object : NfcReaderUtils.OnNfcScanListener {
            override fun nfcBarcode(nfcCode: String) {
                if (returnSearchByBarcodeDialog != null) {
                    if (returnSearchByBarcodeDialog.isVisible) {
                        Debugger.wtf("nfcBarcode", nfcCode)
                        if (returnSearchByBarcodeDialog.isFromMassScan) {
                            hitMassScanApi(nfcCode, "0")
                        } else {
                            hitSearchByBarcodeApi(nfcCode, "")
                        }
                    }
                }
            }
        })

        if (nfcAdapter != null) {
            nfcAdapter?.let { nfc ->
                nfc.isEnabled?.let { enable ->
                    if (!enable) {
                        openNfcSettings(this)
                    }
                }
            }

        } else {
            showToast(this, "There is no NFC Functionality in this device")
        }
    }

    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(ReturnViewModel::class.java)
        viewModel.getWaiverCustomers().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
        viewModel.getMassReturnResponse().observe(this, androidx.lifecycle.Observer {
            consumeMassReturnResponse(it)
        })
    }

    private fun setupRecyclerview() {
        adapter = ReturnAdapter(this, allItems, { from, position ->
            onItemClickListener(from, position)
        })
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvReturnCustomer.layoutManager = mLayoutManager
        binding.rvReturnCustomer.itemAnimator = DefaultItemAnimator()
        binding.rvReturnCustomer.adapter = adapter
        binding.rvReturnCustomer.itemAnimator = null

        upwardPagination()
    }

    private fun upwardPagination() {
        binding.rvReturnCustomer.addOnScrollListener(object :
            PaginationScrollUpListener(binding.rvReturnCustomer.layoutManager as LinearLayoutManager) {
            override val isLastPage: Boolean
                get() = metaData?.current_page?.toInt() == metaData?.total_pages?.toInt()
            override val isLoading: Boolean
                get() = isFooterProgressVisible() || isSearchOpen()

            override fun showDownArrow(show: Boolean) {
            }

            override fun loadMoreItems() {
                currentPage = currentPage.toInt().inc().toString()
                binding.pbFooter.visibility = View.VISIBLE
                hitApi(fromPagination = false)
            }
        })
    }

    fun isFooterProgressVisible(): Boolean {
        return binding.pbFooter.visibility == View.VISIBLE
    }

    fun onItemClickListener(from: String, position: Int) {
        if (allItems.size > position) {
            var model = allItems[position]
            when (from) {
                "next" -> {
                    callNextActivity(position)
                }
                "map" -> {
                    GoToMap(this, model.mapUrl)
                }
                "notes" -> {
                    if (model.returnNotes.isNotEmpty()) {
                        Debugger.wtf("Notes", "->" + model.returnNotes)
                        var finalNotes = ""
                        for (notes in model.returnNotes) {
                            finalNotes = """
                        $finalNotes
                        $notes
                        """.trimIndent()
                        }
                        CommonAlert("Notes", finalNotes, isNotes = true)
                    } else {
                        showToast(this, "There is no note")
                    }
                }
            }
        }
    }


    private fun setupViews() {
        selectedDate = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.tvDateSelected.text = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.rgFindDelivery.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbMyOwnVan -> {
                    isOwnVan = "0"
                }
                R.id.rbAllDeliveries -> {
                    isOwnVan = "1"
                }
            }
            hitApi()
        }

        binding.btnSelectDate.setOnClickListener {
            openDatePicker()
        }

        selectedSort = "firstname"
        val sortByValue = arrayOf("firstname", "lastname", "lodging", "time", "ready")
        val sortByText = arrayOf("First Name", "Last Name", "Lodging", "Time", "Ready")
        val adapter = ArrayAdapter(this, R.layout.spinner_layout, sortByText)
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spinnerSortBy.setAdapter(adapter)

        binding.spinnerSortBy.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                if (!isFirstTime) {
                    selectedSort = sortByValue[position]
                    hitApi()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })




        binding.cbMassEquipmentScan.setOnClickListener {
            if (binding.cbMassEquipmentScan.isChecked) {
                initilizeSearchByBarDialog(true)
            }
        }

        binding.cbSearchByCode.setOnClickListener {
            if (binding.cbSearchByCode.isChecked) {
                initilizeSearchByBarDialog(false)
            }
        }
    }

    fun setUpStatusSpinner() {
        selectedStatus = "Pending"
        val sortByText =
            arrayOf("Pending", "Completed", "Ready")
        val adapter = ArrayAdapter(this, R.layout.spinner_layout, sortByText)
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spinnerStatus.adapter = adapter

        try {
            binding.spinnerStatus.setSelection(0)
        } catch (e: Exception) {
            Debugger.wtf("setUpStatusSpinner", "${e.message}")
        }
        binding.spinnerStatus.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                try {
                    if (!isFirstTime) {
                        selectedStatus = sortByText[position]
                        hitApi()
                    }
                } catch (e: Exception) {
                    Debugger.wtf("setUpStatusSpinner", "OnItemSelectedListener ${e.message}")
                }
                isFirstTime = false
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Returns"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { onBackPressed() })
    }

    fun hitApi(fromPagination: Boolean = true) {
        if (fromPagination) {
            currentPage = "1"
            allItems.clear()
            allItemBigArray.clear()

        }
        val date: String = parseDateToddMMddyyy(selectedDate) ?: ""

        val map = HashMap<String, String>()
        map["date"] = date
        map["sortby"] = selectedSort
        map["filter_by_status"] = selectedStatus
        map["my_van"] = if (isOwnVan == "0") "1" else "0"
        map["all"] = if (isOwnVan == "1") "1" else "0"
        map["ready"] = if (isOwnVan == "2") "1" else "0"
        map["current_page"] = currentPage
        map["per_page"] = AppConstants.PER_PAGE
        Debugger.wtf("hitApi", "${date.toString()} / ${map.toString()}")

        viewModel.hitGetReturnCustomersList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), map
        )
    }

    fun hitMassScanApi(barcode: String, force: String) {

        val map = HashMap<String, String>()
        map["barcode"] = barcode
        map["force_return"] = force

        viewModel.hitMassReturnApi(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), map
        )
    }

    fun hitSearchByBarcodeApi(barcode: String, force: String) {
        val map = HashMap<String, String>()
        map["barcode"] = barcode

        viewModel.hitMassReturnSearch(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), map
        )
    }

    fun hideAllProgressLoaders() {
        loaderDialog.dismissDialog()
        binding.pbFooter.visibility = View.GONE
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<ReturnsCustomerModel>
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                hideSearchIcon()
                setViewAndChildrenEnabled(binding.layReturn, false)
                loaderDialog.showDialog(!isFooterProgressVisible())
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")

                var model = apiResponse.data as ReturnsCustomerModel
                metaData = model.meta_data

                if (metaData?.total_pages?.toInt() == 0 && apiResponse.data?.data.isNullOrEmpty()) {
                    allItemBigArray.clear()
                    allItems.clear()
                } else {
                    model.data?.let {
                        allItemBigArray.addAll(it)
                    }
                    try {
                        allItemBigArray =
                            allItemBigArray.distinct() as ArrayList<ReturnsCustomerModelItem>
                    } catch (e: Exception) {

                    }

                }
                setViewAndChildrenEnabled(binding.layReturn, true)
                hideAllProgressLoaders()
                showData(allItemBigArray)

            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                hideAllProgressLoaders()
                setViewAndChildrenEnabled(binding.layReturn, true)
                showToast(
                    this@ReturnCustomerActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                hideAllProgressLoaders()
                logout(this@ReturnCustomerActivity)
            }
        }
    }


    private fun consumeMassReturnResponse(
        apiResponse:
        ApiResponse<MassReturnBaseModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeMassReturnResponse",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )
                var baseModel = apiResponse.data as MassReturnBaseModel
                if (returnSearchByBarcodeDialog != null) {
                    if (returnSearchByBarcodeDialog.isVisible) {
                        returnSearchByBarcodeDialog.clearEditText()
                        returnSearchByBarcodeDialog.showRenterData(baseModel)
                    }
                }
                if (!baseModel.success) {
                    if (baseModel.message.equals(
                            "This is not yet scheduled to be returned. Are you sure you want to return this gear?",
                            true
                        )
                    ) {
                        CommonAlert("Alert", baseModel.message, barcode = baseModel.barcode)
                    } else {
                        showToast(this, baseModel.message, baseModel.success)
                    }
                }
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@ReturnCustomerActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ReturnCustomerActivity)
            }
        }
    }

    private fun showData(list: ArrayList<ReturnsCustomerModelItem>?) {
        Debugger.wtf("showData", "${list?.size}")
        if (list?.size!! > 0) {
            allItems.clear()
            allItems.addAll(list)
            binding.noReturnsTv.visibility = View.GONE
            adapter.notifyDataSetChanged()
        } else if (allItems.isNullOrEmpty()) {
            allItems.clear()
            adapter.notifyDataSetChanged()
            binding.noReturnsTv.visibility = View.VISIBLE
        }
    }


    private fun openDatePicker() {
        val datePickerDialog: DatePickerDialog =
            object : DatePickerDialog(
                this@ReturnCustomerActivity,
                null, mYear, mMonth, mDay
            ) {
                override fun onDateChanged(
                    @NonNull view: DatePicker,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    mYear = year
                    mMonth = monthOfYear
                    mDay = dayOfMonth
                    selectedDate = "${mMonth + 1}/$dayOfMonth/$year"
                    binding.tvDateSelected.setText("${mMonth + 1}/$dayOfMonth/$year")
                    hitApi()
                    dismiss()
                }
            }
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).visibility = View.GONE
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).visibility = View.GONE
    }

    fun callNextActivity(position: Int) {
        val i = Intent(this, ReturnRentersActivity::class.java)
        i.putExtra("shipmentId", allItems[position].vanShipmentId)
        resultLauncher.launch(i)
    }

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                hitApi()
            }
        }

    fun CommonAlert(title: String, des: String, isNotes: Boolean = false, barcode: String = "") {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                title,
                des
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                if (!isNotes) {
                    hitMassScanApi(barcode, "1")
                }
            }

        })
    }

    fun initilizeSearchByBarDialog(isFromMassScan: Boolean) {
        returnSearchByBarcodeDialog =
            ReturnSearchByBarcodeDialog.newInstance(isFromMassScan = isFromMassScan)
        returnSearchByBarcodeDialog.show(supportFragmentManager, "") //S
        returnSearchByBarcodeDialog.setOnSubmitListener(object :
            ReturnSearchByBarcodeDialog.OnScanClick {
            override fun onScanBarcode(barcode: String) {
                if (returnSearchByBarcodeDialog != null) {
                    if (returnSearchByBarcodeDialog.isVisible) {
                        Debugger.wtf("nfcBarcode", barcode)
                        if (returnSearchByBarcodeDialog.isFromMassScan) {
                            hitMassScanApi(barcode, "0")
                        } else {
                            hitSearchByBarcodeApi(barcode, "")
                        }
                    }
                }
            }

            override fun onDismiss() {
                binding.cbSearchByCode.isChecked = false
                binding.cbMassEquipmentScan.isChecked = false
                Debugger.wtf("addScanBarcodeDialog", "onDismiss")
                //  stopNfcScan()
            }
        })
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        nfcReaderUtils.resolveIntent(intent!!)
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
        hideSearchIcon()

    }

    override fun onPause() {
        super.onPause()
        if (nfcAdapter != null) {
            nfcAdapter!!.disableForegroundDispatch(this)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> return true
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.delivery_cus_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView
        searchView?.setSearchableInfo(
            searchManager
                .getSearchableInfo(componentName)
        )
        searchView?.maxWidth = Integer.MAX_VALUE
        searchView?.queryHint = "Last Name";

        searchMessage()


        return super.onCreateOptionsMenu(menu)
    }

    private fun searchMessage() {
        io.reactivex.Observable.create<String> { emitter ->
            searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    emitter.onNext(query)
                    return false
                }

                override fun onQueryTextChange(query: String): Boolean {
                    emitter.onNext(query)
                    return false
                }
            })

        }.debounce(200, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Debugger.wtf("searchQuery", it + " sd")
                filterItems(it.toString())
            }, {
                Debugger.wtf("Search error", it.message.toString())
            }, {
                Debugger.wtf("Search Complete", "<---------->")
            })
    }

    fun filterItems(charString: String) {
        if (charString.isEmpty()) {
            allItems.clear()
            showData(allItemBigArray)
        } else {
            allItems.clear()
            var filterList: ArrayList<ReturnsCustomerModelItem> =
                ArrayList<ReturnsCustomerModelItem>()
            allItemBigArray.let {
                for (row in it) {
                    if (row.customerName.toString().contains(charString, true)) {
                        filterList.add(row)
                    }
                }
            }
            showData(filterList)
        }
    }


    fun hideSearchIcon() {
        if (searchView != null) {
            if (!searchView?.isIconified!!) {
                searchView?.isIconified = true;
                searchView?.onActionViewCollapsed();
            }
        }
    }


    fun isSearchOpen(): Boolean {
        if (searchView != null) {
            if (!searchView!!.isIconified) {
                return true
            }
        }
        return false
    }


    override fun onBackPressed() {
        if (searchView != null) {
            if (!searchView!!.isIconified) {
                searchView!!.isIconified = true
                searchView?.onActionViewCollapsed();
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }
}
