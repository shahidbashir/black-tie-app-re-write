package com.app.btmobile.UI.Activity.Return

import android.app.PendingIntent
import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.UI.Adapter.ReturnRentersAdapter
import com.app.btmobile.UI.Dialog.AddScanBarcodeDialog
import com.app.btmobile.UI.Dialog.CommonDialog
import com.app.btmobile.UI.Dialog.ReturnAddScanBarcodeDialog
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityRetunRentersActivityBinding
import com.google.gson.Gson
import com.ogoul.kalamtime.viewmodel.PackingViewModel
import com.ogoul.kalamtime.viewmodel.ReturnViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ReturnRentersActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityRetunRentersActivityBinding
    lateinit var loaderDialog: LoaderDialog

    var pendingIntent: PendingIntent? = null
    var nfcAdapter: NfcAdapter? = null
    lateinit var nfcReaderUtils: NfcReaderUtils


    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: ReturnViewModel
    lateinit var packingViewModel: PackingViewModel


    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper
    var allItems: ArrayList<ReturnRenterModelItem> = ArrayList()
    lateinit var adapter: ReturnRentersAdapter
    var shipmentId: Int = -1
    var renter_id: Int = -1

    var addScanBarcodeDialog: ReturnAddScanBarcodeDialog = ReturnAddScanBarcodeDialog.newInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRetunRentersActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setupViewModel()
        setupToolbarAndLoader()
        setupRecyclerview()
        hitApi()
        handleNfc()
        // initilizaBarCodeScanDialog()
    }

    fun getIntentData() {
        if (intent != null) {
            shipmentId = intent.getIntExtra("shipmentId", -1)
        }
    }

    @Throws(ParseException::class)
    private fun reFormatDate(dateIn: String): String? {
        var simpleDateFormat =
            SimpleDateFormat("MMM dd, yyyy")
        val date = simpleDateFormat.parse(dateIn)
        simpleDateFormat = SimpleDateFormat("MM/dd/yy")
        Debugger.wtf("my_new_date", simpleDateFormat.format(date))
        return simpleDateFormat.format(date)
    }

    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(ReturnViewModel::class.java)
        packingViewModel = ViewModelProvider(this, factory).get(PackingViewModel::class.java)
        viewModel.getReturnRenterModelResponse().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
        viewModel.getBaseResponse().observe(this, androidx.lifecycle.Observer {
            consumeBaseResponse(it)
        })

        viewModel.getReturnInventoryResponse().observe(this, androidx.lifecycle.Observer {
            consumeInventoryReturnResponse(it)
        })

        packingViewModel.getResponseSaveAddonsWithBarcode()
            .observe(this, androidx.lifecycle.Observer {
                consumeSaveAddonsWithBarcode(it)
            })
    }

    private fun consumeSaveAddonsWithBarcode(apiResponse: ApiResponse<SaveAddonWithBarcodeModel>?) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeSaveAddonsWithBarcode",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )
                var model: SaveAddonWithBarcodeModel =
                    (apiResponse.data as SaveAddonWithBarcodeModel)
                if (model.status) {
                    hitApi()
                }
                showToast(this, model.message, model.status)
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(
                    TAG, "consumeResponse ERROR: " + apiResponse.error.toString()
                )
                loaderDialog.dismissDialog()

                showToast(
                    this@ReturnRentersActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ReturnRentersActivity)
            }
        }

    }


    private fun setupRecyclerview() {
        adapter = ReturnRentersAdapter(this, allItems)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvWaiverCustomer.layoutManager = mLayoutManager
        binding.rvWaiverCustomer.itemAnimator = DefaultItemAnimator()
        binding.rvWaiverCustomer.adapter = adapter
    }

    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog(isCancelable = false)

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Return Renters"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener {
            setResult(RESULT_OK, intent);
            finish()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.return_renter_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.goToHome) {
            GoToHomeScreen(this)
        } else if (item.itemId == R.id.markAllReturned) {
            CommonAlert("Alert", "Are you sure you want to mark all as Returned?")
        }
        return super.onOptionsItemSelected(item)
    }

    fun hitApi() {
        viewModel.hitGetReturnRenterList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), shipmentId
        )
    }

    fun addRemoveClothingApi(
        type_this: String,
        addon_id_this: String,
        renter_id_this: Int,
        return_status: String
    ) {

        val map = HashMap<String, String>()
        map["type"] = type_this + ""
        map["addon_id"] = addon_id_this
        map["renter_id"] = renter_id_this.toString() + ""
        map["return"] = return_status + ""

        viewModel.hitReturnAddonOrClothing(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), shipmentId, map
        )

    }

    fun returnRemoveApi(model: ReturnInventory) {
        val map = HashMap<String, String>()
        map["barcode"] = model.barcode
        map["van_shipment_id"] = shipmentId.toString()

        viewModel.hitRemoveReturn(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), map
        )

    }

    fun hitReturnUsingBarcode(
        barcode: String,
        force_return: String
    ) {

        val map = HashMap<String, String>()
        map["van_shipment_id"] = shipmentId.toString()
        map["barcode"] = barcode
        map["force_return"] = force_return

        viewModel.hitReturnUsingBarcode(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), shipmentId, map
        )

    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<ReturnRenterModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(
                    "consumeResponse",
                    "consumeResponse SUCCESS : ${Gson().toJson(apiResponse.data)}"
                )
                Debugger.wtf("showData", "showData ${allItems.size}")
                showData(apiResponse.data as ReturnRenterModel)
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@ReturnRentersActivity,
                    "${apiResponse.error.toString()}"
                )
                binding.layReturnInvDetails.visibility = View.GONE
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ReturnRentersActivity)
            }
        }
    }

    private fun showData(model: ReturnRenterModel) {
        if (model.size > 0) {
            renter_id = model[0].renterId
            binding.noDeliveriesTv.visibility = View.GONE
            allItems.clear()
            allItems.addAll(model)
            Debugger.wtf("showData", "showData ${allItems.size}")
            adapter.notifyDataSetChanged()

            showExtraData(model[0])
        } else {
            Debugger.wtf("showData", "else")
            allItems.clear()
            adapter.notifyDataSetChanged()
            binding.noDeliveriesTv.visibility = View.VISIBLE
            binding.layReturnInvDetails.visibility = View.GONE
        }
    }

    fun showExtraData(model: ReturnRenterModelItem) {
        if (model != null) {
            binding.layReturnInvDetails.visibility = View.VISIBLE
            model?.let {
                binding.run {
                    var address = if (!model.property_name.isNullOrEmpty()) {
                        model.property_name
                    } else if (!model.lodging.isNullOrEmpty()) {
                        model.lodging
                    } else {
                        ""
                    }
                    rentersAddressTv.text = address
                    model.reservation_id?.let {
                        rvResId.text = "Res ID:  " + model.reservation_id.toString()
                    }
                    rentersDayTv.text =
                        "${model.deliveryDate ?: "Pending"} - ${model.returnDate ?: "Pending"}"
                    rentersCountTv.text = "${it.numberOfRenters} Renters"
                    if (!model.customerPhone.isNullOrEmpty() &&
                        !model.customerPhone.isNullOrBlank()
                    ) {
                        ivCall.visibility = View.VISIBLE
                        rentersPhoneNumber.text = (model.customerPhone)
                    } else {
                        ivCall.visibility = View.INVISIBLE
                    }

                    if (!model.deliveryTime.isNullOrEmpty() &&
                        !model.deliveryTime.isNullOrBlank()
                    ) {
                        ivTime.visibility = View.VISIBLE
                        rentersTimeTv.text = model.deliveryTime
                    } else {
                        ivTime.visibility = View.INVISIBLE
                    }

                    if (!model.room_no.isNullOrEmpty()) {
                        tvRoomNum.visibility = View.VISIBLE
                        tvRoomNum.text = "Room No: ${model.room_no}"
                    } else {
                        tvRoomNum.visibility = View.GONE
                        tvRoomNum.text = ""
                    }
                }
            }
        } else {
            binding.layReturnInvDetails.visibility = View.GONE
        }

    }

    private fun consumeInventoryReturnResponse(
        apiResponse:
        ApiResponse<InventorReturnModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                Debugger.wtf(TAG, "consumeInventoryReturnResponse LOADING : ${apiResponse.data}")
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeInventoryReturnResponse SUCCESS : ${apiResponse.data}")
                loaderDialog.dismissDialog()
                handleInventoryReturn(apiResponse.data as InventorReturnModel)
            }
            Status.ERROR -> {
                Debugger.wtf(
                    TAG,
                    "consumeInventoryReturnResponse ERROR: " + apiResponse.error.toString()
                )
                loaderDialog.dismissDialog()
                showToast(
                    this@ReturnRentersActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ReturnRentersActivity)
            }
        }
    }


    private fun handleInventoryReturn(baseModel: InventorReturnModel) {
        if (baseModel.success) {
            hitApi()
            showToast(this, baseModel.message, baseModel.success)
        } else {
            if (baseModel.message.equals(
                    "This is not yet scheduled to be returned. Are you sure you want to return this gear?",
                    true
                )
            ) {
                CommonAlert("Revert Inventory", baseModel.message, baseModel.barcode)
            } else {
                hitApi()
                showToast(this, baseModel.message, baseModel.success)
            }

        }

    }

    private fun consumeBaseResponse(
        apiResponse:
        ApiResponse<BaseModel>
    ) {
        when (apiResponse.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                try {
                    var data: BaseModel? = apiResponse.data as BaseModel
                    if (data != null) {
                        if (data.message != null) {
                            showToast(this, data.message, data.success)
                        }
                    }
                } catch (e: Exception) {
                    Debugger.wtf(TAG, "Erorrr-------------> : ${e.message}")
                } finally {
                    loaderDialog.dismissDialog()
                    hitApi()
                }


            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@ReturnRentersActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@ReturnRentersActivity)
            }
        }
    }

    fun showDeleteInventoryAlert(model: ReturnInventory) {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                "Alert",
                "Are you sure you want to delete this \n' " + model.model.toString() + " ' Inventory?"
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                returnRemoveApi(model)
            }

        })
    }

    fun CommonAlert(title: String, des: String, barcode: String = "") {
        val commonDialog: CommonDialog =
            CommonDialog.newInstance(
                title,
                des
            )
        commonDialog.show(supportFragmentManager, "") //S
        commonDialog.setOnContinueCancelClick(object : CommonDialog.OnOKClick {
            override fun onOk() {
                if (barcode.isNotEmpty()) {
                    hitReturnUsingBarcode(barcode, "1")
                } else {
                    viewModel.hitMarkAllAsReturned(
                        sharedPrefsHelper.getUser()
                            ?.accessToken.toString(), shipmentId
                    )
                }
            }

        })
    }

    fun handleNfc() {
        pendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcReaderUtils = NfcReaderUtils()
        nfcReaderUtils.setOnNfcScanListener(object : NfcReaderUtils.OnNfcScanListener {
            override fun nfcBarcode(nfcCode: String) {
                if (addScanBarcodeDialog != null) {
                    if (addScanBarcodeDialog.isVisible) {
                        Debugger.wtf("nfcBarcode", nfcCode)
                        if (addScanBarcodeDialog.isAddon) {
                            hitSaveAddonWithBarcode(
                                nfcCode,
                                addScanBarcodeDialog.addonStatus,
                                addScanBarcodeDialog.renterId
                            )
                        } else {
                            hitReturnUsingBarcode(nfcCode, "0")
                        }
                        addScanBarcodeDialog.dismiss()
                    }
                }
            }
        })

        if (nfcAdapter != null) {
            nfcAdapter?.let { nfc ->
                nfc.isEnabled?.let { enable ->
                    if (!enable) {
                        openNfcSettings(this)
                    }
                }
            }

        } else {
            showToast(this, "There is no NFC Functionality in this device")
        }
    }

    fun showNfcScanDialog1(
        isAddons: Boolean = false, addonStatus: String = "",
        addonId: String = "",
        isUndoBtnVisible: Boolean = false
    ) {
        addScanBarcodeDialog.isAddon = isAddons
        addScanBarcodeDialog.addonStatus = addonStatus
        addScanBarcodeDialog.show(supportFragmentManager, "") //Show t
    }


    fun showNfcScanDialog(
        isAddons: Boolean = false,
        addonStatus: String = "",
        addonId: String = "",
        isUndoBtnVisible: Boolean = false,
        addOnBarcode: String = "",
        renterId: String = ""
    ) {
        addScanBarcodeDialog.isAddon = isAddons
        addScanBarcodeDialog.addonStatus = addonStatus
        addScanBarcodeDialog.addonId = addonId
        addScanBarcodeDialog.showUndoButton = isUndoBtnVisible
        addScanBarcodeDialog.addonBarcode = addOnBarcode
        addScanBarcodeDialog.renterId = renterId
        addScanBarcodeDialog.show(supportFragmentManager, "") //Sho
        Debugger.wtf(
            "isManuall", "Ac $isUndoBtnVisible  " +
                    "/ ${addScanBarcodeDialog.showUndoButton}"
        )

        addScanBarcodeDialog.setOnSubmitListener(object :
            ReturnAddScanBarcodeDialog.OnScanClick {
            override fun onScanBarcode(barcode: String, isBoot: Boolean, isAddon: Boolean) {
                if (addScanBarcodeDialog != null) {
                    if (addScanBarcodeDialog.isVisible) {
                        if (isAddon) {
                            hitSaveAddonWithBarcode(
                                barcode,
                                addScanBarcodeDialog.addonStatus,
                                renterId
                            )
                        } else {
                            hitReturnUsingBarcode(barcode, "0")
                        }
                        addScanBarcodeDialog.dismiss()
                    }
                }
            }

            override fun onUndoClickListener(addonId: String) {
                if (addScanBarcodeDialog.isAddon) {
                    if (addonStatus.equals("Fitted", true)) {
                        hitSaveAddonWithBarcode(
                            addOnBarcode,
                            addScanBarcodeDialog.addonStatus,
                            renterId
                        )
                    } else {
                        hitUndoAddonWithoutBarcode(addonId, addonStatus, renterId)
                    }
                    addScanBarcodeDialog.dismiss()
                }
            }

            override fun onDismiss() {
                Debugger.wtf("addScanBarcodeDialog", "onDismiss")
                //  stopNfcScan()
            }
        })
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        nfcReaderUtils.resolveIntent(intent!!)
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
    }


    fun stopNfcScan() {
        if (nfcAdapter != null) {
            nfcAdapter!!.disableForegroundDispatch(this)
        }
    }

    fun hitSaveAddonWithBarcode(barcode: String, addOnStatus: String, renterId: String) {
        val map = HashMap<String, String>()
        map["renter_id"] = renterId
        map["barcode"] = barcode
        map["status"] = addOnStatus
//        if (addOnStatus.equals("Returned", true)) {
        map["van_shipment_id"] = shipmentId.toString()
//        }

        packingViewModel.hitSaveAddonsWithBarcodeApi(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            map
        )
    }

    fun hitUndoAddonWithoutBarcode(addonId: String, addonStatus: String, renterId: String) {
        val map = HashMap<String, String>()
        map["renter_id"] = renterId
        map["addon_id"] = addonId
        map["van_shipment_id"] = shipmentId.toString()


        map["status"] = addonStatus
        map["is_manual"] = "1"
        packingViewModel.hitSaveAddonsWithBarcodeApi(
            sharedPrefsHelper.getUser()?.accessToken.toString(),
            map
        )
    }

    override fun onBackPressed() {
        setResult(RESULT_OK, intent);
        super.onBackPressed()

    }
}