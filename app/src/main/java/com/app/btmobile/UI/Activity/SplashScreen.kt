package com.app.btmobile.UI.Activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.SharedPrefsHelper
import com.google.gson.Gson
import javax.inject.Inject


class SplashScreen : AppCompatActivity() {
    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        MyApplication.getAppComponent(this).doInjection(this)
        var intent = Intent(this, LoginActivity::class.java)
        if (sharedPrefsHelper.isLoggedIn()) {
            intent = Intent(this, MainActivity::class.java)
        }
        Debugger.wtf("sharedPrefsHelper","${Gson().toJson(sharedPrefsHelper.getUser())}")
        Handler().postDelayed({
            startActivity(intent)
            finish()
        }, 1000)
    }
}