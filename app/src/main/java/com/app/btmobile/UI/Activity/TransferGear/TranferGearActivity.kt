package com.app.btmobile.UI.Activity.TransferGear

import android.R
import android.app.PendingIntent
import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.MyApplication
import com.app.btmobile.Status
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityTransfarGearBinding
import com.ogoul.kalamtime.viewmodel.TransferGearViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.util.*
import javax.inject.Inject

class TranferGearActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityTransfarGearBinding
    lateinit var loaderDialog: LoaderDialog

    var pendingIntent: PendingIntent? = null
    var nfcAdapter: NfcAdapter? = null
    lateinit var nfcReaderUtils: NfcReaderUtils


    lateinit var adapter: ArrayAdapter<TransferGearInventoryLocation>

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: TransferGearViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    var allItems: ArrayList<TransferGearInventoryLocation> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransfarGearBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupToolbarAndLoader()
        nfcBarcodeInitialise()
        setUpSpinnerAdapter()
        hitApi()

        binding.btnTransferGear.setOnClickListener {
            validate()
        }
    }

    fun nfcBarcodeInitialise() {
        pendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
        nfcReaderUtils = NfcReaderUtils()
        nfcReaderUtils.setOnNfcScanListener(object : NfcReaderUtils.OnNfcScanListener {
            override fun nfcBarcode(nfcCode: String) {
                hitTransferGearApi(nfcCode)
            }
        })

        if (nfcAdapter != null) {
            nfcAdapter?.let { nfc ->
                nfc.isEnabled?.let { enable ->
                    if (!enable) {
                        openNfcSettings(this)
                    }
                }
            }

        } else {
            showToast(this, "There is no NFC Functionality in this device")
        }
    }


    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(TransferGearViewModel::class.java)
        viewModel.getTransferGearList().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
        viewModel.getBaseResponse().observe(this, androidx.lifecycle.Observer {
            consumeBaseResponse(it)
        })
    }

    private fun setUpSpinnerAdapter() {
        adapter = ArrayAdapter<TransferGearInventoryLocation>(
            this,
            R.layout.simple_spinner_item, allItems
        )
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        binding.spinnerLocation.adapter = adapter
        binding.spinnerLocation.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val tutorialsName = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }


    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Transfer Gear"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })
    }

    fun validate() {
        if (binding.etGear.getText().toString() == "") {
            showToast(this, "Please Enter Barcode Code")
        } else {
            hitTransferGearApi(binding.etGear.getText().toString())
        }
    }


    fun hitApi() {
        viewModel.hitGetTransferGearList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString()
        )
    }

    fun hitTransferGearApi(barcode: String) {
        val model: TransferGearInventoryLocation =
            allItems.get(binding.spinnerLocation.getSelectedItemPosition())
        if (model != null) {
            val params = HashMap<String, String>()
            params["barcode"] = barcode + ""
            params["inventory_location_id"] = model.id.toString()

            viewModel.hitSaveTransferGear(
                sharedPrefsHelper.getUser()
                    ?.accessToken.toString(),
                params
            )
        }
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<TransferGearModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                showData(apiResponse.data as TransferGearModel)
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@TranferGearActivity,
                    "Something went wrong"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@TranferGearActivity)
            }
        }
    }

    private fun consumeBaseResponse(
        apiResponse:
        ApiResponse<BaseModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                try {
                    var data: BaseModel? = apiResponse.data as BaseModel
                    if (data != null) {
                        if (data.message != null) {
                            showToast(this, data.message, data.success)
                        }
                    }
                } catch (e: Exception) {
                    Debugger.wtf(TAG, "Erorrr-------------> : ${e.message}")
                } finally {
                    loaderDialog.dismissDialog()
                    // hitApi()
                }
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@TranferGearActivity,
                    "Something went wrong"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@TranferGearActivity)
            }
        }
    }

    private fun showData(model: TransferGearModel) {
        binding.run {
            allItems.clear()
            allItems.addAll(model.data.inventoryLocations)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        nfcReaderUtils.resolveIntent(intent!!)
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
    }

    override fun onPause() {
        super.onPause()
        if (nfcAdapter != null) {
            nfcAdapter!!.disableForegroundDispatch(this)
        }
    }
}