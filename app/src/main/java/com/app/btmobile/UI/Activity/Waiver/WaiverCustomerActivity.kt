package com.app.btmobile.UI.Activity.Waiver

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.WaiverCustomerModel
import com.app.btmobile.Models.WaiverCustomerModelItem
import com.app.btmobile.MyApplication
import com.app.btmobile.Status
import com.app.btmobile.UI.Activity.Waiver.WaiverRentersActivity
import com.app.btmobile.UI.Adapter.WaiversAdapter
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityWaiverCustomerActivityBinding
import com.ogoul.kalamtime.viewmodel.WaiverViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import java.util.*
import javax.inject.Inject

class WaiverCustomerActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityWaiverCustomerActivityBinding
    lateinit var loaderDialog: LoaderDialog

    lateinit var adapter: WaiversAdapter

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: WaiverViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    var allItems: ArrayList<WaiverCustomerModelItem> = ArrayList()


    var selectedDate: String = "";
    var c = Calendar.getInstance()
    var mYear = c[Calendar.YEAR]
    var mMonth = c[Calendar.MONTH]
    var mDay = c[Calendar.DAY_OF_MONTH]

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWaiverCustomerActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewModel()
        setupViews()
        setupToolbarAndLoader()
        setupRecyclerview()
    }

    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(WaiverViewModel::class.java)
        viewModel.getWaiverCustomers().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
    }

    private fun setupRecyclerview() {
        adapter = WaiversAdapter(this, allItems)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvWaiverCustomer.layoutManager = mLayoutManager
        binding.rvWaiverCustomer.itemAnimator = DefaultItemAnimator()
        binding.rvWaiverCustomer.adapter = adapter
    }

    private fun setupViews() {
        selectedDate = (mMonth + 1).toString() + "/" + mDay + "/" + mYear
        binding.tvDateSelected.text = (mMonth + 1).toString() + "/" + mDay + "/" + mYear

        binding.btnSelectDate.setOnClickListener {
            openDatePicker()
        }
    }

    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Waiver Customer"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })
    }


    override fun onResume() {
        super.onResume()
        hitApi()
    }

    fun hitApi() {
        val date: String = parseDateToddMMddyyy(selectedDate) ?: ""
        Debugger.wtf("hitApi", "${date.toString()}")
        viewModel.hitGetWaiverCustomersList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), date
        )
    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<WaiverCustomerModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                showData(apiResponse.data as WaiverCustomerModel)
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@WaiverCustomerActivity,
                    "${apiResponse.error.toString()}"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@WaiverCustomerActivity)
            }
        }
    }

    private fun showData(waiverCustomerModel: WaiverCustomerModel) {
        val waiversList = waiverCustomerModel
        Debugger.wtf("showData", "${waiversList.size}")
        if (waiversList.size > 0) {
            binding.noDeliveriesTv.visibility = View.GONE
            allItems.clear()
            allItems.addAll(waiversList)
            adapter.notifyDataSetChanged()
        } else {
            allItems.clear()
            adapter.notifyDataSetChanged()
            binding.noDeliveriesTv.visibility = View.VISIBLE
        }
    }


    private fun openDatePicker() {
        val datePickerDialog: DatePickerDialog =
            object : DatePickerDialog(
                this@WaiverCustomerActivity,
                null, mYear, mMonth, mDay
            ) {
                override fun onDateChanged(
                    @NonNull view: DatePicker,
                    year: Int,
                    monthOfYear: Int,
                    dayOfMonth: Int
                ) {
                    mYear = year
                    mMonth = monthOfYear
                    mDay = dayOfMonth
                    selectedDate = "${mMonth + 1}/$dayOfMonth/$year"
                    binding.tvDateSelected.setText("${mMonth + 1}/$dayOfMonth/$year")
                    hitApi()
                    dismiss()
                }
            }
        datePickerDialog.show()
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).visibility = View.GONE
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).visibility = View.GONE
    }

    fun callNextActivity(position:Int) {
        val i = Intent(this, WaiverRentersActivity::class.java)
        i.putExtra("from", AppConstants.DeliveryCustomerActivity)
        i.putExtra("reservationId", allItems[position].id)
        startActivity(i)
    }
}