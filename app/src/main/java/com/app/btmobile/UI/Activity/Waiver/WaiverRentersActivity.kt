package com.app.btmobile.UI.Activity.Waiver

import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.util.Base64
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.BaseModel
import com.app.btmobile.Models.WaiverRenter
import com.app.btmobile.Models.WaiverRentersModel
import com.app.btmobile.MyApplication
import com.app.btmobile.R
import com.app.btmobile.Status
import com.app.btmobile.UI.Activity.Delivery.ConfirmationActivity
import com.app.btmobile.UI.Activity.Delivery.DeliveryRentersActivity
import com.app.btmobile.UI.Adapter.WaiverRentersAdapter
import com.app.btmobile.UI.Dialog.SendToPhoneDialog
import com.app.btmobile.UI.Dialog.WaiverSignatureDialog
import com.app.btmobile.Utils.*
import com.app.btmobile.databinding.ActivityWaiverRentersBinding
import com.ogoul.kalamtime.viewmodel.WaiverViewModel
import com.ogoul.kalamtime.viewmodel.factory.ViewModelFactory
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.util.*
import javax.inject.Inject

class WaiverRentersActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    lateinit var binding: ActivityWaiverRentersBinding
    lateinit var loaderDialog: LoaderDialog

    lateinit var adapter: WaiverRentersAdapter

    @Inject
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: WaiverViewModel

    @Inject
    lateinit var sharedPrefsHelper: SharedPrefsHelper

    var allItems: ArrayList<WaiverRenter> = ArrayList()
    lateinit var waiverRentersModel: WaiverRentersModel


    var reservationId = -1
    var renterId: Int = -1
    var deliveryId: Int = -1
    var fromFitting = ""
    var switchKeep = false
    var isDinVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWaiverRentersBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getIntentData()
        setupViewModel()
        setupToolbarAndLoader()
        setupRecyclerview()
    }

    fun getIntentData() {
        if (intent != null) {
            reservationId = intent.getIntExtra("reservationId", -1)
            renterId = intent.getIntExtra("renter_id", -1)
            deliveryId = intent.getIntExtra("deliveryId", -1)
            fromFitting = intent.getStringExtra("fromFitting").toString()
            switchKeep = intent.getBooleanExtra("switchKeep", false)
            isDinVisible = intent.getBooleanExtra("isDinVisible", false)
        }
    }

    private fun setupViewModel() {
        MyApplication.getAppComponent(this).doInjection(this)
        viewModel = ViewModelProvider(this, factory).get(WaiverViewModel::class.java)
        viewModel.getWaiverRenters().observe(this, androidx.lifecycle.Observer {
            consumeResponse(it)
        })
        viewModel.getSendToPhoneResponse().observe(this, androidx.lifecycle.Observer {
            consumeSendToPhoneResponse(it)
        })
    }

    private fun setupRecyclerview() {
        adapter = WaiverRentersAdapter(this, allItems, deliveryId, { from, position ->
            onItemClickListener(from, position)
        })
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.rvWaiverRenters.layoutManager = mLayoutManager
        binding.rvWaiverRenters.itemAnimator = DefaultItemAnimator()
        binding.rvWaiverRenters.adapter = adapter
    }

    fun onItemClickListener(from: String, position: Int) {
        var model = allItems[position]
        when (from) {
            "view" -> {
                showSignatureDialog(model, true)
            }
            "sign" -> {
                showSignatureDialog(model)
            }
            "sendToPhone" -> {
                showSendToPhoneDialog(
                    model.id.toString(),
                    waiverRentersModel.phoneNumber
                )
            }
        }
    }


    private fun setupToolbarAndLoader() {
        loaderDialog = LoaderDialog(this)
        loaderDialog.initializeDialog()

        setSupportActionBar(binding.toolbarLayout.toolbar)
        supportActionBar!!.title = "Waiver Renters"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(View.OnClickListener { finish() })
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.waiver_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.goToConfirmation -> {
                moveToConfirmationScreen()
            }
            R.id.menuHome -> {
                GoToHomeScreen(this)
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onResume() {
        super.onResume()
        hitApi()
    }

    fun hitApi() {
        viewModel.hitGetWaiverRentersList(
            sharedPrefsHelper.getUser()
                ?.accessToken.toString(), reservationId, deliveryId
        )

    }

    private fun consumeResponse(
        apiResponse:
        ApiResponse<WaiverRentersModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                showData(apiResponse.data as WaiverRentersModel)
                loaderDialog.dismissDialog()
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@WaiverRentersActivity,
                    "Something went wrong"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@WaiverRentersActivity)
            }
        }
    }

    private fun consumeSendToPhoneResponse(
        apiResponse:
        ApiResponse<BaseModel>?
    ) {
        when (apiResponse?.status) {
            Status.LOADING -> {
                loaderDialog.showDialog()
            }
            Status.SUCCESS -> {
                Debugger.wtf(TAG, "consumeResponse SUCCESS : ${apiResponse.data}")
                try {
                    var data: BaseModel? = apiResponse.data as BaseModel
                    if (data != null) {
                        if (data.message != null) {
                            showToast(this, data.message, data.success)
                        }
                    }
                } catch (e: Exception) {
                    Debugger.wtf(TAG, "Erorrr-------------> : ${e.message}")
                } finally {
                    loaderDialog.dismissDialog()
                    hitApi()
                }
            }
            Status.ERROR -> {
                Debugger.wtf(TAG, "consumeResponse ERROR: " + apiResponse.error.toString())
                loaderDialog.dismissDialog()
                showToast(
                    this@WaiverRentersActivity,
                    "Something went wrong"
                )
            }
            else -> {
                loaderDialog.dismissDialog()
                logout(this@WaiverRentersActivity)
            }
        }
    }

    private fun showData(waiverRentersModel: WaiverRentersModel) {
        this.waiverRentersModel = waiverRentersModel
        var waiversList = waiverRentersModel.renters
        Debugger.wtf("showData", "${waiversList.size}")
        binding.run {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                waiverDesTv.text = Html.fromHtml(
                    waiverRentersModel.waiverText,
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                waiverDesTv.text = Html.fromHtml(waiverRentersModel.waiverText)
            }

            if (!waiverRentersModel.parentGuardianText.isNullOrEmpty()) {
                guardianTv.visibility = View.VISIBLE
                guardianTv.text = waiverRentersModel.parentGuardianText
            }
            if (waiversList.isNotEmpty()) {
                noDeliveriesTv.visibility = View.GONE
                allItems.clear()

                if (fromFitting == "Yes") {
                    for (renter in waiversList) {
                        if (renter.id == renterId) {
                            allItems.add(renter)
                            break
                        }
                    }
                } else {
                    allItems.addAll(waiversList)
                }

                adapter.notifyDataSetChanged()
                object : CountDownTimer(500, 1) {
                    override fun onTick(millisUntilFinished: Long) {
                        waiverScrollView.fullScroll(View.FOCUS_DOWN)
                    }

                    override fun onFinish() {}
                }.start()

            } else {
                allItems.clear()
                adapter.notifyDataSetChanged()
                noDeliveriesTv.visibility = View.VISIBLE
            }
        }
    }

    fun showSendToPhoneDialog(renterId: String, phoneNumber: String?) {
        var sendToPhoneDialog: SendToPhoneDialog = SendToPhoneDialog.newInstance(phoneNumber)
        sendToPhoneDialog.show(supportFragmentManager, "")
        sendToPhoneDialog.setonSendClickListener(object : SendToPhoneDialog.OnSendClick {
            override fun onSend(phoneNum: String) {
                val params = HashMap<String, Any>()
                params["renter_id"] = renterId + ""
                if (deliveryId != 0 &&
                    deliveryId != -1
                ) {
                    params["delivery_id"] = deliveryId.toString()
                }
                if (phoneNum.isNotEmpty()) {
                    params["send_to_customer"] = false
                    params["phone"] = phoneNum
                } else {
                    params["send_to_customer"] = true
                }
                viewModel.hitSendToPhone(
                    sharedPrefsHelper.getUser()
                        ?.accessToken.toString(), params
                )
            }
        })
    }

    fun showSignatureDialog(model: WaiverRenter, isOnlyViewData: Boolean = false) {
        val waiverSignatureDialog: WaiverSignatureDialog =
            WaiverSignatureDialog.newInstance(
                model, switchKeep, isDinVisible, isOnlyViewData
            )
        waiverSignatureDialog.setOnContinueCancelClick(object :
            WaiverSignatureDialog.OnSignatureSaved {
            override fun onSignDone(signBitmap: Bitmap) {
                // signatureBitmap = signBitmap

                viewModel.hitSaveWaiverSignature(
                    sharedPrefsHelper.getUser()
                        ?.accessToken.toString(),
                    signatureData(
                        signBitmap, model.fullName,
                        model.dob, "", model.id
                    )
                )

            }
        })
        waiverSignatureDialog.show(supportFragmentManager, "") //S
    }


    fun callNextActivity(position: Int) {
        val i = Intent(this, DeliveryRentersActivity::class.java)
        i.putExtra("from", AppConstants.DeliveryCustomerActivity)
        i.putExtra("modelObject", allItems[position])
        startActivity(i)
    }

    private fun signatureData(
        signatureBitmap: Bitmap,
        name: String, date_of_birth: String,
        din_setting: String, id: Int
    ): RequestBody {
        val builder: MultipartBody.Builder = MultipartBody.Builder().setType(MultipartBody.FORM)
        val byteArrayOutputStream = ByteArrayOutputStream()
        signatureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)
        Debugger.wtf(
            "signatureData", name + " / " + date_of_birth + " / " +
                    renterId + " / " + din_setting + " / " + deliveryId
        )

        builder
            .addFormDataPart("name", name)
            .addFormDataPart("date_of_birth", date_of_birth)
            .addFormDataPart("renter_id", id.toString() + "")
            .addFormDataPart("din_setting", din_setting)
            .addFormDataPart("signature", encoded)

        if (deliveryId != 0) {
            builder.addFormDataPart("delivery_id", deliveryId.toString())
        }
        return builder.build()
    }

    fun moveToConfirmationScreen() {
        val i = Intent(this, ConfirmationActivity::class.java)
        i.putExtra("reservationId", reservationId)
        startActivity(i)
    }
}