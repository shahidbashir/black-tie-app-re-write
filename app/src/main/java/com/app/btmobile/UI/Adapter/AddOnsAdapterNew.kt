package com.app.btmobile.UI.Adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.ReturnAddon
import com.app.btmobile.Models.ReturnRenterModelItem
import com.app.btmobile.UI.Activity.Return.ReturnRentersActivity
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.GoToHomeScreen
import com.app.btmobile.databinding.AddonsItemNewBinding
import java.util.*
import kotlin.collections.ArrayList

class AddOnsAdapterNew(
    var mContext: Context,
) : RecyclerView.Adapter<AddOnsAdapterNew.VHolder>() {
    var data: ReturnRenterModelItem? = null
    var addonList: ArrayList<ReturnAddon> = ArrayList()

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): VHolder {
        val binding: AddonsItemNewBinding =
            AddonsItemNewBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return VHolder(binding)
    }

    override fun onBindViewHolder(
        vHolder: VHolder,
        i: Int
    ) {
        Debugger.wtf("AddonsList", "" + addonList.size)
        val model = addonList[i]

        vHolder.binding.run {
            tvAddons.text = model.name
            tvNameAddOns.text = data!!.renterName
            if (model.isReturned.toString().equals("1", true)) {
                cbReturnedAddOnes.isChecked = true
                cbReturnedAddOnes.isEnabled = false
                tvRemoveAddons.visibility = View.VISIBLE
                tvReturnedStatusAddons.text = "Returned"
                tvReturnedStatusAddons.background.setColorFilter(
                    Color.parseColor("#2dd57b"),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                cbReturnedAddOnes.isChecked = false
                cbReturnedAddOnes.isEnabled = true
                tvRemoveAddons.visibility = View.GONE
                tvReturnedStatusAddons.text = "Pending"
                tvReturnedStatusAddons.background.setColorFilter(
                    Color.parseColor("#d95350"),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
            if (!addonList[i].addonBarcode.isNullOrBlank() &&
                !addonList[i].addonBarcode.isNullOrEmpty()
            ) {
                tvAddonsBarcode.text = addonList[i].addonBarcode.toString()
            }
            if (!addonList[i].addonSize.isNullOrBlank() &&
                !addonList[i].addonSize.isNullOrEmpty()
            ) {
                tvAddonSize.text = addonList[i].addonSize.toString()
            }

            Debugger.wtf(
                "isManuall",
                "$i ${data?.renterName} ${addonList[i].name}  / ${addonList[i].is_manual} / ${addonList.size}"
            )
            cbReturnedAddOnes.setOnClickListener {
                if (cbReturnedAddOnes.isChecked) {
                    if (addonList[i].isAddonWithBarcode) {
                        cbReturnedAddOnes.isChecked = false
                        Debugger.wtf(
                            "isManuall",
                            "cbReturnedAddOnes $i ${data?.renterName} ${addonList[i].addonBarcode} ${addonList[i].name} / ${addonList[i].is_manual}  " +
                                    "/ ${addonList[i].is_manual == 1}"
                        )
                        if (addonList[i].is_manual == 0) {
                            (mContext as ReturnRentersActivity).showNfcScanDialog(
                                true, "Returned",
                                addonList[i].id.toString(),
                                false,
                                renterId = data?.renterId.toString()
                            )
//                            (mContext as ReturnRentersActivity).hitSaveAddonWithBarcode(
//                                addonList[i].addonBarcode.toString(),
//                                "Returned",
//                                data?.renterId.toString()
//                            )
                        } else {
                            (mContext as ReturnRentersActivity).showNfcScanDialog(
                                true, "Returned",
                                addonList[i].id.toString(),
                                addonList[i].is_manual == 1,
                                renterId = data?.renterId.toString()
                            )
                        }
                    } else {
                        // GoToHomeScreen(mContext as ReturnRentersActivity)
                        (mContext as ReturnRentersActivity).addRemoveClothingApi(
                            "addon",
                            addonList[i].id.toString(),
                            data?.renterId!!,
                            "1"
                        )
                        cbReturnedAddOnes.isEnabled = false
                    }

                }
            }
            tvRemoveAddons.setOnClickListener {
                if (addonList[i].isAddonWithBarcode) {
                    Debugger.wtf(
                        "isManuall",
                        "tvRemoveAddons $i ${data?.renterName} ${addonList[i].addonBarcode} ${addonList[i].name} / ${addonList[i].is_manual}  " +
                                "/ ${addonList[i].is_manual == 1}"
                    )

                    if (addonList[i].is_manual == 0) {
                        (mContext as ReturnRentersActivity).showNfcScanDialog(
                            true, "Fitted",
                            addonList[i].id.toString(),
                            false,
                            addonList[i].addonBarcode.toString(),
                            renterId = data?.renterId.toString()
                        )
//                        (mContext as ReturnRentersActivity).hitSaveAddonWithBarcode(
//                            addonList[i].addonBarcode.toString(),
//                            "Fitted",
//                            data?.renterId.toString()
//                        )
                    } else {
                        (mContext as ReturnRentersActivity).showNfcScanDialog(
                            true, "Fitted",
                            addonList[i].id.toString(),
                            addonList[i].is_manual == 1,
                            addonList[i].addonBarcode.toString(),
                            renterId = data?.renterId.toString()
                        )
                    }
                } else {
                    (mContext as ReturnRentersActivity).addRemoveClothingApi(
                        "addon",
                        addonList[i].id.toString(),
                        data?.renterId!!,
                        "0"
                    )
                    cbReturnedAddOnes.isEnabled = false
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return addonList.size
    }

    inner class VHolder(val binding: AddonsItemNewBinding) :
        RecyclerView.ViewHolder(binding.root) {}


    fun updateData(data_this: ReturnRenterModelItem?) {
        data = data_this
        data?.addons?.let { addonList.addAll(it) }
        data?.addonsWithBarcodeList?.let { addonList.addAll(it) }
        notifyDataSetChanged()
    }

}