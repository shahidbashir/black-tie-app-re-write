package com.app.btmobile.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.R
import com.app.btmobile.UI.Adapter.DeliveryAddonsAdapter.VHolder
import com.app.btmobile.databinding.CustomDeliveryAddonsItemBinding
import com.app.btmobile.databinding.CustomDeliveryRentersLayBinding
import java.util.*

class DeliveryAddonsAdapter(
    var context: Context,
    var addonsList: List<String>
) : RecyclerView.Adapter<VHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): VHolder {

        val binding: CustomDeliveryAddonsItemBinding =
            CustomDeliveryAddonsItemBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return VHolder(binding)
    }

    override fun onBindViewHolder(vHolder: VHolder, i: Int) {
        vHolder.binding.run {
            when {
                addonsList[i] == "Damage Waiver" -> {
                    tvAddonsLatest.text = "D.W"
                }
                addonsList[i] == "Premium Boot Upgrade - With HEATED BOOTS" -> {
                    tvAddonsLatest.text = "Heated Boots"
                }
                else -> {
                    tvAddonsLatest.text = addonsList[i]
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return addonsList.size
    }

    inner class VHolder(val binding: CustomDeliveryAddonsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {}

}