package com.app.btmobile.UI.Adapter

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.DeliveryCustomersModel
import com.app.btmobile.Models.DeliveryOverViewModel
import com.app.btmobile.UI.Activity.Delivery.DeliveryCustomerActivity
import com.app.btmobile.Utils.GoToMap
import com.app.btmobile.Utils.getPackingStatusColor
import com.app.btmobile.databinding.CustomOverviewLayBinding
import com.app.btmobile.databinding.IsgroupingLayoutBinding

class DeliveryCustomerAdapter(
    var context: Context,
    var allitems: List<DeliveryOverViewModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return if (viewType == 1) {
            GroupingViewHolder(
                IsgroupingLayoutBinding
                    .inflate(LayoutInflater.from(parent.context), parent, false)
            )

        } else {
            CustomOverLayViewHolder(
                CustomOverviewLayBinding
                    .inflate(LayoutInflater.from(parent.context), parent, false)
            )
        }
    }


    override fun getItemCount(): Int {
        return allitems.size
    }

    override fun getItemViewType(position: Int): Int {
        return allitems.get(position).viewType
    }

    inner class GroupingViewHolder(val binding: IsgroupingLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {}

    inner class CustomOverLayViewHolder(val binding: CustomOverviewLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}


    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val overViewModel: DeliveryOverViewModel = allitems[position]

        if (overViewModel.viewType == 1) {
            val holder = viewHolder as GroupingViewHolder

            val adapter =
                GroupingDeliveriesAdapter(context, overViewModel.arrayList!!)
            val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
            holder.binding.rvIsGrouping.layoutManager = mLayoutManager
            holder.binding.rvIsGrouping.itemAnimator = DefaultItemAnimator()
            holder.binding.rvIsGrouping.adapter = adapter
        } else if (overViewModel.viewType == 2) {
            val model: DeliveryCustomersModel = overViewModel.model!!
            val holder =
                viewHolder as CustomOverLayViewHolder

            if (model.schedule_indicator_class != null) {
                var colorStr = ""
                colorStr = when {
                    model.schedule_indicator_class.equals("group-fit", true) -> {
                        "#d95350"
                    }
                    model.schedule_indicator_class.equals("switch-out", true) -> {
                        "#2dd57b"
                    }
                    else -> {
                        "#60b1fe"
                    }
                }
                if (model.schedule_indicator_class.toString().contains("switch-out-border")) {
                    colorStr = "#2dd57b"
                }
                val color =
                    arrayOf(ColorDrawable(Color.parseColor(colorStr)))
                val trans =
                    TransitionDrawable(color)
                holder.binding.viewSideColor.setBackgroundDrawable(trans)
            }
            if (!model.time.toString().isNullOrEmpty()) {
                val s: String = model.time.toString()
                val splitedTime =
                    s.split("\\s+".toRegex()).toTypedArray()
                if (splitedTime.size >= 2) {
                    holder.binding.amTv.text = splitedTime[1]
                    holder.binding.timeTv.text = splitedTime[0]
                }
            }

            var address = if (!model.address.isNullOrEmpty()) {
                (model.address)
            } else if (!model.lodging.isNullOrEmpty()) {
                (model.lodging)
            } else {
                ""
            }
            if (!address?.isEmpty()!!) {
                holder.binding.renterAddresTv.visibility = View.VISIBLE
                holder.binding.renterAddresTv.text = address
            } else {
                holder.binding.renterAddresTv.visibility = View.GONE
            }


            holder.binding.renterNameTv.text = (
                    if (model.customer_name == null) "" else model.customer_name
                        .toString()
                    )

            holder.binding.rentersCountTv.text = model.number_of_renters.toString()
            holder.binding.tvVanNum.text = model.van

            if (!model.room_number.toString().isNullOrEmpty() &&
                !model.room_number.toString().equals("null", true)
            ) {
                holder.binding.layRoomNum.visibility = View.VISIBLE
                holder.binding.tvRoomNum.text = model.room_number.toString()
            } else {
                holder.binding.layRoomNum.visibility = View.INVISIBLE
            }
            holder.binding.deliveryWaiverTv.text = model.packing_status
            getPackingStatusColor(holder.binding.deliveryWaiverTv, model.packing_status.toString())
            holder.itemView.setOnClickListener(View.OnClickListener {
                (context as DeliveryCustomerActivity).callNextActivity(
                    model
                )
            })
            if (model.map_url.toString().isNullOrBlank() ||
                model.map_url.toString().isNullOrBlank() ||
                model.map_url.toString().equals("null", true)
            ) {
                holder.binding.mapViewDelivery.visibility = View.GONE
            } else {
                holder.binding.mapViewDelivery.visibility = View.VISIBLE
            }
            holder.binding.mapViewDelivery.setOnClickListener(View.OnClickListener { //
                GoToMap(
                    context as Activity,
                    model.map_url.toString()
                )
                //  String strUri = "http://maps.google.com/maps?q=loc:" + 51.5074 + "," + 0.1278 + " (" + "Testing" + ")";
//                if (model.map_url == null) {
//                    Toast.makeText(context, "Location is not found", Toast.LENGTH_SHORT).show()
//                } else {
//                    val strUri: String = model.map_url.toString()
//                    val intent = Intent(
//                        Intent.ACTION_VIEW,
//                        Uri.parse(strUri)
//                    )
//                    intent.setClassName(
//                        "com.google.android.apps.maps",
//                        "com.google.android.maps.MapsActivity"
//                    )
//                    context.startActivity(intent)
//                }
            })
        }

    }
}