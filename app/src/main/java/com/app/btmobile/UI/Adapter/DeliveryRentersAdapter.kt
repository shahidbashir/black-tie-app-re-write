package com.app.btmobile.UI.Adapter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.DeliveryRenterListModel
import com.app.btmobile.R
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.CustomDeliveryRentersLayBinding
import com.app.btmobile.databinding.CustomOverviewGroupingCopyLayBinding
import java.util.*

class DeliveryRentersAdapter(
    var context: Context,
    private val allItemsNew: ArrayList<DeliveryRenterListModel>,
    var isSwitchout: Boolean,
    private val onItemClickListener: (String, Int) -> Unit
) : RecyclerView.Adapter<DeliveryRentersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ViewHolder {

        val binding: CustomDeliveryRentersLayBinding =
            CustomDeliveryRentersLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ViewHolder(binding)
    }

    fun showSwitchOutItems(isSwitchout: Boolean) {
        this.isSwitchout = isSwitchout
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) {

        val renterNew = allItemsNew[position]

        holder.binding.run {
            if (!renterNew.addons.isNullOrEmpty()) {
                var adapter = DeliveryAddonsAdapter(context, renterNew.addons)
                val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
                rvAddOnLatest.layoutManager = mLayoutManager
                rvAddOnLatest.itemAnimator = DefaultItemAnimator()
                rvAddOnLatest.adapter = adapter
            }

            if (!renterNew.clothing.isNullOrEmpty()) {
                renterClothingTv.text = renterNew.clothing
            }
            if (!renterNew.addons.isNullOrEmpty()) {
                var addons = ""
                for (i in renterNew.addons.indices) {
                    addons = if (i == 0) {
                        when {
                            renterNew.addons[i] == "Damage Waiver" -> {
                                addons + "D.W"
                            }
                            renterNew.addons[i] == "Premium Boot Upgrade - With HEATED BOOTS" -> {
                                addons + "Heated Boots"
                            }
                            else -> {
                                addons + renterNew.addons[i]
                            }
                        }
                    } else {
                        when {
                            renterNew.addons[i] == "Damage Waiver" -> {
                                "$addons, D.W"
                            }
                            renterNew.addons[i] == "Premium Boot Upgrade - With HEATED BOOTS" -> {
                                "$addons, Heated Boots"
                            }
                            else -> {
                                addons + ", " + renterNew.addons[i]
                            }
                        }
                    }
                }
                renterAddonsTv.text = addons
                Debugger.wtf("addonsNew", "->$addons")
            }
            renterNameTv.text = renterNew.name
            renterPackageTv.setText(renterNew.`package`)
            if (renterNew.has_history) {
                rentersHistoryTv.visibility = View.VISIBLE
            } else {
                rentersHistoryTv.visibility = View.GONE
            }
            if (isSwitchout) {
                switchOutTv.visibility = View.VISIBLE
                switchOutReasonTv.visibility = View.VISIBLE
            } else {
                switchOutTv.visibility = View.GONE
                switchOutReasonTv.visibility = View.GONE
            }
            switchOutTv.setOnClickListener {
                onItemClickListener("SwitchOut", position)
            }
            switchOutReasonTv.setOnClickListener {
                onItemClickListener("SwitchOutReturn", position)
            }
            goForPackTv.setOnClickListener {
                onItemClickListener("Pack", position)
            }
            goForFitTv.setOnClickListener {
                onItemClickListener("Fit", position)
            }
            waiverTv.setOnClickListener {
                onItemClickListener("Waiver", position)
            }
            rentersHistoryTv.setOnClickListener {
                onItemClickListener("History", position)
            }

            setBtnBackColor(goForFitTv, renterNew.fitted_color)
            setBtnBackColor(goForPackTv, renterNew.packing_color)

            var waiverColor = if (renterNew.is_waiver_signed) {
                "#81c868"
            } else {
                "#f05050"
            }
            setBtnBackColor(waiverTv, waiverColor)

            if (isSwitchout && renterNew.fitted_color.contains("f05050")) {
                setBtnBackColor(waiverTv, "#f05050")
            }
        }
    }

    fun setBtnBackColor(btn: Button, color1: String) {
        val color = arrayOf(ColorDrawable(Color.parseColor(color1)))
        val trans =
            TransitionDrawable(color)
        btn.background = (trans)
    }

    override fun getItemCount(): Int {
        return allItemsNew.size
    }

    inner class ViewHolder(val binding: CustomDeliveryRentersLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}
}