package com.app.btmobile.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.FittingAddon
import com.app.btmobile.databinding.CustomFitAddonsLayBinding

class FitAddonsAdapter(
    var context: Context,
    var list: List<FittingAddon>,
    private val onItemClickListener: (String, Int) -> Unit
) : RecyclerView.Adapter<FitAddonsAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val binding: CustomFitAddonsLayBinding =
            CustomFitAddonsLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: MyViewHolder,
        position: Int
    ) {
        val model = list[position]
        holder.binding.run {
            if (list[position].purchased_addon) {
                tvRequire.visibility = View.VISIBLE
            } else {
                tvRequire.visibility = View.GONE
            }
            tvValue.text = "$"+model.price + " Per Day"
            if (model.name.equals("Damage Waiver", ignoreCase = true)) {
                cbAddOn.text = "D.W."
            } else if (model.name.equals(
                    "Premium Boot Upgrade - With HEATED BOOTS",
                    ignoreCase = true
                )
            ) {
                cbAddOn.text = "Heated Boots"
            } else {
                cbAddOn.text = model.name
            }

            val completeSize = list[position].completeSize
            if (completeSize.isNullOrEmpty() ||
                completeSize.equals("N/A", true)
            ) {
                layAddonsSize.visibility = View.GONE
            } else {
                layAddonsSize.visibility = View.VISIBLE
                tvAddonSize.text = "${list[position].completeSize}"
            }

            cbAddOn.isChecked = model.selected
            cbAddOn.setOnClickListener {
                if (list[position].isAddonWithBarcode) {
                    if (cbAddOn.isChecked) {
                        cbAddOn.isChecked = false
                        onItemClickListener("Fitted", position)
                    } else {
                        cbAddOn.isChecked = true
                        onItemClickListener("Packed", position)
                    }
                } else {
                    model.selected = cbAddOn.isChecked
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(val binding: CustomFitAddonsLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}

}