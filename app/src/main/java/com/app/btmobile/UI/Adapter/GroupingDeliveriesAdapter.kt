package com.app.btmobile.UI.Adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.DeliveryCustomersModel
import com.app.btmobile.UI.Activity.Delivery.DeliveryCustomerActivity
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.GoToMap
import com.app.btmobile.Utils.getPackingStatusColor
import com.app.btmobile.databinding.CustomOverviewGroupingCopyLayBinding
import java.util.*

class GroupingDeliveriesAdapter(
    private val context: Context,
    private val allItems: ArrayList<DeliveryCustomersModel>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {

        val binding: CustomOverviewGroupingCopyLayBinding =
            CustomOverviewGroupingCopyLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return CustomOverLayViewHolder(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val model = allItems[i]
        val holder = viewHolder as CustomOverLayViewHolder

        holder.binding.run {
//            if (model.schedule_indicator_class != null) {
//                var colorStr = ""
//                colorStr =
//                    when {
//                        model.schedule_indicator_class.equals("group-fit", ignoreCase = true) -> {
//                            "#d95350"
//                        }
//                        model.schedule_indicator_class.equals("switch-out", ignoreCase = true) -> {
//                            "#2dd57b"
//                        }
//                        else -> {
//                            "#60b1fe"
//                        }
//                    }
//                if (model.status_indicator_class!!.contains("switch-out-border")) {
//                    colorStr = "#2dd57b"
//                }
//                val color =
//                    arrayOf(ColorDrawable(Color.parseColor(colorStr)))
//                val trans =
//                    TransitionDrawable(color)
//                viewSideColor.setBackgroundDrawable(trans)
//            }
            val s = model.time
            val splitedTime = s!!.split("\\s+".toRegex()).toTypedArray()
            Debugger.wtf("Timeee", s)
            if (splitedTime.size >= 2) {
                timeTv.text = splitedTime[0]
                amTv.text = splitedTime[1]
            }
            var address = if (!model.address.isNullOrEmpty()) {
                (model.address)
            } else if (!model.lodging.isNullOrEmpty()) {
                (model.lodging)
            } else {
                ""
            }
            if (!address?.isEmpty()!!) {
                holder.binding.renterAddresTv.visibility = View.VISIBLE
                holder.binding.renterAddresTv.text = address
            } else {
                holder.binding.renterAddresTv.visibility = View.GONE
            }
            renterNameTv.text = if (model.customer_name == null) "" else """
     ${model.customer_name}
     """.trimIndent()
            rentersCountTv.text = model.number_of_renters.toString()
            tvVanNum.text = model.van
            if (!model.room_number.toString().isNullOrEmpty() &&
                !model.room_number.toString().equals("null", true)
            ) {
                holder.binding.layRoomNum.visibility = View.VISIBLE
                holder.binding.tvRoomNum.text = model.room_number.toString()
            } else {
                holder.binding.layRoomNum.visibility = View.INVISIBLE
            }
            deliveryWaiverTv.text = model.packing_status
            getPackingStatusColor(deliveryWaiverTv, model.packing_status.toString())
            holder.itemView.setOnClickListener {
                (context as DeliveryCustomerActivity).callNextActivity(
                    model
                )
            }
            if (i == allItems.size - 1) {
                groupFittingLay.visibility = View.GONE
            } else {
                groupFittingLay.visibility = View.VISIBLE
            }
            if (model.map_url.toString().isNullOrBlank() ||
                model.map_url.toString().isNullOrBlank() ||
                model.map_url.toString().equals("null", true)
            ) {
                mapViewDelivery.visibility = View.GONE
            } else {
                mapViewDelivery.visibility = View.VISIBLE
            }

            mapViewDelivery.setOnClickListener { //   String strUri = "http://maps.google.com/maps?q=loc:" + 51.5074 + "," + 0.1278 + " (" + "Testing" + ")";
                GoToMap(
                    context as Activity,
                    model.map_url.toString()
                )
            }
        }

    }

    override fun getItemCount(): Int {
        return allItems.size
    }

    inner class CustomOverLayViewHolder(val binding: CustomOverviewGroupingCopyLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}
}