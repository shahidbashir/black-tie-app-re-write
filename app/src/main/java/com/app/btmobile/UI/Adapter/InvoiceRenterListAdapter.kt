package com.app.btmobile.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.ConfirmationRenter
import com.app.btmobile.R
import com.app.btmobile.databinding.CustomInvoiceRenterLayBinding
import com.app.btmobile.databinding.CustomNotesLayoutBinding

class InvoiceRenterListAdapter(
    var mContext: Context,
    var list: List<ConfirmationRenter>
) : RecyclerView.Adapter<InvoiceRenterListAdapter.VHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): VHolder {
        val binding: CustomInvoiceRenterLayBinding =
            CustomInvoiceRenterLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return VHolder(binding)
    }

    var hide_status: Boolean = true
    fun hideStatus(hideStatus: Boolean) {
        hide_status = hideStatus
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: VHolder, i: Int) {
        val renter = list[i]
        holder.binding.run {
            renterNameTv.text = renter.renter
            packageTypeTv.text = renter.`package`
            daysTv.text = renter.days.toString() + ""
            if (hide_status) {
                priceTv.text = "$0.00"
            } else {
                priceTv.text = renter.total
            }
            val stringBuilder = StringBuilder()
            for (addOns in renter.addons) {
                stringBuilder.append("$addOns \n")
            }
            addonsTv.text = stringBuilder
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class VHolder(val binding: CustomInvoiceRenterLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}
}