package com.app.btmobile.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.Invoice
import com.app.btmobile.R
import com.app.btmobile.Utils.setColor
import com.app.btmobile.databinding.CustomInvoiceMainItemBinding
import com.app.btmobile.databinding.CustomNotesLayoutBinding

class InvoicesAdapter(
    var mContext: Context, var list: List<Invoice>,
    private val onItemClickListener: (String, Int) -> Unit,
    private val onSubmitClickListener: (String, Int, String) -> Unit
) :
    RecyclerView.Adapter<InvoicesAdapter.VHolder>() {
    private var hide_status = false
    var isCardAvailable = false
    var adapter: InvoiceRenterListAdapter? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): VHolder {
        val binding: CustomInvoiceMainItemBinding =
            CustomInvoiceMainItemBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return VHolder(binding)
    }

    fun hideStatus(hideStatus: Boolean) {
        hide_status = hideStatus
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: VHolder, i: Int) {
        list[i].run {
            holder.binding.run {
                edSaveNotes.setText("")
                cbInvoiceChange.isChecked = false
                if (hide_status) {
                    totalTaxTv.text = "\$0.00"
                    totalPriceTv.text = "\$0.00"
                } else {
                    totalTaxTv.text = invoice_total_tax
                    totalPriceTv.text = invoice_total
                }

                if (customer_confirmation) {
                    tvGuestSign.setBackgroundColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                } else {
                    tvGuestSign.setBackgroundColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.red
                        )
                    )
                }
                if (tech_confirmation) {
                    btnSaveSubmit.setBackgroundColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                } else {
                    btnSaveSubmit.setBackgroundColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.red
                        )
                    )
                }

                if (invoice_id != null) {
                    tvInvoice.text = invoice_id.toString() + ""
                }
                adapter = InvoiceRenterListAdapter(mContext, renters)
                rvRenterListNew.setHasFixedSize(true)
                rvRenterListNew.layoutManager = LinearLayoutManager(mContext)
                rvRenterListNew.adapter = adapter
                adapter?.hideStatus(hide_status)
                tvGuestSign.setOnClickListener {
                    onItemClickListener("next", i)
                }

                btnSendToPhone.setOnClickListener {
                    onItemClickListener("sendToPhone", i)
                }
                btnSaveSubmit.setOnClickListener {
                    onSubmitClickListener(
                        edSaveNotes.text.toString(),
                        i,
                        if (cbInvoiceChange.isChecked) "1" else "0"
                    )
                }

                if (invoice_status.equals("not_paid", true)) {
                    tvPaid.text = "- Not Paid"
                    tvPaid.setTextColor(setColor(mContext, R.color.red))
                } else {
                    tvPaid.text = "- Paid"
                    tvPaid.setTextColor(setColor(mContext, R.color.green))
                }

                if (tour_op) {
                    tvTour.text = "- Tour Op"
                    tvTour.visibility = View.VISIBLE
                } else {
                    tvTour.text = ""
                    tvTour.visibility = View.GONE
                }
                if (isCardAvailable) {
                    tvCard.setTextColor(setColor(mContext, R.color.green))
                    tvCard.text = "- Card On File"
                } else {
                    tvCard.setTextColor(setColor(mContext, R.color.red))
                    tvCard.text = "- No Card On File"
                }
            }
        }
    }

    fun haveCards(isCard: Boolean) {
        isCardAvailable = isCard
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class VHolder(val binding: CustomInvoiceMainItemBinding) :
        RecyclerView.ViewHolder(binding.root) {}

    fun setHide_status(status: Boolean) {
        hide_status = status
    }
}