package com.app.btmobile.UI.Adapter

import android.content.Context
import com.app.btmobile.Models.MassReturnModel
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import com.app.btmobile.R
import android.content.Intent
import android.view.View
import com.app.btmobile.UI.Activity.Return.ReturnRentersActivity
import android.widget.TextView
import androidx.cardview.widget.CardView
import java.util.ArrayList

class MassReturnAdapter(var act: Context, private val dataSet: ArrayList<MassReturnModel>) :
    RecyclerView.Adapter<MassReturnAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.custom_mas_return_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        val renter = dataSet[listPosition]
        holder.renterNameTv.text = renter.name
        holder.reservationNameTv.text = renter.reservation_name
        if (renter.size != null) {
            holder.tvBootSize.text = renter.size
        }
        holder.tvType.text = renter.type
        holder.tvBootModel.text = renter.model
        holder.tvBootBarcode.text = renter.barcode
        holder.tvBootStatus.text = renter.status
        holder.cvMassScanReturn.setOnClickListener {
            if (!renter.shipment_id.isNullOrEmpty()) {
                val i = Intent(act, ReturnRentersActivity::class.java)
                i.putExtra("shipmentId", renter.shipment_id.toInt())
                act.startActivity(i)
            }
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var reservationNameTv: TextView
        var tvType: TextView
        var tvBootModel: TextView
        var tvBootSize: TextView
        var tvBootBarcode: TextView
        var tvBootStatus: TextView
        var renterNameTv: TextView
        var cvMassScanReturn: CardView

        init {
            reservationNameTv = itemView.findViewById(R.id.reservationNameTv)
            tvType = itemView.findViewById(R.id.tvType)
            tvBootModel = itemView.findViewById(R.id.tvBootModel)
            tvBootSize = itemView.findViewById(R.id.tvBootSize)
            tvBootBarcode = itemView.findViewById(R.id.tvBootBarcode)
            tvBootStatus = itemView.findViewById(R.id.tvBootStatus)
            renterNameTv = itemView.findViewById(R.id.renterNameTv)
            cvMassScanReturn = itemView.findViewById(R.id.cvMassScanReturn)
        }
    }
}