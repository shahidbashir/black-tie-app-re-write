package com.app.btmobile.UI.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.btmobile.Models.MassReturnBigModel;
import com.app.btmobile.Models.MassReturnModel;
import com.app.btmobile.R;

import java.util.ArrayList;

public class MassReturnBigAdapter extends RecyclerView.Adapter<MassReturnBigAdapter.MyViewHolder> {

    private ArrayList<MassReturnBigModel> dataSet;
    Context act;


    public MassReturnBigAdapter(Context activity, ArrayList<MassReturnBigModel> data) {
        this.act = activity;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_mas_return_big_item, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        MassReturnBigModel renter = dataSet.get(listPosition);
        holder.renterNameTv.setText(renter.getReservationName());

        MassReturnAdapter adapter = new
                MassReturnAdapter(act, (ArrayList<MassReturnModel>) renter.getArrayList());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(act);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        holder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        holder.recyclerView.setAdapter(adapter);

    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView renterNameTv;
        RecyclerView recyclerView;

        public MyViewHolder(View itemView) {
            super(itemView);
            renterNameTv = itemView.findViewById(R.id.renterNameTv);
            recyclerView = itemView.findViewById(R.id.rvMassReturnSmall);

        }
    }
}
