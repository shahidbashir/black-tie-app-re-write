package com.app.btmobile.UI.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.NoteData
import com.app.btmobile.R
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.CustomNotesLayoutBinding
import com.app.btmobile.databinding.CustomOverviewGroupingCopyLayBinding
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class NotesAdapter(val context: Context, var allItems: ArrayList<NoteData>) :
    RecyclerView.Adapter<NotesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        @NonNull viewGroup: ViewGroup,
        viewType: Int
    ): NotesAdapter.ViewHolder {
        val binding: CustomNotesLayoutBinding =
            CustomNotesLayoutBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(@NonNull holder: NotesAdapter.ViewHolder, position: Int) {
        val modelCopy: NoteData = allItems[position]
        holder.binding.run {
            notesTv.text = (modelCopy.notes)

            val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val format2 = SimpleDateFormat(" dd-MMM-yyyy hh:mm a")
            var date: Date? = null
            try {
                date = format1.parse(modelCopy.created_at)
                dateTimeTv.text = (format2.format(date))
                Debugger.wtf("date", format2.format(date))
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        }

    }

    override fun getItemCount(): Int {
        return allItems.size
    }


    inner class ViewHolder(val binding: CustomNotesLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {}
}