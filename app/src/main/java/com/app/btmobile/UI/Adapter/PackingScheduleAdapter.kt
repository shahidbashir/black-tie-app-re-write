package com.app.btmobile.UI.Adapter

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.ScheduleDelivery
import com.app.btmobile.R
import com.app.btmobile.Utils.GoToMap
import com.app.btmobile.Utils.getPackingStatusColor
import com.app.btmobile.Utils.setColor
import com.app.btmobile.databinding.CustomOverviewLayBinding

class PackingScheduleAdapter(
    var context: Context,
    var allitems: List<ScheduleDelivery>,
    private val onItemClickListener: (String, Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return CustomOverLayViewHolder(
            CustomOverviewLayBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }


    override fun getItemCount(): Int {
        return allitems.size
    }


    inner class CustomOverLayViewHolder(val binding: CustomOverviewLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}


    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val model: ScheduleDelivery = allitems[position]

        //  val model: DeliveryCustomersModel = overViewModel
        val holder =
            viewHolder as CustomOverLayViewHolder


        if (model.isFirst) {
            holder.binding.tvScheduleDel.visibility = View.VISIBLE
            if (model.isSchedule) {
                holder.binding.tvScheduleDel.text = "Scheduled Fits"
                holder.binding.tvScheduleDel.setTextColor(
                    setColor(
                        context,
                        R.color.colorPrimaryDark
                    )
                )
            } else {
                holder.binding.tvScheduleDel.text = "Unassigned Fits"
                holder.binding.tvScheduleDel.setTextColor(setColor(context, R.color.red))
            }
        } else {
            holder.binding.tvScheduleDel.visibility = View.GONE
        }

        if (model.scheduleIndicatorClass != null) {
            var colorStr = ""
            colorStr = when {
                model.scheduleIndicatorClass.equals("group-fit", true) -> {
                    "#d95350"
                }
                model.scheduleIndicatorClass.equals("switch-out", true) -> {
                    "#2dd57b"
                }
                else -> {
                    "#60b1fe"
                }
            }
            if (model.scheduleIndicatorClass.toString().contains("switch-out-border")) {
                colorStr = "#2dd57b"
            }
            val color =
                arrayOf(ColorDrawable(Color.parseColor(colorStr)))
            val trans =
                TransitionDrawable(color)
            holder.binding.viewSideColor.setBackgroundDrawable(trans)
        }
        if (!model.time.toString().isNullOrEmpty()) {
            val s: String = model.time.toString()
            val splitedTime =
                s.split("\\s+".toRegex()).toTypedArray()
            if (splitedTime.size >= 2) {
                holder.binding.amTv.text = splitedTime[1]
                holder.binding.timeTv.text = splitedTime[0]
            }
        }

        var address = if (!model.address.isNullOrEmpty()) {
            (model.address)
        } else if (!model.lodging.isNullOrEmpty()) {
            (model.lodging)
        } else {
            ""
        }
        if (!address?.isEmpty()!!) {
            holder.binding.renterAddresTv.visibility = View.VISIBLE
            holder.binding.renterAddresTv.text = address
        } else {
            holder.binding.renterAddresTv.visibility = View.GONE
        }

        holder.binding.renterNameTv.text =
            if (model.customerName == null) "" else model.customerName
                .toString()


        holder.binding.rentersCountTv.text = "${model.numberOfRenters.toString()} Renters"
        holder.binding.tvVanNum.text = model.van
        if (!model.room_number.toString().isNullOrEmpty() &&
            !model.room_number.toString().equals("null", true)
        ) {
            holder.binding.layRoomNum.visibility = View.VISIBLE
            holder.binding.tvRoomNum.text = model.room_number.toString()
        } else {
            holder.binding.layRoomNum.visibility = View.INVISIBLE
        }
        holder.binding.deliveryWaiverTv.text = model.packingStatus
        getPackingStatusColor(holder.binding.deliveryWaiverTv, model.packingStatus.toString())
        holder.itemView.setOnClickListener(View.OnClickListener {
            onItemClickListener("next", position)

        })
        if (model.mapUrl.toString().isNullOrBlank() ||
            model.mapUrl.toString().isNullOrBlank() ||
            model.mapUrl.toString().equals("null", true)
        ) {
            holder.binding.mapViewDelivery.visibility = View.GONE
        } else {
            holder.binding.mapViewDelivery.visibility = View.VISIBLE
        }
        holder.binding.mapViewDelivery.setOnClickListener(View.OnClickListener { //
            GoToMap(
                context as Activity,
                model.mapUrl.toString()
            )
        })

    }
}