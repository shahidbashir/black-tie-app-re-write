package com.app.btmobile.UI.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.PackedInventoryModel
import com.app.btmobile.R
import com.app.btmobile.UI.Activity.Fitting.FittingActivity
import com.app.btmobile.UI.Activity.Packing.PackingActivity
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.setColor
import com.app.btmobile.databinding.CustomPackingSkisBootsLayBinding
import java.util.*

class PackingSkiBootsAdapter(
    private val context: Context,
    private val allItems: ArrayList<PackedInventoryModel>,
    var isAddOn: Boolean, var isFromPacking: Boolean, var isPackingList: Boolean
) : RecyclerView.Adapter<PackingSkiBootsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            CustomPackingSkisBootsLayBinding
                .inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var model = allItems[position]
        holder.binding.run {
            packingNameTv.visibility = View.VISIBLE
            packingPackageTv.visibility = View.VISIBLE
            packingSizeTv.visibility = View.VISIBLE
            packingBarCodeTv.visibility = View.VISIBLE
            Debugger.wtf("BarCode", model.barcode)
            if (isFromPacking) {
                if (!isAddOn) {
                    packingNameTv.text = model.name
                    if (model.manufacture != null) {
                        packingPackageTv.text = model.manufacture
                        packingSizeTv.text = "Size : ${model.sizeLength}"
                        packingBarCodeTv.text = "B: ${model.barcode}"
                    } else if (model.soleLength != null) {
                        packingSizeTv.text = "Sole : ${model.soleLength}"
                        packingPackageTv.text = "Size : ${model.sizeLength}"
                    } else {
                        packingSizeTv.text = "Size : ${model.sizeLength}"
                    }
                } else {
                    val helmetSize = allItems[position].helmetSize
                    packingNameTv.text = model.name
                    if (helmetSize.isNullOrEmpty() ||
                        helmetSize.equals("N/A", true)
                    ) {
                        packingSizeTv.visibility = View.GONE
                    } else {
                        packingSizeTv.text = "${allItems[position].helmetSize}"
                    }
                    if (model.manufacture != null) {
                        packingPackageTv.text = model.manufacture
                    } else {
                        packingPackageTv.visibility = View.GONE
                    }
                    if (!model.barcode.isNullOrEmpty()) {
                        packingBarCodeTv.text = "B: ${model.barcode}"
                    } else {
                        packingBarCodeTv.visibility = View.GONE
                    }
//                    packingPackageTv.visibility = View.GONE
//                    packingBarCodeTv.visibility = View.GONE
                    deleteInventoryIv.visibility = View.GONE
                    val params = cardPackSkisBoots.layoutParams
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT
                    cardPackSkisBoots.layoutParams = params
                    root.setOnClickListener {
                        (context as (PackingActivity)).showUpdateAddonStatusDialog(allItems[position]);
                    }
                    if (model.status != null) {
                        if (model.status.equals("Packed", ignoreCase = true)) {
                            packingNameTv.setTextColor(setColor(context, R.color.green))
                        } else {
                            packingNameTv.setTextColor(setColor(context, R.color.red))
                        }
                    } else {
                        packingNameTv.setTextColor(setColor(context, R.color.green))
                    }
                }
                deleteInventoryIv.setOnClickListener {
                    (context as (PackingActivity)).showDeleteInvertoryAlert(allItems[position]);
                }
            } else {
                deleteInventoryIv.visibility = View.GONE
                packingNameTv.setTextColor(setColor(context, R.color.green))
                if (model.name == "Not Packed Yet") {
                    packingNameTv.visibility = View.VISIBLE
                    packingNameTv.text = model.name
                    packingPackageTv.visibility = View.GONE
                    packingSizeTv.visibility = View.GONE
                    packingBarCodeTv.visibility = View.GONE
                    packingNameTv.setTextColor(setColor(context, R.color.red))
                } else if (model.name == "Fit") {
                    packingNameTv.visibility = View.VISIBLE
                    packingNameTv.text = model.name
                    packingPackageTv.visibility = View.GONE
                    packingSizeTv.visibility = View.GONE
                    packingBarCodeTv.visibility = View.GONE
                    packingNameTv.setTextColor(setColor(context, R.color.green))
                } else if (!isAddOn) {
                    deleteInventoryIv.visibility = View.VISIBLE
                    packingNameTv.text = model.name
                    if (model.manufacture != null) {
                        packingPackageTv.text = model.manufacture
                        packingSizeTv.text = "Size : ${model.sizeLength}"
                        packingBarCodeTv.text = "B : ${model.barcode}"
                    } else if (model.soleLength != null) {
                        packingSizeTv.text = "Sole : ${model.soleLength}"
                        packingPackageTv.text = "Size : ${model.sizeLength}"
                    } else {
//                   packingSizeTv.setText("Size : " + model.getData().getSizeLength()+" 3");
                        packingSizeTv.visibility = View.GONE
                    }
                } else {
                    val helmetSize = allItems[position].helmetSize
                    packingNameTv.text = model.name
                    //  if (isPackingList) {
                    if (helmetSize.isNullOrEmpty() ||
                        helmetSize.equals("N/A", true)
                    ) {
                        packingSizeTv.visibility = View.GONE
                    } else {
                        packingSizeTv.text = "${allItems[position].helmetSize}"
                    }
//                        } else {
//                            packingSizeTv.visibility = View.GONE
//                        }
                    if (model.manufacture != null) {
                        packingPackageTv.text = model.manufacture
                    } else {
                        packingPackageTv.visibility = View.GONE
                    }
                    if (!model.barcode.isNullOrEmpty()) {
                        packingBarCodeTv.text = "B: ${model.barcode}"
                    } else {
                        packingBarCodeTv.visibility = View.GONE
                    }
//                        packingBarCodeTv.visibility = View.GONE
//                        packingPackageTv.visibility = View.GONE

                    val params = cardPackSkisBoots.layoutParams
                    params.height = ViewGroup.LayoutParams.WRAP_CONTENT
                    cardPackSkisBoots.layoutParams = params
                    if (model.status != null) {
                        if (model.status.equals("Fitted", ignoreCase = true) ||
                            model.status.equals("Packed", ignoreCase = true)
                        ) {
                            packingNameTv.setTextColor(setColor(context, R.color.green))
                        } else {
                            packingNameTv.setTextColor(setColor(context, R.color.red))
                        }
                    } else {
                        packingNameTv.setTextColor(setColor(context, R.color.red))
                    }
                    root.setOnClickListener {
                        (context as (FittingActivity)).showUpdateAddonStatusDialog(allItems[position]);
                    }
                }

                deleteInventoryIv.setOnClickListener {
                    if (isPackingList) {
                        (context as (FittingActivity)).showDeleteInventoryAlert(
                            allItems[position],
                            true
                        );
                    } else {
                        (context as (FittingActivity)).showDeleteInventoryAlert(
                            allItems[position],
                            false
                        );
                    }
                }

            }

        }
    }

    override fun getItemCount(): Int {
        return allItems.size
    }

    inner class ViewHolder(val binding: CustomPackingSkisBootsLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}

}