package com.app.btmobile.UI.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.btmobile.Models.ExistingBootSkis;
import com.app.btmobile.R;

import java.util.ArrayList;
import java.util.List;

public class PreviousBootDataAdapter extends RecyclerView.Adapter<PreviousBootDataAdapter.vHolder> {
    Context context;
    List<ExistingBootSkis> existingBootList = new ArrayList<>();

    public PreviousBootDataAdapter(Context context, List<ExistingBootSkis> existingBootList) {
        this.context = context;
        this.existingBootList = existingBootList;
    }

    @NonNull
    @Override
    public vHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.previous_boot_data_item, viewGroup, false);
        return new vHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull vHolder vHolder, int i) {
        ExistingBootSkis existingBoot = existingBootList.get(i);
        vHolder.tvPreviousSkiModel.setText(existingBoot.getModel());
        vHolder.tvPreviousSkiBarcode.setText(existingBoot.getBarcode());
        vHolder.tvPreviousSkiLength.setText(existingBoot.getSize());
        vHolder.tvPreviousBootLength.setText(existingBoot.getLength());
    }

    @Override
    public int getItemCount() {
        return existingBootList.size();
    }

    public class vHolder extends RecyclerView.ViewHolder {
        TextView tvPreviousSkiModel, tvPreviousSkiLength, tvPreviousSkiBarcode, tvPreviousBootLength;

        public vHolder(@NonNull View itemView) {
            super(itemView);
            tvPreviousSkiModel = itemView.findViewById(R.id.tvPreviousSkiModel);
            tvPreviousSkiLength = itemView.findViewById(R.id.tvPreviousSkiLength);
            tvPreviousSkiBarcode = itemView.findViewById(R.id.tvPreviousSkiBarcode);
            tvPreviousBootLength = itemView.findViewById(R.id.tvPreviousBootLength);
        }
    }
}
