package com.app.btmobile.UI.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.btmobile.Models.ExistingBootSkis;
import com.app.btmobile.R;

import java.util.ArrayList;
import java.util.List;

public class PreviousSkiDataAdapter extends RecyclerView.Adapter<PreviousSkiDataAdapter.vHolder> {

    Context context;
    List<ExistingBootSkis> existingSkiList = new ArrayList<>();

    public PreviousSkiDataAdapter(Context context, List<ExistingBootSkis> existingSkiList) {
        this.context = context;
        this.existingSkiList = existingSkiList;
    }

    @NonNull
    @Override
    public vHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.previous_ski_data_item, viewGroup, false);
        return new vHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull vHolder vHolder, int i) {
        ExistingBootSkis existingSki = existingSkiList.get(i);
        vHolder.tvPreviousSkiModel.setText(existingSki.getModel());
        vHolder.tvPreviousSkiBarcode.setText(existingSki.getBarcode());
        vHolder.tvPreviousSkiLength.setText(existingSki.getLength());
    }

    @Override
    public int getItemCount() {
        return existingSkiList.size();
    }



    public class vHolder extends RecyclerView.ViewHolder {
        TextView tvPreviousSkiModel, tvPreviousSkiLength, tvPreviousSkiBarcode;

        public vHolder(@NonNull View itemView) {
            super(itemView);
            tvPreviousSkiModel = itemView.findViewById(R.id.tvPreviousSkiModel);
            tvPreviousSkiLength = itemView.findViewById(R.id.tvPreviousSkiLength);
            tvPreviousSkiBarcode = itemView.findViewById(R.id.tvPreviousSkiBarcode);
        }
    }
}
