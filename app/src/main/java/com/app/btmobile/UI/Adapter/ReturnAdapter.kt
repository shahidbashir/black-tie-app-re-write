package com.app.btmobile.UI.Adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.ReturnsCustomerModelItem
import com.app.btmobile.R
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.setColor
import com.app.btmobile.databinding.CustomRentersLayBinding
import java.util.*

class ReturnAdapter(
    var act: Context,
    private val dataSet: ArrayList<ReturnsCustomerModelItem>,
    private val onItemClickListener: (String, Int) -> Unit
) :
    RecyclerView.Adapter<ReturnAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val binding: CustomRentersLayBinding =
            CustomRentersLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        var model = dataSet[listPosition]
        holder.binding.run {
            rentersNameTv.text = model.customerName
            totalRenterTv.text = "${model.numberOfRenters} Renters"
            if (!model.returnTime.isNullOrEmpty()) {
                rentersDateTimeTv.visibility = View.VISIBLE
                rentersDateTimeTv.text = model.returnTime
            } else {
                rentersDateTimeTv.visibility = View.GONE
            }
            if (model.isSwitchout) {
                rentersSwitchStatusTv.visibility = View.VISIBLE
            } else {
                rentersSwitchStatusTv.visibility = View.GONE
            }
//            if (customerName != null) {
//                rentersPhoneNumberTv.visibility = View.VISIBLE
//                rentersPhoneNumberTv.text = customerName
//            }

            var address = if (!model.property_name.isNullOrEmpty()) {
                model.property_name
            } else if (!model.lodging.isNullOrEmpty()) {
                model.lodging
            } else {
                ""
            }
            RentersAddressTv.text = address

            if (model.van != null) {
                vanTv.text = model.van
            }
            Debugger.wtf("getAll_renter_returned", model.allRenterReturned.toString() + "")
            if (model.roomNo != null &&
                !model.roomNo.isNullOrEmpty()
            ) {
                roomsTv.visibility = View.VISIBLE
                roomsTv.text = "Room #:${model.roomNo}"
            } else {
                roomsTv.visibility = View.GONE
            }

            if (model.mapUrl.toString().isNullOrBlank() ||
                model.mapUrl.toString().isNullOrBlank() ||
                model.mapUrl.toString().equals("null", true)
            )  {
                rentersShowMapIv.visibility = View.GONE
            } else {
                rentersShowMapIv.visibility = View.VISIBLE
            }


            if (model.status != null) {
                rentersStatusTv.text = model.status
                if (model.status.equals("Ready", ignoreCase = true)) {
                    rentersStatusTv.background.setColorFilter(
                        Color.parseColor("#337ab7"),
                        PorterDuff.Mode.SRC_ATOP
                    )
                } else if (model.status.equals("Completed", ignoreCase = true)) {
                    rentersStatusTv.background.setColorFilter(
                        Color.parseColor("#4CAF50"),
                        PorterDuff.Mode.SRC_ATOP
                    )
                } else if (model.status.equals("Pending", ignoreCase = true)) {
                    rentersStatusTv.background.setColorFilter(
                        Color.parseColor("#f05050"),
                        PorterDuff.Mode.SRC_ATOP
                    )
                } else if (model.status.equals("Partial Return", ignoreCase = true)) {
                    rentersStatusTv.background.setColorFilter(
                        Color.parseColor("#A568AF"),
                        PorterDuff.Mode.SRC_ATOP
                    )
                } else if (model.status.equals("All Returned", ignoreCase = true)) {
                    rentersStatusTv.background.setColorFilter(
                        Color.parseColor("#337ab7"),
                        PorterDuff.Mode.SRC_ATOP
                    )
                }
            }

            holder.itemView.setOnClickListener {
                onItemClickListener("next", listPosition)
            }
            rentersShowMapIv.setOnClickListener {
                onItemClickListener("map", listPosition)
            }
            if (!model.returnNotes.isNullOrEmpty()) {
                tvNotes.setTextColor(setColor(act, R.color.red))
            } else {
                tvNotes.setTextColor(setColor(act, R.color.black))
            }

            tvNotes.setOnClickListener {
                if (model.returnNotes.isNotEmpty()) {
                    Debugger.wtf("Notes", "->" + model.returnNotes[0])
                    var finalNotes = ""
                    for (notes in model.returnNotes) {
                        finalNotes = """
                        $finalNotes
                        $notes
                        """.trimIndent()
                    }
                    onItemClickListener("notes", listPosition)
                } else {
                    Toast.makeText(act, "There are no notes to show", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    inner class MyViewHolder(val binding: CustomRentersLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}

}