package com.app.btmobile.UI.Adapter

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.ReturnInventory
import com.app.btmobile.Models.ReturnRenterModelItem
import com.app.btmobile.UI.Activity.Return.ReturnRentersActivity
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.ReturnInventoryListBinding
import java.util.*

class ReturnInventoryAdapter : RecyclerView.Adapter<ReturnInventoryAdapter.VHolder> {
    var context: Context
    var inventoryList: List<ReturnInventory> = ArrayList()
    var data: ReturnRenterModelItem? = null
    var flag = false

    constructor(context: Context) {
        this.context = context
    }

    constructor(context: Context, inventoryList: List<ReturnInventory>) {
        this.context = context
        this.inventoryList = inventoryList
    }

    fun changeView(flag: Boolean) {
        this.flag = flag
        Debugger.wtf("flag", "-> $flag")
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): VHolder {
        val binding: ReturnInventoryListBinding =
            ReturnInventoryListBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return VHolder(binding)
    }

    override fun onBindViewHolder(vHolder: VHolder, i: Int) {
        val (barcode, manufacture, model, renterReturned, _, sizeLength, type) = inventoryList[i]
        vHolder.binding.run {
            if (flag) {
                Debugger.wtf("flag2", "-> $flag")
                rentListView.visibility = View.VISIBLE
                card.visibility = View.GONE
            } else {
                Debugger.wtf("flag3", "-> $flag")
                card.visibility = View.VISIBLE
                rentListView.visibility = View.GONE
            }
            //          CardView Items DataSet
            if (data!!.renterName != null) {
                tvName.text = data!!.renterName
            }
            if (type != null) {
                tvTypeRenter.text = type.toUpperCase()
            }
            if (manufacture != null) {
                tvManufactureRenter.text = manufacture
            }
            if (model != null) {
                tvModel.text = model
            }
            if (barcode != null) {
                tvBarcode.text = barcode
            }
            if (type != null) {
                if (type.toLowerCase() == "ski" || type.toLowerCase() == "skiboot" || type == "SnowBoard" || type == "SnowBoardBoot") {
                    tvType.text = sizeLength
                    tvTypeList.text = sizeLength
                } else {
                    tvTypeList.text = sizeLength
                }
            }

//          ListView Items DataSet
            if (data!!.renterName != null) {
                tvNameList.text = data!!.renterName
            }
            if (type != null) {
                tvTypeRenterList.text = type.toUpperCase()
            }
            if (manufacture != null) {
                tvManufactureRenterList.text = manufacture
            }
            if (model != null) {
                tvModelList.text = model
            }
            if (barcode != null) {
                tvBarcodeList.text = barcode
            }
            if (renterReturned == 1) {
                cbRent.isChecked = true
                cbRent.isEnabled = false
                tvRemove.visibility = View.VISIBLE
                tvReturnedStatus.text = "Returned"
                tvReturnedStatus.background.setColorFilter(
                    Color.parseColor("#2dd57b"),
                    PorterDuff.Mode.SRC_ATOP
                )
            } else {
                cbRent.isChecked = false
                cbRent.isEnabled = true
                tvRemove.visibility = View.GONE
                tvReturnedStatus.text = "Pending"
                tvReturnedStatus.background.setColorFilter(
                    Color.parseColor("#d95350"),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
            tvRemove.setOnClickListener {
                (context as ReturnRentersActivity).showDeleteInventoryAlert(inventoryList[i]);
            }
            cbRent.setOnClickListener {
                if (cbRent.isChecked) {
                    (context as ReturnRentersActivity).hitReturnUsingBarcode(barcode, "0")
                    cbRent.isEnabled = false;
                }
            }

            tcReturnScan.setOnClickListener {
                (context as ReturnRentersActivity).showNfcScanDialog(renterId = data?.renterId.toString())
            }
        }
    }

    override fun getItemCount(): Int {
        return inventoryList.size
    }

    inner class VHolder(val binding: ReturnInventoryListBinding) :
        RecyclerView.ViewHolder(binding.root) {}

    fun updateData(data_this: ReturnRenterModelItem?) {
        data = data_this
    }

}