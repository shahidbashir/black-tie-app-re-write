package com.app.btmobile.UI.Adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Debug
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.ReturnAddon
import com.app.btmobile.Models.ReturnInventory
import com.app.btmobile.Models.ReturnRenterModelItem
import com.app.btmobile.UI.Activity.Return.ReturnRentersActivity
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.CustomReturnRenterLayoutBinding
import java.util.*

class ReturnRentersAdapter(
    var act: Context, var dataList: List<ReturnRenterModelItem>
) : RecyclerView.Adapter<ReturnRentersAdapter.MyViewHolder>() {
    var flag = false

    //    New Code
    private var addOnsAdapterNew: AddOnsAdapterNew? = null
    private var returnInventoryAdapter: ReturnInventoryAdapter? = null
    private val inventoryList: MutableList<ReturnInventory> =
        ArrayList()
    private val addonLatest: MutableList<ReturnAddon> =
        ArrayList()

    inner class MyViewHolder(val binding: CustomReturnRenterLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {}


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val binding: CustomReturnRenterLayoutBinding =
            CustomReturnRenterLayoutBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return MyViewHolder(binding)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(
        holder: MyViewHolder,
        listPosition: Int
    ) {
        val data = dataList[listPosition]
        val clothing = dataList[listPosition].clothing
        inventoryList.clear()
        inventoryList.addAll(data.inventories)
        data.addons.map {
            it.isAddonWithBarcode = false
        }
        data.addonsWithBarcodeList.map {
            it.isAddonWithBarcode = true
        }
        addonLatest.clear()
//        addonLatest.addAll(data.addons)
//        addonLatest.addAll(data.addonsWithBarcodeList)

        holder.binding.run {
//        Inventories
            rvAllRentersInventories.setHasFixedSize(true)
            rvAllRentersInventories.layoutManager = LinearLayoutManager(act)
            returnInventoryAdapter = ReturnInventoryAdapter(act, data.inventories)
            rvAllRentersInventories.adapter = returnInventoryAdapter
            returnInventoryAdapter!!.updateData(data)

            Debugger.wtf("addonLatest","${addonLatest.size}")
//        Addons
            rvReturnAddOns.setHasFixedSize(true)
            rvReturnAddOns.layoutManager = LinearLayoutManager(act)
            addOnsAdapterNew = AddOnsAdapterNew(act)
            rvReturnAddOns.adapter = addOnsAdapterNew
            addOnsAdapterNew!!.updateData(data)

//        Clothing
            if (clothing.name != null) {
                cardViewClothing.visibility = View.VISIBLE
                tvNameClothing.text = data.renterName
                tvClothing.text = clothing.name
                if (clothing.isReturned.toString().equals("1", true)) {
                    cbClothing.isChecked = true
                    cbClothing.isEnabled = false
                    tvClothingRemove.visibility = View.VISIBLE
                    tvReturnedStatusClothing.text = "Returned"
                    tvReturnedStatusClothing.background.setColorFilter(
                        Color.parseColor("#2dd57b"),
                        PorterDuff.Mode.SRC_ATOP
                    )
                } else {
                    cbClothing.isChecked = false
                    cbClothing.isEnabled = true
                    tvClothingRemove.visibility = View.GONE
                    tvReturnedStatusClothing.text = "Pending"
                    tvReturnedStatusClothing.background.setColorFilter(
                        Color.parseColor("#d95350"),
                        PorterDuff.Mode.SRC_ATOP
                    )
                }
            } else {
                cardViewClothing.visibility = View.GONE
            }
            cbClothing.setOnClickListener {
                if (cbClothing.isChecked) {
                    (act as ReturnRentersActivity).addRemoveClothingApi(
                        "clothing",
                        "",
                        data.renterId,
                        "1"
                    )
                }
            }
            tvClothingRemove.setOnClickListener {
                (act as ReturnRentersActivity).addRemoveClothingApi(
                    "clothing",
                    "",
                    data.renterId,
                    "0"
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

}