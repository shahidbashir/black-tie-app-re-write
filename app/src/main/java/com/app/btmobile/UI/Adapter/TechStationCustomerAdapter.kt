package com.app.btmobile.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.TechCustomerModel
import com.app.btmobile.R
import com.app.btmobile.UI.Activity.Fitting.TechStationCustomersActivity
import com.app.btmobile.UI.Adapter.TechStationCustomerAdapter.MyViewHolder
import com.app.btmobile.Utils.setColor
import com.app.btmobile.databinding.CustomeTechCustomerLayBinding

class TechStationCustomerAdapter(
    var context: Context,
    var allItems: List<TechCustomerModel>
) : RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val binding: CustomeTechCustomerLayBinding =
            CustomeTechCustomerLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        val techData = allItems[listPosition]
        holder.binding.run {
            tvName.text = techData.name
            tvNoOfPeople.text = techData.numberOfPeople.toString()
            tvWaivers.text = techData.waiver
            tvBootStatus.text = techData.bootFitted.toString()
            tvSkiFittedStatus.text = techData.skiFitted.toString()
            tvBootStatus.setBackgroundColor(setColor(context, R.color.red))
            tvSkiFittedStatus.setBackgroundColor(setColor(context, R.color.red))
            val _status = techData.waiver
            when {
                _status.contains("Waiting for Signature") -> {
                    tvWaivers.text = "WAITING FOR SIGNATURE"
                    tvWaivers.setBackgroundColor(setColor(context, R.color.red))
                }
                _status.contains("Partially Signed") -> {
                    tvWaivers.text = "PARTIALLY SIGNED"
                    tvWaivers.setBackgroundColor(setColor(context, R.color.yellow))
                }
                _status.contains("Signed") -> {
                    tvWaivers.text = "SIGNED"
                    tvWaivers.setBackgroundColor(setColor(context, R.color.light_blue))
                }
            }
            tvActionText.setBackgroundColor(setColor(context, R.color.light_blue))

            tvActionText.setOnClickListener {
                (context as (TechStationCustomersActivity)).goToViewDetails(
                    techData.vanShipmentId
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return allItems.size
    }

    inner class MyViewHolder(val binding: CustomeTechCustomerLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}
}