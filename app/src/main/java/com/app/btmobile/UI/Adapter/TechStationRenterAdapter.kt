package com.app.btmobile.UI.Adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.TechRenterData
import com.app.btmobile.databinding.CustomTechRenterLayBinding
import java.util.*

class TechStationRenterAdapter(
    var context: Activity,
    private val dataSet: ArrayList<TechRenterData>,
    private val onItemClickListener: (String, Int) -> Unit
) : RecyclerView.Adapter<TechStationRenterAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val binding: CustomTechRenterLayBinding =
            CustomTechRenterLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: MyViewHolder,
        position: Int
    ) {
        val renterModel = dataSet[position]
        holder.binding.run {
            tvName.text = renterModel.renterName
            if (renterModel.isPackedSki) {
                tvpackedSkiNum.text = "View Details"
            }
            if (renterModel.isPackedBoot) {
                tvpackedBootsNum.text = "View Details"
            }
            tvEquipmentsFittedSt.text = renterModel.equipmentFitted
            if (renterModel.isSigned.toString()
                    .contentEquals("false")
            ) {
                tvWaiverName.text = "NOT SIGNED"
            } else {
                tvWaiverName.text = "SIGNED"
            }

            tvShoeSizeName.text = renterModel.shoeSize + ""
            if (renterModel.skiFitted) {
                tvSkiFittedStatus.text = renterModel.skiFitted.toString()
            } else {
                tvSkiFittedStatus.text =
                    "Not fitted"
            }
            tvBootStatus.text = renterModel.bootFitted.toString()
            tvActionText.setOnClickListener {
                onItemClickListener("next", position)
            }
            tvpackedBootsNum.setOnClickListener {
                if (tvpackedBootsNum.text.toString() == "View Details") {
                    onItemClickListener("boot", position)
                }
            }
            tvpackedSkiNum.setOnClickListener {
                if (tvpackedSkiNum.text.toString() == "View Details") {
                    onItemClickListener("ski", position)
                }
            }

            tvWaiverName.setOnClickListener {
                if (!renterModel.isSigned) {
                    onItemClickListener("waiver", position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    inner class MyViewHolder(val binding: CustomTechRenterLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}

}