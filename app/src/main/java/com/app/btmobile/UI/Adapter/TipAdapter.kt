package com.app.btmobile.UI.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.TipPercentage
import com.app.btmobile.R
import com.app.btmobile.UI.Activity.Delivery.InvoiceDetailsActivity
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.AddonsItemNewBinding
import com.app.btmobile.databinding.CustomTipLayoutBinding
import java.util.*

class TipAdapter(
    private val context: Context,
    var selectedItem: Int
) : RecyclerView.Adapter<TipAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding: CustomTipLayoutBinding =
            CustomTipLayoutBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return ViewHolder(binding)
    }

    var allItems: ArrayList<TipPercentage> = ArrayList<TipPercentage>()
    var isTipPerRenter: Boolean = false
    var amountSign: String = "$"


    fun showTipData(
        allItems: ArrayList<TipPercentage>,
        isTipPerRenter: Boolean,
        amountSign: String
    ) {
        this.allItems = allItems
        this.isTipPerRenter = isTipPerRenter
        this.amountSign = amountSign
        notifyDataSetChanged()
    }

    inner class ViewHolder(val binding: CustomTipLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {}


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = allItems[position]
        Debugger.wtf("onBindViewHolder", "selectedItem $selectedItem")
        var tipName = model.tip + "%"



        holder.binding.run {
            if (selectedItem == position) {
                tipLay.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.selectedGray
                    )
                )
            } else {
                tipLay.background = ContextCompat.getDrawable(
                    context,
                    R.drawable.border_rounded
                )
            }
            tvTip.text = model.display_text
            tvTipPercentage.text = "$amountSign" + model.tipPercentage
            Debugger.wtf("isTipPerRenter", "$isTipPerRenter")
//            if (isTipPerRenter) {
//                tvTipPercentage.visibility = View.GONE
//            } else {
//                tvTipPercentage.visibility = View.VISIBLE
//            }
            tipLay.setOnClickListener {
                selectedItem = position
                (context as InvoiceDetailsActivity).tipSelectedListner(position)
            }
        }
    }

    fun clearTipSelection(po: Int) {
        selectedItem = po
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return allItems.size
    }

}