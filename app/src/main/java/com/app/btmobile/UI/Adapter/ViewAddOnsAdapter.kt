package com.app.btmobile.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.Addon
import com.app.btmobile.R
import com.app.btmobile.databinding.CustomPackingSkisBootsLayBinding
import java.util.*

class ViewAddOnsAdapter(
    private val context: Context,
    private val allItems: ArrayList<Addon>,
    private val onItemClickListener: (String, Int) -> Unit
) : RecyclerView.Adapter<ViewAddOnsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem =
            layoutInflater.inflate(R.layout.custom_addone_view_lay, parent, false)
        return ViewHolder(listItem)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (_, isPackable, _, name, selected, status) = allItems[position]
        if (name.equals("Damage Waiver", ignoreCase = true)) {
            holder.addOneNameTv.text = "D.W."
        } else if (name.equals("Premium Boot Upgrade - With HEATED BOOTS", ignoreCase = true)) {
            holder.addOneNameTv.text = "Heated Boots"
        } else {
            holder.addOneNameTv.text = name
        }
        if (status != null) {
            if (status.equals("Packed", ignoreCase = true)) {
                holder.addOneNameTv.setTextColor(context.resources.getColor(R.color.green))
            } else {
                holder.addOneNameTv.setTextColor(context.resources.getColor(R.color.red))
            }
        } else {
            holder.addOneNameTv.setTextColor(context.resources.getColor(R.color.red))
        }

//        if (allItems[position].purchased_addon) {
//            holder.tvRequire.visibility = View.VISIBLE
//        } else {
//            holder.tvRequire.visibility = View.GONE
//        }
        if (isPackable) {
            holder.cbAddOnes.visibility = View.VISIBLE
        } else {
            holder.cbAddOnes.visibility = View.INVISIBLE
        }
        holder.cbAddOnes.isChecked = selected == 1

        val completeSize = allItems[position].completeSize
        if (completeSize.isNullOrEmpty() ||
            completeSize.equals("N/A", true)
        ) {
            holder.layAddonsSize.visibility = View.GONE
        } else {
            holder.layAddonsSize.visibility = View.VISIBLE
            holder.tvAddonSize.text = "${allItems[position].completeSize}"
        }

        holder.cbAddOnes.setOnClickListener {
            allItems[position].selected = if (holder.cbAddOnes.isChecked) 1 else 0

//            if (allItems[position].isAddonWithBarcode) {
//                if (holder.cbAddOnes.isChecked) {
//                    holder.cbAddOnes.isChecked = false
//                    onItemClickListener("Packed", position)
//                } else {
//                    holder.cbAddOnes.isChecked = true
//                    onItemClickListener("Not Packed", position)
//                }
//            } else {
//                allItems[position].selected = if (holder.cbAddOnes.isChecked) 1 else 0
//            }
        }
        holder.addOneNameTv.setOnClickListener {
//            String status =(!modelCopy.getStatus().equalsIgnoreCase("Packed"))
//            ? "Packed" : "Not Packed";
//
//            changeAddonsStatus(
//                modelCopy.getId().toString(), status,
//                holder, position
//            );
        }
    }

    override fun getItemCount(): Int {
        return allItems.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cbAddOnes: CheckBox
        var addOneNameTv: TextView
        var tvRequire: TextView
        var tvAddonSize: TextView
        var layAddonsSize: LinearLayout

        init {
            cbAddOnes = itemView.findViewById(R.id.cbAddOnes)
            addOneNameTv = itemView.findViewById(R.id.addOneNameTv)
            tvAddonSize = itemView.findViewById(R.id.tvAddonSize)
            layAddonsSize = itemView.findViewById(R.id.layAddonsSize)
            tvRequire = itemView.findViewById(R.id.tvRequire)
        } //
    }

}