package com.app.btmobile.UI.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.ClothingModel
import com.app.btmobile.R
import java.util.*

class ViewClothingAdapter(
    private val context: Context,
    private val allItems: ArrayList<ClothingModel>
) : RecyclerView.Adapter<ViewClothingAdapter.VHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        i: Int
    ): VHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem =
            layoutInflater.inflate(R.layout.custom_addone_view_lay, parent, false)
        return VHolder(listItem)
    }

    override fun onBindViewHolder(
        vHolder: VHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        val (_, name, status) = allItems[position]


        vHolder.clothingNameTv.text = name
        if (status != null) {
            if (status.equals("Packed", ignoreCase = true)) {
                vHolder.clothingNameTv.setTextColor(context.resources.getColor(R.color.green))
            } else {
                vHolder.clothingNameTv.setTextColor(context.resources.getColor(R.color.red))
            }
        } else {
            vHolder.clothingNameTv.setTextColor(context.resources.getColor(R.color.red))
        }
        vHolder.cbClothing.isChecked = allItems[position].selected == 1
//        if (allItems[position].purchased_clothing) {
//            vHolder.tvRequire.visibility = View.VISIBLE
//        } else {
//            vHolder.tvRequire.visibility = View.GONE
//        }
        vHolder.cbClothing.setOnClickListener {
            allItems[position].selected = if (vHolder.cbClothing.isChecked) 1 else 0
        }
    }

    override fun getItemCount(): Int {
        return allItems.size
    }

    inner class VHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var cbClothing: CheckBox
        var clothingNameTv: TextView
        var tvRequire: TextView

        init {
            cbClothing = itemView.findViewById(R.id.cbAddOnes)
            clothingNameTv = itemView.findViewById(R.id.addOneNameTv)
            tvRequire = itemView.findViewById(R.id.tvRequire)
        }
    }
}