package com.app.btmobile.UI.Adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.ClothingModel
import com.app.btmobile.Models.FittedClothingModel
import com.app.btmobile.R
import java.util.*

class ViewFittedClothingAdapter(
    private val context: Context,
    private val allItems: ArrayList<FittedClothingModel>,
    private val onItemClickListener: (String, Int) -> Unit
) : RecyclerView.Adapter<ViewFittedClothingAdapter.VHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        i: Int
    ): VHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val listItem =
            layoutInflater.inflate(R.layout.custom_fit_cloth_addon, parent, false)
        return VHolder(listItem)
    }

    override fun onBindViewHolder(
        vHolder: VHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        var model = allItems[position]

        if (allItems[position].purchased_clothing) {
            vHolder.tvRequire.visibility = View.VISIBLE
        } else {
            vHolder.tvRequire.visibility = View.GONE
        }
        vHolder.tvValue.text = "$"+model.price + " Per Day"

        vHolder.cbClothing.text = model.name
        vHolder.cbClothing.isChecked = allItems[position].selected
        vHolder.cbClothing.setOnClickListener {
            if (vHolder.cbClothing.isChecked) {
                onItemClickListener("true", position)
            } else {
                onItemClickListener("false", position)
            }
        }
    }

    override fun getItemCount(): Int {
        return allItems.size
    }

    inner class VHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var cbClothing: CheckBox
        var tvValue: TextView
        var tvRequire: TextView

        init {
            cbClothing = itemView.findViewById(R.id.cbAddOn)
            tvValue = itemView.findViewById(R.id.tvValue)
            tvRequire = itemView.findViewById(R.id.tvRequire)
        }
    }
}