package com.app.btmobile.UI.Adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.WaiverRenter
import com.app.btmobile.R
import com.app.btmobile.Utils.setColor
import com.app.btmobile.databinding.CustomWaiverCustomerLayBinding
import com.app.btmobile.databinding.CustomWaiverRentersLayBinding
import java.util.*

class WaiverRentersAdapter(
    var act: Activity,
    private val dataSet: ArrayList<WaiverRenter>,
    var deliveryId: Int,
    private val onItemClickListener: (String, Int) -> Unit
) : RecyclerView.Adapter<WaiverRentersAdapter.MyViewHolder>() {

    inner class MyViewHolder(val binding: CustomWaiverRentersLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val binding: CustomWaiverRentersLayBinding =
            CustomWaiverRentersLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        val model = dataSet[listPosition]
        holder.binding.run {
            tvName.text = model.fullName
            tvId.text = model.reservationId.toString() + ""
            tvStatusName.text = model.signedStatus
            if (deliveryId != -1) {
                tvStatusName.text = model.signedStatusSwitchout
            }
            val _status = tvStatusName.text.toString()
            if (_status.equals("SIGNED", ignoreCase = true)) {
                tvStatusName.setBackgroundColor(setColor(act, R.color.green))
            } else {
                tvStatusName.setBackgroundColor(setColor(act, R.color.red))
            }
            tvActionView.setOnClickListener {
                onItemClickListener("view", listPosition)
            }
            tvActionSigned.setOnClickListener {
                onItemClickListener("sign", listPosition)
            }
            tvSendToPhone.setOnClickListener {
                onItemClickListener("sendToPhone", listPosition)
            }
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }
}