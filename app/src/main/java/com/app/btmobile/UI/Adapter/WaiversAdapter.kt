package com.app.btmobile.UI.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.WaiverCustomerModelItem
import com.app.btmobile.R
import com.app.btmobile.UI.Activity.Waiver.WaiverCustomerActivity
import com.app.btmobile.Utils.setColor
import com.app.btmobile.databinding.CustomWaiverCustomerLayBinding
import java.util.*

class WaiversAdapter(
    var context: Context,
    private val dataSet: ArrayList<WaiverCustomerModelItem>
) : RecyclerView.Adapter<WaiversAdapter.MyViewHolder>() {

    inner class MyViewHolder(val binding: CustomWaiverCustomerLayBinding) :
        RecyclerView.ViewHolder(binding.root) {}

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val binding: CustomWaiverCustomerLayBinding =
            CustomWaiverCustomerLayBinding
                .inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {

        val model = dataSet[listPosition]

        holder.binding.run {
            tvName.text = model.customerName
            tvNoOfPeoples.text = model.rentersCount.toString() + ""
            when {
                model.waiverStatus.contains("waiting_for_signature") -> {
                    tvStatusName.text = waiting_for_signature
                    tvStatusName.setBackgroundColor(setColor(context, R.color.red))
                }
                model.waiverStatus.contains("partially_signed") -> {
                    tvStatusName.text = partially_signed
                    tvStatusName.setBackgroundColor(setColor(context, R.color.yellow))
                }
                model.waiverStatus.contains("signed") -> {
                    tvStatusName.text = signed
                    tvStatusName.setBackgroundColor(setColor(context, R.color.green))
                }
            }
            tvActionText.setOnClickListener {
                (context as WaiverCustomerActivity).callNextActivity(listPosition);
            }
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    companion object {
        private const val partially_signed = "PARTIALLY SIGNED"
        private const val signed = "SIGNED"
        private const val waiting_for_signature = "WAITING FOR SIGNATURE"
    }
}