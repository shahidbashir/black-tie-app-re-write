package com.app.btmobile.UI.Dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.DialogAddAddonsSizeManuallyBinding
import com.app.btmobile.databinding.DialogAddScanBarcodeBinding
import com.app.btmobile.databinding.DialogRenterHistoryBinding
import java.util.HashMap

class AddAddonSizeManuallyDialog(
    var addonStatus: String = "",
    var addonId: String = ""
) : DialogFragment() {
    lateinit var binding: DialogAddAddonsSizeManuallyBinding

    private var onScanClick: OnScanClick? =
        null

    val sizeOnly =
        arrayOf(
            "Select Size",
            "XS",
            "S",
            "M",
            "L",
            "XL"
        )
    val sizeType =
        arrayOf("Select Size Type", "Adult", "Junior")
    val size =
        arrayOf(
            "Select Size",
            "XS (Extra Small)",
            "S (Small)",
            "M (Medium)",
            "L (Large)",
            "XL (Extra Large)"
        )

    fun setOnSubmitListener(onScanClick: OnScanClick?) {
        this.onScanClick = onScanClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogAddAddonsSizeManuallyBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        setUpSizeTypeSpinner()
        setUpSizeSpinner()
        binding.run {
            btnScanBarcode.setOnClickListener {
                onScanClick?.clickOnSaveButton(
                    addonStatus,
                    addonId,
                    sizeType[spSizeType.selectedItemPosition],
                    sizeOnly[spSize.selectedItemPosition]
                )

            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }

        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnScanClick {
        fun clickOnSaveButton(
            addonStatus: String,
            addonId: String,
            sizeType: String,
            size: String,
        )

        fun onDismiss()
    }

    companion object {
        fun newInstance(
            addonStatus: String = "",
            addonId: String = "",
        ): AddAddonSizeManuallyDialog {
            return AddAddonSizeManuallyDialog(addonStatus, addonId)
        }
    }


    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onScanClick?.onDismiss()

    }

    fun setUpSizeTypeSpinner() {

        val adapter = ArrayAdapter(requireContext(), R.layout.spinner_layout, sizeType)
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spSizeType.adapter = adapter
        binding.spSizeType.setSelection(0)

        binding.spSizeType.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun setUpSizeSpinner() {


        val adapter = ArrayAdapter(requireContext(), R.layout.spinner_layout, size)
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spSize.adapter = adapter
        binding.spSize.setSelection(0)

        binding.spSize.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }
}