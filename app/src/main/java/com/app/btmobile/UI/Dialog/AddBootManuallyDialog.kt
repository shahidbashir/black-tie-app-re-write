package com.app.btmobile.UI.Dialog

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.Utils.isNotEmptyOrBlank
import com.app.btmobile.Utils.showToast
import com.app.btmobile.databinding.DialogAddBootManuallyBinding

class AddBootManuallyDialog(var isSwitchOut: Boolean, var inventoryType: String) :
    DialogFragment() {
    lateinit var binding: DialogAddBootManuallyBinding

    private var onSubmitClick: OnSubmitClick? =
        null

    fun setOnSubmitListener(onSubmitClick: OnSubmitClick?) {
        this.onSubmitClick = onSubmitClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogAddBootManuallyBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        var keepSwitchText = ""
        binding.run {
            val isInventorySki = inventoryType.equals("ski", ignoreCase = true) ||
                    inventoryType.equals("skier", ignoreCase = true)

            if (!isInventorySki) {
                etSoleLength.visibility = View.GONE
            }
            if (isSwitchOut) {
                rgKeepSwitch.visibility = View.VISIBLE
            } else {
                rgKeepSwitch.visibility = View.GONE
            }

            rgKeepSwitch.setOnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    R.id.rbKeep -> {
                        keepSwitchText = "keep"
                        onSubmitClick?.onSwitchClick(keepSwitchText)
                        dismiss()
                    }
                    R.id.rbSwitchOut -> {
                        keepSwitchText = "switchout"
                    }
                }
            }

            btnSubmit.setOnClickListener {
                if (isInventorySki) {
                    if (isNotEmptyOrBlank(etSoleLength.text.toString())) {
                        onSubmitClick?.onSubmitClick(
                            etManufacture.text.toString(),
                            etBootSize.text.toString(),
                            etSoleLength.text.toString(),
                            keepSwitchText
                        )
                        dismiss()
                    } else {
                        showToast(context as Activity, "Sole Length is Compulsory")
                    }
                } else {
                    onSubmitClick?.onSubmitClick(
                        etManufacture.text.toString(),
                        etBootSize.text.toString(),
                        etSoleLength.text.toString(),
                        keepSwitchText
                    )
                    dismiss()
                }
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }

        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnSubmitClick {
        fun onSubmitClick(
            manufacture: String,
            bootSize: String,
            soleLength: String,
            keepSwitchText: String
        )

        fun onSwitchClick(keepSwitchText: String)
    }

    companion object {
        fun newInstance(isSwitchOut: Boolean, inventoryType: String): AddBootManuallyDialog {
            return AddBootManuallyDialog(isSwitchOut, inventoryType)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

    }
}