package com.app.btmobile.UI.Dialog

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.Utils.isNotEmptyOrBlank
import com.app.btmobile.Utils.showToast
import com.app.btmobile.databinding.DialogAddBootManuallyBinding
import com.app.btmobile.databinding.DialogAddSkisManuallyBinding

class AddSkiManuallyDialog(var isSwitchOut: Boolean, var inventoryType: String) :
    DialogFragment() {
    lateinit var binding: DialogAddSkisManuallyBinding

    private var onSubmitClick: OnSubmitClick? =
        null

    fun setOnSubmitListener(onSubmitClick: OnSubmitClick?) {
        this.onSubmitClick = onSubmitClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogAddSkisManuallyBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        var keepSwitchText = ""
        binding.run {
            if (isSwitchOut) {
                rgKeepSwitch.visibility = View.VISIBLE
            } else {
                rgKeepSwitch.visibility = View.GONE
            }

            rgKeepSwitch.setOnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    R.id.rbKeep -> {
                        keepSwitchText = "keep"
                        onSubmitClick?.onSwitchClick(keepSwitchText)
                        dismiss()
                    }
                    R.id.rbSwitchOut -> {
                        keepSwitchText = "switchout"
                    }
                }
            }

            btnSubmit.setOnClickListener {
                if (isNotEmptyOrBlank(etManufacture.text.toString()) &&
                    isNotEmptyOrBlank(etBindingModel.text.toString())
                ) {
                    onSubmitClick?.onSubmitClick(
                        etManufacture.text.toString(),
                        etBindingModel.text.toString(),
                        keepSwitchText
                    )
                    dismiss()
                } else {
                    showToast(context as Activity, "Please fill all fields")

                }
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }

        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnSubmitClick {
        fun onSubmitClick(
            model: String,
            bindingModel: String,
            keepSwitchText: String
        )

        fun onSwitchClick(keepSwitchText: String)
    }

    companion object {
        fun newInstance(isSwitchOut: Boolean, inventoryType: String): AddSkiManuallyDialog {
            return AddSkiManuallyDialog(isSwitchOut, inventoryType)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)

    }
}