package com.app.btmobile.UI.Dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.databinding.CommonDialogBinding
import com.app.btmobile.databinding.DialogRenterHistoryBinding

class CommonDialog(
    var title: String, var description: String,
    var positiveButtonText: String = "Ok",
    var negativeButtonText: String = "Cancel",
    var isOnlyOkButtonVisible: Boolean = true,
    var isDialogCancelable: Boolean = true

) : DialogFragment() {
    lateinit var binding: CommonDialogBinding

    private var onOkClick: OnOKClick? =
        null

    fun setOnContinueCancelClick(onOkClick: OnOKClick?) {
        this.onOkClick = onOkClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CommonDialogBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            btnCancelDialog.setOnClickListener {
                dismiss()
            }
            ivCross.setOnClickListener {
                dismiss()
            }
            btnOk.setOnClickListener {
                onOkClick?.onOk()
                dismiss()
            }
            tvTitle.text = title
            tvDes.text = description
            btnOk.text = positiveButtonText
            btnCancelDialog.text = negativeButtonText
            // if (isOnlyOkButtonVisible) {
          //  btnCancelDialog.visibility = View.GONE
            //}
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
        dialog!!.setCancelable(isDialogCancelable)

    }

    companion object {
        fun newInstance(
            title: String,
            description: String,
            positiveButtonText: String = "Ok",
            negativeButtonText: String = "Cancel",
            isOnlyOkButtonVisible: Boolean = true,
            isDialogCancelable: Boolean = true
        ): CommonDialog {
            return CommonDialog(
                title, description, positiveButtonText, negativeButtonText,
                isOnlyOkButtonVisible,
                isDialogCancelable
            )
        }
    }

    interface OnOKClick {
        fun onOk()
    }
}