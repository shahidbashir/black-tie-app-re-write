package com.app.btmobile.UI.Dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.databinding.DialogAddScanBarcodeBinding
import com.app.btmobile.databinding.DialogFitAddonWithBarcodeBinding
import com.app.btmobile.databinding.DialogRenterHistoryBinding

class FitAddonWithBarcodeDialog(
    var addonStatus: String = "",
    var addonId: String = "",
    var addonName: String = "",
    var addonSize: String = "",
    var is_manual: Int = 0
) : DialogFragment() {
    lateinit var binding: DialogFitAddonWithBarcodeBinding

    private var onScanClick: OnScanClick? =
        null

    fun setOnSubmitListener(onScanClick: OnScanClick?) {
        this.onScanClick = onScanClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogFitAddonWithBarcodeBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            btnScanBarcode.setOnClickListener {
                if (etScanBarcode.text.isNotEmpty() &&
                    etScanBarcode.text.isNotBlank()
                ) {
                    onScanClick?.onScanBarcode(etScanBarcode.text.toString())
                }
            }

            if (is_manual == 1 && addonStatus.equals("Packed", true)) {
                layAddonsSize.visibility = View.VISIBLE
                tvAddonSize.text = addonSize
                tvAddonName.text = addonName
            } else {
                layAddonsSize.visibility = View.GONE
                tvAddonSize.text = ""
                tvAddonName.text = ""
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }

            btnAddSizeManually.setOnClickListener {
                onScanClick?.clickOnAddSizeManuallyBtn(addonId)
            }
            ivDeleteAddon.setOnClickListener {
                onScanClick?.clickOnUndoAddon(addonId)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnScanClick {
        fun onScanBarcode(barcode: String)
        fun clickOnAddSizeManuallyBtn(addonId: String)
        fun onDismiss()
        fun clickOnUndoAddon(addonId: String)

    }

    companion object {
        fun newInstance(
            addonStatus: String = "",
            addonId: String = "",
            addonName: String = "",
            addonSize: String = "",
            is_manual: Int = 0
        ): FitAddonWithBarcodeDialog {
            return FitAddonWithBarcodeDialog(
                addonStatus, addonId, addonName, addonSize,
                is_manual
            )
        }
    }

    fun clearEditText() {
        binding.etScanBarcode.setText("")
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        binding.etScanBarcode.setText("")
        onScanClick?.onDismiss()

    }
}