package com.app.btmobile.UI.Dialog

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.Utils.isNotEmptyOrBlank
import com.app.btmobile.Utils.showToast
import com.app.btmobile.databinding.DialogAddBootSkiBinding

class FitBootSkiInventoryDialog(
    var isBootAdded: Boolean,
    var isSwitchOut: Boolean,
    var inventoryType: String
) :
    DialogFragment() {
    lateinit var binding: DialogAddBootSkiBinding

    private var onSubmitClick: OnSubmitClick? =
        null

    var keepSwitchText = ""

    fun setOnSubmitListener(onSubmitClick: OnSubmitClick?) {
        this.onSubmitClick = onSubmitClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogAddBootSkiBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            val isInventorySki = inventoryType.equals("ski", ignoreCase = true) ||
                    inventoryType.equals("skier", ignoreCase = true)
            if (isBootAdded) {
                btnAddBoot.text = "Add Boot Manually"
            } else {
                if (isInventorySki) {
                    btnAddBoot.text = "Add Ski Manually"
                } else {
                    btnAddBoot.text = "Add Snowboard"
                }
            }

            if (isSwitchOut) {
                rgKeepSwitch.visibility = View.VISIBLE
            } else {
                rgKeepSwitch.visibility = View.GONE
            }

            rgKeepSwitch.setOnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    R.id.rbKeep -> {
                        keepSwitchText = "keep"
                        onSubmitClick?.onSwitchClick(isBootAdded, keepSwitchText)
                        dismiss()
                    }
                    R.id.rbSwitchOut -> {
                        keepSwitchText = "switchout"
                    }
                }
            }

            btnSubmit.setOnClickListener {
                if (isNotEmptyOrBlank(etBarcode.text.toString())) {
                    if (!isSwitchOut) {
                        onSubmitClick?.onSubmitClick(
                            isBootAdded,
                            etBarcode.text.toString(),
                            keepSwitchText
                        )
                        dismiss()
                    } else {
                        if (keepSwitchText.isEmpty() ||
                            keepSwitchText.isBlank()
                        ) {
                            showToast(
                                context as Activity,
                                "Please select one option from Keep or Switch out."
                            )
                        } else {
                            onSubmitClick?.onSubmitClick(
                                isBootAdded,
                                etBarcode.text.toString(),
                                keepSwitchText
                            )
                            dismiss()
                        }
                    }
                } else {
                    showToast(context as Activity, "Please Enter Barcode")
                }
            }
            btnAddBoot.setOnClickListener {
                onSubmitClick?.addManualButtonClick(isBootAdded)
                dismiss()
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }
        }
    }

    fun getKeepSwitchOut(): String {
        return keepSwitchText
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnSubmitClick {
        fun onSubmitClick(
            isBootAdded: Boolean,
            barcode: String,
            keepSwitchText: String
        )

        fun onSwitchClick(isBootAdded: Boolean, keepSwitchText: String)
        fun addManualButtonClick(isBootAdded: Boolean)
        fun onDismiss()


    }

    companion object {
        fun newInstance(
            isBootAdded: Boolean,
            isSwitchOut: Boolean,
            inventoryType: String
        ): FitBootSkiInventoryDialog {
            return FitBootSkiInventoryDialog(isBootAdded, isSwitchOut, inventoryType)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onSubmitClick?.onDismiss()

    }
}