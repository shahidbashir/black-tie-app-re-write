package com.app.btmobile.UI.Dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.databinding.DialogAddScanBarcodeBinding
import com.app.btmobile.databinding.DialogRenterHistoryBinding

class InventoryCountDialog(
    var isBoot: Boolean = false,
    var isAddon: Boolean = false,
    var addonStatus: String = ""
) : DialogFragment() {
    lateinit var binding: DialogAddScanBarcodeBinding

    private var onScanClick: OnScanClick? =
        null

    fun setOnSubmitListener(onScanClick: OnScanClick?) {
        this.onScanClick = onScanClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogAddScanBarcodeBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            cbHelmet.visibility = View.VISIBLE
            btnScanBarcode.setOnClickListener {
                if (etScanBarcode.text.isNotEmpty() &&
                    etScanBarcode.text.isNotBlank()
                ) {
                    onScanClick?.onScanBarcode(etScanBarcode.text.toString(), isBoot, isAddon)
                }
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }
            ivCross.setOnClickListener {
                dismiss()
            }

            cbHelmet.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    if (etScanBarcode.text.toString().isNotEmpty() &&
                        etScanBarcode.text.toString().isNotBlank()
                    ) {
                        onScanClick?.onScanBarcode(etScanBarcode.text.toString(), isBoot, isAddon)
                    }
                }
            }
        }
    }

    fun isHelmetChecked(): Boolean {
        return binding.cbHelmet.isChecked
    }

    fun unselectHelmet() {
        binding.cbHelmet.isChecked = false

    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnScanClick {
        fun onScanBarcode(barcode: String, isBoot: Boolean, isAddon: Boolean)
        fun onDismiss()

    }

    companion object {
        fun newInstance(
            isBoot: Boolean = false,
            isAddon: Boolean = false,
            addonStatus: String = ""
        ): InventoryCountDialog {
            return InventoryCountDialog(isBoot, isAddon, addonStatus)
        }
    }

    fun clearEditText() {
        binding.etScanBarcode.setText("")
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        binding.etScanBarcode.setText("")
        onScanClick?.onDismiss()

    }
}