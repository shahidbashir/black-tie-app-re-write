package com.app.btmobile.UI.Dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R

class LogoutDialog : DialogFragment() {
    private var btnCancelDialog: Button? = null
    private var btnSignUpDialog: Button? = null
    private var continueCancelClick: OnLogoutClick? =
        null

    fun setOnContinueCancelClick(continueCancelClick: OnLogoutClick?) {
        this.continueCancelClick = continueCancelClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.logout_dialog, container)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        btnCancelDialog =
            view.findViewById<View>(R.id.btnCancelDialog) as Button
        btnSignUpDialog =
            view.findViewById<View>(R.id.btnSignUpDialog) as Button
        btnCancelDialog!!.setOnClickListener { dismiss() }
        btnSignUpDialog!!.setOnClickListener {
            if (continueCancelClick != null) continueCancelClick!!.onContinueClicked()
            dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnLogoutClick {
        fun onContinueClicked()
    }

    companion object {
        fun newInstance(): LogoutDialog {
            return LogoutDialog()
        }
    }
}