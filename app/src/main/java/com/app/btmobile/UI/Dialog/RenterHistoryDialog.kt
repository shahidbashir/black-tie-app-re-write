package com.app.btmobile.UI.Dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.databinding.DialogRenterHistoryBinding

class RenterHistoryDialog(var history: String, var renterName: String) : DialogFragment() {
    lateinit var binding: DialogRenterHistoryBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogRenterHistoryBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            btnOkDialog.setOnClickListener {
                dismiss()
            }
            ivCross.setOnClickListener {
                dismiss()
            }
            tvRenterHistory.text = history
            tvRenterHistoryName.text = renterName
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    companion object {
        fun newInstance(history: String, renterName: String): RenterHistoryDialog {
            return RenterHistoryDialog(history, renterName)
        }
    }
}