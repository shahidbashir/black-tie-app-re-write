package com.app.btmobile.UI.Dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.DialogAddScanBarcodeBinding
import com.app.btmobile.databinding.DialogRenterHistoryBinding
import com.app.btmobile.databinding.DialogReturnAddScanBarcodeBinding

class ReturnAddScanBarcodeDialog(
    var isBoot: Boolean = false,
    var isAddon: Boolean = false,
    var addonStatus: String = "",
    var addonId: String = "",
    var showUndoButton: Boolean = false,
    var addonBarcode: String = "",
    var renterId: String = "",

    ) : DialogFragment() {
    lateinit var binding: DialogReturnAddScanBarcodeBinding

    private var onScanClick: OnScanClick? =
        null

    fun setOnSubmitListener(onScanClick: OnScanClick?) {
        this.onScanClick = onScanClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogReturnAddScanBarcodeBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            Debugger.wtf("isManuall", "Dia $showUndoButton")

            if (showUndoButton) {
                btnUndoAddon.visibility = View.VISIBLE
                btnScanBarcode.visibility = View.GONE
                etScanBarcode.visibility = View.GONE
            } else {
                btnUndoAddon.visibility = View.GONE
                btnScanBarcode.visibility = View.VISIBLE
                etScanBarcode.visibility = View.VISIBLE
            }

            if (showUndoButton && addonStatus.equals("Fitted", true)) {
                btnUndoAddon.text = "Removed"
                etScanBarcode.setText(addonBarcode)
            } else {
                btnUndoAddon.text = "Returned"
                etScanBarcode.setText("")
            }

            btnScanBarcode.setOnClickListener {
                if (etScanBarcode.text.isNotEmpty() &&
                    etScanBarcode.text.isNotBlank()
                ) {
                    onScanClick?.onScanBarcode(etScanBarcode.text.toString(), isBoot, isAddon)
                }
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }
            ivCross.setOnClickListener {
                dismiss()
            }
            btnUndoAddon.setOnClickListener {
                onScanClick?.onUndoClickListener(addonId)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnScanClick {
        fun onScanBarcode(barcode: String, isBoot: Boolean, isAddon: Boolean)
        fun onUndoClickListener(addonId: String)
        fun onDismiss()

    }

    companion object {
        fun newInstance(
            isBoot: Boolean = false,
            isAddon: Boolean = false,
            addonStatus: String = "",
            addonId: String = "",
            showUndoButton: Boolean = false,
            addonBarcode: String = "",
            renterId: String = ""
        ): ReturnAddScanBarcodeDialog {
            return ReturnAddScanBarcodeDialog(
                isBoot,
                isAddon,
                addonStatus,
                addonId,
                showUndoButton,
                addonBarcode,
                renterId
            )
        }
    }

    fun clearEditText() {
        binding.etScanBarcode.setText("")
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        binding.etScanBarcode.setText("")
        onScanClick?.onDismiss()
    }
}