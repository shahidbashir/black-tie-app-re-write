package com.app.btmobile.UI.Dialog

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.app.btmobile.Models.MassReturnBaseModel
import com.app.btmobile.Models.MassReturnModel
import com.app.btmobile.R
import com.app.btmobile.UI.Activity.Return.ReturnRentersActivity
import com.app.btmobile.Utils.showToast
import com.app.btmobile.databinding.DialogAddScanBarcodeBinding
import com.app.btmobile.databinding.DialogRenterHistoryBinding
import com.app.btmobile.databinding.DialogReturnSearchByCodeBinding

class ReturnSearchByBarcodeDialog(
    var isFromMassScan: Boolean = false
) : DialogFragment() {
    lateinit var binding: DialogReturnSearchByCodeBinding

    private var onScanClick: OnScanClick? =
        null

    fun setOnSubmitListener(onScanClick: OnScanClick?) {
        this.onScanClick = onScanClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogReturnSearchByCodeBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            clearRenterData()
            if (isFromMassScan) {
                tvScanTitle.text = "Scan Return"
                layReturnDate.visibility = View.GONE
            } else {
                tvScanTitle.text = "Search Barcode"
                layReturnDate.visibility = View.VISIBLE
            }
            btnScanBarcode.setOnClickListener {
                if (etScanBarcode.text.isNotEmpty() &&
                    etScanBarcode.text.isNotBlank()
                ) {
                    onScanClick?.onScanBarcode(etScanBarcode.text.toString())
//                    dismiss()
                }
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }
            ivCross.setOnClickListener {
                dismiss()
            }

        }
    }

    fun showRenterData(model: MassReturnBaseModel) {
        clearRenterData()
        if (model.success) {
            if (model.data != null) {
                binding.run {
                    cvRenterData.visibility = View.VISIBLE
                    tvRenterName.paintFlags =
                        tvRenterName.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                    tvRenterName.text = model.data.name
                    tvMake.text = model.data.manufacture
                    tvType.text = model.data.type
                    tvBootModel.text = model.data.model
                    tvBootBarcode.text = model.data.barcode
                    tvBootStatus.text = model.data.status
                    tvBootSize.text = model.data.size
                    tvRez.text = model.data.customer_name
                    tvRez.paintFlags =
                        tvRez.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                    if (!model.data.return_date.isNullOrEmpty()) {
                        tvReturnDate.text = model.data.return_date
                    }

                    tvRez.setOnClickListener {
                        if (!model.data.shipment_id.isNullOrEmpty()) {
                            val i = Intent(requireActivity(), ReturnRentersActivity::class.java)
                            i.putExtra("shipmentId", model.data.shipment_id.toInt())
                            requireActivity().startActivity(i)
                        }
                        dismiss()
                    }

//                    tvRenterName.setOnClickListener {
//                        if (!model.data.shipment_id.isNullOrEmpty()) {
//                            val i = Intent(requireActivity(), ReturnRentersActivity::class.java)
//                            i.putExtra("shipmentId", model.data.shipment_id.toInt())
//                            requireActivity().startActivity(i)
//                        }
//                        dismiss()
//                    }
//                    tvBootStatus.setOnClickListener {
//                        if (!model.data.shipment_id.isNullOrEmpty()) {
//                            val i = Intent(requireActivity(), ReturnRentersActivity::class.java)
//                            i.putExtra("shipmentId", model.data.shipment_id.toInt())
//                            requireActivity().startActivity(i)
//                        }
//                        dismiss()
//                    }

                }
            }
        }
    }


    fun clearRenterData() {
        binding.run {
            cvRenterData.visibility = View.GONE
            tvMake.text = ""
            tvRenterName.text = ""
            tvType.text = ""
            tvBootModel.text = ""
            tvBootBarcode.text = ""
            tvBootStatus.text = ""
            tvBootSize.text = ""
            tvReturnDate.text = ""
            tvRez.text = ""
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnScanClick {
        fun onScanBarcode(barcode: String)
        fun onDismiss()

    }

    companion object {
        fun newInstance(
            isFromMassScan: Boolean = false,
        ): ReturnSearchByBarcodeDialog {
            return ReturnSearchByBarcodeDialog(isFromMassScan)
        }
    }

    fun clearEditText() {
        binding.etScanBarcode.setText("")
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        binding.etScanBarcode.setText("")
        onScanClick?.onDismiss()

    }
}