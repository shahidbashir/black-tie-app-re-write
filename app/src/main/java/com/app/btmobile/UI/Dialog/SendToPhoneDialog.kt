package com.app.btmobile.UI.Dialog

import android.app.Activity
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.app.btmobile.R
import com.app.btmobile.Utils.showToast
import com.app.btmobile.databinding.DialogAddScanBarcodeBinding
import com.app.btmobile.databinding.DialogRenterHistoryBinding
import com.app.btmobile.databinding.DialogSendToPhoneBinding

class SendToPhoneDialog(var customerPhone: String? = "") : DialogFragment() {
    lateinit var binding: DialogSendToPhoneBinding

    private var onSendClick: OnSendClick? =
        null

    fun setonSendClickListener(onSendClick: OnSendClick?) {
        this.onSendClick = onSendClick
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogSendToPhoneBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window
            ?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        binding.run {
            btnSendCustomer.setOnClickListener {
                onSendClick?.onSend(
                    ""
                )
                dismiss()
            }
            btnSend.setOnClickListener {
                if (etEnterPhone.text.isNotEmpty() &&
                    etEnterPhone.text.isNotBlank()
                ) {
                    onSendClick?.onSend(
                        etEnterPhone.text.toString()
                    )
                    dismiss()
                } else {
                    showToast(context as Activity, "Please Enter Phone Number", false)
                }
            }

            if (!customerPhone.isNullOrEmpty() &&
                !customerPhone.isNullOrBlank()
            ) {
                layPhone.visibility = View.VISIBLE
                tvCustomerPhone.text = customerPhone
            }
            ivCross.setOnClickListener {
                dismiss()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnSendClick {
        fun onSend(phoneNum: String)
    }

    companion object {
        fun newInstance(customerPhone: String? = ""): SendToPhoneDialog {
            return SendToPhoneDialog(customerPhone)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        binding.etEnterPhone.setText("")
    }
}