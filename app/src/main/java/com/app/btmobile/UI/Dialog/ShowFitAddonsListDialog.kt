package com.app.btmobile.UI.Dialog

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.Addon
import com.app.btmobile.Models.ClothingModel
import com.app.btmobile.Models.FittedClothingModel
import com.app.btmobile.Models.FittingAddon
import com.app.btmobile.UI.Adapter.FitAddonsAdapter
import com.app.btmobile.UI.Adapter.ViewAddOnsAdapter
import com.app.btmobile.UI.Adapter.ViewClothingAdapter
import com.app.btmobile.UI.Adapter.ViewFittedClothingAdapter
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.ShowAddonsDialogBinding
import com.app.btmobile.databinding.ShowFitAddonsDialogBinding
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList

class ShowFitAddonsListDialog(
    var addOnsList: ArrayList<FittingAddon>,
    var clothingList: ArrayList<FittedClothingModel>
) :
    DialogFragment() {
    lateinit var binding: ShowFitAddonsDialogBinding

    private var onAddOnSubmit: OnAddonsSubmitted? =
        null
    lateinit var viewAddOnsAdapter: FitAddonsAdapter
    lateinit var viewClothingAdapter: ViewFittedClothingAdapter

    fun setOnSubmitListener(
        onAddOnSubmit: OnAddonsSubmitted?
    ) {
        this.onAddOnSubmit = onAddOnSubmit
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ShowFitAddonsDialogBinding.inflate(inflater, container, false);
        return binding.root
    }

    fun checkAddonItem(
        addon_id: Int, addon_status: String, helmentSize: String,
        is_manual: Int
    ) {
        addOnsList.forEachIndexed { index, element ->
            if (element.isAddonWithBarcode && element.id == addon_id) {
                if (addon_status == "Fitted") {
                    addOnsList[index].selected = true
                    addOnsList[index].completeSize = helmentSize
                    addOnsList[index].completeSize = helmentSize
                    addOnsList[index].is_manual = is_manual
                    viewAddOnsAdapter.notifyItemChanged(index)
                } else {
                    addOnsList[index].selected = false
                    addOnsList[index].completeSize = ""
                    addOnsList[index].is_manual = is_manual
                    viewAddOnsAdapter.notifyItemChanged(index)
                }
            }
        }
    }

    fun onItemClickListener(from: String, position: Int) {
        var model = addOnsList[position]
        onAddOnSubmit?.clickOnAddonsWithBarcode(
            from, model.id.toString(),
            model.name ?: "", model.completeSize ?: "",
            model.is_manual ?: 0
        )
    }

    fun onItemClothingClickListener(from: String, position: Int) {
        if (from == "true") {
            clothingList.forEachIndexed { index, fittedClothingModel ->
                clothingList[index].selected = position == index
            }
        } else {
            clothingList.map {
                it.selected = false
            }
        }
        viewClothingAdapter.notifyDataSetChanged()
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            if (!addOnsList.isNullOrEmpty()) {
                viewAddOnsAdapter = FitAddonsAdapter(
                    context!!, addOnsList, { from, position ->
                        onItemClickListener(from, position)
                    }
                )
                rvFitAddons.setHasFixedSize(true)
                val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
                rvFitAddons.layoutManager = mLayoutManager
                rvFitAddons.itemAnimator = DefaultItemAnimator()
                rvFitAddons.adapter = viewAddOnsAdapter
            } else {
                tvNoAddonsFound.visibility = View.VISIBLE
            }

            if (!clothingList.isNullOrEmpty()) {
                clothingList.map {
                    it.selected = it.status == "Fitted" && it.already_rented
                }

                viewClothingAdapter = ViewFittedClothingAdapter(
                    context!!, clothingList, { from, position ->
                        onItemClothingClickListener(from, position)
                    }
                )

                rvFittedClothing.setHasFixedSize(true)
                val mLayoutManagerClothing: RecyclerView.LayoutManager =
                    LinearLayoutManager(context)
                rvFittedClothing.layoutManager = mLayoutManagerClothing
                rvFittedClothing.itemAnimator = DefaultItemAnimator()
                rvFittedClothing.adapter = viewClothingAdapter
                rvFittedClothing.isNestedScrollingEnabled = false
            } else {
                tvNoClothingFound.visibility = View.VISIBLE
            }
            btnOk.setOnClickListener {
                var addOns: ArrayList<FittingAddon> = ArrayList()
                var list = addOnsList.filter { item -> !item.isAddonWithBarcode }
                addOns.addAll(list)
                Debugger.wtf("onSubmit", "${Gson().toJson(addOnsList)}")

                var clothing: ArrayList<FittedClothingModel> = ArrayList()
                var clothingList = clothingList.filter { item -> item.selected }
                clothing.addAll(clothingList)

                onAddOnSubmit?.onSubmit(addOnsList, clothing)
                dismiss()
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }
        }

    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnAddonsSubmitted {
        fun onSubmit(
            addOnsList: ArrayList<FittingAddon>,
            clothingList: ArrayList<FittedClothingModel>
        )

        fun clickOnAddonsWithBarcode(
            status: String, addonId: String,
            addonName: String, addonSize: String,
            is_manual: Int
        )
    }

    companion object {
        fun newInstance(
            addOnsList: ArrayList<FittingAddon>,
            clothingList: ArrayList<FittedClothingModel>
        ): ShowFitAddonsListDialog {
            return ShowFitAddonsListDialog(addOnsList, clothingList)
        }
    }
}