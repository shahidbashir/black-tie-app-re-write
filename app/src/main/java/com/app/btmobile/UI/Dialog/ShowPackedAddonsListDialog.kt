package com.app.btmobile.UI.Dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.btmobile.Models.Addon
import com.app.btmobile.Models.ClothingModel
import com.app.btmobile.UI.Adapter.ViewAddOnsAdapter
import com.app.btmobile.UI.Adapter.ViewClothingAdapter
import com.app.btmobile.databinding.ShowAddonsDialogBinding
import kotlin.collections.ArrayList

class ShowPackedAddonsListDialog(
    var addOnsList: ArrayList<Addon>,
    var clothingList: ArrayList<ClothingModel>
) :
    DialogFragment() {
    lateinit var binding: ShowAddonsDialogBinding

    private var onAddOnSubmit: OnAddonsSubmitted? = null

    lateinit var viewAddOnsAdapter: ViewAddOnsAdapter

    fun setOnSubmitListener(
        onAddOnSubmit: OnAddonsSubmitted?
    ) {
        this.onAddOnSubmit = onAddOnSubmit
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ShowAddonsDialogBinding.inflate(inflater, container, false);
        return binding.root
    }

    fun onItemClickListener(from: String, position: Int) {
        var model = addOnsList[position]
        onAddOnSubmit?.clickOnAddonsWithBarcode(from)

//        when (from) {
//            "addonWithBarcode" -> {
//            }
//        }
    }

    fun checkAddonItem(addon_id: Int, addon_status: String, helmentSize: String) {
        addOnsList.forEachIndexed { index, element ->
            if (element.isAddonWithBarcode && element.id == addon_id) {
                if (addon_status == "Packed") {
                    addOnsList[index].selected = 1
                    addOnsList[index].status = addon_status
                    addOnsList[index].completeSize = helmentSize
                    viewAddOnsAdapter.notifyItemChanged(index)
                } else {
                    addOnsList[index].selected = 0
                    addOnsList[index].status = addon_status
                    addOnsList[index].completeSize = ""
                    viewAddOnsAdapter.notifyItemChanged(index)
                }
            }
        }
    }


    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            if (!addOnsList.isNullOrEmpty()) {

                viewAddOnsAdapter = ViewAddOnsAdapter(
                    context!!, addOnsList, { from, position ->
                        onItemClickListener(from, position)
                    }
                )
                rvAddons.setHasFixedSize(true)
                val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
                rvAddons.layoutManager = mLayoutManager
                rvAddons.itemAnimator = DefaultItemAnimator()
                rvAddons.adapter = viewAddOnsAdapter
                rvAddons.isNestedScrollingEnabled = false
            } else {
                tvNoAddonsFound.visibility = View.VISIBLE
            }

            if (!clothingList.isNullOrEmpty()) {
                var viewClothingAdapter = ViewClothingAdapter(
                    context!!, clothingList
                )

                rvClothing.setHasFixedSize(true)
                val mLayoutManagerClothing: RecyclerView.LayoutManager =
                    LinearLayoutManager(context)
                rvClothing.layoutManager = mLayoutManagerClothing
                rvClothing.itemAnimator = DefaultItemAnimator()
                rvClothing.adapter = viewClothingAdapter
                rvAddons.isNestedScrollingEnabled = false

            } else {
                tvNoClothingFound.visibility = View.VISIBLE
            }

            btnOk.setOnClickListener {
//                var addons: ArrayList<Addon> = ArrayList()
//                var list = addOnsList.filter { item -> !item.isAddonWithBarcode }
//                addons.addAll(list)
                onAddOnSubmit?.onSubmit(addOnsList, clothingList)
                dismiss()
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnAddonsSubmitted {
        fun onSubmit(
            addOnsList: ArrayList<Addon>,
            clothingList: ArrayList<ClothingModel>
        )

        fun clickOnAddonsWithBarcode(status: String)
    }

    companion object {
        fun newInstance(
            addOnsList: ArrayList<Addon>,
            clothingList: ArrayList<ClothingModel>
        ): ShowPackedAddonsListDialog {
            return ShowPackedAddonsListDialog(addOnsList, clothingList)
        }
    }
}