package com.app.btmobile.UI.Dialog

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.app.btmobile.databinding.SignatureDialogBinding
import com.github.gcacace.signaturepad.views.SignaturePad.OnSignedListener

class SignatureDialog(val signUrl: String, var signBitmap: Bitmap? = null) : DialogFragment() {
    lateinit var binding: SignatureDialogBinding

    private var onSignSaved: OnSignatureSaved? =
        null

    fun setOnContinueCancelClick(onSignSaved: OnSignatureSaved?) {
        this.onSignSaved = onSignSaved
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SignatureDialogBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            if (signUrl != null) {

            }

            btnSaveSign.setOnClickListener {
                if (signaturePad.isEmpty) {

                } else {
                    if (onSignSaved != null) {
                        onSignSaved?.onSignDone(signaturePad.signatureBitmap)
                        dismiss()
                    }
                }
            }
            signaturePad.setOnSignedListener(object : OnSignedListener {
                override fun onStartSigning() {}
                override fun onSigned() {
                    signButtons.visibility = View.VISIBLE
                }

                override fun onClear() {
                    signButtons.visibility = View.GONE
                }
            })

            if (signBitmap != null) {
                signaturePad.signatureBitmap = signBitmap
            }
            btnClearSign.setOnClickListener(View.OnClickListener { signaturePad.clear() })
        }

    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnSignatureSaved {
        fun onSignDone(signBitmap: Bitmap)
    }

    companion object {
        fun newInstance(signUrl: String, signBitmap: Bitmap? = null): SignatureDialog {
            return SignatureDialog(signUrl, signBitmap)
        }
    }
}