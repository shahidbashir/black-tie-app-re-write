package com.app.btmobile.UI.Dialog

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.app.btmobile.Models.PackedInventoryModel
import com.app.btmobile.R
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.setColor
import com.app.btmobile.databinding.DialogChangeAddoneStatusBinding

class UpdateAddonStatusDialog(
    var model: PackedInventoryModel
) :
    DialogFragment() {
    lateinit var binding: DialogChangeAddoneStatusBinding

    private var onAddOnSubmit: OnAddOnStatusSubmitted? =
        null

    fun setOnSubmitListener(
        onAddOnSubmit: OnAddOnStatusSubmitted?
    ) {
        this.onAddOnSubmit = onAddOnSubmit
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogChangeAddoneStatusBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)


        val arrayListSizes = resources.getStringArray(R.array.helmetSizesList).toList()
        val arrayAdapter = ArrayAdapter(
            requireContext(),
            R.layout.support_simple_spinner_dropdown_item,
            arrayListSizes
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.packUnpackSpinner.adapter = arrayAdapter



        binding.run {
            for (i in arrayListSizes.indices) {
                if (!model.helmetSize.equals("N/A", true)) {
                    if (model.helmetSize.equals(arrayListSizes[i], true)) {
                        packUnpackSpinner.setSelection(i, true)
                        Debugger.wtf(
                            "arrayListSizes",
                            "$i ${model.helmetSize} ${packUnpackSpinner.selectedItem}"
                        )

                        break
                    }
                }
            }

            Debugger.wtf("arrayListSizes", "${arrayListSizes}  / ${model.helmetSize}")


            packUnPackTv.text = model.name
            if (model.name.equals("Helmet", true)) {
                packUnpackSubmitLay.visibility = View.VISIBLE
                btnSubmitDialog.visibility = View.VISIBLE
            } else {
                packUnpackSubmitLay.visibility = View.GONE
                btnSubmitDialog.visibility = View.GONE
            }


            if (model.status != null) {
                if (model.status.equals("Packed", true)) {
                    packUnPackTv.setTextColor(setColor(requireContext(), R.color.green))
                    packUnpackCB.isChecked = true
                } else {
                    packUnPackTv.setTextColor(setColor(requireContext(), R.color.red))
                    packUnpackCB.isChecked = false
                }
            } else {
                packUnPackTv.setTextColor(setColor(requireContext(), R.color.red))
                packUnpackCB.isChecked = false
            }

            packUnpackCB.setOnClickListener(View.OnClickListener {
                val status = if (!model.status
                        .equals("Packed", true)
                ) "Packed" else "Not Packed"
                model.status = status
                val color =
                    if (model.status.equals("Packed", true)) setColor(requireContext(), R.color.green)
                    else setColor(requireContext(), R.color.red)
                packUnPackTv.setTextColor(color)
                val size: String = packUnpackSpinner.selectedItem.toString()
                if (!model.name.equals("Helmet", true)) {
                    onAddOnSubmit?.onSubmit(model.id.toString() + "", status, "")
                    dialog!!.dismiss()
                } else {
                    onAddOnSubmit?.onSubmit(model.id.toString() + "", status, size)
                }
            })

            btnCancelDialog.setOnClickListener(View.OnClickListener { //getPackedItemsList(shipment_id);
                dialog!!.dismiss()
            })

            btnSubmitDialog.setOnClickListener(View.OnClickListener {
                val size: String = packUnpackSpinner.selectedItem.toString()
                if (size != "Please Select Size") {
                    val status = if (model.status
                            .equals("Packed", true)
                    ) "Packed" else "Not Packed"
                    onAddOnSubmit?.onSubmit(model.id.toString() + "", status, size)
                    dialog!!.dismiss()
                } else {
                    Toast.makeText(requireContext(), "Please Select Size", Toast.LENGTH_LONG).show()
                }
            })

        }

    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnAddOnStatusSubmitted {
        fun onSubmit(
            addonId: String,
            status: String,
            helmetSize: String
        )
    }

    companion object {
        fun newInstance(
            packedInventoryModel: PackedInventoryModel
        ): UpdateAddonStatusDialog {
            return UpdateAddonStatusDialog(packedInventoryModel)
        }
    }
}