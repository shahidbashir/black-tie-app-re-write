package com.app.btmobile.UI.Dialog

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.app.btmobile.Utils.showToast
import com.app.btmobile.databinding.DialogUpdateDinDialogBinding

class UpdateDinSettingDialog(val newDingSetting: String) : DialogFragment() {
    lateinit var binding: DialogUpdateDinDialogBinding

    private var onDinSettingSaved: OnUpdateDinSettingSaved? =
        null

    fun setOnUpdateDinSettingSubmit(onDinSettingSaved: OnUpdateDinSettingSaved?) {
        this.onDinSettingSaved = onDinSettingSaved
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogUpdateDinDialogBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            // edtDinText.setText(dinSetting);
            if (newDingSetting == "N/A") {
                edtDinText.setText("")
            } else {
                edtDinText.setText(newDingSetting)
            }
            btnSubmitDialog.setOnClickListener {
                val dinText = edtDinText.text.toString()
                if (dinText != "") {
                    if (cbConfirmDinUpdate.isChecked) {
                        onDinSettingSaved?.onDinSettingDone(dinText)
                        dialog!!.dismiss()
                    } else {
                        showToast(context as Activity, "Please Confirm Din Setting")
                    }
                } else {
                    showToast(context as Activity, "Please Give Valid Values")
                }
            }
            btnCancelDialog.setOnClickListener {
                dismiss()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnUpdateDinSettingSaved {
        fun onDinSettingDone(dinSetting: String)
    }

    companion object {
        fun newInstance(newDingSetting: String): UpdateDinSettingDialog {
            return UpdateDinSettingDialog(newDingSetting)
        }
    }
}