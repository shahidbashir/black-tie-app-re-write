package com.app.btmobile.UI.Dialog

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.app.btmobile.Models.GetAbilityShoeSizeHeightListModel
import com.app.btmobile.Models.PackingData
import com.app.btmobile.R
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.showToast
import com.app.btmobile.databinding.DialogUpdateRenterDataBinding
import java.util.*
import kotlin.collections.ArrayList


class UpdateRenterDataDialog(
    var getAbilityShoeSizeHeightListModel: GetAbilityShoeSizeHeightListModel,
    var packingData: PackingData
) : DialogFragment() {
    lateinit var binding: DialogUpdateRenterDataBinding

    val current = arrayOf("")
    val ddmmyyyy = "DDMMYYYY"
    val cal = Calendar.getInstance()


    private var onUpdateRenterDataListener: OnUpdateRenterDataListener? =
        null

    fun setOnSubmitListener(onUpdateRenterDataListener: OnUpdateRenterDataListener?) {
        this.onUpdateRenterDataListener = onUpdateRenterDataListener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogUpdateRenterDataBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            tvDay.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence,
                    start: Int,
                    before: Int,
                    count: Int
                ) {
                    if (s.toString() != current[0]) {
                        var clean = s.toString().replace("[^\\d.]|\\.".toRegex(), "")
                        val cleanC =
                            current[0].replace("[^\\d.]|\\.".toRegex(), "")
                        val cl = clean.length
                        var sel = cl
                        var i = 2
                        while (i <= cl && i < 6) {
                            sel++
                            i += 2
                        }
                        //Fix for pressing delete next to a forward slash
                        if (clean == cleanC) sel--
                        if (clean.length < 8) {
                            clean = clean + ddmmyyyy.substring(clean.length)
                        } else {
                            //This part makes sure that when we finish entering numbers
                            //the date is correct, fixing it otherwise
                            var day = clean.substring(0, 2).toInt()
                            var mon = clean.substring(2, 4).toInt()
                            var year = clean.substring(4, 8).toInt()
                            mon = if (mon < 1) 1 else if (mon > 12) 12 else mon
                            cal[Calendar.MONTH] = mon - 1
                            year = if (year < 1900) 1900 else if (year > 2100) 2100 else year
                            cal[Calendar.YEAR] = year
                            // ^ first set year for the line below to work correctly
                            //with leap years - otherwise, date e.g. 29/02/2012
                            //would be automatically corrected to 28/02/2012
                            day =
                                if (day > cal.getActualMaximum(Calendar.DATE)) cal.getActualMaximum(
                                    Calendar.DATE
                                ) else day
                            clean = String.format("%02d%02d%02d", day, mon, year)
                        }
                        clean = String.format(
                            "%s/%s/%s", clean.substring(0, 2),
                            clean.substring(2, 4),
                            clean.substring(4, 8)
                        )
                        sel = if (sel < 0) 0 else sel
                        current[0] = clean
                        tvDay.setText(current[0])
                        tvDay.setSelection(if (sel < current[0].length) sel else current[0].length)
                    }
                }

                override fun afterTextChanged(s: Editable) {}
            })

            if (packingData.gender != null) {
                if (packingData.gender.toString()
                        .equals("male", ignoreCase = true)
                ) {
                    (rgGender.getChildAt(0) as RadioButton).isChecked = true
                } else {
                    (rgGender.getChildAt(1) as RadioButton).isChecked = true
                }
            }

            when (packingData.shoeType.toLowerCase()) {
                "male", "m" -> {
                    (rgShoType.getChildAt(0) as RadioButton).isChecked = true
                    showShoeSizeSpinner(getAbilityShoeSizeHeightListModel.menShoeSize, spShoeSize)
                }
                "women", "w" -> {
                    (rgShoType.getChildAt(1) as RadioButton).isChecked = true
                    showShoeSizeSpinner(getAbilityShoeSizeHeightListModel.womenShoeSize, spShoeSize)
                }
                "youth", "y" -> {
                    (rgShoType.getChildAt(2) as RadioButton).isChecked = true
                    showShoeSizeSpinner(getAbilityShoeSizeHeightListModel.youthShoeSize, spShoeSize)
                }
                else -> {
                    (rgShoType.getChildAt(0) as RadioButton).isChecked = true
                    showShoeSizeSpinner(getAbilityShoeSizeHeightListModel.menShoeSize, spShoeSize)
                }
            }
            etWeightUpdateDialog.setText(
                packingData.weight.toString().replace("([a-z])".toRegex(), "")
            )

            tvDay.setText(packingData.dob)

            btnCancelDialog.setOnClickListener {
                dialog!!.dismiss()
            }

            rgShoType.setOnCheckedChangeListener { group, checkedId ->
                val id: Int = rgShoType.getCheckedRadioButtonId()
                if (id == R.id.rbShoeMale) {
                    showShoeSizeSpinner(getAbilityShoeSizeHeightListModel.menShoeSize, spShoeSize)
                } else if (id == R.id.rbShoeWomen) {
                    showShoeSizeSpinner(getAbilityShoeSizeHeightListModel.womenShoeSize, spShoeSize)
                } else {
                    showShoeSizeSpinner(getAbilityShoeSizeHeightListModel.youthShoeSize, spShoeSize)
                }
            }

            btnUpdate.setOnClickListener(View.OnClickListener {
                var dob = ""
                var gender = ""
                var shoeType = ""

                dob = tvDay.text.toString()
                //Get Gender
                val id: Int = rgGender.checkedRadioButtonId
                if (id == R.id.rbMale) {
                    gender = "Male"
                    Debugger.wtf("RadioGroup", "Male")
                } else {
                    gender = "Female"
                    Debugger.wtf("RadioGroup", "Female")
                }

                //Get Show Type
                val shoeId: Int = rgShoType.checkedRadioButtonId
                if (shoeId == R.id.rbShoeMale) {
                    shoeType = "M"
                    Debugger.wtf("shoeType", "Male")
                } else if (shoeId == R.id.rbShoeWomen) {
                    shoeType = "W"
                    Debugger.wtf("shoeType", "Male")
                } else {
                    shoeType = "Y"
                    Debugger.wtf("shoeType", "Female")
                }

                if (!spAbility.selectedItem.toString().equals("Select", true)) {
                    dialog!!.dismiss()
                    onUpdateRenterDataListener?.onUpdateClick(
                        dob,
                        gender,
                        shoeType,
                        spHeight.selectedItem.toString(),
                        etWeightUpdateDialog.text.toString(),
                        spShoeSize.selectedItem.toString(),
                        spAbility.selectedItem.toString()
                    )
                    tvDay.setText("")
                } else {
                    showToast(requireActivity(), "Please Select Ability")
                }
            })
            var abiltyList: ArrayList<String> = ArrayList<String>()
            abiltyList.add("Select")
            abiltyList.addAll(getAbilityShoeSizeHeightListModel.ability)
            loadHeightShoeData(
                getAbilityShoeSizeHeightListModel.height,
                getAbilityShoeSizeHeightListModel.womenShoeSize,
                abiltyList, spHeight, spShoeSize, spAbility
            )
        }
    }

    fun loadHeightShoeData(
        Height: List<String>,
        ShoeSize: List<String>,
        Ability: List<String>,
        spHeight: Spinner,
        spShoeSize: Spinner,
        spAbility: Spinner
    ) {
        val spinnerAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            context as Activity,
            R.layout.support_simple_spinner_dropdown_item, Height
        )
        //      spinner.setOnItemSelectedListener(onItemSelectedSpinner);
        spHeight.adapter = spinnerAdapter
        for (i in Height.indices) {
            if (packingData.height.trim()
                    .equals(Height[i], ignoreCase = true)
            ) {
                spHeight.setSelection(i)
                break
            }
        }

        val spinnerAdapter3: ArrayAdapter<*> = ArrayAdapter<String>(
            context as Activity,
            R.layout.support_simple_spinner_dropdown_item, Ability
        )
        //      spinner.setOnItemSelectedListener(onItemSelectedSpinner);
        spAbility.adapter = spinnerAdapter3
        if (packingData.ability != null) {
            for (i in Ability.indices) {
                if (packingData.ability.toString().trim()
                        .equals(Ability[i], ignoreCase = true)
                ) {
                    spAbility.setSelection(i)
                    break
                }
            }
        }
    }

    fun showShoeSizeSpinner(
        ShoeSize: List<String>,
        spShoeSize: Spinner
    ) {
        val spinnerAdapter2: ArrayAdapter<*> = ArrayAdapter<String>(
            context as Activity,
            R.layout.support_simple_spinner_dropdown_item, ShoeSize
        )
        //      spinner.setOnItemSelectedListener(onItemSelectedSpinner);
        spShoeSize.adapter = spinnerAdapter2
        if (!packingData.shoeSize.isNullOrEmpty()) {
            for (i in ShoeSize.indices) {
                Debugger.wtf("showShoeSizeSpinner", ShoeSize[i].toString())
                if (packingData.shoeSize.trim()
                        .equals(ShoeSize[i], ignoreCase = true)
                ) {
                    spShoeSize.setSelection(i)
                    break
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnUpdateRenterDataListener {
        fun onUpdateClick(
            dob: String,
            gender: String,
            shoeType: String,
            height: String,
            weight: String,
            showSize: String,
            ability: String

        )
    }

    companion object {
        fun newInstance(
            getAbilityShoeSizeHeightListModel: GetAbilityShoeSizeHeightListModel,
            packingData: PackingData
        ): UpdateRenterDataDialog {
            return UpdateRenterDataDialog(getAbilityShoeSizeHeightListModel, packingData)
        }
    }
}