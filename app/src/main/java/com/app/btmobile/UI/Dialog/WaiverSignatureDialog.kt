package com.app.btmobile.UI.Dialog

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.app.btmobile.Models.WaiverRenter
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.databinding.DialogWaiverShowRenterBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.github.gcacace.signaturepad.views.SignaturePad.OnSignedListener

class WaiverSignatureDialog(
    var model: WaiverRenter,
    var switchKeep: Boolean,
    var isDinVisible: Boolean,
    var isOnlyViewData: Boolean = false
) : DialogFragment() {
    lateinit var binding: DialogWaiverShowRenterBinding

    private var onSignSaved: OnSignatureSaved? =
        null

    fun setOnContinueCancelClick(onSignSaved: OnSignatureSaved?) {
        this.onSignSaved = onSignSaved
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogWaiverShowRenterBinding.inflate(inflater, container, false);
        return binding.root
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            etName.text = model.fullName
            tvGenderName.text = model.gender
            tvAgeText.text = model.age
            tvWeightText.text = model.weight
            etHeightText.text = model.height


            if (isDinVisible) {
                if (switchKeep) {
                    if (model.waiver_din_setting != null) {
                        etDNSetting.text = model.waiver_din_setting
                    }
                } else {
                    if (model.dinSetting != null) {
                        etDNSetting.text = model.dinSetting
                    }
                }
            }
            Debugger.wtf(
                "isDinVisible", "$isDinVisible / $switchKeep / " +
                        "${model.waiver_din_setting} / ${model.dinSetting}"
            )
            if (model.soleLength != null) {
                tvSoleLengthText.text = model.soleLength
            } else {
                tvSoleLengthText.text = "N/A"
            }

            if (model.ability != null) {
                if (model.ability.equals("Cautious", true)) {
                    tvSkiText.text = "1"
                } else if (model.ability.equals("Moderate", true)) {
                    tvSkiText.text = "2"
                } else if (model.ability.equals("Aggressive", true)) {
                    tvSkiText.text = "3"
                }
            }


            btnSaveSign.setOnClickListener {
                if (signaturePad.isEmpty) {

                } else {
                    if (onSignSaved != null) {
                        onSignSaved?.onSignDone(signaturePad.signatureBitmap)
                        dismiss()
                    }
                }
            }
            signaturePad.setOnSignedListener(object : OnSignedListener {
                override fun onStartSigning() {}
                override fun onSigned() {
                    if (!isOnlyViewData) {
                        tvClearSign.visibility = View.VISIBLE
                    }
                }

                override fun onClear() {
                    tvClearSign.visibility = View.GONE
                }
            })
            if (!model.waiver_image_path.isNullOrEmpty()) {
                Debugger.wtf("Siaggsd", model.waiver_image_path.toString())
                signProgressBar.visibility = View.VISIBLE
                Glide.with(requireActivity())
                    .asBitmap()
                    .load(model.waiver_image_path.toString())
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            signProgressBar.visibility = View.GONE
                            signaturePad.signatureBitmap = resource
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                            // this is called when imageView is cleared on lifecycle call or for
                            // some other reason.
                            // if you are referencing the bitmap somewhere else too other than this imageView
                            // clear it here as you can no longer have the bitmap
                        }
                    })

            }
            btnClose.setOnClickListener {
                dismiss()
            }

            tvClearSign.setOnClickListener {
                signaturePad.clear()
            }

            if (isOnlyViewData) {
                btnSaveSign.visibility = View.GONE
                signaturePad.isEnabled = false
            }
        }
    }


    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    interface OnSignatureSaved {
        fun onSignDone(signBitmap: Bitmap)
    }

    companion object {
        fun newInstance(
            waiverRenter: WaiverRenter,
            keepSwitch: Boolean,
            isDinVisible: Boolean,
            isOnlyViewData: Boolean = false
        ): WaiverSignatureDialog {
            return WaiverSignatureDialog(waiverRenter, keepSwitch, isDinVisible, isOnlyViewData)
        }
    }
}