package com.app.btmobile.Utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.provider.Settings
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.app.btmobile.UI.Activity.MainActivity
import com.app.btmobile.UI.Activity.SplashScreen
import es.dmoral.toasty.Toasty
//import retrofit2.HttpException
//import com.jakewharton.retrofit2.adapter.rxjava2.HttpException

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object AppConstants {
    const val KEY_USER_OBJECT = "BTMobile_user"
    const val KEY_IS_LOGIN = "is_logged_in_BTMobile"

    const val DeliveryCustomerActivity = "DeliveryCustomerActivity"
    const val PackingCustomerScreen = "PackingCustomerScreen"

    val IS_LIVE_MODE: Boolean = false
    var BASE_URL = ""

    val PER_PAGE: String = "5"

    //Stage
    const val BASE_STAG_URL = "https://staging.blacktieskis.com/"

    //Live
    const val BASE_LIVE_URL = "https://booknow.blacktieskis.com/"


}

object NetworkUtil {
    /**
     * Returns true if the Throwable is an instance of RetrofitError with an
     * http status code equals to the given one.
     */
    fun isHttpStatusCode(throwable: Throwable, statusCode: Int): Boolean {
        if (throwable is retrofit2.HttpException) {
            return (throwable as retrofit2.HttpException).code() === statusCode
        } else if (throwable is com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
            return (throwable as com.jakewharton.retrofit2.adapter.rxjava2.HttpException).code() === statusCode
        }
        return false
        //  return (throwable is retrofit2.HttpException && (throwable as retrofit2.HttpException).code() === statusCode)
    }
}

fun logout(activity: Activity) {
    val sharedPrefsHelper =
        activity.getSharedPreferences("BTMobile-prefs", Context.MODE_PRIVATE)
    val editor = sharedPrefsHelper.edit()
    editor.clear().apply()

    val intent = Intent(activity, SplashScreen::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    activity.startActivity(intent)
    activity.finish()

}

fun GoToHomeScreen(activity: Activity) {
    val intent = Intent(activity, MainActivity::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    activity.startActivity(intent)
    activity.finish()
}

fun GoToMap(activity: Activity, mapUrl: String?) {
    if (mapUrl.isNullOrEmpty() ||
        mapUrl.isNullOrBlank()
    ) {
        showToast(activity, "Location is not found")
    } else {
        val strUri: String = mapUrl
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(strUri)
        )
        intent.setClassName(
            "com.google.android.apps.maps",
            "com.google.android.maps.MapsActivity"
        )
        activity.startActivity(intent)
    }
}

fun showToast(activity: Activity, msg: String, isSuccess: Boolean = false) {
    //Toast.makeText(activity, "$msg", Toast.LENGTH_LONG).show()
    if (!msg.isNullOrEmpty() && !msg.isNullOrBlank()) {
        if (isSuccess) {
            Toasty.success(activity, "$msg", Toast.LENGTH_LONG, false).show();
        } else {
            Toasty.error(activity, "$msg", Toast.LENGTH_LONG, false).show();
        }
    }
}

fun openNfcSettings(activity: Activity) {
    showToast(activity, "You need to enable NFC")
    val intent = Intent(Settings.ACTION_WIRELESS_SETTINGS)
    activity.startActivity(intent)
}

fun parseDateToddMMddyyy(time: String?): String? {
    val inputPattern = "MM/dd/yyyy"
    val outputPattern = "MM/dd/yyyy"
    val inputFormat = SimpleDateFormat(inputPattern)
    val outputFormat = SimpleDateFormat(outputPattern)
    var date: Date? = null
    var str: String? = null
    try {
        date = inputFormat.parse(time)
        str = outputFormat.format(date)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return str
}

fun setColor(context: Context, color: Int): Int {
    return ContextCompat.getColor(
        context,
        color
    )
}

fun isNotEmptyOrBlank(text: String): Boolean {
    return text.toString().isNotEmpty() && text.toString().isNotBlank()
}

fun getPackingStatusColor(layPackStatus: AppCompatTextView, packingStatus: String) {
    when (packingStatus) {
        "Not Packed", "Pending" -> {
            layPackStatus.background.setColorFilter(
                Color.parseColor("#f05050"),
                PorterDuff.Mode.SRC_ATOP
            )
        }
        "Packed", "Completed" -> {
            layPackStatus.background.setColorFilter(
                Color.parseColor("#81c868"),
                PorterDuff.Mode.SRC_ATOP
            )
        }
        "Partial Pack" -> {
            layPackStatus.background.setColorFilter(
                Color.parseColor("#ffbd4a"),
                PorterDuff.Mode.SRC_ATOP
            )
        }
        else -> {
            layPackStatus.background.setColorFilter(
                Color.parseColor("#337ab7"),
                PorterDuff.Mode.SRC_ATOP
            )
        }
    }

}

fun setViewAndChildrenEnabled(view: View, enabled: Boolean) {
    view.isEnabled = enabled
    if (view is ViewGroup) {
        for (i in 0 until view.childCount) {
            val child = view.getChildAt(i)
            setViewAndChildrenEnabled(child, enabled)
        }
    }
}