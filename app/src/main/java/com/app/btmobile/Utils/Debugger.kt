package com.app.btmobile.Utils

import android.util.Log
import com.app.btmobile.Utils.AppConstants.IS_LIVE_MODE

object Debugger {
    var IS_DEVELOPMENT_MODE = !IS_LIVE_MODE

    fun d(tag: String, msg: String) {
        if (IS_DEVELOPMENT_MODE)
            Log.d(tag, msg)
    }

    fun e(tag: String, msg: String) {
        if (IS_DEVELOPMENT_MODE)
            Log.e(tag, msg)

    }

    fun i(tag: String, msg: String) {
        if (IS_DEVELOPMENT_MODE)
            Log.i(tag, msg)
    }

    fun wtf(tag: String, msg: String?) {
        if (IS_DEVELOPMENT_MODE)
            Log.wtf(tag, msg)
    }

    /*fun v(tag: String, msg: String) {
        if (IS_DEVELOPMENT_MODE)
            Log.v(tag, msg)
    }*/

    /*fun w(tag: String, msg: String) {
        if (IS_DEVELOPMENT_MODE)
            Log.w(tag, msg)
    }*/
}