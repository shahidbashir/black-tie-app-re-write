package com.app.btmobile.Utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.app.btmobile.EventBusCallbacks.InvoiceConfirmationBusEvent
import com.app.btmobile.R
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.channel.Channel
import com.pusher.client.connection.ConnectionEventListener
import com.pusher.client.connection.ConnectionState
import com.pusher.client.connection.ConnectionStateChange
import org.greenrobot.eventbus.EventBus
import org.json.JSONException
import org.json.JSONObject


class PusherIO private constructor() {
    private val TAG = this.javaClass.simpleName
    var context: Context? = null
    var pusher: Pusher? = null
    var channel: Channel? = null
    var api_key = ""
    var cluster = ""
    var channal_name = ""
    var event_name = ""


    companion object {
        private var instance: PusherIO? = null

        @Synchronized
        fun getInstance(): PusherIO {
            if (instance == null) {
                instance = PusherIO()
            }
            return instance as PusherIO
        }
    }

    fun connectToPusher(techId: String, context: Context) {
        this.context = context

        api_key = "ad332a821da1fbdf158f"
        cluster = "mt1"
        channal_name = "customer-confirmation-$techId"
        event_name = "confirmation-done"
        Debugger.wtf("channal_name", "$channal_name")

//        My Testing Project
//        api_key = "6568425b0481e8ecb538"
//        cluster = "ap4"
//        channal_name = "my-channel-$techId"
//        event_name = "my-event"
//        Debugger.wtf("channal_name", "$channal_name")


        val options = PusherOptions()
        options.setCluster(cluster);
        pusher = Pusher(api_key, options)
        pusher?.connect(object : ConnectionEventListener {
            override fun onConnectionStateChange(change: ConnectionStateChange) {
                Debugger.wtf(
                    "PusherIO",
                    "State changed from ${change.previousState} to ${change.currentState}"
                )
            }

            override fun onError(
                message: String,
                code: String,
                e: Exception
            ) {
                Debugger.wtf(
                    "PusherIO",
                    "There was a problem connecting! code ($code), message ($message), exception($e)"
                )
            }
        }, ConnectionState.ALL)

        channel = pusher?.subscribe(channal_name)
        channel?.bind(event_name) { event ->
            Debugger.wtf("Pusher", "Received event with data: $event")
            //NOTIFICATION
            val data = event.data
            var msg = ""
            try {
                val jsonObject = JSONObject(data)
                msg = jsonObject.getString("msg")
                EventBus.getDefault().post(InvoiceConfirmationBusEvent(msg))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            val mBuilder: NotificationCompat.Builder
            val notificationType = "Tag Notifications"

            val mNotificationManager =
                context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setupNotificationChannels(mNotificationManager, notificationType)
            }
            val defaultSoundUri =
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            mBuilder = NotificationCompat.Builder(context, notificationType)
                .setContentTitle("BTMobile App")
                .setSmallIcon(R.drawable.icon)
                .setContentText(msg)
                .setColor(Color.parseColor("#337ab7"))
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setSound(defaultSoundUri)
            mNotificationManager.notify(System.currentTimeMillis().toInt(), mBuilder.build())

        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setupNotificationChannels(notification: NotificationManager, type: String) {
        val adminChannelDescription = "BTMobile Notifications"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val audioAttributes = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()
        val adminChannel = NotificationChannel(
            type,
            type,
            NotificationManager.IMPORTANCE_DEFAULT
        )
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        adminChannel.setSound(defaultSoundUri, audioAttributes)
        notification.createNotificationChannel(adminChannel)
    }

    fun disconnectPusher() {
        if (pusher != null) {
            pusher?.disconnect()
            pusher = null
            channel?.let {
                Log.wtf("PusherIO", "Disconnect channel ${it.isSubscribed}")
            }
        }
    }

    fun isPusherNotConnected(): Boolean {
        return pusher == null

    }
}