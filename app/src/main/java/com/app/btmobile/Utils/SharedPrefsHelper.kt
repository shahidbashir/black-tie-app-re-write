package com.app.btmobile.Utils

import android.content.SharedPreferences
import com.app.btmobile.Utils.AppConstants
import com.app.btmobile.Utils.AppUser
import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPrefsHelper @Inject
constructor(private val mSharedPreferences: SharedPreferences) {

    private fun put(key: String, value: String) {
        mSharedPreferences.edit().putString(key, value).apply()
    }

    fun put(key: String, value: Int) {
        mSharedPreferences.edit().putInt(key, value).apply()
    }

    private fun put(key: String, value: Boolean) {
        mSharedPreferences.edit().putBoolean(key, value).apply()
    }

    operator fun get(key: String, defaultValue: String): String? {
        return mSharedPreferences.getString(key, defaultValue)
    }

    operator fun get(key: String, defaultValue: Int): Int? {
        return mSharedPreferences.getInt(key, defaultValue)
    }

    operator fun get(key: String, defaultValue: Float): Float? {
        return mSharedPreferences.getFloat(key, defaultValue)
    }

    operator fun get(key: String, defaultValue: Boolean): Boolean? {
        return mSharedPreferences.getBoolean(key, defaultValue)
    }


    fun isLoggedIn(): Boolean {
        return get(AppConstants.KEY_IS_LOGIN, false) ?: false
    }

    fun setUser(user: AppUser?) {
        put(AppConstants.KEY_IS_LOGIN, true)
        val json = Gson().toJson(user)
        put(AppConstants.KEY_USER_OBJECT, json)
    }

    fun getUser(): AppUser? {
        return try {
            val json = get(AppConstants.KEY_USER_OBJECT, "")
            Gson().fromJson(json, AppUser::class.java)
        } catch (e: Exception) {
            null
        }
    }

}
