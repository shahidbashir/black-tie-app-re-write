package com.ogoul.kalamtime.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.util.ArrayList
import javax.inject.Inject


class ConfirmationViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposables = CompositeDisposable()
    private val responseUpdateConfirmationData =
        MutableLiveData<ApiResponse<BaseModel>>()

    private val responseConfirmationData =
        MutableLiveData<ApiResponse<ConfirmationModel>>()


    private val responseSaveConfirmationData =
        MutableLiveData<ApiResponse<BaseModel>>()


    fun getConfirmationData(): MutableLiveData<ApiResponse<ConfirmationModel>> {
        return responseConfirmationData
    }

    fun getSaveConfirmationData(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseSaveConfirmationData
    }

    fun getUpdateConfirmationData(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseUpdateConfirmationData
    }

    fun hitGetConfirmationData(auth: String, reservation_id: Int) {
        Debugger.wtf("hitGetConfirmationData", "$auth / ${reservation_id.toString()}")
        disposables.add(repository.hitGetAllConfirmationInvoices("Bearer " + auth, reservation_id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseConfirmationData.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseConfirmationData.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseConfirmationData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseConfirmationData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitSaveConfirmationData(auth: String, reservation_id: Int, file: RequestBody) {
        Debugger.wtf("hitSaveConfirmationData", "$auth / ${reservation_id.toString()}")
        disposables.add(repository.saveConfirmationData("Bearer " + auth, reservation_id, file)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSaveConfirmationData.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseSaveConfirmationData.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseSaveConfirmationData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseSaveConfirmationData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitUpdateConfirmationData(
        auth: String,
        reservation_id: Int,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf(
            "hitUpdateConfirmationData",
            "$auth / ${reservation_id.toString()} / ${parameters.toString()}"
        )
        disposables.add(repository.updateConfirmationData(
            "Bearer " + auth,
            reservation_id,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseUpdateConfirmationData.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseUpdateConfirmationData.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseUpdateConfirmationData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseUpdateConfirmationData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitSendToPhoneConfirmation(
        auth: String, parameters: HashMap<String, Any>
    ) {
        Debugger.wtf("hitSaveConfirmationData", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitSendToPhoneConfirmation("Bearer " + auth, parameters)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSaveConfirmationData.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseSaveConfirmationData.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseSaveConfirmationData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseSaveConfirmationData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}