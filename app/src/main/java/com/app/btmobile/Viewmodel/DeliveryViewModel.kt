package com.ogoul.kalamtime.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import okhttp3.ResponseBody
import java.util.ArrayList
import javax.inject.Inject


class DeliveryViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    var jobCoroutine: Job? = null

    private val disposables = CompositeDisposable()
    private val responseDeliveryCustomerData =
        MutableLiveData<ApiResponse<DeliveryCustomersMainModel>>()

    private val responseDeliveryRentersData =
        MutableLiveData<ApiResponse<DeliveryRenterMainModel>>()


    private val responseRenterHistory = MutableLiveData<ApiResponse<RenterHistoryModel>>()
    private val responseSwitchRenter = MutableLiveData<ApiResponse<BaseModel>>()
    private val responseVanList = MutableLiveData<ApiResponse<ArrayList<VanModel>>>()


    fun getVanListResponse(): MutableLiveData<ApiResponse<ArrayList<VanModel>>> {
        return responseVanList
    }

    fun deliveryCustomerResponse(): MutableLiveData<ApiResponse<DeliveryCustomersMainModel>> {
        return responseDeliveryCustomerData
    }

    fun deliveryRenterResponse(): MutableLiveData<ApiResponse<DeliveryRenterMainModel>> {
        return responseDeliveryRentersData
    }

    fun getRenterHistoryResponse(): MutableLiveData<ApiResponse<RenterHistoryModel>> {
        return responseRenterHistory
    }

    fun getSwitchHistoryResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseSwitchRenter
    }

    fun hitDeliveryCustomerApiCoro(auth: String, options: Map<String, String>) {
        if (jobCoroutine != null) {
            jobCoroutine?.cancel()
        }
        Debugger.wtf("hitDeliveryCustomerApiCoro", "$auth / ${options.toString()}")
        val scope = viewModelScope
        jobCoroutine = scope.launch {
            responseDeliveryCustomerData.value = ApiResponse.loading()
            try {
                val response = repository.hitDeliveryCustomersApiCoro("Bearer " + auth, options)
                responseDeliveryCustomerData.setValue(ApiResponse.success(response))
            } catch (ce: CancellationException) {
                // You can ignore or log this exception
            } catch (e: Exception) {
                if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                    responseDeliveryCustomerData.setValue(ApiResponse.notSuccess())
                } else {
                    responseDeliveryCustomerData.setValue(ApiResponse.error(e))
                }
            }
        }
    }

    fun hitDeliveryCustomerApi(auth: String, options: Map<String, String>) {
        Debugger.wtf("hitDeliveryCustomerApi", "$auth / ${options.toString()}")
        disposables.add(repository.hitDeliveryCustomersApi("Bearer " + auth, options)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseDeliveryCustomerData.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseDeliveryCustomerData.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseDeliveryCustomerData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseDeliveryCustomerData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun getVanList(auth: String) {
        Debugger.wtf("hitNfcScan", "$auth}")
        disposables.add(repository.getVanList("Bearer " + auth)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseVanList.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseVanList.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseVanList.setValue(ApiResponse.notSuccess())
                    } else {
                        responseVanList.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitDeliveryRenterApi(auth: String, shipmentId: Int, date: String) {
        Debugger.wtf("hitDeliveryRenterApi", " $shipmentId / $date")
        disposables.add(repository.hitDeliveryRentersApi("Bearer " + auth, shipmentId, date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseDeliveryRentersData.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseDeliveryRentersData.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseDeliveryRentersData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseDeliveryRentersData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitGetRenterHistory(auth: String, renterId: Int, renterName: String) {
        Debugger.wtf("hitDeliveryRenterApi", " $renterId")
        disposables.add(repository.hitGetRenterHistoryApi("Bearer " + auth, renterId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseRenterHistory.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    it.data.renterName = renterName
                    responseRenterHistory.setValue(ApiResponse.success(it))
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseRenterHistory.setValue(ApiResponse.notSuccess())
                    } else {
                        responseRenterHistory.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitSwitchReturn(auth: String, shipmentId: Int) {
        Debugger.wtf("hitDeliveryRenterApi", " $shipmentId")
        disposables.add(repository.hitSwitchReturnApi("Bearer " + auth, shipmentId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSwitchRenter.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseSwitchRenter.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseSwitchRenter.setValue(ApiResponse.notSuccess())
                    } else {
                        responseSwitchRenter.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}