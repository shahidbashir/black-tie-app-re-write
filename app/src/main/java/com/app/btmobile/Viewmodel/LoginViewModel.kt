package com.ogoul.kalamtime.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.LoginModel
import com.app.btmobile.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposables = CompositeDisposable()
    private val responseLiveData = MutableLiveData<ApiResponse<LoginModel>>()

    fun loginResponse(): MutableLiveData<ApiResponse<LoginModel>> {
        return responseLiveData
    }

    fun hitLogin(parameters: HashMap<String, String>) {
        disposables.add(repository.hitLogin(parameters)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { responseLiveData.setValue(ApiResponse.loading()) }
            .subscribe(
                {
                    responseLiveData.setValue(
                        ApiResponse.success(it)
                    )
                },
                { responseLiveData.setValue(ApiResponse.error(it)) }
            ))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    fun login(parameters: HashMap<String, String>) {
        viewModelScope.launch {
            responseLiveData.setValue(ApiResponse.loading())
            try {
                val loginModel = repository.hitLogin(parameters);
                responseLiveData.setValue(
                    ApiResponse.success(data = loginModel as LoginModel)
                )
            } catch (exception: Exception) {
                responseLiveData.setValue(ApiResponse.error(exception))
            }
        }
    }

}