package com.ogoul.kalamtime.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.BaseModel
import com.app.btmobile.Models.GetTorqueTestModel
import com.app.btmobile.Models.NfcModel
import com.app.btmobile.Models.SaveFitBootAndSkisByBarcodeModel
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import javax.inject.Inject


class NfcViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposables = CompositeDisposable()
    private val responseNfcLiveData = MutableLiveData<ApiResponse<NfcModel>>()
    private val responseAddTorqueTesting = MutableLiveData<ApiResponse<BaseModel>>()
    private val responseForceAvailable = MutableLiveData<ApiResponse<BaseModel>>()
    private val responseInventoryCount =
        MutableLiveData<ApiResponse<SaveFitBootAndSkisByBarcodeModel>>()
    private val responseViewTorqueTesting = MutableLiveData<ApiResponse<GetTorqueTestModel>>()


    fun GetViewTorqueTesting(): MutableLiveData<ApiResponse<GetTorqueTestModel>> {
        return responseViewTorqueTesting
    }

    fun NfcScanResponse(): MutableLiveData<ApiResponse<NfcModel>> {
        return responseNfcLiveData
    }

    fun AddTorqueTestingResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseAddTorqueTesting
    }

    fun ForceAvailableResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseForceAvailable
    }

    fun ResponseInventoryCount(): MutableLiveData<ApiResponse<SaveFitBootAndSkisByBarcodeModel>> {
        return responseInventoryCount
    }

    fun hitNfcScan(auth: String, barcode: String, search_type: String) {
        Debugger.wtf("hitNfcScan", "$auth / $barcode ${search_type.toString()}")
        disposables.add(
            repository.hitNfc(
                "Bearer " + auth,
                barcode,
                search_type
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    responseNfcLiveData.setValue(ApiResponse.loading())
                }
                .subscribe(
                    {
                        responseNfcLiveData.setValue(
                            ApiResponse.success(it)
                        )
                    },
                    { e ->
                        if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                            responseNfcLiveData.setValue(ApiResponse.notSuccess())
//                        val body: ResponseBody? = (e as HttpException).response().errorBody()
//                        body.let {
//
//                        }

                        } else {
                            responseNfcLiveData.setValue(ApiResponse.error(e))
                        }
                    }
                ))
    }

    fun hitAdTorqueTestingApi(auth: String, parameters: HashMap<String, String>) {
        Debugger.wtf("hitNfcScan", "$auth / $parameters")
        disposables.add(repository.hitAdTorqueTestingApi("Bearer " + auth, parameters)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddTorqueTesting.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddTorqueTesting.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddTorqueTesting.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddTorqueTesting.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitForceAvailable(auth: String, barcode: String) {
        Debugger.wtf("hitAdTorqueTestingApi", "$auth / $barcode")
        disposables.add(repository.hitNfcApiForToShow("Bearer " + auth, barcode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseForceAvailable.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseForceAvailable.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseForceAvailable.setValue(ApiResponse.notSuccess())
                    } else {
                        responseForceAvailable.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitInventoryCountStATUSuPDATE(
        auth: String,
        parameters: HashMap<String, Any>,
        inventorySearch: String
    ) {
        Debugger.wtf("hitInventoryCountStATUSuPDATE", "$auth / $parameters")
        disposables.add(repository.hitInventoryCountStATUSuPDATE("Bearer " + auth, parameters,inventorySearch)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseInventoryCount.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    val map = java.util.HashMap<String, String>()
                    map["barcode"] = parameters["barcode"].toString()
                    map["message"] = it.message
                    it.parameters = map
                    responseInventoryCount.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseInventoryCount.setValue(ApiResponse.notSuccess())
                    } else {
                        responseInventoryCount.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitViewTorqueTestingApi(auth: String, barcode: String) {
        Debugger.wtf("hitViewTorqueTestingApi", "$auth / $barcode")
        disposables.add(repository.hitViewTorqueTestingApi("Bearer " + auth, barcode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseViewTorqueTesting.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseViewTorqueTesting.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseViewTorqueTesting.setValue(ApiResponse.notSuccess())
                    } else {
                        responseViewTorqueTesting.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}