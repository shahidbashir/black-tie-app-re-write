package com.ogoul.kalamtime.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import java.util.ArrayList
import javax.inject.Inject


class NotesViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposables = CompositeDisposable()
    private val responseNotesList =
        MutableLiveData<ApiResponse<NotesListModel>>()


    private val responseAddNotes =
        MutableLiveData<ApiResponse<BaseModel>>()

    fun getNotesListResponse(): MutableLiveData<ApiResponse<NotesListModel>> {
        return responseNotesList
    }

    fun addNotesResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseAddNotes
    }

    fun hitGetNotesList(auth: String, reservation_id: Int) {
        Debugger.wtf("hitNfcScan", "$auth / ${reservation_id.toString()}")
        disposables.add(repository.hitGetNotesListApi("Bearer " + auth, reservation_id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseNotesList.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseNotesList.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseNotesList.setValue(ApiResponse.notSuccess())
                    } else {
                        responseNotesList.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitAddNotes(
        auth: String,
        reservation_id: Int,
        options: HashMap<String, String>
    ) {
        Debugger.wtf("hitNfcScan", "$auth  $reservation_id / ${options.toString()}")
        disposables.add(repository.hitAddNotes("Bearer " + auth, reservation_id, options)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddNotes.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseAddNotes.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddNotes.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddNotes.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}