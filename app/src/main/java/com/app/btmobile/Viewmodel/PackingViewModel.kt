package com.ogoul.kalamtime.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.http.Url
import javax.inject.Inject


class PackingViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    var jobCoroutine: Job? = null

    private val disposables = CompositeDisposable()
    private val responsePackingScheduleData = MutableLiveData<ApiResponse<PackingScheduleModel>>()
    private val responseRenterPackFitData = MutableLiveData<ApiResponse<PackingModel>>()
    private val responseUnScheduleInventoryData =
        MutableLiveData<ApiResponse<UnScheduleInventoryModel>>()
    private val responseOwnBootModel = MutableLiveData<ApiResponse<OwnBootModel>>()
    private val responseAddPackedItems =
        MutableLiveData<ApiResponse<SaveFitBootAndSkisByBarcodeModel>>()
    private val responseAddPoles = MutableLiveData<ApiResponse<BaseModel>>()
    private val responseSaveFitBootAndSkiByBarcode =
        MutableLiveData<ApiResponse<SaveFitBootAndSkisByBarcodeModel>>()
    private val responseGetAddonsList = MutableLiveData<ApiResponse<AddonsModel>>()
    private val responseGetTechCustomerList =
        MutableLiveData<ApiResponse<TechStationCustomerModel>>()
    private val responseGetTechRentersList =
        MutableLiveData<ApiResponse<TechStationRenterModel>>()
    private val responseGetAbilityShoeSizeHeightList =
        MutableLiveData<ApiResponse<GetAbilityShoeSizeHeightListModel>>()
    private val responseGetFittingAddonsList =
        MutableLiveData<ApiResponse<GetFittingAddonsModel>>()
    private val responseUpdateDinSettingResponse =
        MutableLiveData<ApiResponse<BaseModel>>()

    private val responseSaveAddonsWithBarcode =
        MutableLiveData<ApiResponse<SaveAddonWithBarcodeModel>>()

    fun getResponseSaveAddonsWithBarcode(): MutableLiveData<ApiResponse<SaveAddonWithBarcodeModel>> {
        return responseSaveAddonsWithBarcode
    }

    fun updateDinSettingResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseUpdateDinSettingResponse
    }

    fun getGetFittingAddonsModelResponse(): MutableLiveData<ApiResponse<GetFittingAddonsModel>> {
        return responseGetFittingAddonsList
    }

    fun getAbilityShoeSizeHeightListResponse(): MutableLiveData<ApiResponse<GetAbilityShoeSizeHeightListModel>> {
        return responseGetAbilityShoeSizeHeightList
    }

    fun unScheduleResponse(): MutableLiveData<ApiResponse<PackingScheduleModel>> {
        return responsePackingScheduleData
    }

    fun renterPackFitModel(): MutableLiveData<ApiResponse<PackingModel>> {
        return responseRenterPackFitData
    }

    fun unScheduleInventoryResponse(): MutableLiveData<ApiResponse<UnScheduleInventoryModel>> {
        return responseUnScheduleInventoryData
    }

    fun ownBootResponse(): MutableLiveData<ApiResponse<OwnBootModel>> {
        return responseOwnBootModel
    }

    fun addPackedItemResponse(): MutableLiveData<ApiResponse<SaveFitBootAndSkisByBarcodeModel>> {
        return responseAddPackedItems
    }

    fun addPolesResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseAddPoles
    }


    fun getSaveFitBootAndSkiByBarcode(): MutableLiveData<ApiResponse<SaveFitBootAndSkisByBarcodeModel>> {
        return responseSaveFitBootAndSkiByBarcode
    }

    fun getAddOnsListResponse(): MutableLiveData<ApiResponse<AddonsModel>> {
        return responseGetAddonsList
    }

    fun getTechCustomerListResponse(): MutableLiveData<ApiResponse<TechStationCustomerModel>> {
        return responseGetTechCustomerList
    }

    fun getTechRentersListResponse(): MutableLiveData<ApiResponse<TechStationRenterModel>> {
        return responseGetTechRentersList
    }


    fun hitDeliveryCustomerApiCoro(auth: String, map: HashMap<String, String>) {
        if (jobCoroutine != null) {
            jobCoroutine?.cancel()
        }
        Debugger.wtf("hitUnSchedulePackingApi", "$auth / ${map.toString()}")
        val scope = viewModelScope
        jobCoroutine = scope.launch {
            responsePackingScheduleData.value = ApiResponse.loading()
            try {
                val response = repository.getUnSchedulePackingLisCoro("Bearer " + auth, map)
                responsePackingScheduleData.setValue(ApiResponse.success(response))
            } catch (ce: CancellationException) {
                // You can ignore or log this exception
            } catch (e: Exception) {
                if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                    responsePackingScheduleData.setValue(ApiResponse.notSuccess())
                } else {
                    responsePackingScheduleData.setValue(ApiResponse.error(e))
                }
            }
        }
    }


    fun hitUnSchedulePackingApi(auth: String, map: HashMap<String, String>) {
        Debugger.wtf("hitUnSchedulePackingApi", "$auth / ${map.toString()}")
        disposables.add(repository.getUnSchedulePackingList("Bearer " + auth, map)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responsePackingScheduleData.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responsePackingScheduleData.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responsePackingScheduleData.setValue(ApiResponse.notSuccess())
                    } else {
                        responsePackingScheduleData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitRenterPackFitData(
        auth: String, van_shipment_id: Int,
        renter_id: Int
    ) {
        Debugger.wtf("hitRenterPackFitData", "$auth / ${van_shipment_id.toString()} / ${renter_id}")
        disposables.add(repository.getRenterPackFitData(
            "Bearer " + auth,
            van_shipment_id,
            renter_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseRenterPackFitData.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseRenterPackFitData.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseRenterPackFitData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseRenterPackFitData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitUnScheduleInventory(
        auth: String, deliveryId: Int
    ) {
        Debugger.wtf("hitUnScheduleInventory", "$auth / ${deliveryId.toString()}")
        disposables.add(repository.unScheduleInventory(
            "Bearer " + auth,
            deliveryId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseUnScheduleInventoryData.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseUnScheduleInventoryData.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseUnScheduleInventoryData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseUnScheduleInventoryData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitOwnBootApi(
        auth: String,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf("hitUnScheduleInventory", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitOwnBootApi(
            "Bearer " + auth,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseOwnBootModel.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseOwnBootModel.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseOwnBootModel.setValue(ApiResponse.notSuccess())
                    } else {
                        responseOwnBootModel.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitAddPackedItems(
        auth: String,
        van_shipment_id: Int,
        parameters: HashMap<String, String>,
        isBoot: Boolean
    ) {
        Debugger.wtf("hitUnScheduleInventory", "$auth / $isBoot / ${parameters.toString()}")
        disposables.add(
            if (isBoot) {
                repository.hitBootPackedItem(
                    "Bearer " + auth, van_shipment_id,
                    parameters
                )
            } else {
                repository.hitSkiPackedItem(
                    "Bearer " + auth, van_shipment_id,
                    parameters
                )
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    responseAddPackedItems.setValue(ApiResponse.loading())
                }
                .subscribe(
                    {
                        it.parameters = parameters
                        it.isForBoot = isBoot
                        responseAddPackedItems.setValue(
                            ApiResponse.success(it)
                        )
                    },
                    { e ->
                        if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                            responseAddPackedItems.setValue(ApiResponse.notSuccess())
                        } else {
                            responseAddPackedItems.setValue(ApiResponse.error(e))
                        }
                    }
                ))
    }


    fun hitAddPoles(
        auth: String,
        renterId: Int,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf("hitUnScheduleInventory", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitAddPolesApi(
            "Bearer " + auth, renterId,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitPackPolesApi(
        auth: String,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf("hitPackPolesApi", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitPackPolesApi(
            "Bearer " + auth,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                Debugger.wtf("hitPackPolesApi", "Successs / ${parameters.toString()}")
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitDeleteInventory(
        auth: String,
        @Url deleteUrl: String
    ) {
        Debugger.wtf("hitDeleteInventory", "$deleteUrl")
        disposables.add(repository.hitDeleteInventory(
            "Bearer " + auth,
            deleteUrl
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                    Debugger.wtf("hitUnScheduleInventory", "${e.message}")
                }
            ))
    }

    fun hitGetAddonsList(
        auth: String,
        renterId: Int
    ) {
        Debugger.wtf("hitDeleteInventory", "$renterId")
        disposables.add(repository.hitGetAddonsList(
            "Bearer " + auth,
            renterId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseGetAddonsList.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    it.addons.map { addon ->
                        addon.isAddonWithBarcode = false
                    }
                    it.addonsWithBarcodeList.map { addonBarcode ->
                        addonBarcode.isAddonWithBarcode = true
                    }
                    responseGetAddonsList.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseGetAddonsList.setValue(ApiResponse.notSuccess())
                    } else {
                        responseGetAddonsList.setValue(ApiResponse.error(e))
                    }
                    Debugger.wtf("hitDeleteInventory", "${e.message}")
                }
            ))
    }

    fun hitSaveAddOnsList(
        auth: String,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf("hitSaveAddOnsList", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitSaveAddOnsList(
            "Bearer " + auth,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitSaveAddonsWithBarcodeApi(
        auth: String,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf("hitSaveAddonsWithBarcodeApi", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitSaveAddonsWithBarcodeApi(
            "Bearer " + auth,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSaveAddonsWithBarcode.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseSaveAddonsWithBarcode.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseSaveAddonsWithBarcode.setValue(ApiResponse.notSuccess())
                    } else {
                        responseSaveAddonsWithBarcode.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitFitSaveAddonsManual(
        auth: String,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf("hitFitSaveAddonsManual", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitFitSaveAddonsManuallyApi(
            "Bearer " + auth,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSaveAddonsWithBarcode.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseSaveAddonsWithBarcode.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseSaveAddonsWithBarcode.setValue(ApiResponse.notSuccess())
                    } else {
                        responseSaveAddonsWithBarcode.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitGetAllTechCustomersList(
        auth: String,
        date: String
    ) {
        Debugger.wtf("hitSaveAddOnsList", "$auth / ${date.toString()}")
        disposables.add(repository.hitGetTechCustomersList(
            "Bearer " + auth,
            date
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseGetTechCustomerList.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseGetTechCustomerList.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseGetTechCustomerList.setValue(ApiResponse.notSuccess())
                    } else {
                        responseGetTechCustomerList.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitGetAllTechRentersList(
        auth: String,
        van_shipment_id: Int
    ) {
        Debugger.wtf("hitGetAllTechRentersList", "$auth / ${van_shipment_id.toString()}")
        disposables.add(repository.hitGetTechRentersList(
            "Bearer " + auth,
            van_shipment_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseGetTechRentersList.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseGetTechRentersList.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseGetTechRentersList.setValue(ApiResponse.notSuccess())
                    } else {
                        responseGetTechRentersList.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitSaveFitBootByBarcode(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf(
            "hitSaveFitBootByBarcode",
            "$auth / ${renterId} / ${van_shipment_id} / $parameters"
        )
        disposables.add(
            repository.hitSaveFitBootByBarcode(
                "Bearer " + auth,
                renterId,
                van_shipment_id,
                parameters
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    responseSaveFitBootAndSkiByBarcode.setValue(ApiResponse.loading())
                }
                .subscribe(
                    {
                        it.parameters = parameters
                        it.isForBoot = true
                        responseSaveFitBootAndSkiByBarcode.setValue(
                            ApiResponse.success(it)
                        )
                    },
                    { e ->
                        if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                            responseSaveFitBootAndSkiByBarcode.setValue(ApiResponse.notSuccess())
                        } else {
                            responseSaveFitBootAndSkiByBarcode.setValue(ApiResponse.error(e))
                        }
                    }
                ))
    }

    fun hitSaveFitSkiByBarcode(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf(
            "hitSaveFitBootByBarcode",
            "$auth / ${renterId} / ${van_shipment_id} / $parameters"
        )
        disposables.add(repository.hitSaveFitSkisByBarcode(
            "Bearer " + auth,
            renterId,
            van_shipment_id,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSaveFitBootAndSkiByBarcode.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    it.isForBoot = false
                    it.parameters = parameters
                    responseSaveFitBootAndSkiByBarcode.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseSaveFitBootAndSkiByBarcode.setValue(ApiResponse.notSuccess())
                    } else {
                        responseSaveFitBootAndSkiByBarcode.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitDeleteFittingBootSkisInventory(
        auth: String,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf(
            "hitSaveFitBootByBarcode",
            "$auth // $parameters"
        )
        disposables.add(repository.hitDeleteBootSkisFittingInventory(
            "Bearer " + auth,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitSaveFitBootManually(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf(
            "hitSaveFitBootManually",
            "$auth / ${renterId} / ${van_shipment_id} / $parameters"
        )
        disposables.add(repository.hitSaveFitBootManually(
            "Bearer " + auth,
            renterId,
            van_shipment_id,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitSaveFitSkiManually(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf(
            "hitSaveFitBootManually",
            "$auth / ${renterId} / ${van_shipment_id} / $parameters"
        )
        disposables.add(repository.hitSaveFitSkiManually(
            "Bearer " + auth,
            renterId,
            van_shipment_id,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitUpdateRenterData(
        auth: String,
        renterId: Int,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf(
            "hitUpdateRenterData",
            "$auth / ${renterId}  / $parameters"
        )
        disposables.add(repository.hitUpdateRenterData(
            "Bearer " + auth,
            renterId,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                Debugger.wtf(
                    "hitUpdateRenterData",
                    "loading"
                )
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(

                {
                    Debugger.wtf(
                        "hitUpdateRenterData",
                        "success"
                    )
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        Debugger.wtf(
                            "hitUpdateRenterData",
                            "notSuccess"
                        )
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        Debugger.wtf(
                            "hitUpdateRenterData",
                            "notSuccess ${e}"
                        )
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitGetAbilityHeightShowSizeData(
        auth: String
    ) {
        Debugger.wtf(
            "hitGetAbilityHeightShowSizeData",
            "$auth "
        )
        disposables.add(repository.hitGetAbilityHeightShowSizeData(
            "Bearer " + auth
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseGetAbilityShoeSizeHeightList.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseGetAbilityShoeSizeHeightList.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseGetAbilityShoeSizeHeightList.setValue(ApiResponse.notSuccess())
                    } else {
                        responseGetAbilityShoeSizeHeightList.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitGetFittingAddonsData(
        auth: String,
        renterId: Int,
        van_shipment_id: Int
    ) {
        Debugger.wtf(
            "hitSaveFitBootManually",
            "$auth / ${renterId} / ${van_shipment_id} "
        )
        disposables.add(repository.hitGetFittingAddonsData(
            "Bearer " + auth,
            renterId,
            van_shipment_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseGetFittingAddonsList.setValue(ApiResponse.loading())
            }
            .subscribe(
                {

                    it.data.addons.map { addon ->
                        addon.isAddonWithBarcode = false
                    }
                    it.data
                        .addonsWithBarcodeList.map { addonBarcode ->
                            addonBarcode.isAddonWithBarcode = true
                        }
                    responseGetFittingAddonsList.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseGetFittingAddonsList.setValue(ApiResponse.notSuccess())
                    } else {
                        responseGetFittingAddonsList.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitSaveFitAddonsList(
        auth: String,
        options: Map<String, String>,
        fitSaveAddonsModel: FitSaveAddonsModel
    ) {
        Debugger.wtf(
            "hitSaveFitBootManually",
            "$auth / ${options.toString()}  / ${Gson().toJson(fitSaveAddonsModel)}"
        )
        disposables.add(repository.hitSaveFitAddons(
            "Bearer " + auth,
            options,
            fitSaveAddonsModel
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitUpdateAddonStatus(
        auth: String,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf(
            "hitUpdateAddonStatus",
            "$auth  / $parameters"
        )
        disposables.add(repository.hitUpdateAddonStatus(
            "Bearer " + auth,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseAddPoles.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseAddPoles.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseAddPoles.setValue(ApiResponse.notSuccess())
                    } else {
                        responseAddPoles.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitUpdateDinSetting(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>

    ) {
        Debugger.wtf(
            "hitUpdateAddonStatus",
            "$auth  / $parameters"
        )

        disposables.add(repository.hitUpdateDinSetting(
            "Bearer " + auth, renterId, van_shipment_id,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseUpdateDinSettingResponse.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseUpdateDinSettingResponse.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseUpdateDinSettingResponse.setValue(ApiResponse.notSuccess())
                    } else {
                        responseUpdateDinSettingResponse.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitUpdateDinSettingWithDelivery(
        auth: String,
        renterId: Int,
        van_shipment_id: Int,
        parameters: HashMap<String, String>,
        deliveryId: Int

    ) {
        Debugger.wtf(
            "hitUpdateAddonStatus",
            "$auth  / $parameters"
        )

        disposables.add(repository.hitUpdateDinSettingWithDelivery(
            "Bearer " + auth, renterId, van_shipment_id,
            parameters, deliveryId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseUpdateDinSettingResponse.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseUpdateDinSettingResponse.setValue(
                        ApiResponse.success(it)
                    )
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseUpdateDinSettingResponse.setValue(ApiResponse.notSuccess())
                    } else {
                        responseUpdateDinSettingResponse.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}