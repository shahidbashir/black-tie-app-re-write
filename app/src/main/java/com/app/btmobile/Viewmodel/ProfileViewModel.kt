package com.ogoul.kalamtime.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody
import java.util.HashMap
import javax.inject.Inject


class ProfileViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposables = CompositeDisposable()

    private val responseProfileData =
        MutableLiveData<ApiResponse<ProfileModel>>()
    private val responseBase =
        MutableLiveData<ApiResponse<BaseModel>>()


    fun getProfileResponse(): MutableLiveData<ApiResponse<ProfileModel>> {
        return responseProfileData
    }

    fun getBaseResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseBase
    }


    fun hitGetWaiverRentersList(
        auth: String
    ) {
        disposables.add(repository.hitGetProfileData(
            "Bearer " + auth
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseProfileData.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseProfileData.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseProfileData.setValue(ApiResponse.notSuccess())
                    } else {
                        responseProfileData.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitSaveProfileData(
        auth: String,
        id: Int,
        file: RequestBody
    ) {
        Debugger.wtf(
            "hitGetWaiverRentersList",
            "$auth / ${file.toString()}"
        )
        disposables.add(repository.hitSaveProfileData(
            "Bearer " + auth,
            id,
            file
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseBase.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseBase.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseBase.setValue(ApiResponse.notSuccess())
                    } else {
                        responseBase.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
