package com.ogoul.kalamtime.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import java.util.HashMap
import javax.inject.Inject


class ReturnViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    var jobCoroutine: Job? = null

    private val disposables = CompositeDisposable()
    private val responseReturnCustomer =
        MutableLiveData<ApiResponse<ReturnsCustomerModel>>()
    private val responseMassReturn =
        MutableLiveData<ApiResponse<MassReturnBaseModel>>()
    private val responseReturnRenterModel =
        MutableLiveData<ApiResponse<ReturnRenterModel>>()

    private val responseReturnInventory =
        MutableLiveData<ApiResponse<InventorReturnModel>>()


    private val responseBaseResponse =
        MutableLiveData<ApiResponse<BaseModel>>()

    fun getReturnInventoryResponse(): MutableLiveData<ApiResponse<InventorReturnModel>> {
        return responseReturnInventory
    }

    fun getBaseResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseBaseResponse
    }

    fun getWaiverCustomers(): MutableLiveData<ApiResponse<ReturnsCustomerModel>> {
        return responseReturnCustomer
    }

    fun getMassReturnResponse(): MutableLiveData<ApiResponse<MassReturnBaseModel>> {
        return responseMassReturn
    }

    fun getReturnRenterModelResponse(): MutableLiveData<ApiResponse<ReturnRenterModel>> {
        return responseReturnRenterModel
    }

    fun hitGetReturnCustomersListCoro(auth: String, options: Map<String, String>) {
        if (jobCoroutine != null) {
            jobCoroutine?.cancel()
        }
        Debugger.wtf("hitGetReturnCustomersList", "$auth / ${options.toString()}")
        val scope = viewModelScope
        jobCoroutine = scope.launch {
            responseReturnCustomer.value = ApiResponse.loading()
            try {
                val response = repository.hitReturnCustomersListCoro("Bearer " + auth, options)
                responseReturnCustomer.setValue(ApiResponse.success(response))
            }catch (ce: CancellationException) {
                // You can ignore or log this exception
            }  catch (e: Exception) {
                if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                    responseReturnCustomer.setValue(ApiResponse.notSuccess())
                } else {
                    responseReturnCustomer.setValue(ApiResponse.error(e))
                }
            }
        }
    }


    fun hitGetReturnCustomersList(
        auth: String,
        options: Map<String, String>
    ) {
        Debugger.wtf("hitGetReturnCustomersList", "$auth / ${options.toString()}")
        disposables.add(repository.hitReturnCustomersList("Bearer " + auth, options)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseReturnCustomer.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseReturnCustomer.setValue(ApiResponse.success(it))
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseReturnCustomer.setValue(ApiResponse.notSuccess())
                    } else {
                        responseReturnCustomer.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitMassReturnSearch(
        auth: String,
        options: Map<String, String>
    ) {
        Debugger.wtf("MassReturnSctreen", "hitMassReturnSearch $auth / ${options.toString()}")
        disposables.add(repository.hitMassReturnSearchList("Bearer " + auth, options)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseMassReturn.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    responseMassReturn.setValue(ApiResponse.success(it))
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseMassReturn.setValue(ApiResponse.notSuccess())
                    } else {
                        responseMassReturn.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitMassReturnApi(
        auth: String,
        options: Map<String, String>
    ) {
        Debugger.wtf("MassReturnSctreen", " hitMassReturnApi$auth / ${options.toString()}")
        disposables.add(repository.hitMassReturnApi("Bearer " + auth, options)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseMassReturn.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    it.barcode = options["barcode"].toString()
                    responseMassReturn.setValue(ApiResponse.success(it))
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseMassReturn.setValue(ApiResponse.notSuccess())
                    } else {
                        responseMassReturn.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitReturnAddonOrClothing(
        auth: String,
        shipmentId: Int,
        parameters: HashMap<String, String>
    ) {
        Debugger.wtf("hitGetReturnRenterList", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitReturnAddonOrClothing(
            "Bearer " + auth,
            shipmentId,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseBaseResponse.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseBaseResponse.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseBaseResponse.setValue(ApiResponse.notSuccess())
                    } else {
                        responseBaseResponse.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitGetReturnRenterList(
        auth: String,
        shipmentId: Int
    ) {
        Debugger.wtf("hitGetReturnRenterList", "$auth / ${shipmentId.toString()}")
        disposables.add(repository.hitReturnRenterApi(
            "Bearer " + auth,
            shipmentId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseReturnRenterModel.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseReturnRenterModel.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseReturnRenterModel.setValue(ApiResponse.notSuccess())
                    } else {
                        responseReturnRenterModel.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


//    {van_shipment_id=298618, force_return=0, barcode=16744}


    fun hitReturnUsingBarcode(
        auth: String,
        shipmentId: Int,
        parameters: HashMap<String, String>

    ) {
        Debugger.wtf("hitReturnUsingBarcode", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitReturnUsingBarcode(
            "Bearer " + auth,
            shipmentId,
            parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseReturnInventory.setValue(ApiResponse.loading())
            }
            .subscribe(
                {
                    it.barcode = parameters["barcode"].toString()
                    responseReturnInventory.setValue(ApiResponse.success(it))
                },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseReturnInventory.setValue(ApiResponse.notSuccess())
                    } else {
                        responseReturnInventory.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitRemoveReturn(
        auth: String,
        parameters: HashMap<String, String>

    ) {
        Debugger.wtf("hitRemoveReturn", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitRemoveReturn(
            "Bearer " + auth, parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseBaseResponse.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseBaseResponse.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseBaseResponse.setValue(ApiResponse.notSuccess())
                    } else {
                        responseBaseResponse.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitMarkAllAsReturned(
        auth: String,
        shipmentId: Int
    ) {
        Debugger.wtf("hitRemoveReturn", "$auth / ${shipmentId.toString()}")
        disposables.add(repository.hitMarkAllReturned(
            "Bearer " + auth, shipmentId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseBaseResponse.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseBaseResponse.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseBaseResponse.setValue(ApiResponse.notSuccess())
                    } else {
                        responseBaseResponse.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}