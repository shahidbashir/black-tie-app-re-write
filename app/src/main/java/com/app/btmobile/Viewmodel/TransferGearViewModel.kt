package com.ogoul.kalamtime.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody
import java.util.HashMap
import javax.inject.Inject


class TransferGearViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposables = CompositeDisposable()

    private val responseTransferGearList =
        MutableLiveData<ApiResponse<TransferGearModel>>()
    private val responseBase =
        MutableLiveData<ApiResponse<BaseModel>>()


    fun getTransferGearList(): MutableLiveData<ApiResponse<TransferGearModel>> {
        return responseTransferGearList
    }

    fun getBaseResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseBase
    }


    fun hitGetTransferGearList(
        auth: String
    ) {
        disposables.add(repository.hitGetTransferGearList(
            "Bearer " + auth
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseTransferGearList.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseTransferGearList.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseTransferGearList.setValue(ApiResponse.notSuccess())
                    } else {
                        responseTransferGearList.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    fun hitSaveTransferGear(
        auth: String,
        parameters: HashMap<String, String>

    ) {
        Debugger.wtf("hitRemoveReturn", "$auth / ${parameters.toString()}")
        disposables.add(repository.hitSaveTransferGear(
            "Bearer " + auth, parameters
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseBase.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseBase.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseBase.setValue(ApiResponse.notSuccess())
                    } else {
                        responseBase.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
