package com.ogoul.kalamtime.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.btmobile.ApiResponse
import com.app.btmobile.Models.*
import com.app.btmobile.Repository
import com.app.btmobile.Utils.Debugger
import com.app.btmobile.Utils.NetworkUtil.isHttpStatusCode
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.RequestBody
import java.util.HashMap
import javax.inject.Inject


class WaiverViewModel @Inject constructor(private val repository: Repository) : ViewModel() {
    private val disposables = CompositeDisposable()
    private val responseWaiverCustomers =
        MutableLiveData<ApiResponse<WaiverCustomerModel>>()

    private val responseWaiverRenters =
        MutableLiveData<ApiResponse<WaiverRentersModel>>()
    private val responseSendToPhone =
        MutableLiveData<ApiResponse<BaseModel>>()


    fun getWaiverCustomers(): MutableLiveData<ApiResponse<WaiverCustomerModel>> {
        return responseWaiverCustomers
    }

    fun getWaiverRenters(): MutableLiveData<ApiResponse<WaiverRentersModel>> {
        return responseWaiverRenters
    }

    fun getSendToPhoneResponse(): MutableLiveData<ApiResponse<BaseModel>> {
        return responseSendToPhone
    }

    fun hitGetWaiverCustomersList(
        auth: String,
        date: String
    ) {
        Debugger.wtf("hitGetWaiverCustomersList", "$auth / ${date.toString()}")
        disposables.add(repository.hitWaiverCustomersApi("Bearer " + auth, date)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseWaiverCustomers.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseWaiverCustomers.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseWaiverCustomers.setValue(ApiResponse.notSuccess())
                    } else {
                        responseWaiverCustomers.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitGetWaiverRentersList(
        auth: String,
        reservation_id: Int,
        delivery_id: Int
    ) {
        Debugger.wtf(
            "hitGetWaiverRentersList",
            "$auth  $reservation_id / ${reservation_id.toString()} / $delivery_id"
        )
        disposables.add(repository.hitWaiverRenterSApi(
            "Bearer " + auth,
            reservation_id,
            delivery_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseWaiverRenters.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseWaiverRenters.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseWaiverRenters.setValue(ApiResponse.notSuccess())
                    } else {
                        responseWaiverRenters.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitGetWaiverRentersList(
        auth: String,
        reservation_id: Int
    ) {
        Debugger.wtf(
            "hitGetWaiverRentersList",
            "$auth  $reservation_id / ${reservation_id.toString()}"
        )
        disposables.add(repository.hitWaiverRenterSApi(
            "Bearer " + auth,
            reservation_id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseWaiverRenters.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseWaiverRenters.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseWaiverRenters.setValue(ApiResponse.notSuccess())
                    } else {
                        responseWaiverRenters.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitSendToPhone(
        auth: String,
        options: HashMap<String, Any>
    ) {
        Debugger.wtf(
            "hitGetWaiverRentersList",
            "$auth / ${options.toString()}"
        )
        disposables.add(repository.hitWaiverRenterSApi(
            "Bearer " + auth,
            options
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSendToPhone.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseSendToPhone.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseSendToPhone.setValue(ApiResponse.notSuccess())
                    } else {
                        responseSendToPhone.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }

    fun hitSaveWaiverSignature(
        auth: String,
        file: RequestBody
    ) {
        Debugger.wtf(
            "hitGetWaiverRentersList",
            "$auth / ${file.toString()}"
        )
        disposables.add(repository.saveWaiverSignature(
            "Bearer " + auth,
            file
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                responseSendToPhone.setValue(ApiResponse.loading())
            }
            .subscribe(
                { responseSendToPhone.setValue(ApiResponse.success(it)) },
                { e ->
                    if (isHttpStatusCode(e, 400) || isHttpStatusCode(e, 401)) {
                        responseSendToPhone.setValue(ApiResponse.notSuccess())
                    } else {
                        responseSendToPhone.setValue(ApiResponse.error(e))
                    }
                }
            ))
    }


    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
